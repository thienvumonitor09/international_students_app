﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class composeMail : System.Web.UI.Page
{
    //email fields:
    string sendTo = "";
    string sentFrom = "";
    string strSubj = "";
    string strBody = "";
    string strCCaddresses = "l_padden@hotmail.com";
    string strEmailResult = "";

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void sendEmail_Click(object sender, EventArgs e)
    {
        //send email notifying administrators of submission and showing all the data submitted
        Utility uEmail = new Utility();
        sendTo = "laura.padden@ccs.spokane.edu";
        sentFrom = "globalprograms@ccs.spokane.edu";
        strSubj = "International Student Application Submitted";
        //prepare the body of the email
        strBody = "";
        strBody = "<style type='text/css'>";
        strBody += "#emailContainer";
        strBody += "{";
        strBody += "    font: 14px/20px ArialMT, Verdana, sans-serif;";
        strBody += "    color:black;";
        strBody += " }";
        strBody += "#emailContainer a, a:visited";
        strBody += "{";
        strBody += "    color: #1e4aa6;";
        strBody += "    text-decoration:none;";
        strBody += "}";
        strBody += " #emailContainer a:hover";
        strBody += "{";
        strBody += "    text-decoration:underline;";
        strBody += " }";

        strBody += "</style>";
        
        strBody += "<div id='emailContainer'>";
        strBody += "<table width='750px' cellpadding='15' cellspacing='0' border='0' bgcolor='#e5e6e7'>";
        strBody += "<tbody>";
        strBody += "<tr>";
        strBody += "<td style='width:750px;'>";
        strBody += "<table width='100%' cellpadding='5px'><tbody>";
        strBody += "<tr style='background-color: #00519b;'><td colspan='3' style='width:100%; valign:middle;'>";

        strBody += "<a href='http://www.ccs.spokane.edu'>&nbsp;&nbsp;&nbsp;<img src='https://portal.ccs.spokane.edu/_netapps/internationalsa/App_Themes/TuneUp/images/ccsLogo.jpg' alt='CCS Logo' title='Community Colleges of Spokane Homepage' /></a>";
        strBody += "<span style='width: 500px; position: relative; float: left; color: #fff; Font-family: Palanquin, sans-serif; font-weight: 100; font-size: 30px;'>";
        strBody += "<a href='http://www.ccs.spokane.edu' style='text-decoration:none;color:White;'>&nbsp;&nbsp;&nbsp;Community&nbsp;Colleges&nbsp;of&nbsp;Spokane</a>";
        strBody += "</td></tr>";

        strBody += "<tr>";
        strBody += "<td colspan='3' style='width:100%;'><img src='https://portal.ccs.spokane.edu/_netapps/internationalsa/App_Themes/images/email/GE1.jpg' border='1px solid' width='750' alt='International Students' />";
        strBody += "</td>";
        strBody += "</tr>";
        strBody += "<tr>";
        strBody += "<td><h3>Thank you for submitting your international student online application to Community Colleges of Spokane!</h3>";  
        strBody += "If you applied to the International High School Completion Program, <a href='http://www.ccs.spokane.edu/Forms/International-programs/HSC-Admission-Requirements.aspx' target='_blank'> see your application requirements here</a>.";
        strBody += "<p>In addition to completing the online form, please submit the following documentation:</p>";
        strBody += "<ul><li>Copy of your <strong>valid passport</strong></li>";
        strBody += "<li><strong><del>$100 application fee</del></strong> <span style='color: #ee3b41;font-weight:600;'>Waived</span></li>";
        strBody += "<li><strong>High school completion documents</strong> (or equivalent)</li>";
        strBody += "<li><strong><a href='http://www.ccs.spokane.edu/Forms/International-programs/ccs-40-214.aspx' target='_blank'>Affidavit of Financial Support Form</a></strong></li>";
        strBody += "<li><strong>Bank statement</strong> (no older than six months from the I20 isuing date) verifying you have the funding available to cover your educational and living expenses here for one academic year ($18,000 + $7000 for each dependent).<br />";
        strBody += "<a href='http://higheredublog.com/sample-bank-statement-letter-for-f1-visa/' target='_blank'>Sample bank statement letter</a></li>";
        strBody += "<li><strong><a href='http://www.ccs.spokane.edu/GlobalEducation/Files/ProofProficiency.aspx' target='_blank'>Proof of English proficiency</a></strong> (if applying for academic admission, not ESL)</li></ul>";
        strBody += "</td>";
        strBody += "</tr>";
        strBody += "<tr><td colspan='3' style='width:100%;'><p>If you are already in the U.S. on an F-1 student visa, we consider you a transfer student and you are required to submit these additional documents:</p>";
        strBody += "<ul><li><strong><a href='http://ccs.spokane.edu/Forms/International-programs/Transfer_IN.aspx' target='_blank'>Transfer-In Form</a></strong></li>";
        strBody += "<li>Copy of your <strong>F-1 visa</strong></li>";
        strBody += "<li>Copy of your most <strong>recent I-20</strong></li>";
        strBody += "<li><strong>Transcripts</strong> from current institution(s) of study</li>";
        strBody += "</ul>";
        
        strBody += "<p>Please scan and email these documents to <a href='mailto:globalprograms@ccs.spokane.edu'>globalprograms@ccs.spokane.edu</a></p>";
        strBody += "<p>You will hear back from us within 7 business days; please contact us if you have any further questions!</p>";
        strBody += "<p>All the best,</p>";
        strBody += "Community Colleges of Spokane<br />Global Education Office Admissions<br /><a href='mailto:globalprograms@ccs.spokane.edu'>globalprograms@ccs.spokane.edu</a>";
                    
        // create linked resource

        strBody += "<p><a href='https://www.youtube.com/channel/UC_eDihSP-7PT-JEhVQS8--Q' target='_blank'><img src='https://portal.ccs.spokane.edu/_netapps/internationalsa/App_Themes/images/email/globalUTube105.png'></a>&nbsp;&nbsp;&nbsp;";
        strBody += "<a href='https://www.facebook.com/Communitycollegesofspokane/?ref=aymt_homepage_panel' target='_blank'><img src='https://portal.ccs.spokane.edu/_netapps/internationalsa/App_Themes/images/email/globalFB.jpg'></a>&nbsp;&nbsp;&nbsp;";
        strBody += "<a href='https://www.instagram.com/globaleducationccs/' target='_blank'><img src='https://portal.ccs.spokane.edu/_netapps/internationalsa/App_Themes/images/email/globalInstagram.jpg'></a>&nbsp;&nbsp;&nbsp;";
        strBody += "</p></td>";
        strBody += "</tr>";
        strBody += "<tr>";
        strBody += "<td colspan='3' style='width:100%;'><img src='https://portal.ccs.spokane.edu/_netapps/internationalsa/App_Themes/images/email/SFCCGateway-m.jpg' border='1px solid' width='750' alt='SFCC Gateway Bldg' />";
        strBody += "</td>";
        strBody += "</tr>";
        strBody += "<tr style='background-color: #00519b;'><td colspan='3' style='width:100%; valign:top;'>&nbsp;</td></tr>";
        strBody += "</tbody>";
        strBody += "</table>";
        strBody += "</td>";
        strBody += "</tr>";
        strBody += "</tbody>";
        strBody += "</table>";
        strBody += "</div>";

      //  Response.Write("strCC: " + strCCaddresses + "<br />");
        strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubj, true, strBody, strCCaddresses, "");

      //  Response.Write("return from email: " + strEmailResult + "<br />");
    }
}