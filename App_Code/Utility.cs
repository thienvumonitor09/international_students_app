﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.DirectoryServices;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
	public Utility()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    // https://www.codeproject.com/Articles/22777/Email-Address-Validation-Using-Regular-Expression
    public const string MatchEmailPattern =
       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";


    public static bool IsValidEmail(string email)
    {
        if (email != null)
        {
            return Regex.IsMatch(email, MatchEmailPattern);
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Remove dangerous script tags; prepare/convert text for email in HTML or Plain Text; escape aostrophes for SQL server 
    /// </summary>
    /// <param name="strText"></param>
    /// <param name="IsHTML"></param>
    /// <param name="IsSQL"></param>
    /// <returns></returns>
    public string ConvertAndFixText(string strText, bool IsHTML, bool IsSQL)
    {
        
        StringBuilder objBuilder = new StringBuilder();
        char strChar;
        string strTemp = "";
        Int32 intLength = strText.Length;
        for (Int32 i = 0; i < intLength; i++)
        {
            strTemp = strText.Substring(i, 1);
            strChar = Convert.ToChar(strTemp);
            if (IsHTML == true)
            {
                if (strChar.ToString() == "\n")
                { strTemp = "<br />"; }
                if (strChar.ToString() == "\t")
                { strTemp = "&nbsp;&nbsp;"; }
                if (strChar == (char)39)
                { strTemp = "&#39;"; }
                if (strChar == (char)34)
                { strTemp = "&#34;"; }
            }
            if (IsSQL == true)
            {
                // ESCAPE APOSTROPHES
                if (strChar == (char)39)
                { strTemp = "''"; }
            }
            objBuilder.Append(strTemp);
        }
        if (IsHTML == false)
        {
            objBuilder.Replace("<br />", "\n");
            if (IsSQL == true)
            // ESCAPE APOSTROPHES
            { objBuilder.Replace("&#39;", "''"); }
            else
            { objBuilder.Replace("&#39;", "'"); }
            objBuilder.Replace("&#34;", "\"");
        }
        objBuilder.Replace("<script", "");
        objBuilder.Replace("\x3cscript", "");
        objBuilder.Replace("/script", "");
        objBuilder.Replace("%3cscript", "");

        return objBuilder.ToString();
    }

    /// <summary>
    /// Calculate a date accounting for weekends
    /// </summary>
    /// <param name="intBusinessDayCount"></param>
    /// <param name="dtStartDate"></param>
    /// <returns></returns>
    public DateTime GetBusinessDate(int intBusinessDayCount, DateTime dtStartDate)
    {
        DateTime dtBusinessDate = dtStartDate;
        int intDateAdd = intBusinessDayCount;
        int intDayOfWeek = 0;
        for (int i = 0; i <= intBusinessDayCount; i++)
        {
            intDayOfWeek = (int)dtStartDate.AddDays(i).DayOfWeek;
            if (intDayOfWeek == 6 || intDayOfWeek == 0)
            {
                intDateAdd = intDateAdd + 1;
            }
        }
        //If the last calculated day is Sunday, add one more day
        if ((int)dtStartDate.AddDays(intDateAdd).DayOfWeek == 0)
        {
            intDateAdd = intDateAdd + 1;
        }
        dtBusinessDate = dtStartDate.AddDays(intDateAdd);
        return dtBusinessDate;
    }

    /// <summary>
    /// strCCEmailAddresses is a semi-colon delineated list of email addresses to parse.  
    /// </summary>
    /// <param name="strTo"></param>
    /// <param name="strFrom"></param>
    /// <param name="strSubject"></param>
    /// <param name="blIsHTML"></param>
    /// <param name="strBody"></param>
    /// <param name="strCCEmailAddresses"></param>
    /// <returns>Returns "Success" or error message.</returns>
    public string SendEmailMsg(string strTo, string strFrom, string strSubject, bool blIsHTML, string strBody, string strCCEmailAddresses)
    {
        StringBuilder objBuilder = new StringBuilder();
        if (strFrom == "") { strFrom = "CCSWebApp@ccs.spokane.edu"; }
        Boolean blIsValidTo = false;
        Boolean blIsValidFrom = false;
        if (strFrom == "") { strFrom = "CCSWebApp@ccs.spokane.edu"; }
        if (strTo != "" && IsValidEmail(strTo)) { blIsValidTo = true; }
        if (IsValidEmail(strFrom)) { blIsValidFrom = true; }

        if (blIsValidTo && blIsValidFrom && strSubject != "" && strBody != "")
        {
            if (blIsHTML == false)
            {
                strBody = strBody.Replace("<br />", "\n");  //Convert to a horizontal tab/carriage return
                strBody = strBody.Replace("&quot;", "\"");
                strBody = strBody.Replace("&rsquo;", "\'");
                strBody = strBody.Replace("&#39;", "\'");
                strBody = strBody.Replace("<p>", "");
                strBody = strBody.Replace("</p>", "\n\n");
                strBody = strBody.Replace("&nbsp;", " ");
                strBody = strBody.Replace("<strong>", "");
                strBody = strBody.Replace("</strong>", "");
                strBody = strBody.Replace("<b>", "");
                strBody = strBody.Replace("</b>", "");
                strSubject = strSubject.Replace("&#39;", "\'");
                strSubject = strSubject.Replace("&quot;", "\"");
            }
            SmtpClient objSender = new SmtpClient();
            MailMessage objEmail = new MailMessage(strFrom, strTo, strSubject, strBody);
            objEmail.IsBodyHtml = blIsHTML;
            string[] arrCCList = parseList(strCCEmailAddresses);
            int uBound = arrCCList.GetUpperBound(0);
            if (strCCEmailAddresses != "")
            {
                for (int i = 0; i < uBound; i++)
                {
                    if (!string.IsNullOrEmpty(arrCCList[i]))
                    {
                        
                        objEmail.CC.Add(arrCCList[i]);
                    }
                }
            }
            //set host in web.config
            try
            {
                //TESTING MODE: UNCOMMENT NEXT LINE BEFORE GOING LIVE
                objSender.Send(objEmail);

                //TESTING ONLY 
                //objBuilder.Append("Email Message Parameters: " + strTo + "; " + strFrom + "; " + strSubject + "; " + strCCEmailAddresses + "<br />" + strBody + "<br />");
                //END TESTING
            }
            catch (Exception exc)
            {
                objBuilder.Append("<br /><b>Email Delivery Failed for " + strTo + ".</b> Error returned: " + exc + ".");
                return objBuilder.ToString();
            }
            if (objBuilder.ToString() == "")
            {
                objBuilder.Append("Success");
            }

        }
        return objBuilder.ToString();
    }

    /// <summary>
    /// strCCEmailAddresses is a semi-colon delineated list of email addresses to parse
    /// </summary>
    /// <param name="strTo"></param>
    /// <param name="strFrom"></param>
    /// <param name="strSubject"></param>
    /// <param name="blIsHTML"></param>
    /// <param name="strBody"></param>
    /// <param name="strCCEmailAddresses"></param>
    /// <returns></returns>
    public string SendEmailMsg(string strTo, string strFrom, string strSubject, bool blIsHTML, string strBody, string strCCEmailAddresses, string filePath)
    {
        StringBuilder objBuilder = new StringBuilder();
        if (strTo != "" && strFrom != "" && strSubject != "" && strBody != "")
        {
            if (blIsHTML == false)
            {
                strBody = strBody.Replace("<br />", "\n");  //Convert to a horizontal tab/carriage return
                strBody = strBody.Replace("&quot;", "\"");
                strBody = strBody.Replace("&rsquo;", "\'");
                strBody = strBody.Replace("&#39;", "\'");
                strBody = strBody.Replace("<p>", "");
                strBody = strBody.Replace("</p>", "\n\n");
                strBody = strBody.Replace("&nbsp;", " ");
                strBody = strBody.Replace("<strong>", "");
                strBody = strBody.Replace("</strong>", "");
                strBody = strBody.Replace("<b>", "");
                strBody = strBody.Replace("</b>", "");
                strSubject = strSubject.Replace("&#39;", "\'");
                strSubject = strSubject.Replace("&quot;", "\"");
            }
            SmtpClient objSender = new SmtpClient();
            MailMessage objEmail = new MailMessage(strFrom, strTo, strSubject, strBody);
            objEmail.IsBodyHtml = blIsHTML;
            string[] arrCCList = parseList(strCCEmailAddresses);
            int uBound = arrCCList.GetUpperBound(0);
            if (strCCEmailAddresses != "")
            {
                for (int i = 0; i < uBound; i++)
                {
                    if (arrCCList[i] != "")
                    {
                        objEmail.CC.Add(arrCCList[i]);
                    }
                }
            }
            if (filePath != "")
            {
                objEmail.Attachments.Add(new Attachment(filePath));
            }
            //set host in web.config
            try
            {
                //TESTING MODE: UNCOMMENT NEXT LINE BEFORE GOING LIVE
                objSender.Send(objEmail);

                //TESTING ONLY 
                //objBuilder.Append("Email Message Parameters: " + strTo + "; " + strFrom + "; " + strSubject + "; " + strCCEmailAddresses + "<br />" + strBody + "<br />");
                //END TESTING
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    switch (status)
                    {
                        case SmtpStatusCode.MailboxBusy:
                            objBuilder.Append("Delivery Failed - Mailbox Busy.");
                            break;
                        case SmtpStatusCode.MailboxUnavailable:
                            objBuilder.Append("Delivery Failed - Mailbox Unavailable.");
                            break;
                        case SmtpStatusCode.ExceededStorageAllocation:
                            objBuilder.Append("Delivery Failed - Mailbox Exceeds Storage Allocation.");
                            break;
                        default:
                            objBuilder.Append("Delivery Failed - " + status + ".");
                            break;
                    }
                }
                if (objBuilder.ToString() == "") { objBuilder.Append("Delivery Failed."); }
                return objBuilder.ToString();
            }
            catch (Exception exc)
            {
                objBuilder.Append("Delivery Failed - " + exc + ".");
                return objBuilder.ToString();
            }
            if (objBuilder.ToString() == "")
            {
                objBuilder.Append("Success");
            }

        }
        return objBuilder.ToString();
    }

    /// <summary>
    /// Used to parse any semi-colon delineated list of items
    /// </summary>
    /// <param name="strList"></param>
    /// <returns></returns>
    public string[] parseList(string strList)
    {
        Int32 intLength = strList.Length;
        if (intLength > 0)
        {
            //Add a final semicolon if missing
            string strChar = strList.Substring(intLength - 1, 1);
            if (strChar != ";")
            {
                strList += ";";
            }
        }
        string[] arrStrList;
        arrStrList = strList.Split(';');
        return arrStrList;
    }

    /// <summary>
    /// Fetches class_D stu_D information by class ID and college code
    /// </summary>
    /// <param name="strClassID"></param>
    /// <param name="strCollegeCode"></param>
    /// <returns></returns>
    public DataTable GetClassStuInfo(string strClassID, string strCollegeCode)
    {
        SqlConnection objConn = new SqlConnection();
        DataTable _returnDataTable = new DataTable();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CcsSm"].ConnectionString;
        objConn = new System.Data.SqlClient.SqlConnection();
        objConn.ConnectionString = strConnectionString;
        try
        {
            objConn.Open();
            string strSQL = "usp_GetClassDStuD_ByClassID '" + strClassID + "', '" + strCollegeCode + "'";
            SqlCommand objCommand = new SqlCommand(strSQL, objConn);
            objCommand.CommandType = CommandType.Text;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds, "ClassStuInfo");
            _returnDataTable = ds.Tables["ClassStuInfo"];
            objCommand.Dispose();
        }
        catch
        {
            //do nothing
        }
        finally
        {
            objConn.Close();
        }
        objConn.Dispose();
        return _returnDataTable;
    }

    /// <summary>
    /// Returns official student info from CCSsm
    /// </summary>
    /// <param name="strSID"></param>
    /// <param name="strCollegeCode"></param>
    /// <returns></returns>
    public string[] GetStudentInfo_CCSsm(string strSID, string strCollegeCode)
    {
        //Currently returns a one-dimensional array with 10 elements:
        //0 = Last Name
        //1 = First Name
        //2 = Middle Initial
        //3 = Name as it appears in the student records
        //4 = Email address
        //5 = Phone Number
        //6 = Address 
        //7 = City
        //8 = State
        //9 = ZIP Code
        string[] strReturnArray = new string[10];
        string strFullName = "";
        string strFName = "";
        string strLName = "";
        string strMInitial = "";
        string strEmail = "";
        string strPhone1 = "";
        string strAddr1 = "";
        string strCity = "";
        string strState = "";
        string strZIP = "";

        int intUpBound = 0;
        int i = 0;
        string strTemp1 = "";
        string strTemp2 = "";
        string strSQL = "usp_GetOneStu_D '" + strSID + "', '" + strCollegeCode + "'";
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CcsSm"].ToString();
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataReader objReader;
        try
        {
            objConn.ConnectionString = strConnectionString;
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objReader = objCommand.ExecuteReader();
            if (objReader.HasRows)
            {
                while (objReader.Read())
                {
                    strFullName = objReader["STU_NAME"].ToString();
                    strEmail = objReader["StuEmailAddr"].ToString();
                    strPhone1 = "(" + objReader["DAY_AREA_CODE"].ToString() + ") " + objReader["DAY_PREFIX"].ToString() + "-" + objReader["DAY_SUFFIX"].ToString();
                    strAddr1 = objReader["STU_STREET"].ToString();
                    strCity = objReader["STU_CITY"].ToString();
                    strState = objReader["STU_ST"].ToString();
                    strZIP = objReader["STU_ZIP"].ToString();
                }
            }
            objReader.Dispose();
            string[] strArray = strFullName.Split(new Char[] { ' ' });
            intUpBound = strArray.GetUpperBound(0);
            for (i = 0; i <= intUpBound; i++)
            {
                //strResultMsg =+ "i: " + i + "<br />";
                switch (i)
                {
                    case 0:
                        strLName = strArray[0];
                        strTemp2 = strLName.Substring(1, strLName.Length - 1).ToLower();
                        strTemp1 = strLName.Substring(0, 1);
                        strReturnArray[0] = strTemp1 + strTemp2;
                        break;
                    case 1:
                        strFName = strArray[1];
                        strTemp2 = strFName.Substring(1, strFName.Length - 1).ToLower();
                        strTemp1 = strFName.Substring(0, 1);
                        strReturnArray[1] = strTemp1 + strTemp2;
                        break;
                    case 2:
                        strMInitial = strArray[2];
                        strReturnArray[2] = strMInitial;
                        break;
                }
            }
            strReturnArray[3] = strFullName;
            strReturnArray[4] = strEmail;
            strReturnArray[5] = strPhone1;
            strReturnArray[6] = strAddr1;
            strReturnArray[7] = strCity;
            strReturnArray[8] = strState;
            strReturnArray[9] = strZIP;
        }
        catch
        {
            //do nothing
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return strReturnArray;
    }

    /// <summary>
    /// Fetch faculty employee ID by SSN
    /// </summary>
    /// <param name="strSSN"></param>
    /// <returns></returns>
    public string LookupEmployeeSIDbySSN(string strSSN)
    {
        string strSID = "";
        SqlParameter param = new SqlParameter();
        string strSQLCmd = "";
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSReplNLink"].ConnectionString;
        SqlConnection objConn = new System.Data.SqlClient.SqlConnection();
        objConn.ConnectionString = strConnectionString;
        try
        {
            objConn.Open();
            strSQLCmd = "CCS_SP_EmpM_LookUpSID_bySSN";
            SqlCommand objCommand = new SqlCommand(strSQLCmd, objConn);
            objCommand.CommandType = CommandType.StoredProcedure;
            param.ParameterName = "@SSN";
            param.Value = strSSN;
            param.Direction = ParameterDirection.Input;
            param.SqlDbType = SqlDbType.Char;
            param.Size = 9;
            objCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@SID";
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Char;
            param.Size = 9;
            objCommand.Parameters.Add(param);

            objCommand.ExecuteReader();
            strSID = (objCommand.Parameters["@SID"].Value.ToString());
        }
        catch (Exception ex)
        {
            return ex.Message; //+"; Source: " + ex.Source + "; Stack Trace: " + ex.StackTrace;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
        }
        return strSID;
    }

    public string LookupEmployeeSSNbySID(string strSID)
    {
        string strSSN = "";
        SqlParameter param = new SqlParameter();
        string strSQLCmd = "";
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSReplNLink"].ConnectionString;
        SqlConnection objConn = new System.Data.SqlClient.SqlConnection();
        objConn.ConnectionString = strConnectionString;
        try
        {
            objConn.Open();
            strSQLCmd = "CCS_SP_EmpM_LookUpSSN_bySID";
            SqlCommand objCommand = new SqlCommand(strSQLCmd, objConn);
            objCommand.CommandType = CommandType.StoredProcedure;

            param.ParameterName = "@SID";
            param.Value = strSID;
            param.Direction = ParameterDirection.Input;
            param.SqlDbType = SqlDbType.Char;
            param.Size = 9;
            objCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@SSN";
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Char;
            param.Size = 9;
            objCommand.Parameters.Add(param);

            objCommand.ExecuteReader();
            strSSN = (objCommand.Parameters["@SSN"].Value.ToString());
        }
        catch (Exception ex)
        {
            return ex.Message; //+"; Source: " + ex.Source + "; Stack Trace: " + ex.StackTrace;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
        }
        return strSSN;
    }

    /// <summary>
    /// Detects correct characters for IEL credit Class for first character in class item number
    /// </summary>
    /// <param name="strChar"></param>
    /// <returns></returns>
    public bool IsIELCreditClass(string strChar)
    {
        bool blIsIEL = false;
        switch (strChar.ToUpper())
        {
            case "A":
                blIsIEL = true;
                break;
            case "B":
                blIsIEL = true;
                break;
            case "C":
                blIsIEL = true;
                break;
            case "D":
                blIsIEL = true;
                break;
            case "E":
                blIsIEL = true;
                break;
            case "F":
                blIsIEL = true;
                break;
            case "G":
                blIsIEL = true;
                break;
            case "H":
                blIsIEL = true;
                break;
        }
        return blIsIEL;
    }

    /// <summary>
    /// Replace illegal characters for file upload's name
    /// </summary>
    /// <param name="strFileName"></param>
    /// <returns>The method return file name after replacing illegal characters</returns>
    public static string replaceIllegalChars(string strFileName)
    {
        //Must replace illegal chars with "_" to prevent error when uploading
        string[] illegalChars = { "\\", "/", ":", "*", "?", "\"", "<", ">", "!" };
        foreach (string element in illegalChars)
        {
            strFileName = strFileName.Replace(element, "_");
        }
        return strFileName;
    }

    /// <summary>
    /// Detect if a string contains non-english characters
    /// </summary>
    /// <param name="toValidateStr"></param>
    /// <returns>The method return true if there is non-english character, otherwise false</returns>
    public static bool ContainsNonEnglishChars(string toValidateStr)
    {
        Regex regexUplFile = new Regex(@"[^ -~]+");//match non-english characters
        return regexUplFile.IsMatch(toValidateStr);
    }
}