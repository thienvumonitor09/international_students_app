﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;

/// <summary>
/// Quarter_MetaData supercedes the class QuarterMetaData using ctcLink data
/// </summary>
public class Quarter_MetaData
{
    public Quarter_MetaData()
	{
		// CONSTRUCTOR
        setQuarter();
    }

    public int PreviousQtrID = 0; 
    public int CurrentQtrID = 0;
    public int NextQtrID = 0;
    public string PreviousYrQ = "";
    public string CurrentYrQ = "";
    public string NextYrQ = "";
    public string PreviousSTRM = "";
    public string CurrentSTRM = "";
    public string NextSTRM = "";
    public string PreviousQtrDescr = "";
    public string CurrentQtrDescr = "";
    public string NextQtrDescr = "";
    public System.DateTime PreviousQtrStartDate = System.DateTime.Today;
    public System.DateTime CurrentQtrStartDate = System.DateTime.Today;
    public System.DateTime NextQtrStartDate = System.DateTime.Today;
    public System.DateTime PreviousRegStartDate = System.DateTime.Today;
    public System.DateTime CurrentRegStartDate = System.DateTime.Today;
    public System.DateTime NextRegStartDate = System.DateTime.Today;
    public System.DateTime PreviousQtrFirstClass = System.DateTime.Today;
    public System.DateTime CurrentQtrFirstClass = System.DateTime.Today;
    public System.DateTime NextQtrFirstClass = System.DateTime.Today;
    public System.DateTime PreviousQtrLastClass = System.DateTime.Today;
    public System.DateTime CurrentQtrLastClass = System.DateTime.Today;
    public System.DateTime NextQtrLastClass = System.DateTime.Today;
    public System.DateTime PreviousQtrEndDate = System.DateTime.Today;
    public System.DateTime CurrentQtrEndDate = System.DateTime.Today;
    public System.DateTime NextQtrEndDate = System.DateTime.Today;
    public string CurrentTab = "";
    public string strPrevCurrNext_MetaData = "";
    public string strResultMsg = "";

    #region "Base Functions"

    public string setQuarter()
    {
        //RETURNS strQtrMetaData, a semicolon delimited list of current and next quarter values
        //Internally sets modular variables for other functions

        SqlConnection objConn = new SqlConnection();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
        SqlCommand objCommand = new SqlCommand();
        SqlDataReader objReader;
        try
        {

            objConn.Open();
            objCommand.CommandText = "usp_Quarter_Metadata_ReturnRowActiveFlag 'PreviousQtr'";
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objReader = objCommand.ExecuteReader();
            if (objReader.HasRows)
            {
                while (objReader.Read())
                {
                    PreviousQtrID = Convert.ToInt32(objReader["Index"]);
                    PreviousYrQ = objReader["YrQ"].ToString();
                    PreviousSTRM = objReader["STRM"].ToString();
                    PreviousRegStartDate = Convert.ToDateTime(objReader["RegOpensNextQtr"]);
                    PreviousQtrStartDate = Convert.ToDateTime(objReader["StartDate"]);
                    PreviousQtrFirstClass = Convert.ToDateTime(objReader["FirstDayOfClass"]);
                    PreviousQtrEndDate = Convert.ToDateTime(objReader["EndDate"]);
                    PreviousQtrDescr = GetLabel(PreviousSTRM, "DisplayDesc");
                    strPrevCurrNext_MetaData = PreviousQtrID + "; " + PreviousSTRM + "; " + PreviousQtrDescr;
                }
            }
            else
            {
                strResultMsg += "<p>setQuarter: Unable to retrieve Previous Quarter records.</p>";
            }
            objReader.Close();
            objCommand.CommandText = "usp_Quarter_Metadata_ReturnRowActiveFlag 'CurrentQtr'";
            objReader = objCommand.ExecuteReader();
            if (objReader.HasRows)
            {
                while (objReader.Read())
                {
                    CurrentQtrID = Convert.ToInt32(objReader["Index"]);
                    CurrentYrQ = objReader["YrQ"].ToString();
                    CurrentSTRM = objReader["STRM"].ToString();
                    CurrentRegStartDate = Convert.ToDateTime(objReader["RegOpensNextQtr"]);
                    CurrentQtrStartDate = Convert.ToDateTime(objReader["StartDate"]);
                    CurrentQtrFirstClass = Convert.ToDateTime(objReader["FirstDayOfClass"]);
                    CurrentQtrEndDate = Convert.ToDateTime(objReader["EndDate"]);
                    CurrentQtrDescr = GetLabel(CurrentSTRM, "DisplayDesc"); ;
                    CurrentTab = objReader["Name"].ToString();
                    strPrevCurrNext_MetaData += "; " + CurrentQtrID + "; " + CurrentYrQ + "; " + CurrentQtrDescr;
                }
            }
            else
            {
                strResultMsg += "<p>setQuarter: Unable to retrieve Current Quarter records.</p>";
            }
            objReader.Close();
            objCommand.CommandText = "usp_Quarter_Metadata_ReturnRowActiveFlag 'NextQtr'";
            objReader = objCommand.ExecuteReader();
            if (objReader.HasRows)
            {
                while (objReader.Read())
                {
                    NextQtrID = Convert.ToInt32(objReader["Index"]);
                    NextYrQ = objReader["YrQ"].ToString();
                    NextSTRM = objReader["STRM"].ToString();
                    NextRegStartDate = Convert.ToDateTime(objReader["RegOpensNextQtr"]);
                    NextQtrStartDate = Convert.ToDateTime(objReader["StartDate"]);
                    NextQtrFirstClass = Convert.ToDateTime(objReader["FirstDayOfClass"]);
                    NextQtrLastClass = Convert.ToDateTime(objReader["LastDayOfClass"]);
                    NextQtrEndDate = Convert.ToDateTime(objReader["EndDate"]);
                    NextQtrDescr = GetLabel(NextSTRM, "DisplayDesc"); ;
                    strPrevCurrNext_MetaData += "; " + NextQtrID + "; " + NextYrQ + "; " + NextQtrDescr;
                }
            }
            else
            {
                strResultMsg += "<p>setQuarter: Unable to retrieve Next Quarter records.</p>";
            }
            objReader.Close();
            objReader.Dispose();
        }
        catch (Exception ex)
        {
            strResultMsg += "<p>Error - unable to retrieve setQuarter records: " + ex.Message + "</p>";
            return strResultMsg;
        }
        finally
        {
            objConn.Close();
        }
        objConn.Dispose();
        objCommand.Dispose();
        return strPrevCurrNext_MetaData;
    }


    Int32 runQMDSQLQuery(string strSQL, bool blIsInsert)
    {
        Int32 intResult = 0;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            if (blIsInsert)
            {
                //Execute scalar
                intResult = (Int32)objCommand.ExecuteScalar();
            }
            else
            {
                //Execute NonQuery
                intResult = objCommand.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            //capture error
            strResultMsg += "Error runQMDSQLQuery: " + ex.Message + "<br />";
            strResultMsg += "SQL: " + strSQL + "; Is Insert? " + blIsInsert + "<br />";
            return intResult;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return intResult;
    }


    public DataTable RunGetQuery(string strQuery)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        DataTable DT = new DataTable();
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
            objConn.Open();
            objCommand.CommandText = strQuery;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;

            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            DT = ds.Tables[0]; 
        }
        catch (Exception ex)
        {
            //capture error
            strResultMsg += "Error RunGetQuery: " + ex.Message + "<br />";
            strResultMsg += "SQL: " + strQuery + "<br />";
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return DT;
    }

#endregion

    #region "Get Data"

    /// <summary>
    /// UDF - Returns labels for reporting.
    /// Supported FieldNames/outputs:
    /// CTCLinkDesc – "WINTER 2016"
    /// DisplayDesc – "Winter 2016"
    /// Name – "Winter"
    /// SchoolYears – "2015-2016"
    /// SchoolYears2 – "2015 2016"
    /// FiscalYear – "2016" (Derived from the latter half of SchoolYears)
    /// SchoolYear – "2016" (Gets calendar year for specified quarter)
    /// SchoolYearShort – "2015-16"
    /// SchoolYearMinimum – "1516"
    /// </summary>
    /// <param name="strSTRM"></param>
    /// <param name="strFieldName"></param>
    /// <returns>String label</returns>
    public String GetLabel(string strSTRM, string strFieldName)
    {
        String strResult = "";
        SqlConnection objConn = new SqlConnection();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
        try
        {
            objConn.Open();
            SqlCommand objCommand = new SqlCommand("SELECT dbo.udf_Quarter_Metadata_ReturnText(@STRM, @FIELDNAME)", objConn);
            objCommand.Parameters.Add(new SqlParameter("@STRM", strSTRM));
            objCommand.Parameters.Add(new SqlParameter("@FIELDNAME", strFieldName));
            strResult = (string)objCommand.ExecuteScalar();
            //cleanup
            objCommand.Dispose();
        }
        catch
        {
            return strResult;
        }
        finally
        {
            objConn.Close();
        }
        return strResult;
    }

    public DataTable GetRangeRows_Quarter_MetaData_Base(Int32 intBeforeSTRM, Int32 intAfterSTRM)
    {
        string strSQL = "usp_Quarter_Metadata_Base_ReturnRange '" + CurrentSTRM + "', " + intBeforeSTRM + ", " + intAfterSTRM;
        DataTable DT = RunGetQuery(strSQL);
        return DT;
    }

    public DataTable GetOneRow_Quarter_MetaData_Base(string strSTRM)
    {
        string strSQL = "usp_Quarter_Metadata_ReturnRowSTRM '" + strSTRM + "'";
        DataTable DT = RunGetQuery(strSQL);
        return DT;
    }

    public DataTable GetRowsBySTRM_Quarter_MetaData_Detail(string strSTRM)
    {
        string strSQL = "usp_Quarter_MetaData_Detail_GetRowsBySTRM '" + strSTRM + "'";
        DataTable DT = RunGetQuery(strSQL);
        return DT;
    }

    public DataTable GetOneRowByID_Quarter_MetaData_Detail(Int32 intID)
    {
        string strSQL = "usp_Quarter_MetaData_Detail_GetOneRowByID " + intID;
        DataTable DT = RunGetQuery(strSQL);
        return DT;
    }


    public DataTable GetOneRowBySTRM_AppName_Quarter_MetaData_Detail(Int32 intSTRM, string strAppName)
    {
        string strSQL = "usp_Quarter_MetaData_Detail_GetOneRowBySTRM_AppName " + intSTRM + ", '" + strAppName + "'";
        DataTable DT = RunGetQuery(strSQL);
        //strResultMsg += "Rows Returned: " + DT.Rows.Count + "<br />";
        return DT;
    }

    /// <summary>
    /// Returns STRM and DisplayDescr for a range of rows for drop-down list construction
    /// </summary>
    /// <param name="intBeforeSTRM"></param>
    /// <param name="intAfterSTRM"></param>
    /// <returns>Range of rows calculated by Indexe values before and after current quarter</returns>
    public DataTable GetSTRM_DisplayDescr_Quarter_MetaData_Base(int intBeforeSTRM, int intAfterSTRM)
    {
        string strSQL = "usp_Quarter_Metadata_ReturnRangeSTRM_DisplayDesc '" + CurrentSTRM + "', " + intBeforeSTRM + ", " + intAfterSTRM; 
        DataTable DT = RunGetQuery(strSQL);
        //strResultMsg += "<p>Current STRM: " + CurrentSTRM + " - " + GetLabel(CurrentSTRM, "DisplayDesc") + "</p>";
        return DT;
    }

    public DataTable GetApplicationNames()
    {
        string strSQL = "usp_Quarter_MetaData_Detail_GetApplicationNames";
        DataTable DT = RunGetQuery(strSQL);
        return DT;

    }
    public String CreateQuarterDropDownListOptions(int intBeforeSTRM, int intAfterSTRM, string strSelectedSTRM)
    {
        string strResult = "";
        string strSelectTag = "";
        SqlConnection objConn = new SqlConnection();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
        SqlCommand objCommand = new SqlCommand();
        SqlDataReader objReader;
        string strMsg = "";
        try
        {

            objConn.Open();
            
            //Fetch range of STRMs - usp_Quarter_Metadata_Base_ReturnRange
            objCommand.CommandText = "usp_Quarter_Metadata_ReturnRangeSTRM_DisplayDesc '" + CurrentSTRM + "', " + intBeforeSTRM + ", " + intAfterSTRM;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;
            objReader = objCommand.ExecuteReader();
            if (objReader.HasRows)
            {
                //Loop through and create the list items in HTML
                while (objReader.Read())
                {
                    //Use the GetLabel function to get the display text
                    if (strSelectedSTRM == objReader["STRM"].ToString()) { strSelectTag = " SELECTED"; } else { strSelectTag = ""; }
                    strResult += "<option value=\"" + objReader["STRM"].ToString() + strSelectTag + "\">" + objReader["DisplayDesc"].ToString() + "</option>";
                }
            }
            objReader.Close();

        }
        catch (Exception ex)
        {
            strMsg += "<p>Error - unable to retrieve setQuarter records: " + ex.Message + "</p>";
            return strMsg;
        }
        finally
        {
            objConn.Close();
        }
        objConn.Dispose();
        objCommand.Dispose();
        return strResult;
    }

    public DataTable GetRowsAllActiveFlags()
    {
        string strSQL = "usp_Quarter_MetaData_ReturnRowsAllActiveFlags";
        DataTable DT = RunGetQuery(strSQL);
        return DT;
    }

    #endregion

    #region "Insert/Update/Delete functions"

    public Int32 InsertQuarterMetadataBase(int intSTRM, string strYrQ, string strName, string strSchoolYears, string dtRegOpensCurrQtr, string dtStartDate, string dtFirstDayOfClass,
    string dtCensusDate, string dtRegOpensNextQtr, string dtLastDayOfClass, string dtGradesDueDate, string dtEndDate)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string strSQL = "usp_Quarter_MetaData_Base_Insert " + intSTRM + ", '" + strYrQ + "', '" + strName + "', '" + strSchoolYears + "', '" + dtRegOpensCurrQtr + "', '" + dtStartDate
         + "', '" + dtFirstDayOfClass + "', '" + dtCensusDate + "', '" + dtRegOpensNextQtr + "', '" + dtLastDayOfClass + "', '" + dtGradesDueDate + "', '" + dtEndDate + "'";
        Int32 intResult = runQMDSQLQuery(strSQL, true);
        if (intResult > 0)
        {
            strResultMsg += "Your new Quarter_MetaData_Base row was inserted successfully!<br />";
        }
        else
        {
            strResultMsg += "Error inserting Quarter_MetaData_Base row: 0 rows affected.<br />";
        }
        return intResult;
    }

    public Int32 InsertQuarterMetadataDetail(int intSTRM, string strApplicationName, string blIsActive, string dtStartDate, string dtEndDate)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string strSQL = "usp_Quarter_MetaData_Detail_Insert " + intSTRM + ", '" + strApplicationName + "', " + blIsActive + ", '" + dtStartDate + "', '" + dtEndDate + "'";
        Int32 intResult = runQMDSQLQuery(strSQL, true);
        if (intResult > 0)
        {
            strResultMsg += "Your new Quarter_MetaData_Detail row was inserted successfully!<br />";
        }
        else
        {
            strResultMsg += "Error inserting Quarter_MetaData_Detail row: 0 rows affected.<br />";
        }
        return intResult;
    }

    public Int32 UpdateQuarterMetadataBase(int intSTRM, string YrQ, string strName, string strSchoolYears, string dtRegOpensCurrQtr, string dtStartDate, string dtFirstDayOfClass,
    string dtCensusDate, string dtRegOpensNextQtr, string dtLastDayOfClass, string dtGradesDueDate, string dtEndDate)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string strSQL = "usp_Quarter_MetaData_Base_Update " + intSTRM + ", '" + YrQ + "', '" + strName + "', '" + strSchoolYears + "', '" + dtRegOpensCurrQtr + "', '" + dtStartDate
         + "', '" + dtFirstDayOfClass + "', '" + dtCensusDate + "', '" + dtRegOpensNextQtr + "', '" + dtLastDayOfClass + "', '" + dtGradesDueDate + "', '" + dtEndDate + "'";
        Int32 intResult = runQMDSQLQuery(strSQL, false);
        if (intResult > 0)
        {
            strResultMsg += "Your new Quarter_MetaData_Base row was updated successfully!<br />";
        }
        else
        {
            strResultMsg += "Error updating Quarter_MetaData_Base row: 0 rows affected.<br />";
        }
        return intResult;
    }

    public Int32 UpdateQuarterMetadataDetail(int intID, int intSTRM, string strApplicationName, string blIsActive, string dtStartDate, string dtEndDate)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string strSQL = "usp_Quarter_Metadata_Detail_Update " + intID + ", " + intSTRM + ", '" + strApplicationName + "', " + blIsActive + ", '" + dtStartDate + "', '" + dtEndDate + "'";
        Int32 intResult = runQMDSQLQuery(strSQL, false);
        if (intResult > 0)
        {
            strResultMsg += "Your new Quarter_MetaData_Detail row was updated successfully!<br />";
        }
        else
        {
            strResultMsg += "Error updating Quarter_MetaData_Detail row: 0 rows affected.<br />";
        }
        return intResult;
    }
    
    /// <summary>
    /// Delete one QMD_Base row by STRM
    /// </summary>
    /// <param name="strSTRM"></param>
    /// <returns>Result message by rows affected</returns>
    public Int32 DeleteQuarterMetadataBase(string strSTRM)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string strSQL = "usp_Quarter_MetaData_Base_Delete '" + strSTRM + "'";
        Int32 intResult = runQMDSQLQuery(strSQL, false);
        if (intResult > 0)
        {
            strResultMsg += "Your new Quarter_MetaData_Base was deleted successfully!<br />";
        }
        else
        {
            strResultMsg += "Error deleting Quarter_MetaData_Base row: 0 rows affected.<br />";
        }
        return intResult;
    }

    /// <summary>
    /// Delete one QMD_Detail row by ID
    /// </summary>
    /// <param name="intID"></param>
    /// <returns>Result message by rows affected</returns>
    public Int32 DeleteQuarterMetadataDetailByID(int intID)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string strSQL = "usp_Quarter_MetaData_Detail_Delete " + intID;
        Int32 intResult = runQMDSQLQuery(strSQL, false);
        if (intResult > 0)
        {
            strResultMsg += "Your new Quarter_MetaData_Detail row was deleted successfully!<br />";
        }
        else
        {
            strResultMsg += "Error deleting Quarter_MetaData_Detail row: 0 rows affected.<br />";
        }
        return intResult;
    }

    /// <summary>
    /// Delete all QMD_Detail rows with specified STRM
    /// </summary>
    /// <param name="strSTRM"></param>
    /// <returns>Result message by rows affected</returns>
    public Int32 DeleteQuarterMetadataDetailBySTRM(string strSTRM)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        string strSQL = "usp_Quarter_MetaData_Detail_Delete '" + strSTRM + "'";
        Int32 intResult = runQMDSQLQuery(strSQL, false);
        if (intResult > 0)
        {
            strResultMsg += "Your new Quarter_MetaData_Detail rows were deleted successfully!<br />";
        }
        else
        {
            strResultMsg += "Error deleting Quarter_MetaData_Detail row: 0 rows affected.<br />";
        }
        return intResult;
    }
    
    #endregion
}