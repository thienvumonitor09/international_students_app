﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Configuration;

/// <summary>
/// Summary description for Homestay
/// </summary>
public class Homestay
{
    public String strResultMsg = "";
    public Int32 intNewIdentity = 0;
    public Boolean blSuccess = false;
    String strSQL = "";
    Boolean blReturnIdentity = false;

    public Homestay()
    {
        /************************************************************************************
         *  address codes:  1:  permanent address
         *                  2:  parent's address (if different)
         *                  3:  agency address (if any)
         *                  4:  relative address (if in US)
         *                  5:  emergency contact address
         *  Use applicant ID to match to student
         * ********************************************************************************/

    }

    public DataTable RunGetQuery(string strQuery)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        //strResultMsg += "RunGetQuery: " + strQuery + "<br />";
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
            objConn.Open();
            objCommand.CommandText = strQuery;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;

            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            return ds.Tables[0];
        }
        catch (Exception ex)
        {
            strResultMsg += "Error RunGetQuery: " + ex.Message + "<br />SQL: " + strQuery + "<br />";
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return null;
    }
    public DataTable RunGetQueryCTC(string strQuery)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        //strResultMsg += "RunGetQuery: " + strQuery + "<br />";
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
            objConn.Open();
            objCommand.CommandText = strQuery;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;

            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            return ds.Tables[0];
        }
        catch (Exception ex)
        {
            strResultMsg += "Error RunGetQueryCTC: " + ex.Message + "<br />SQL: " + strQuery + "<br />";
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return null;
    }

    public Int32 runSPQuery(string strSQL, bool blIsInsert)
    {
        Int32 intResult = 0;
        blSuccess = true;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSInternationalStudent"].ToString();
        //strResultMsg += "runSPQuery: " + strSQL + "<br />";
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            if (blIsInsert)
            {
                //Execute scalar
                intResult = (Int32)objCommand.ExecuteScalar();
            }
            else
            {
                //Execute NonQuery
                intResult = objCommand.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            //capture error
            strResultMsg += "Error runSPQuery: " + ex.Message + "<br />";
            strResultMsg += "SQL: " + strSQL + "<br />";
            blSuccess = false;
            return intResult;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return intResult;
    }

    /// <summary>
    /// Select all fields by various criteria
    /// Default is by applicant ID (no type specified)
    /// Types:
    /// DOB:  submit DOB, First and Last name to match
    /// SID:  submit SID to match
    /// homestay: gets all info on all homestay students for grid view

    /// </summary>
    /// <param name="strSelectType"></param>
    /// <param name="intApplicantID"></param>
    /// <param name="strDOB"></param>
    /// <param name="strFirstName"></param>
    /// <param name="strLastName"></param>
    /// <param name="strSID"></param>
    /// <returns></returns>
    public DataTable GetOneInternationalStudent(String strSelectType, Int32 intApplicantID, String strDOB, String strFirstName, String strLastName, String strSID)
    {
        strSQL = "uspGetApplicantBasictInfoExtended '" + strSelectType + "', " + intApplicantID + ", '" + strDOB + "', '" + strFirstName + "', '" + strLastName + "', '" + strSID + "'";
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public DataTable GetOneHomestayStudent(Int32 intApplicantID)
    {
        strSQL = "uspGetOneHomestayStudent " + intApplicantID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public DataTable GetOneContact(Int32 intAddressCode, Int32 intApplicantID)
    {
        strSQL = "uspGetContactInfo " + intAddressCode + ", " + intApplicantID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public DataTable GetCountries()
    {
        strSQL = "uspGetCountries";
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }
    /// <summary>
    /// Get student info from applicant basic info table
    /// -- TYPES:
    /// -- DOB:  date of birth, first and last name authentication
    /// -- SID:  get student by ctcLink ID
    /// -- Homestay:  get all flagged homstay students 
    /// </summary>
    /// <returns>DataTable</returns>


    public DataTable GetHomestayInternationalStudents()
    {
        //Returns all flagged homestay students, active and inactive
        strSQL = "uspGetApplicantBasictInfoExtended 'homestay', 0, '', '', '', ''";
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Types:
    /// Active = all active homestay students for DDLs
    /// ID = Get one student by (applicant) ID
    /// </summary>
    /// <param name="selectedType"></param>
    /// <param name="intApplicantID"></param>
    /// <returns>DataTable</returns>
    public DataTable GetHomestayStudents(String selectedType, Int32 intApplicantID)
    {
        strSQL = "uspGetHomestayStudentInfoExtended '" + selectedType + "', " + intApplicantID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public DataTable GetStudentStudyInfo(Int32 intApplicantID)
    {
        strSQL = "uspGetStudentStudyInformation " + intApplicantID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public DataTable GetArrivalQuarter(String showUpDate)
    {
        strSQL = "usp_InternationalHomestay_GetArrivalQuarter '" + showUpDate + "'";
        DataTable dt = RunGetQueryCTC(strSQL);
        return dt;
    }

    public DataTable GetHomestayRelatives(Int32 applicantID, Int32 familyID)
    {
        strSQL = "uspGetHomestayRelatives " + applicantID + ", " + familyID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public DataTable GetHomestayPreferenceSelections(Int32 applicantID, Int32 familyID)
    {
        strSQL = "uspGetHomestayPreferenceSelections " + applicantID + ", " + familyID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public DataTable GetHomestayStatusValues(String strType)
    {
        strSQL = "uspGetHomestayStatusValues '" + strType + "'";
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    /// <summary>
    /// Insert new or update existing homestay student info record.  SP searches for existing before insert; otherwise updates
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="SelectedQuarter"></param>
    /// <param name="ArrivalDate"></param>
    /// <param name="PayMethod"></param>
    /// <param name="CreditCardName"></param>
    /// <param name="CreditCardType"></param>
    /// <param name="CreditCardNumber"></param>
    /// <param name="CreditCardExprDate"></param>
    /// <param name="CreditCardPIN"></param>
    /// <param name="LetterSubmitted"></param>
    /// <param name="PhotoSubmitted"></param>
    /// <param name="IsActive"></param>
    /// <returns>Rows affected</returns>
    public Int32 InsertUpdateP1HomestayStudentInfo(Int32 ID, String SelectedQuarter, String ArrivalDate, String HomestayOption, String PayMethod,
        Boolean LetterSubmitted, Boolean PhotoSubmitted, Int32 IsActive, Boolean InsertMode)
    {
        blReturnIdentity = InsertMode;
        strSQL = "uspInsertUpdateP1HomestayStudentInfo " + ID + ", '" + SelectedQuarter + "', '" + ArrivalDate + "', '" + HomestayOption + "', '" + PayMethod;
        strSQL += "', " + LetterSubmitted + ", " + PhotoSubmitted + ", " + IsActive;
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateP2HomestayStudentInfo(Int32 ID, String traveledOutsideCountry, String smokingHabits, String activitiesEnjoyed, String homeEnvironmentPreferences, String allergies,
        String healthConditions, String healthStatus, Int32 driveCar, String anythingElse)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateP2HomestayStudentInfo " + ID + ", '" + traveledOutsideCountry + "', '" + smokingHabits + "', '" + activitiesEnjoyed + "', '" + homeEnvironmentPreferences + "', '" + allergies;
        strSQL += "', '" + healthConditions + "', '" + healthStatus + "', " + driveCar + ", '" + anythingElse + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;

    }

    public Int32 UpdateWaiverSubmittedBitflagHomestayFamilyInfo(Int32 familyID, Int32 bitValue)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateWaiverSubmittedBitflagHomestayFamilyInfo " + familyID + ", " + bitValue;
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 ResetLoopStatus()
    {
        blReturnIdentity = false;
        strSQL = "uspResetLoopStatusValue";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 GetMatchStuNoSmokeButAllowOthers()
    {
        blReturnIdentity = false;
        strSQL = "uspGetMatchNoSmokeButAllowOthers";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 InsertHomestayRelatives(Int32 applicantID, Int32 familyID, String familyName, String firstName, String occupation, String DOB, Int32 age, String gender,
    String relationship, String email, String cellPhone, String workPhone, String driversLicenseNumber, String middleName)
    {
        blReturnIdentity = false;
        strSQL = "uspInsertHomestayRelatives " + applicantID + ", " + familyID + ", '" + familyName + "', '" + firstName + "', '" + occupation + "', '" + DOB + "', " + age + ", '" + gender;
        strSQL += "', '" + relationship + "', '" + email + "', '" + cellPhone + "', '" + workPhone + "', '" + driversLicenseNumber + "', '" + middleName + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 InsertUpdateHomestayPreferenceSelections(String preferenceID, Int32 applicantID, Int32 familyID)
    {
        blReturnIdentity = false;
        strSQL = "uspInsertUpdateHomestayPreferenceSelections '" + preferenceID + "', " + applicantID + ", " + familyID;
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateHomestayFamilyHome(Int32 intFamilyID, String strHomeName, String strStreetAddress, String strCity, String strState, String strZIP, String strLandLinePhone,
        String strInsuranceCompany, String strInsurancePolicyNumber, String strInsuranceAgentName, String strInsuranceAgentPhone,
        String strRoom1, String strRoom1DateOpen, String strRoom1DateClosed, String strRoom1Bathroom, String strRoom1Occupancy,
        String strRoom2, String strRoom2DateOpen, String strRoom2DateClosed, String strRoom2Bathroom, String strRoom2Occupancy,
        String strRoom3, String strRoom3DateOpen, String strRoom3DateClosed, String strRoom3Bathroom, String strRoom3Occupancy,
        String strRoom4, String strRoom4DateOpen, String strRoom4DateClosed, String strRoom4Bathroom, String strRoom4Occupancy)

    {
        blReturnIdentity = false;
        strSQL = "uspUpdateHomestayFamilyHome " + intFamilyID + ", '" + strHomeName + "', '" + strStreetAddress + "', '" + strCity + "', '" + strState + "', '" + strZIP + "', '" + strLandLinePhone;
        strSQL += "', '" + strInsuranceCompany + "', '" + strInsurancePolicyNumber + "', '" + strInsuranceAgentName + "', '" + strInsuranceAgentPhone;
        strSQL += "', '" + strRoom1 + "', '" + strRoom1DateOpen + "', '" + strRoom1DateClosed + "', '" + strRoom1Bathroom + "', '" + strRoom1Occupancy;
        strSQL += "', '" + strRoom2 + "', '" + strRoom2DateOpen + "', '" + strRoom2DateClosed + "', '" + strRoom2Bathroom + "', '" + strRoom2Occupancy;
        strSQL += "', '" + strRoom3 + "', '" + strRoom3DateOpen + "', '" + strRoom3DateClosed + "', '" + strRoom3Bathroom + "', '" + strRoom3Occupancy;
        strSQL += "', '" + strRoom4 + "', '" + strRoom4DateOpen + "', '" + strRoom4DateClosed + "', '" + strRoom4Bathroom + "', '" + strRoom4Occupancy + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdatePetsInHome(Int32 intFamilyID, String strPetsInHome)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdatePetsInHome " + intFamilyID + ", '" + strPetsInHome + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 DeleteHomestayPreferenceSelections(Int32 appplicantID, Int32 familyID)
    {
        blReturnIdentity = false;
        strSQL = "uspDeleteHomestayPreferenceSelectionsByID " + appplicantID + ", " + familyID;
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateHomestayRelatives(Int32 ID, String familyName, String firstName, String occupation, String DOB, Int32 age, String gender,
        String relationship, String email, String cellPhone, String workPhone, String driversLicenseNumber, String middleName)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateHomestayRelatives " + ID + ", '" + familyName + "', '" + firstName + "', '" + occupation + "', '" + DOB + "', " + age + ", '" + gender;
        strSQL += "', '" + relationship + "', '" + email + "', '" + cellPhone + "', '" + workPhone + "', '" + driversLicenseNumber + "', '" + middleName + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        //strResultMsg += "UpdateP1HomestayRelatives: " + strSQL + "<br />";
        return intResult;
    }

    public Int32 InsertUploadedFilesInformation(Int32 applicantID, String applicantName, String fileName, String filePath)
    {
        blReturnIdentity = false;
        strSQL = "uspInsertUploadedFilesInformation " + applicantID + ", '" + applicantName + "', '" + fileName + "', '" + filePath + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public DataTable GetUploadedFiles(Int32 applicantID)
    {
        strSQL = "uspGetUploadedFilesInformation " + applicantID;
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public Int32 GetEmailSent(Int32 applicantID, String schoolOrFamily)
    {
        strSQL = "uspGetEmailSent " + applicantID + ", '" + schoolOrFamily;
        Int32 dt = runSPQuery(strSQL, false);
        return dt;
    }

    public Int32 InsertHomestayFamilyInfo(String strUserName, String strHomeName, String strStreetAddress, String strCity, String strState, String strZIP, String strLandLinePhone, String strWspBackgroundCheck,
        String strInsuranceCompany, String strInsurancePolicyNumber, String strInsuranceAgentName, String strInsuranceAgentPhone, String strWhyHost, String strHostExperience, String strPrimaryLanguage, String strOtherLanguages,
        String strReligiousActivities, String strSmokingGuidelines, String strPetsInHome, String strGeneralLifestyle, String strOtherSchool, String strAdditionalExpectations, Boolean blLicenseSubmitted, String strHowReferred,
        String strRoom1, String strRoom1DateOpen, String strRoom1DateClosed, String strRoom1Bathroom, String strRoom1Occupancy,
        String strRoom2, String strRoom2DateOpen, String strRoom2DateClosed, String strRoom2Bathroom, String strRoom2Occupancy,
        String strRoom3, String strRoom3DateOpen, String strRoom3DateClosed, String strRoom3Bathroom, String strRoom3Occupancy,
        String strRoom4, String strRoom4DateOpen, String strRoom4DateClosed, String strRoom4Bathroom, String strRoom4Occupancy,
        Int32 intMainLevelRooms, Int32 intUpstairsRooms, Int32 intBasementRooms)
    {
        //We are using the id from ApplicantBasicInfo table as primary identifier
        blReturnIdentity = true;
        intNewIdentity = 0;
        strSQL = "uspInsertP1HomestayFamilyInfo '" + strUserName + "', '" + strHomeName + "', '" + strStreetAddress + "', '" + strCity + "', '" + strState + "', '" + strZIP + "', '" + strLandLinePhone + "', '" + strWspBackgroundCheck;
        strSQL += "', '" + strInsuranceCompany + "', '" + strInsurancePolicyNumber + "', '" + strInsuranceAgentName + "', '" + strInsuranceAgentPhone + "', '" + strWhyHost + "', '" + strHostExperience + "', '" + strPrimaryLanguage + "', '" + strOtherLanguages;
        strSQL += "', '" + strReligiousActivities + "', '" + strSmokingGuidelines + "', '" + strPetsInHome + "', '" + strGeneralLifestyle + "', '" + strOtherSchool + "', '" + strAdditionalExpectations + "', 1, " + blLicenseSubmitted + ", '" + strHowReferred;
        strSQL += "', '" + strRoom1 + "', '" + strRoom1DateOpen + "', '" + strRoom1DateClosed + "', '" + strRoom1Bathroom + "', '" + strRoom1Occupancy;
        strSQL += "', '" + strRoom2 + "', '" + strRoom2DateOpen + "', '" + strRoom2DateClosed + "', '" + strRoom2Bathroom + "', '" + strRoom2Occupancy;
        strSQL += "', '" + strRoom3 + "', '" + strRoom3DateOpen + "', '" + strRoom3DateClosed + "', '" + strRoom3Bathroom + "', '" + strRoom3Occupancy;
        strSQL += "', '" + strRoom4 + "', '" + strRoom4DateOpen + "', '" + strRoom4DateClosed + "', '" + strRoom4Bathroom + "', '" + strRoom4Occupancy;
        strSQL += "', " + intMainLevelRooms + ", " + intUpstairsRooms + ", " + intBasementRooms;

        try
        {
            intNewIdentity = runSPQuery(strSQL, blReturnIdentity);
        }
        catch (Exception ex)
        {
            strResultMsg += "Error InsertHomestayFamilyInfo: " + ex.Message + "; Stack trace: " + ex.StackTrace + "<br />";
        }
        return intNewIdentity;
        //return strResultMsg;
    }

    public Int32 UpdateP1HomestayFamilyInfo(Int32 intID, String strUserName, String strHomeName, String strStreetAddress, String strCity, String strState, String strZIP, String strLandLinePhone, String strWspBackgroundCheck,
        String strInsuranceCompany, String strInsurancePolicyNumber, String strInsuranceAgentName, String strInsuranceAgentPhone, String strWhyHost, String strHostExperience, String strPrimaryLanguage, String strOtherLanguages,
        String strReligiousActivities, String strSmokingGuidelines, String strPetsInHome, String strGeneralLifestyle, String strOtherSchool, String strAdditionalExpectations, Boolean blLicenseSubmitted, String strHowReferred,
        String strRoom1, String strRoom1DateOpen, String strRoom1DateClosed, String strRoom1Bathroom, String strRoom1Occupancy,
        String strRoom2, String strRoom2DateOpen, String strRoom2DateClosed, String strRoom2Bathroom, String strRoom2Occupancy,
        String strRoom3, String strRoom3DateOpen, String strRoom3DateClosed, String strRoom3Bathroom, String strRoom3Occupancy,
        String strRoom4, String strRoom4DateOpen, String strRoom4DateClosed, String strRoom4Bathroom, String strRoom4Occupancy,
        Int32 intMainLevelRooms, Int32 intUpstairsRooms, Int32 intBasementRooms)
    {
        //We are using the id from ApplicantBasicInfo table as primary identifier
        blReturnIdentity = false;
        Int32 intResult = 0;
        strSQL = "uspUpdateP1HomestayFamilyInfo " + intID + ", '" + strUserName + "', '" + strHomeName + "', '" + strStreetAddress + "', '" + strCity + "', '" + strState + "', '" + strZIP + "', '" + strLandLinePhone + "', '" + strWspBackgroundCheck;
        strSQL += "', '" + strInsuranceCompany + "', '" + strInsurancePolicyNumber + "', '" + strInsuranceAgentName + "', '" + strInsuranceAgentPhone + "', '" + strWhyHost + "', '" + strHostExperience + "', '" + strPrimaryLanguage + "', '" + strOtherLanguages;
        strSQL += "', '" + strReligiousActivities + "', '" + strSmokingGuidelines + "', '" + strPetsInHome + "', '" + strGeneralLifestyle + "', '" + strOtherSchool + "', '" + strAdditionalExpectations + "', " + blLicenseSubmitted + ", '" + strHowReferred;
        strSQL += "', '" + strRoom1 + "', '" + strRoom1DateOpen + "', '" + strRoom1DateClosed + "', '" + strRoom1Bathroom + "', '" + strRoom1Occupancy;
        strSQL += "', '" + strRoom2 + "', '" + strRoom2DateOpen + "', '" + strRoom2DateClosed + "', '" + strRoom2Bathroom + "', '" + strRoom2Occupancy;
        strSQL += "', '" + strRoom3 + "', '" + strRoom3DateOpen + "', '" + strRoom3DateClosed + "', '" + strRoom3Bathroom + "', '" + strRoom3Occupancy;
        strSQL += "', '" + strRoom4 + "', '" + strRoom4DateOpen + "', '" + strRoom4DateClosed + "', '" + strRoom4Bathroom + "', '" + strRoom4Occupancy;
        strSQL += "', " + intMainLevelRooms + ", " + intUpstairsRooms + ", " + intBasementRooms;
        try
        {
            intResult = runSPQuery(strSQL, blReturnIdentity);
        }
        catch (Exception ex)
        {
            strResultMsg += "Error UpdateP1HomestayFamilyInfo: " + ex.Message + "; Stack trace: " + ex.StackTrace + "<br />";
        }
        strResultMsg += "UpdateP1HomestayFamilyInfo: " + strSQL + "<br />";
        return intResult;
        //return strResultMsg;
    }

    public String UpdateP2HomestayFamilyInfo(Int32 intID, String travelOutsideUS, String smokerInHome, String otherHobbies, String homeEnvironment, String additionalPreferences, String homestayOption, String genderPreference)
    {
        blReturnIdentity = false;
        Int32 intResult = 0;
        strResultMsg = "im OK";
        strSQL = "uspUpdateP2HomestayFamilyInfo " + intID + ", '" + travelOutsideUS + "', '" + smokerInHome + "', '" + otherHobbies + "', '" + homeEnvironment + "', '" + additionalPreferences + "', '" + homestayOption + "', '" + genderPreference + "'";
        try
        {
            intResult = runSPQuery(strSQL, blReturnIdentity);
        }
        catch (Exception ex)
        {
            strResultMsg += "Error UpdateP2HomestayFamilyInfo: " + ex.Message + "; Stack trace: " + ex.StackTrace + "<br />";
        }
        // return intResult;
        return strResultMsg;
    }

    public String UpdateTravelTimeHomestayFamilyInfo(Int32 intID, String walkingBusStopMinutes, Decimal walkingBusStopMiles, String walkingBusStopBlocks, String busMinutesSCC, String busMinutesSFCC, String collegeQualified)
    {
        blReturnIdentity = false;
        Int32 intResult = 0;
        string strResult = "";
        strSQL = "uspUpdateTravelTimeHomestayFamilyInfo " + intID + ", " + walkingBusStopMinutes + ", " + walkingBusStopMiles + ", " + walkingBusStopBlocks + ", " + busMinutesSCC + ", " + busMinutesSFCC + ", '" + collegeQualified + "'";
        try
        {
            intResult = runSPQuery(strSQL, blReturnIdentity);
            strResult = strSQL;
        }
        catch (Exception ex)
        {
            strResult += "Error UpdateP2HomestayFamilyInfo: " + ex.Message + "; Stack trace: " + ex.StackTrace + "<br />";
        }
        return strResult;

    }

    /// <summary>
    /// Basic HomestayFamilyInfo get queries
    /// Types:
    /// DDL: Drop-down list options (id and homeName fields only)
    /// DDLinactive: Drop-down list options, inactive only
    /// DDLactive: Drop-down list options, active only
    /// ID:  Get by id
    /// UserName: Get by family user name
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="userName"></param>
    /// <param name="queryType"></param>
    /// <returns></returns>
    public DataTable GetHomestayFamilyInfo(Int32 ID, String userName, String queryType)
    {
        strSQL = "uspGetHomestayFamilyInfo " + ID + ", '" + userName + "', '" + queryType + "'";
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

    public Int32 UpdateHomestayBitflagApplicantBasicInfo(Int32 intID, Int32 bitValue)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateHomestayBitflagApplicantBasicInfo " + intID + ", " + bitValue;
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }
    public String updateMailSentFamily(Int32 intID, Int32 bitValue)
    {
        string strResult = "";
        blReturnIdentity = false;
        strSQL = "uspUpdateMailSentFamily " + intID + ", " + bitValue;
        try
        {
            Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
            strResult = intResult.ToString() + " NO ERROR";
            return strSQL + " - " + strResult;
        }
        catch (Exception exp)
        {
            strResult = strSQL + " ";
            strResult += exp.Message + "<br />" + exp.StackTrace;
        }
        // return intResult;
        return strResult;
    }
    public Int32 UpdateMailSentStudent(Int32 intID, Int32 bitValue)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateMailSentStudent " + intID + ", " + bitValue;
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    /// <summary>
    /// Supply either homestay id or applicantID to update status
    /// </summary>
    /// <param name="id"></param>
    /// <param name="applicantID"></param>
    /// <param name="StudentStatus"></param>
    /// <returns></returns>
    public Int32 UpdateHomestayStudentStatus(Int32 id, Int32 applicantID, String StudentStatus)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateHomestayStatusHomestayStudentInfo " + id + ", " + applicantID + ", '" + StudentStatus + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateFamilyStatus(Int32 id, String FamilyStatus)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateFamilyStatusHomestayFamilyInfo " + id + ", '" + FamilyStatus + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateIsActiveBitFlagHomestayFamilyInfo(Int32 id, String activeStatus)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateIsActiveBitFlagHomestayFamilyInfo " + id + ", " + activeStatus;
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateIsActiveBitFlagHomestayStudentInfo(Int32 applicantID, String activeStatus)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateIsActiveBitFlagHomestayStudentInfo " + applicantID + ", '" + activeStatus + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    /// <summary>
    /// Set bitflags for the following fields:
    /// agreeCCSConduct18
    /// agreeWaiveLiability18
    /// agreeCCSConductUA
    /// agreeNoticesWaiverParentUA
    /// agreeWaiveLiabilityParentUA
    /// </summary>
    /// <param name="applicantID"></param>
    /// <param name="UnderstandAgree"></param>
    /// <param name="agreement"></param>
    /// <returns></returns>
    public Int32 UpdateUnderstandAgreeHomestayStudentInfo(Int32 applicantID, Int32 UnderstandAgree, String agreement)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateUnderstandAgreeHomestayStudentInfo " + applicantID + ", " + UnderstandAgree + ", '" + agreement + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    /// <summary>
    /// Set bitflags for the following fields:
    /// CCSConduct18Submitted
    /// WaiveLiability18Submitted
    /// CCSConductUASubmitted
    /// NoticesWaiverParentUASubmitted
    /// WaiveLiabilityParentUASubmitted
    /// </summary>
    /// <param name="applicantID"></param>
    /// <param name="AgreementSubmitted"></param>
    /// <param name="agreement"></param>
    /// <returns></returns>
    public Int32 UpdateAgreementSubmittedHomestayStudentInfo(Int32 applicantID, Int32 agreementSubmitted, String agreement)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateAgreementSubmittedHomestayStudentInfo " + applicantID + ", " + agreementSubmitted + ", '" + agreement + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateUnderstandAgreeHomestayFamilyInfo(Int32 familyID, Int32 UnderstandAgree)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateUnderstandAgreeHomestayFamilyInfo " + familyID + ", " + UnderstandAgree;
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateDateInactiveHomestayStudentInfo(Int32 applicantID, String dateInactive)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateDateInactiveHomestayStudentInfo " + applicantID + ", '" + dateInactive + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateDateTerminatedHomestayFamilyInfo(Int32 familyID, String dateTerminated)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateDateTerminatedHomestayFamilyInfo " + familyID + ", '" + dateTerminated + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 InsertHomestayNote(Int32 applicantID, Int32 familyID, Int32 placementID, String Note, DateTime dtDate, String employeeID, String actionNeeded, String subject, String noteType)
    {
        blReturnIdentity = false;
        strSQL = "uspInsertHomestayNote " + applicantID + ", " + familyID + ", " + placementID + ", '" + Note + "', '" + dtDate + "', '" + employeeID + "', " + actionNeeded + ", '" + subject + "', '" + noteType + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    public Int32 UpdateHomestayNote(Int32 noteID, Int32 applicantID, Int32 familyID, Int32 placementID, String Note, DateTime dtDate, String employeeID, String actionNeeded, String actionCompleted, String subject, String noteType)
    {
        blReturnIdentity = false;
        strSQL = "uspUpdateHomestayNote " + noteID + ", " + applicantID + ", " + familyID + ", " + placementID + ", '" + Note + "', '" + dtDate + "', '" + employeeID + "', " + actionNeeded + ", " + actionCompleted + ", '" + subject + "', '" + noteType + "'";
        Int32 intResult = runSPQuery(strSQL, blReturnIdentity);
        return intResult;
    }

    /// <summary>
    /// Get notes by various criteria; pass zeros for irrelevant parameters
    /// Types:
    ///  Family = notes by family ID
    ///  Student = notes by student ID
    ///  Placement = notes by placement ID
    ///  Edit = note by Note ID
    ///  Action = notes reqiring action where completed not checked
    ///  DateRange = notes by start and end date inclusive
    ///  NoteType = notes by student/family ID and note type value
    /// </summary>
    /// <param name="strType"></param>
    /// <param name="applicantID"></param>
    /// <param name="familyID"></param>
    /// <param name="placementID"></param>
    /// <param name="selectedNote"></param>
    /// <param name="startDate"></param>
    /// <param name="endDate"></param>
    /// <param name="noteType"></param>
    /// <returns></returns>
    public DataTable GetHomestayNotes(String strType, String applicantID, String familyID, String placementID, String selectedNote, String startDate, String endDate, String noteType)
    {
        strSQL = "uspGetHomestayNotes '" + strType + "', " + applicantID + ", " + familyID + ", " + placementID + ", " + selectedNote + ", '" + startDate + "', '" + endDate + "', '" + noteType + "'";
        DataTable dt = RunGetQuery(strSQL);
        return dt;
    }

}