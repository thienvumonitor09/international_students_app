﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        HttpContext ctx = HttpContext.Current;

        Exception exception = ctx.Server.GetLastError();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        // Get the path of the page
        sb.Append("More information may be available in the system log on the target server.<br /><br /><b>Error in Path:</b> " + Request.Path);
        // Get the QueryString along with the Virtual Path
        sb.Append("<br /><br /><b>Error Raw Url: </b>" + Request.RawUrl);
        // Create an Exception object from the Last error that occurred on the server
        Exception myError = Server.GetLastError();
        // Get the error message
        sb.Append("<br /><br /><b>Error Message:</b> " + myError.Message);
        // Source of the message
        sb.Append("<br /><br /><b>Error Source:</b> " + myError.Source);
        // Stack Trace of the error
        sb.Append("<br /><br /><b>Error Stack Trace:</b> " + myError.StackTrace);
        // Method where the error occurred
        sb.Append("<br /><br /><b>Error TargetSite:</b> " + Request.ServerVariables["SERVER_NAME"]);
        sb.Append("<br /><br /><b>IP:</b> " + Request.ServerVariables["REMOTE_ADDR"] + "<br /><br />" + DateTime.Now.ToString() +
          "<br /><br />" + myError.InnerException + "<br /><br />" + myError.Data + "<br /><br />" +
          "<b>Client Information:</b> " + HttpContext.Current.Request.Browser.Browser + "<br /><br />");
        String temp = "<b>Header Key Value Pairs</b><br />";
        foreach (string st in HttpContext.Current.Request.Headers.AllKeys)
        {
            foreach (string st2 in HttpContext.Current.Request.Headers.GetValues(st))
            {
                temp += "  " + st + " = " + st2 + "<br />";
            }
        }
        sb.Append(temp + "<br /><br />");
        temp = "<b>Form Key Value Pairs:</b><br />";
        foreach (string st in HttpContext.Current.Request.Form.AllKeys)
        {
            temp += "  " + st + " = " + HttpContext.Current.Request.Form[st] + "<br />";
        }
        sb.Append(temp + "<br /><br />");


        Utility sendMail = new Utility();
        sendMail.SendEmailMsg("Laura.Padden@ccs.spokane.edu", "aspxerror@ccs.spokane.edu", "Apps Domain Error", true, sb.ToString(), "", "");

        // Attempt to redirect the user
        Server.Transfer("/_error/Error500.htm");
        // }
        // catch (Exception err)
        //  {
        // Error occured while redirecting user to an error page. We could do nothing
        // now, but setting a status code and completing the request
        //     Response.StatusCode = 404;
        //     String errtext = err.Data.ToString();
        //     this.CompleteRequest();
        // }
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
