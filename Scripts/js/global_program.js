var menu = "#global_sidemenu_subpages";
var currentItem = "";
$(document).ready(
	function(){
		$(menu + ">li").hover(function(){
			var thisItem = $(this).children("ul").attr("id");
			if(thisItem != undefined)
				if( thisItem != currentItem ){
					if( currentItem != "" )
						$("#" + currentItem).slideUp("fast");					
					$("#" + thisItem).slideDown("fast");
					currentItem = thisItem;
				}
		});
		
		$(".MainMenuCMSListMenuHighlightedLI ul").slideDown("fast", function(){currentItem = $(this).attr("id");});
		
		$(".LevelTwoMenuCMSListMenuHighlightedLI").parent().slideDown("fast");
		
		$("#international_quick_select").change(function(){document.location = $(this).val();});
		
	}
);
