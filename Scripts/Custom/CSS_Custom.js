$(document).ready(function($){
  AddBackgroundColortoTiles();
  UpdateTileColumns();
  initializeMultiSlider();
  initializeSingleSlider();
  updateFeaturedContentAlignment();
  
  //Search Box event
  $('#site-search-input, #site-search-input-mobile').keyup(function(event) {
    var query = $(this).val();
    if (event.keyCode == 13) {
      event.preventDefault();
      var location = '/search?query=' + query;
      window.location = location;
    }
  });
  
  //Left nav selection
  var pathname = window.location.pathname;
  $('.left-nav a[href$="'+pathname+'"]').parent().addClass('active'); 
  
  //set top of caption text boxes relative to title height
  $('.caption .caption__overlay__title' ).each(function(){
    var textElement = $(this);
    var textHeight= $(this).height();
    var lineHeight = parseInt($(this).css('line-height').replace('px',''));
    
    if (textHeight/lineHeight > 1) {
      textElement.parent().parent().parent().addClass('two-line');
    }
  });
});


function UpdateTileColumns(){
  console.log("run tile column code");
  var ThreeColumn = $('.FastFacts3Column');
  var twoColumn = $('.FastFacts2Column');
  if(twoColumn)
  {
    console.log("twoColumn");  
    var tiles = twoColumn.find('.FastFactTile');
    if(tiles)
    {
      tiles.each(function (index, value){
        $(value).attr('class', 'col-xs-12 col-sm-6');
      });
    }
  }
}

function initializeMultiSlider(){
  if($('div.slick-grouping').length > 0){
    $('div.slick-grouping').first().show();
    $('div.button.multi-slider').appendTo('#multi-container');
    loadSlider(0);
  }
}

function loadSlider(i){
  hideAllMultiSliders();
  $('div.multi-slider[id$="-'+i+'"]').addClass('selected');
  $('div.slick-grouping[id$="-'+i+'"]').show();
  $('div.slick-grouping[id$="-'+i+'"]').slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });
}

function hideAllMultiSliders(){
  $('div.multi-slider').removeClass('selected');
  $('div.slick-grouping').each(function(){
    if($(this).hasClass('slick-initialized')
       {
           $(this).hide();
          $(this).slick('unslick');
       }
  });
}
function initializeSingleSlider(){
  $(".regular").slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });
  $(".center").slick({
    dots: true,
    infinite: true,
    centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 3
  });
  $(".variable").slick({
    dots: true,
    infinite: true,
    variableWidth: true
  });
  $(".lazy").slick({
    lazyLoad: 'ondemand', // ondemand progressive anticipated
    infinite: true
  });
}

function updateFeaturedContentAlignment(){
  var textRight = $('.FCTextRight');
  var textAlt = $('.FCTextAlternating');
  if(textRight)
  { 
    var contentItems = textRight.find('.FeaturedContent');
    if(contentItems)
    {
      contentItems.each(function (index, value){
        var image =  $(value).find('.FeaturedContentImage');
        if(image)
        {
          image.attr("class","col-xs-12 col-sm-6 col-md-5 HL-image");
        }
        var text =  $(value).find('.FeaturedContentText');
        if(text)
        {
          text.attr("class","col-xs-12 col-sm-6 col-md-7 flex-height right");
        }
      });
    }
  }
  if(textAlt)
	{ 
      console.log("Alternating Text");
      var contentItems = textAlt.find('.FeaturedContent');
		if(contentItems)
		{	
          contentItems.each(function (index, value){
				if(index % 2 == 0)
				{
					var image =  $(value).find('.FeaturedContentImage');
					if(image)
					{
					  image.attr("class","col-xs-12 col-sm-6 col-md-5 HL-image");
					}
					var text =  $(value).find('.FeaturedContentText');
					if(text)
					{
					  text.attr("class","col-xs-12 col-sm-6 col-md-7 flex-height right");
					}
				}
			});
		}
	}
}



function AddBackgroundColortoTiles() {
  var tiles = $('.NeedBackgroundColor');
  if(tiles)
  {
    if(tiles[0])
    {
      if(tiles[0].classList.contains("trans"))
      {
        tiles[0].className += " blue-bg-trans";
      }
      else
      {
        tiles[0].className += " blue-bg";
      }
    }
    if(tiles[1])
    {
      if(tiles[1].classList.contains("trans"))
      {
        tiles[1].className += " green-bg-trans";
      }
      else
      {
        tiles[1].className += " green-bg";
      }
    }
    if(tiles[2])
    {
      if(tiles[2].classList.contains("trans"))
      {
        tiles[2].className += " maroon-bg-trans";
      }
      else
      {
        tiles[2].className += " maroon-bg";
      }
    }
    if(tiles[3])
    {
      if(tiles[3].classList.contains("trans"))
      {
        tiles[3].className += " purple-bg-trans";
      }
      else
      {
        tiles[3].className += " purple-bg";
      }
    }
    if(tiles[4])
    {
      if(tiles[4].classList.contains("trans"))
      {
        tiles[4].className += " brown-bg-trans";
      }
      else
      {
        tiles[4].className += " brown-bg";
      }
    }
    if(tiles[5])
    {
      if(tiles[5].classList.contains("trans"))
      {
        tiles[5].className += " orange-bg-trans";
      }
      else
      {
        tiles[5].className += " orange-bg";
      }
    }
    if(tiles[6])
    {
      if(tiles[6].classList.contains("trans"))
      {
        tiles[6].className += " blue-bg-trans";
      }
      else
      {
        tiles[6].className += " blue-bg";
      }
    }
    if(tiles[7])
    {
      if(tiles[7].classList.contains("trans"))
      {
        tiles[7].className += " green-bg-trans";
      }
      else
      {
        tiles[7].className += " green-bg";
      }
    }
    if(tiles[8])
    {
      if(tiles[8].classList.contains("trans"))
      {
        tiles[8].className += " maroon-bg-trans";
      }
      else
      {
        tiles[8].className += " maroon-bg";
      }
    }
    if(tiles[9])
    {
      if(tiles[9].classList.contains("trans"))
      {
        tiles[9].className += " purple-bg-trans";
      }
      else
      {
        tiles[9].className += " purple-bg";
      }
    }
    if(tiles[10])
    {
      if(tiles[10].classList.contains("trans"))
      {
        tiles[10].className += " brown-bg-trans";
      }
      else
      {
        tiles[10].className += " brown-bg";
      }
    }
    if(tiles[11])
    {
      if(tiles[11].classList.contains("trans"))
      {
        tiles[11].className += " orange-bg-trans";
      }
      else
      {
        tiles[11].className += " orange-bg";
      }
    }
    if(tiles[12])
    {
      if(tiles[12].classList.contains("trans"))
      {
        tiles[12].className += " blue-bg-trans";
      }
      else
      {
        tiles[12].className += " blue-bg";
      }
    }
    if(tiles[13])
    {
      if(tiles[13].classList.contains("trans"))
      {
        tiles[13].className += " green-bg-trans";
      }
      else
      {
        tiles[13].className += " green-bg";
      }
    }
    if(tiles[14])
    {
      if(tiles[14].classList.contains("trans"))
      {
        tiles[14].className += " maroon-bg-trans";
      }
      else
      {
        tiles[14].className += " maroon-bg";
      }
    }
    if(tiles[15])
    {
      if(tiles[15].classList.contains("trans"))
      {
        tiles[15].className += " purple-bg-trans";
      }
      else
      {
        tiles[15].className += " purple-bg";
      }
    }
    if(tiles[16])
    {
      if(tiles[16].classList.contains("trans"))
      {
        tiles[16].className += " brown-bg-trans";
      }
      else
      {
        tiles[16].className += " brown-bg";
      }
    }
    if(tiles[17])
    {
      if(tiles[17].classList.contains("trans"))
      {
        tiles[17].className += " orange-bg-trans";
      }
      else
      {
        tiles[17].className += " orange-bg";
      }
    }
    if(tiles[18])
    {
      if(tiles[18].classList.contains("trans"))
      {
        tiles[18].className += " blue-bg-trans";
      }
      else
      {
        tiles[18].className += " blue-bg";
      }
    }
    if(tiles[19])
    {
      if(tiles[19].classList.contains("trans"))
      {
        tiles[19].className += " green-bg-trans";
      }
      else
      {
        tiles[19].className += " green-bg";
      }
    }
    if(tiles[20])
    {
      if(tiles[20].classList.contains("trans"))
      {
        tiles[20].className += " maroon-bg-trans";
      }
      else
      {
        tiles[20].className += " maroon-bg";
      }
    }
    if(tiles[21])
    {
      if(tiles[21].classList.contains("trans"))
      {
        tiles[21].className += " purple-bg-trans";
      }
      else
      {
        tiles[21].className += " purple-bg";
      }
    }
    if(tiles[22])
    {
      if(tiles[22].classList.contains("trans"))
      {
        tiles[22].className += " brown-bg-trans";
      }
      else
      {
        tiles[22].className += " brown-bg";
      }
    }
    if(tiles[23])
    {
      if(tiles[23].classList.contains("trans"))
      {
        tiles[23].className += " orange-bg-trans";
      }
      else
      {
        tiles[23].className += " orange-bg";
      }
    }
    if(tiles[24])
    {
      if(tiles[24].classList.contains("trans"))
      {
        tiles[24].className += " blue-bg-trans";
      }
      else
      {
        tiles[24].className += " blue-bg";
      }
    }
    if(tiles[25])
    {
      if(tiles[25].classList.contains("trans"))
      {
        tiles[25].className += " green-bg-trans";
      }
      else
      {
        tiles[25].className += " green-bg";
      }
    }
    if(tiles[26])
    {
      if(tiles[26].classList.contains("trans"))
      {
        tiles[26].className += " maroon-bg-trans";
      }
      else
      {
        tiles[26].className += " maroon-bg";
      }
    }
    if(tiles[27])
    {
      if(tiles[27].classList.contains("trans"))
      {
        tiles[27].className += " purple-bg-trans";
      }
      else
      {
        tiles[27].className += " purple-bg";
      }
    }
    if(tiles[28])
    {
      if(tiles[28].classList.contains("trans"))
      {
        tiles[28].className += " brown-bg-trans";
      }
      else
      {
        tiles[28].className += " brown-bg";
      }
    }
    if(tiles[29])
    {
      if(tiles[29].classList.contains("trans"))
      {
        tiles[29].className += " orange-bg-trans";
      }
      else
      {
        tiles[29].className += " orange-bg";
      }
    }
  }
}
