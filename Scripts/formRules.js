﻿$(document).ready(function () {
    // validate the form when it is submitted
    // alert("Hi");
    $.validator.addMethod('requiredIfChecked', function (value, element) {
        if ($("#ContentHolder_chkWhatStudy_7").is(":checked") && ($.trim(value) == '')) {
            $("#ContentHolder_eWhatStudy").val("This field is required when Other is checked");
        } else {
            $("#ContentHolder_eWhatStudy").val("");
            return true;
        }
    });

    $.validator.addMethod("requiredIfChecked2", function (value, el) {
        if ($("#ContentHolder_chkHowPay_5").is(":checked") && ($.trim(value) == '')) {
            $("#ContentHolder_eHowPay").val("This field is required when Other is checked");
        } else {
            $("#ContentHolder_eHowPay").val("");
            return true;
        }
    });

    $.validator.addMethod("phoneUS", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
    phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    }, "Please specify a valid phone number");


    $('#form1').validate({
        rules: {
            ctl00$ContentHolder$txtFamilyName: "required",
            ctl00$ContentHolder$txtGivenName: "required",
            ctl00$ContentHolder$txtMonth: {
                required: true,
                digits: true
            },
            ctl00$ContentHolder$txtDay: {
                required: true,
                digits: true
            },
            ctl00$ContentHolder$txtYear: {
                required: true,
                digits: true
            },
            ctl00$ContentHolder$radGender: "required",
            ctl00$ContentHolder$radStudentStatus: "required",
            ctl00$ContentHolder$ddCountryBirth: "required",
            ctl00$ContentHolder$ddCountryCitizenship: "required",
            ctl00$ContentHolder$txtPermAddress: "required",
            ctl00$ContentHolder$txtPermCity: "required",
            ctl00$ContentHolder$ddPermHomeCountry: "required",
            ctl00$ContentHolder$txtPermPhone: {
                required: true
            },
            ctl00$ContentHolder$txtPermEmail: {
                required: true,
                email: true
            },

            ctl00$ContentHolder$txtParentsEmail: {
                required: false,
                email: true
            },

            ctl00$ContentHolder$txtAgencyEmail: {
                required: false,
                email: true
            },

            ctl00$ContentHolder$txtEmergEmail: {
                required: false,
                email: true
            },
            ctl00$ContentHolder$chkHealthIns: "required",

            ctl00$ContentHolder$radWhenStudy: "required",
            ctl00$ContentHolder$radWhereStudy: "required",
            ctl00$ContentHolder$ddMajor: "required",
            ctl00$ContentHolder$radPayOption: "required",
            ctl00$ContentHolder$radReleaseInformation: "required",
            ctl00$ContentHolder$chkAgree: "required",
            chkWhatStudy: {
                required: true,
                minlength: 1
            },
            chkHowPay: {
                required: true,
                minlength: 1
            },
            ctl00$ContentHolder$txtOtherFinancing: {
                'requiredIfChecked2': true
            },
            ctl00$ContentHolder$txtStudyOtherProg: {
                'requiredIfChecked': true
            }
        },

        messages: {
            ctl00$ContentHolder$txtFamilyName: "Enter your family name",
            ctl00$ContentHolder$txtGivenName: "Enter your first name (given name)",
            ctl00$ContentHolder$txtMonth: "Enter a valid Month",
            ctl00$ContentHolder$txtDay: "Enter a valid Day",
            ctl00$ContentHolder$txtYear: "Enter a valid Year",
            ctl00$ContentHolder$radGender: "Indicate your gender",
            ctl00$ContentHolder$radStudentStatus: "Indicate your student status",
            ctl00$ContentHolder$ddCountryBirth: "Select your country of birth",
            ctl00$ContentHolder$ddCountryCitizenship: "Select your country of citizenship",
            ctl00$ContentHolder$txtPermAddress: "Enter your permanent address",
            ctl00$ContentHolder$txtPermCity: "Enter your permanent city of residence",
            ctl00$ContentHolder$ddPermHomeCountry: "Select your permanent home country",
            ctl00$ContentHolder$txtPermPhone: {
                required: "Please enter your phone number"
            },
            ctl00$ContentHolder$txtPermEmail: {
                required: "Please enter your email address",
                email: "Please enter a valid email address"
            },
            ctl00$ContentHolder$txtParentsEmail: {
                email: "Please enter a valid email address or leave blank"
            },
            ctl00$ContentHolder$txtAgencyEmail: {
                email: "Please enter a valid Agency email address or leave blank"
            },
            ctl00$ContentHolder$txtEmergEmail: {
                email: "Please enter a valid emergency contact email address or leave blank"
            },
            ctl00$ContentHolder$chkHealthIns: "Please acknowledge health insurance requirement",
            ctl00$ContentHolder$radWhenStudy: "Please select the quarter you would like to begin studies",
            chkWhatStudy: "Please choose a valid option",
            ctl00$ContentHolder$radWhereStudy: "Please select where you would like to study",
            ctl00$ContentHolder$txtStudyOtherProg: "Please select your OTHER programs of study",
            ctl00$ContentHolder$ddMajor: "Please select your major",
            ctl00$ContentHolder$txtOtherFinancing: "Please select your OTHER options for living and tuition expenses",
            ctl00$ContentHolder$radPayOption: "Please select a payment option",
            ctl00$ContentHolder$radReleaseInformation: "Authorize or decline the release of information",
            ctl00$ContentHolder$chkAgree: "Indicate statements are true and you agree to the policies"
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "ctl00$ContentHolder$txtFamilyName") {
                $("#ContentHolder_eFamName").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtGivenName") {
                $("#ContentHolder_eGivenName").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtMonth") {
                $("#ContentHolder_eMM").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtDay") {
                $("#ContentHolder_eDD").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtYear") {
                $("#ContentHolder_eYYYY").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$radGender") {
                $("#ContentHolder_eGender").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$radStudentStatus") {
                $("#ContentHolder_eStatus").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$ddCountryBirth") {
                $("#ContentHolder_eCountryOfBirth").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$ddCountryCitizenship") {
                $("#ContentHolder_eCountryOfCitizenship").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtPermAddress") {
                $("#ContentHolder_ePermAddress").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtPermCity") {
                $("#ContentHolder_ePermCity").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$ddPermHomeCountry") {
                $("#ContentHolder_ePermHomeCountry").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtPermPhone") {
                $("#ContentHolder_ePermPhone").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtPermEmail") {
                $("#ContentHolder_ePermEmail").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtParentsEmail") {
                $("#ContentHolder_eParentEmail").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtAgencyEmail") {
                $("#ContentHolder_eAgencyEmail").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtEmergEmail") {
                $("#ContentHolder_eEmergEmail").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$radWhenStudy") {
                $("#ContentHolder_eWhenStudy").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtStudyOtherProg") {
                $("#ContentHolder_eWhatProg").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$radWhereStudy") {
                $("#ContentHolder_eWhereStudy").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$ddMajor") {
                $("#ContentHolder_eWhatMajor").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$radPayOption") {
                $("#ContentHolder_ePaymentOption").append(error)
            } else if (element.attr("name") == "ctl00$ContentHolder$chkHealthIns") {
                $("#ContentHolder_eHealthIns").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$txtOtherFinancing") {
                $("#ContentHolder_eHowPay").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$radReleaseInformation") {
                $("#ContentHolder_eReleaseInformation").append(error);
            } else if (element.attr("name") == "ctl00$ContentHolder$chkAgree") {
                $("#ContentHolder_eAgree").append(error);
            } else {
                element.after(error);
            }
        }
    }); //end of validate

}); // end of document ready

