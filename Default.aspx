﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h3>Heidy Ho!</h3>
        This page will offer the log in to Facebook.<br />
        If you log in you will go to the page-by-page form here.<br />
        If not, you will go to the long form <a href="InternationalStudents/Standardized/internationalApplicationForm.aspx">here</a>
    </div>
    </form>
</body>
</html>
