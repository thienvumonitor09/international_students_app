﻿if ((window.location.href.indexOf("https://") != 0) && (window.location.href.toLowerCase().indexOf("spokane.edu")>0)) {
    window.location = "https://" + window.location.hostname + window.location.pathname + window.location.search;
} // Redirect to https version of URL if a CCS production server is being used.