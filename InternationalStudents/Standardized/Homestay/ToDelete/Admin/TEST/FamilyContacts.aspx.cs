﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Homestay_Admin_TEST_UpdateFamily : System.Web.UI.Page
{
    Int32 intResult = 0;
    Int32 intFamilyID = 0;
    //family home basic information fields
    String strHomeName = "";
    String strStreetAddress = "";
    String strCity = "";
    String strState = "";
    String strZIP = "";
    String strLandLinePhone = "";
    String strInsuranceCompany = "";
    String strInsurancePolicyNumber = "";
    String strInsuranceAgentName = "";
    String strInsuranceAgentPhone = "";
    String strRoom1 = "";
    String strRoom1DateOpen = "";
    String strRoom1DateClosed = "";
    String strRoom1Bathroom = "";
    String strRoom1Occupancy = "";
    String strRoom2 = "";
    String strRoom2DateOpen = "";
    String strRoom2DateClosed = "";
    String strRoom2Bathroom = "";
    String strRoom2Occupancy = "";
    String strRoom3 = "";
    String strRoom3DateOpen = "";
    String strRoom3DateClosed = "";
    String strRoom3Bathroom = "";
    String strRoom3Occupancy = "";
    String strRoom4 = "";
    String strRoom4DateOpen = "";
    String strRoom4DateClosed = "";
    String strRoom4Bathroom = "";
    String strRoom4Occupancy = "";
    String strRelationship = "";
    String strResultMsg = "";

    //primary contact information fields
    String FamilyName = "";
    String FirstName = "";
    String MiddleName = "";
    String DateOfBirth = "";
    String Gender = "";
    String DriversLicenseNumber = "";
    String Occupation = "";
    String CellPhone = "";
    String WorkPhone = "";
    String Email = "";

    //secondary contact information fields
    String FamilyName2 = "";
    String FirstName2 = "";
    String MiddleName2 = "";
    String DateOfBirth2 = "";
    String Gender2 = "";
    String DriversLicenseNumber2 = "";
    String Occupation2 = "";
    String CellPhone2 = "";
    String WorkPhone2 = "";
    String Email2 = "";

    //other adult 1 information fields
    String FamilyName3 = "";
    String FirstName3 = "";
    String MiddleName3 = "";
    String DateOfBirth3 = "";
    String Gender3 = "";
    String DriversLicenseNumber3 = "";
    String Occupation3 = "";
    String CellPhone3 = "";
    String WorkPhone3 = "";
    String Email3 = "";

    //other adult 2 information fields
    String FamilyName4 = "";
    String FirstName4 = "";
    String MiddleName4 = "";
    String DateOfBirth4 = "";
    String Gender4 = "";
    String DriversLicenseNumber4 = "";
    String Occupation4 = "";
    String CellPhone4 = "";
    String WorkPhone4 = "";
    String Email4 = "";

    //minor child 1 information fields
    String FamilyName5 = "";
    String FirstName5 = "";
    String DateOfBirth5 = "";
    String Gender5 = "";

    //minor child 2 information fields
    String FamilyName6 = "";
    String FirstName6 = "";
    String DateOfBirth6 = "";
    String Gender6 = "";

    //minor child 3 information fields
    String FamilyName7 = "";
    String FirstName7 = "";
    String DateOfBirth7 = "";
    String Gender7 = "";

    //minor child 4 information fields
    String FamilyName8 = "";
    String FirstName8 = "";
    String DateOfBirth8 = "";
    String Gender8 = "";

    string strShortDateValue = "";
    DateTime outDate;                   // used in try/catch to validate dob on form
    Boolean dateSuccess = false;        // used in try/catch to validate dob on form
    Boolean isValidDate = false;    
    String postBackControlName = "";    // if postback the result of the drop-down, store the name

    const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
 + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
 + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
 + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
    const string MatchUSPhonePattern =
        @"/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/";
    const string MatchIntlPhonePattern =
        @"/^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}(\s*(ext|x)\s*\.?:?\s*([0-9]+))?$/";

    const string MatchDatePattern = "^(1[0-2]|0[1-9]|\\d)\\/(20\\d{2}|19\\d{2}|0(?!0)\\d|[1-9]\\d)$";

    protected void Page_Load(object sender, EventArgs e)
    {
        
        String strButtonFace = "";      
        String selectedFamily = "";

        litResultMsg.Text = "";
        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (strButtonFace == "Return to Admin Home Page")
        {
            Response.Redirect("/Homestay/Admin/Default.aspx");
        }

        selectedFamily = ddlFamilies.SelectedValue;
        //strResultMsg += "Family ID: " + selectedFamily + "<br />";
        if (selectedFamily != "")
        {
            intFamilyID = Convert.ToInt32(selectedFamily);
        }

        if (!Page.IsPostBack)
            postBackControlName = "";
        else
        {
            postBackControlName = Page.Request.Params["__EVENTTARGET"];
            Response.Write("control name: " + postBackControlName + "<br />");
        }

        Homestay hsInfo = new Homestay();

        if (intFamilyID > 0)
        {
            if (IsPostBack && postBackControlName.Trim().Contains("ddlFamilies"))
            {
                //Family chosen from drop-down so reset fields and populate ALL the fields
                ResetFields();
                //Populate fields
                populateBaseInfo();
                /************* IF POSTBACK FROM DD, THEN POPULATE ALL THE FIELDS - RESET FIELDS THEN CALL EACH FUNCTION IN A ROW **********************/
                //Relatives
                populateFamilyContacts();
            }// end is postback from drop-down list and reset and repopulated all the fields
        }

        if (!IsPostBack)
        {
            //Get families into a drop-down list; 
            DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
            if (dtFamilies != null)
            {
                if (dtFamilies.Rows.Count > 0)
                {
                    ddlFamilies.Items.Clear();
                    ListItem LI;
                    LI = new ListItem();
                    LI.Value = "0";
                    LI.Text = "-- Select One --";
                    ddlFamilies.Items.Add(LI);
                    foreach (DataRow dr in dtFamilies.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["homeName"].ToString();
                        ddlFamilies.Items.Add(LI);
                    }
                    if (selectedFamily != "") { ddlFamilies.SelectedValue = selectedFamily; }
                }
                else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtFamilies is null.<br />"; }

            litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
        }// end NOT a postback - just populate family drop-down

    }// end page load

    private void ResetFields()
    {
        homeName.Text = "";
        streetAddress.Text = "";
        city.Text = "";
        state.Text = "";
        ZIP.Text = "";
        landLinePhone.Text = "";
        insuranceCompanyName.Text = "";
        insurancePolicyNumber.Text = "";
        insuranceAgentName.Text = "";
        insuranceAgentPhone.Text = "";
        familyName.Text = "";
        firstName.Text = "";
        middleName.Text = "";
        rdoGender.SelectedValue = "";
        DOB.Text = "";
        driversLicenseNumber.Text = "";
        occupation.Text = "";
        cellPhone.Text = "";
        workPhone.Text = "";
        email.Text = "";
        hdnPrimaryContactID.Value = "";
        familyName2.Text = "";
        firstName2.Text = "";
        middleName2.Text = "";
        rdoGender2.SelectedValue = "";
        DOB2.Text = "";
        driversLicenseNumber2.Text = "";
        occupation2.Text = "";
        cellPhone2.Text = "";
        workPhone2.Text = "";
        email2.Text = "";
        hdnSecondaryContactID.Value = "";
        familyName3.Text = "";
        firstName3.Text = "";
        middleName3.Text = "";
        rdoGender3.SelectedValue = "";
        DOB3.Text = "";
        driversLicenseNumber3.Text = "";
        occupation3.Text = "";
        cellPhone3.Text = "";
        workPhone3.Text = "";
        email3.Text = "";
        hdnOtherAdult1ID.Value = "";
        familyName4.Text = "";
        firstName4.Text = "";
        middleName4.Text = "";
        rdoGender4.SelectedValue = "";
        DOB4.Text = "";
        driversLicenseNumber4.Text = "";
        occupation4.Text = "";
        cellPhone4.Text = "";
        workPhone4.Text = "";
        email4.Text = "";
        hdnOtherAdult2ID.Value = "";
        familyName5.Text = "";
        firstName5.Text = "";
        DOB5.Text = "";
        rdoGender5.Text = "";
        hdnMinorChild1ID.Value = "";
        familyName6.Text = "";
        firstName6.Text = "";
        DOB6.Text = "";
        rdoGender6.Text = "";
        hdnMinorChild2ID.Value = "";
        familyName7.Text = "";
        firstName7.Text = "";
        DOB7.Text = "";
        rdoGender7.Text = "";
        hdnMinorChild3ID.Value = "";
        familyName8.Text = "";
        firstName8.Text = "";
        DOB8.Text = "";
        rdoGender8.Text = "";
        hdnMinorChild4ID.Value = "";
    }
    protected void btnCmdUpdateFamilyHome_Click(object sender, EventArgs e)
    {
        if (streetAddress.Text.Trim() == "")
        {
            eStreetAddress.InnerHtml = "Street Address is required.";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            eStreetAddress.Focus();
            return;
        }
        else
            eStreetAddress.InnerHtml = "";
        if (city.Text.Trim() == "")
        {
            eCity.InnerHtml = "City is required.";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            city.Focus();
            return;
        }
        else
            eCity.InnerHtml = "";
        if (state.Text.Trim() == "")
        {
            eState.InnerHtml = "State is required.";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            state.Focus();
            return;
        }
        else
            eState.InnerHtml = "";
        if (ZIP.Text.Trim() == "")
        {
            eZip.InnerHtml = "Zip is required.";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            ZIP.Focus();
            return;
        }
        else
            eZip.InnerHtml = "";
        if (landLinePhone.Text.Trim() == "")
        {
            ePhone.InnerHtml = "Contact phone is required.";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            landLinePhone.Focus();
            return;
        }
        else
            ePhone.InnerHtml = "";
        if (rdoRoom1.SelectedIndex == -1)
        {
            eRoom1Level.InnerHtml = "Room 1 level is required.";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            rdoRoom1.Focus();
            return;
        }
        else
            eRoom1Level.InnerHtml = "";
        if (room1DateOpen.Text.Trim() == "")
        {
            eRoom1Date.InnerHtml = "Room 1 availability date is required.";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            room1DateOpen.Focus();
            return;
        }
        else
            eRoom1Date.InnerHtml = "";
        // see if valid date entered
        isValidDate = false;
        isValidDate = checkDate(room1DateOpen);
        if (!isValidDate)
        {
            eRoom1Date.InnerHtml = "Please enter a valid date for the availablility of Room 1";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            room1DateOpen.Focus();
            return;
        }
        else
            eRoom1Date.InnerHtml = "";

        if (room1DateClosed.Text.Trim() != "")
        {
            isValidDate = checkDate(room1DateClosed);
            if (!isValidDate)
            {
                eRoom1Date.InnerHtml = "Please enter a valid date for the 'To' field from Room 1.";
                litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
                room1DateClosed.Focus();
                return;
            }
            else
                eRoom1Date.InnerHtml = "";
        }// entry in room 1 Date Closed

        if (rdoRoom1Bathroom.SelectedIndex == -1)
        {
            eRoom1Bathroom.InnerHtml = "Please selecte Room 1 bathing facilities.";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            rdoRoom1Bathroom.Focus();
            return;
        }
        else
            eRoom1Bathroom.InnerHtml = "";
        if (rdoRoom1Occupancy.SelectedIndex == -1)
        {
            eRoom1Occupancy.InnerHtml = "Please select Room 1 occupancy.";
            litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
            rdoRoom1Occupancy.Focus();
            return;
        }
        else
            eRoom1Occupancy.InnerHtml = "";

        // Determine validity of other date fields.  Although not required, if a value is there, it must resolve to a valid date.
        // If it is blank, it will be assigned the value '01-01-1900' so the sql will execute on the record insert/update.  
        // This will be done when the text box values are assigned to the variables
        if (room2DateOpen.Text.Trim() != "")
        {
            isValidDate = checkDate(room2DateOpen);
            if (!isValidDate)
            {
                eRoom2Date.InnerHtml = "Please enter a valid date for the availability field from Room 2.";
                litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
                room2DateOpen.Focus();
                return;
            }
            else
                eRoom2Date.InnerHtml = "";
        }// entry in room 2 Date Open
        if (room2DateClosed.Text.Trim() != "")
        {
            isValidDate = checkDate(room2DateClosed);
            if (!isValidDate)
            {
                eRoom2Date.InnerHtml = "Please enter a valid date for the 'To' field from Room 2.";
                litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
                room2DateClosed.Focus();
                return;
            }
            else
                eRoom2Date.InnerHtml = "";
        }// entry in room 2 Date Closed
        if (room3DateOpen.Text.Trim() != "")
        {
            isValidDate = checkDate(room3DateOpen);
            if (!isValidDate)
            {
                eRoom3Date.InnerHtml = "Please enter a valid date for the availability field from Room 3.";
                litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
                room3DateOpen.Focus();
                return;
            }
            else
                eRoom3Date.InnerHtml = "";
        }// entry in room 3 Date Open
        if (room3DateClosed.Text.Trim() != "")
        {
            isValidDate = checkDate(room3DateClosed);
            if (!isValidDate)
            {
                eRoom3Date.InnerHtml = "Please enter a valid date for the 'To' field from Room 3.";
                litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
                room3DateClosed.Focus();
                return;
            }
            else
                eRoom3Date.InnerHtml = "";
        }// entry in room 3 Date Closed
        if (room4DateOpen.Text.Trim() != "")
        {
            isValidDate = checkDate(room4DateOpen);
            if (!isValidDate)
            {
                eRoom4Date.InnerHtml = "Please enter a valid date for the availability field from Room 4.";
                litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
                room4DateOpen.Focus();
                return;
            }
            else
                eRoom4Date.InnerHtml = "";
        }// entry in room 4 Date Open
        if (room4DateClosed.Text.Trim() != "")
        {
            isValidDate = checkDate(room4DateClosed);
            if (!isValidDate)
            {
                eRoom4Date.InnerHtml = "Please enter a valid date for the 'To' field from Room 4.";
                litResultMsg.Text = "<span class='divError'>Error with updating the basic home information - please review.</span>";
                room4DateClosed.Focus();
                return;
            }
            else
                eRoom4Date.InnerHtml = "";
        }// entry in room 4 Date Closed

        //Update the record with the new information
        Homestay hsInfo = new Homestay();

        strHomeName = homeName.Text.Replace("'", "''");
        strStreetAddress = streetAddress.Text.Replace("'", "''");
        strCity = city.Text.Replace("'", "''");
        strState = state.Text;
        strZIP = ZIP.Text;
        strLandLinePhone = landLinePhone.Text;
        strInsuranceCompany = insuranceCompanyName.Text.Replace("'", "''");
        strInsurancePolicyNumber = insurancePolicyNumber.Text.Replace("'", "''");
        strInsuranceAgentName = insuranceAgentName.Text.Replace("'", "''");
        strInsuranceAgentPhone = insuranceAgentPhone.Text.Replace("'", "''");
        strRoom1 = rdoRoom1.SelectedValue;
        strRoom1DateOpen = room1DateOpen.Text.Replace("'", "''");
        strRoom1DateClosed = room1DateClosed.Text.Replace("'", "''");
        strRoom1Bathroom = rdoRoom1Bathroom.SelectedValue;
        strRoom1Occupancy = rdoRoom1Occupancy.Text.Replace("'", "''");
        strRoom2 = rdoRoom2.SelectedValue;
        strRoom2DateOpen = room2DateOpen.Text.Replace("'", "''");
        strRoom2DateClosed = room2DateClosed.Text.Replace("'", "''");
        strRoom2Bathroom = rdoRoom2Bathroom.SelectedValue;
        strRoom2Occupancy = rdoRoom2Occupancy.Text.Replace("'", "''");
        strRoom3 = rdoRoom3.SelectedValue;
        strRoom3DateOpen = room3DateOpen.Text.Replace("'", "''");
        strRoom3DateClosed = room3DateClosed.Text.Replace("'", "''");
        strRoom3Bathroom = rdoRoom3Bathroom.SelectedValue;
        strRoom3Occupancy = rdoRoom3Occupancy.Text.Replace("'", "''");
        strRoom4 = rdoRoom4.SelectedValue;
        strRoom4DateOpen = room4DateOpen.Text.Replace("'", "''");
        strRoom4DateClosed = room4DateClosed.Text.Replace("'", "''");
        strRoom4Bathroom = rdoRoom4Bathroom.SelectedValue;
        strRoom4Occupancy = rdoRoom4Occupancy.Text.Replace("'", "''");
        //Call update function
        Response.Write("id: " + intFamilyID.ToString() + "; address: " + strStreetAddress + "; date4Close: " + strRoom4DateClosed + "; agent Name: " + strInsuranceAgentName + "<br />");
        intResult = hsInfo.UpdateHomestayFamilyHome(intFamilyID, strHomeName, strStreetAddress, strCity, strState, strZIP, strLandLinePhone,
            strInsuranceCompany, strInsurancePolicyNumber, strInsuranceAgentName, strInsuranceAgentPhone,
            strRoom1, strRoom1DateOpen, strRoom1DateClosed, strRoom1Bathroom, strRoom1Occupancy,
            strRoom2, strRoom2DateOpen, strRoom2DateClosed, strRoom2Bathroom, strRoom2Occupancy,
            strRoom3, strRoom3DateOpen, strRoom3DateClosed, strRoom3Bathroom, strRoom3Occupancy,
            strRoom4, strRoom4DateOpen, strRoom4DateClosed, strRoom4Bathroom, strRoom4Occupancy);

        //re-populate base info from new data
        populateBaseInfo();
    }// end of btnCmdUpdateFamily

    Boolean checkDate(TextBox txtBox)
    {
        dateSuccess = DateTime.TryParse(txtBox.Text.Trim(), out outDate);
        if (dateSuccess)
            txtBox.Text = outDate.ToShortDateString();
        else
            txtBox.Focus();
        return dateSuccess;
    }

    protected void populateBaseInfo()
    {
        Homestay hsInfo = new Homestay();
        //Base info
        DataTable dtFamily = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
        if (dtFamily != null)
        {
            if (dtFamily.Rows.Count > 0)
            {
                homeName.Text = dtFamily.Rows[0]["homeName"].ToString();
                streetAddress.Text = dtFamily.Rows[0]["streetAddress"].ToString();
                city.Text = dtFamily.Rows[0]["city"].ToString();
                state.Text = dtFamily.Rows[0]["state"].ToString();
                ZIP.Text = dtFamily.Rows[0]["ZIP"].ToString();
                landLinePhone.Text = dtFamily.Rows[0]["landlinePhone"].ToString();
                insuranceCompanyName.Text = dtFamily.Rows[0]["insuranceCompanyName"].ToString();
                insurancePolicyNumber.Text = dtFamily.Rows[0]["insurancePolicyNumber"].ToString();
                insuranceAgentName.Text = dtFamily.Rows[0]["insuranceAgentName"].ToString();
                insuranceAgentPhone.Text = dtFamily.Rows[0]["insuranceAgentPhone"].ToString();
                rdoRoom1.SelectedValue = dtFamily.Rows[0]["room1Level"].ToString();
                strShortDateValue = Convert.ToDateTime(dtFamily.Rows[0]["room1StartDate"]).ToShortDateString();
                if (strShortDateValue == "1/1/1900")
                    room1DateOpen.Text = "";
                else
                    room1DateOpen.Text = strShortDateValue;
                strShortDateValue = Convert.ToDateTime(dtFamily.Rows[0]["room1EndDate"]).ToShortDateString();
                if (strShortDateValue == "1/1/1900")
                    room1DateClosed.Text = "";
                else
                    room1DateClosed.Text = strShortDateValue;
                rdoRoom1Bathroom.SelectedValue = dtFamily.Rows[0]["room1Bath"].ToString();
                rdoRoom1Occupancy.Text = dtFamily.Rows[0]["room1Occupancy"].ToString();
                rdoRoom2.SelectedValue = dtFamily.Rows[0]["room2Level"].ToString();
                strShortDateValue = Convert.ToDateTime(dtFamily.Rows[0]["room2StartDate"]).ToShortDateString();
                if (strShortDateValue == "1/1/1900")
                    room2DateOpen.Text = "";
                else
                    room2DateOpen.Text = strShortDateValue;
                strShortDateValue = Convert.ToDateTime(dtFamily.Rows[0]["room2EndDate"]).ToShortDateString();
                if (strShortDateValue == "1/1/1900")
                    room2DateClosed.Text = "";
                else
                    room2DateClosed.Text = strShortDateValue;
                rdoRoom2Bathroom.SelectedValue = dtFamily.Rows[0]["room2Bath"].ToString();
                rdoRoom2Occupancy.Text = dtFamily.Rows[0]["room2Occupancy"].ToString();
                rdoRoom3.SelectedValue = dtFamily.Rows[0]["room3Level"].ToString();
                strShortDateValue = Convert.ToDateTime(dtFamily.Rows[0]["room3StartDate"]).ToShortDateString();
                if (strShortDateValue == "1/1/1900")
                    room3DateOpen.Text = "";
                else
                    room3DateOpen.Text = strShortDateValue;
                strShortDateValue = Convert.ToDateTime(dtFamily.Rows[0]["room3EndDate"]).ToShortDateString();
                if (strShortDateValue == "1/1/1900")
                    room3DateClosed.Text = "";
                else
                    room3DateClosed.Text = strShortDateValue;
                rdoRoom3Bathroom.SelectedValue = dtFamily.Rows[0]["room3Bath"].ToString();
                rdoRoom3Occupancy.Text = dtFamily.Rows[0]["room3Occupancy"].ToString();
                rdoRoom4.SelectedValue = dtFamily.Rows[0]["room4Level"].ToString();
                strShortDateValue = Convert.ToDateTime(dtFamily.Rows[0]["room4StartDate"]).ToShortDateString();
                if (strShortDateValue == "1/1/1900")
                    room4DateOpen.Text = "";
                else
                    room4DateOpen.Text = strShortDateValue;
                strShortDateValue = Convert.ToDateTime(dtFamily.Rows[0]["room4EndDate"]).ToShortDateString();
                if (strShortDateValue == "1/1/1900")
                    room4DateClosed.Text = "";
                else
                    room4DateClosed.Text = strShortDateValue;
                rdoRoom4Bathroom.SelectedValue = dtFamily.Rows[0]["room4Bath"].ToString();
                rdoRoom4Occupancy.Text = dtFamily.Rows[0]["room4Occupancy"].ToString();
            }
        }
    }// end populate baseInfo

    protected void populateFamilyContacts()
    {
        Homestay hsInfo = new Homestay();
        DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
        if (dtRelatives != null)
        {
            if (dtRelatives.Rows.Count > 0)
            {
                foreach (DataRow dtRow in dtRelatives.Rows)
                {
                    strRelationship = dtRow["relationship"].ToString().Trim();
                    //strResultMsg += "Relationship: " + strRelationship + "<br />";
                    switch (strRelationship)
                    {
                        case "Primary Contact":
                            familyName.Text = dtRow["familyName"].ToString().Trim();
                            firstName.Text = dtRow["firstName"].ToString().Trim();
                            middleName.Text = dtRow["middleName"].ToString().Trim();
                            rdoGender.SelectedValue = dtRow["gender"].ToString().Trim();
                            DOB.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                            driversLicenseNumber.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                            occupation.Text = dtRow["occupation"].ToString().Trim();
                            cellPhone.Text = dtRow["cellPhone"].ToString().Trim();
                            workPhone.Text = dtRow["workPhone"].ToString().Trim();
                            email.Text = dtRow["email"].ToString().Trim();
                            hdnPrimaryContactID.Value = dtRow["id"].ToString();
                            break;
                        case "Secondary Contact":
                            familyName2.Text = dtRow["familyName"].ToString().Trim();
                            firstName2.Text = dtRow["firstName"].ToString().Trim();
                            middleName2.Text = dtRow["middleName"].ToString().Trim();
                            rdoGender2.SelectedValue = dtRow["gender"].ToString().Trim();
                            DOB2.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                            driversLicenseNumber2.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                            occupation2.Text = dtRow["occupation"].ToString().Trim();
                            cellPhone2.Text = dtRow["cellPhone"].ToString().Trim();
                            workPhone2.Text = dtRow["workPhone"].ToString().Trim();
                            email2.Text = dtRow["email"].ToString().Trim();
                            hdnSecondaryContactID.Value = dtRow["id"].ToString();
                            break;
                        case "Other Adult 1":
                            familyName3.Text = dtRow["familyName"].ToString().Trim();
                            firstName3.Text = dtRow["firstName"].ToString().Trim();
                            middleName3.Text = dtRow["middleName"].ToString().Trim();
                            rdoGender3.SelectedValue = dtRow["gender"].ToString().Trim();
                            DOB3.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                            driversLicenseNumber3.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                            occupation3.Text = dtRow["occupation"].ToString().Trim();
                            cellPhone3.Text = dtRow["cellPhone"].ToString().Trim();
                            workPhone3.Text = dtRow["workPhone"].ToString().Trim();
                            email3.Text = dtRow["email"].ToString().Trim();
                            hdnOtherAdult1ID.Value = dtRow["id"].ToString();
                            break;
                        case "Other Adult 2":
                            familyName4.Text = dtRow["familyName"].ToString().Trim();
                            firstName4.Text = dtRow["firstName"].ToString().Trim();
                            middleName4.Text = dtRow["middleName"].ToString().Trim();
                            rdoGender4.SelectedValue = dtRow["gender"].ToString().Trim();
                            DOB4.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                            driversLicenseNumber4.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                            occupation4.Text = dtRow["occupation"].ToString().Trim();
                            cellPhone4.Text = dtRow["cellPhone"].ToString().Trim();
                            workPhone4.Text = dtRow["workPhone"].ToString().Trim();
                            email4.Text = dtRow["email"].ToString().Trim();
                            hdnOtherAdult2ID.Value = dtRow["id"].ToString();
                            break;
                        case "Minor Child 1":
                            familyName5.Text = dtRow["familyName"].ToString().Trim();
                            firstName5.Text = dtRow["firstName"].ToString().Trim();
                            DOB5.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                            rdoGender5.Text = dtRow["gender"].ToString().Trim();
                            hdnMinorChild1ID.Value = dtRow["id"].ToString();
                            break;
                        case "Minor Child 2":
                            familyName6.Text = dtRow["familyName"].ToString().Trim();
                            firstName6.Text = dtRow["firstName"].ToString().Trim();
                            DOB6.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                            rdoGender6.Text = dtRow["gender"].ToString().Trim();
                            hdnMinorChild2ID.Value = dtRow["id"].ToString();
                            break;
                        case "Minor Child 3":
                            familyName7.Text = dtRow["familyName"].ToString().Trim();
                            firstName7.Text = dtRow["firstName"].ToString().Trim();
                            DOB7.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                            rdoGender7.Text = dtRow["gender"].ToString().Trim();
                            hdnMinorChild3ID.Value = dtRow["id"].ToString();
                            break;
                        case "Minor Child 4":
                            familyName8.Text = dtRow["familyName"].ToString().Trim();
                            firstName8.Text = dtRow["firstName"].ToString().Trim();
                            DOB8.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                            rdoGender8.Text = dtRow["gender"].ToString().Trim();
                            hdnMinorChild4ID.Value = dtRow["id"].ToString();
                            break;
                    }
                }
            }
            else { strResultMsg += "No HomestayRelatives records returned.<br />"; }
        }
        else { strResultMsg += "No HomestayRelatives records returned.<br />"; }
    }
    protected void btnCmdUpdatePrimaryContact_Click(object sender, EventArgs e)
    {
        // see if valid date entered
        isValidDate = false;
        isValidDate = checkDate(DOB);
        if (!isValidDate)
        {
            ePrimaryDOB.InnerHtml = "Please enter a valid date of birth";
            DOB.Focus();
            litResultMsg.Text = "<span class='divError'>Error with updating the primary contact information - please review.</span>";
            return;
        }
        else
            ePrimaryDOB.InnerHtml = "";
        if (email.Text.Trim() != "")
        {
            if (!Regex.IsMatch(email.Text.Trim(), MatchEmailPattern))
            {
                eEmailPrimary.InnerHtml = "<b>Please enter a valid email address</b>";
                email.Focus();
                litResultMsg.Text = "<span class='divError'>Error with updating the primary contact information - please review.</span>";
                return;
            }
            else
                eEmailPrimary.InnerHtml = "";
        }// end email field not blank
        
        Homestay hsInfo = new Homestay();
        //Insert HomestayRelatives table
        FamilyName = familyName.Text.Replace("'", "''");
        FirstName = firstName.Text.Replace("'", "''");
        MiddleName = middleName.Text.Replace("'", "''");
        DateOfBirth = DOB.Text;
        Gender = rdoGender.SelectedValue;
        DriversLicenseNumber = driversLicenseNumber.Text.Replace("'", "''");
        Occupation = occupation.Text.Replace("'", "''");
        CellPhone = cellPhone.Text;
        WorkPhone = workPhone.Text;
        Email = email.Text;

        //Primary Contact
        if (hdnPrimaryContactID.Value != null && hdnPrimaryContactID.Value != "")
        {
            Int32 PrimaryID = Convert.ToInt32(hdnPrimaryContactID.Value);
            intResult = hsInfo.UpdateHomestayRelatives(PrimaryID, FamilyName, FirstName, Occupation, DateOfBirth, Gender, "Primary Contact", Email, CellPhone, WorkPhone, DriversLicenseNumber, MiddleName);
        }
        // repopulate the fields with the new data
        populateFamilyContacts();
    }// end update primary contact

    protected void btnCmdUpdateSecondaryContact_Click(object sender, EventArgs e)
    {
        // Do validation
        // see if valid date entered
        isValidDate = false;
        isValidDate = checkDate(DOB2);
        if (!isValidDate)
        {
            eDOB2.InnerHtml = "Please enter a valid date of birth";
            DOB2.Focus();
            litResultMsg.Text = "<span class='divError'>Error with updating the secondary contact information - please review.</span>";
            return;
        }
        else
            eDOB2.InnerHtml = "";
        if (email2.Text.Trim() != "")
        {
            if (!Regex.IsMatch(email2.Text.Trim(), MatchEmailPattern))
            {
                eEmail2.InnerHtml = "<b>Please enter a valid email address</b>";
                email2.Focus();
                litResultMsg.Text = "<span class='divError'>Error with updating the secondary contact information - please review.</span>";
                return;
            }
            else
                eEmail2.InnerHtml = "";
        }// end email field not blank

        // update
        Homestay hsInfo = new Homestay();

        FamilyName2 = familyName2.Text.Replace("'", "''");
        FirstName2 = firstName2.Text.Replace("'", "''");
        if (FamilyName2 != "" && FirstName2 != "")
        {
            MiddleName2 = middleName2.Text.Replace("'", "''");
            DateOfBirth2 = DOB2.Text.Replace("'", "''");
            Gender2 = rdoGender2.SelectedValue;
            DriversLicenseNumber2 = driversLicenseNumber2.Text.Replace("'", "''");
            Occupation2 = occupation2.Text.Replace("'", "''");
            CellPhone2 = cellPhone2.Text;
            WorkPhone2 = workPhone2.Text;
            Email2 = email2.Text;
            //Secondary Contact
            if (hdnSecondaryContactID.Value != null && hdnSecondaryContactID.Value != "")
            {
                Int32 SecondaryID = Convert.ToInt32(hdnSecondaryContactID.Value);
                intResult = hsInfo.UpdateHomestayRelatives(SecondaryID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
            }
            else if (FamilyName2 != "" && FirstName2 != "")
            {
                //Insert new relative
                intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
            }
        }// end family and first names2 are not blank
        populateFamilyContacts();
    }// end update secondary contact

    protected void btnCmdUpdateOtherAdult1_Click(object sender, EventArgs e)
    {
        // do validation
        // see if valid date entered
        isValidDate = false;
        isValidDate = checkDate(DOB3);
        if (!isValidDate)
        {
            eDOB3.InnerHtml = "Please enter a valid date of birth";
            DOB3.Focus();
            litResultMsg.Text = "<span class='divError'>Error with updating the 'other adult 1' contact information - please review.</span>";
            return;
        }
        else
            eDOB3.InnerHtml = "";
        if (email3.Text.Trim() != "")
        {
            if (!Regex.IsMatch(email3.Text.Trim(), MatchEmailPattern))
            {
                eEmail3.InnerHtml = "<b>Please enter a valid email address</b>";
                email3.Focus();
                litResultMsg.Text = "<span class='divError'>Error with updating the 'other adult 1' contact information - please review.</span>";
                return;
            }
            else
                eEmail3.InnerHtml = "";
        }// end email field not blank

        // update 
        Homestay hsInfo = new Homestay();

        FamilyName3 = familyName3.Text.Replace("'", "''");
        FirstName3 = firstName3.Text.Replace("'", "''");
        if (FamilyName3 != "" && FirstName3 != "")
        {
            MiddleName3 = middleName3.Text.Replace("'", "''");
            DateOfBirth3 = DOB3.Text.Replace("'", "''");
            Gender3 = rdoGender3.SelectedValue;
            DriversLicenseNumber3 = driversLicenseNumber3.Text.Replace("'", "''");
            Occupation3 = occupation3.Text.Replace("'", "''");
            CellPhone3 = cellPhone3.Text;
            WorkPhone3 = workPhone3.Text;
            Email3 = email3.Text;
            //Adult 1
            if (hdnOtherAdult1ID.Value != null && hdnOtherAdult1ID.Value != "")
            {
                Int32 Adult1ID = Convert.ToInt32(hdnOtherAdult1ID.Value);
                intResult = hsInfo.UpdateHomestayRelatives(Adult1ID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
            }
            else if (FamilyName3 != "" && FirstName3 != "")
            {
                //Insert new relative
                intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
            }
        }//  other adult 1 first and family name NOT blank
        populateFamilyContacts();
    }// end update 'other adult 1' contact information


    protected void btnCmdUpdateOtherAdult2_Click(object sender, EventArgs e)
    {
        // do validation
        // see if valid date entered
        isValidDate = false;
        isValidDate = checkDate(DOB4);
        if (!isValidDate)
        {
            eDOB4.InnerHtml = "Please enter a valid date of birth";
            DOB4.Focus();
            litResultMsg.Text = "<span class='divError'>Error with updating the 'other adult 1' contact information - please review.</span>";
            return;
        }
        else
            eDOB4.InnerHtml = "";
        if (email4.Text.Trim() != "")
        {
            if (!Regex.IsMatch(email4.Text.Trim(), MatchEmailPattern))
            {
                eEmail4.InnerHtml = "<b>Please enter a valid email address</b>";
                email4.Focus();
                litResultMsg.Text = "<span class='divError'>Error with updating the 'other adult 1' contact information - please review.</span>";
                return;
            }
            else
                eEmail4.InnerHtml = "";
        }// end email field not blank

        // update other adult 2
        Homestay hsInfo = new Homestay();
        FamilyName4 = familyName4.Text;
        FirstName4 = firstName4.Text;
        if (FamilyName4 != "" && FirstName4 != "")
        {
            MiddleName4 = middleName4.Text;
            DateOfBirth4 = DOB4.Text;
            Gender4 = rdoGender4.SelectedValue;
            DriversLicenseNumber4 = driversLicenseNumber4.Text;
            Occupation4 = occupation4.Text;
            CellPhone4 = cellPhone4.Text;
            WorkPhone4 = workPhone4.Text;
            Email4 = email4.Text;
            //Call update function
            //Adult 2
            if (hdnOtherAdult2ID.Value != null && hdnOtherAdult2ID.Value != "")
            {
                Int32 Adult2ID = Convert.ToInt32(hdnOtherAdult2ID.Value);
                intResult = hsInfo.UpdateHomestayRelatives(Adult2ID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
            }
            else if (FamilyName4 != "" && FirstName4 != "")
            {
                //Insert new relative
                intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
            }
        }// names NOT blank
        populateFamilyContacts();
    }// end update 'other adult 2'

    protected void btnCmdUpdateMinorChild1_Click(object sender, EventArgs e)
    {
        // validate DOB
        // see if valid date entered
        isValidDate = false;
        isValidDate = checkDate(DOB5);
        if (!isValidDate)
        {
            eDOB5.InnerHtml = "Please enter a valid date of birth";
            DOB5.Focus();
            litResultMsg.Text = "<span class='divError'>Error with updating the 'minor child 1' information - please review.</span>";
            return;
        }
        else
            eDOB5.InnerHtml = "";
        //Update
        Homestay hsInfo = new Homestay();
        FamilyName5 = familyName5.Text.Replace("'", "''");
        FirstName5 = firstName5.Text.Replace("'", "''");
        if (FamilyName5 != "" && FirstName5 != "")
        {
            DateOfBirth5 = DOB5.Text;
            Gender5 = rdoGender5.SelectedValue;
            //Call update function
            //Minor Child 1
            if (hdnMinorChild1ID.Value != null && hdnMinorChild1ID.Value != "")
            {
                Int32 Child1ID = Convert.ToInt32(hdnMinorChild1ID.Value);
                intResult = hsInfo.UpdateHomestayRelatives(Child1ID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
            }
            else if (FamilyName5 != "" && FirstName5 != "")
            {
                //Insert new relative
                intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
            }
        }
        //re-populate
        populateFamilyContacts();
    }
    protected void btnCmdUpdateMinorChild2_Click(object sender, EventArgs e)
    {
        // do validaiton
        // see if valid date entered
        isValidDate = false;
        isValidDate = checkDate(DOB6);
        if (!isValidDate)
        {
            eDOB6.InnerHtml = "Please enter a valid date of birth";
            DOB6.Focus();
            litResultMsg.Text = "<span class='divError'>Error with updating the 'minor child 2' information - please review.</span>";
            return;
        }
        else
            eDOB6.InnerHtml = "";
        //update
        Homestay hsInfo = new Homestay();
        FamilyName6 = familyName6.Text.Replace("'", "''");
        FirstName6 = firstName6.Text.Replace("'", "''");
        if (FamilyName6 != "" && FirstName6 != "")
        {
            DateOfBirth6 = DOB6.Text;
            Gender6 = rdoGender6.SelectedValue;
            //Call update function
            //Minor Child 2
            if (hdnMinorChild2ID.Value != null && hdnMinorChild2ID.Value != "")
            {
                Int32 Child2ID = Convert.ToInt32(hdnMinorChild2ID.Value);
                intResult = hsInfo.UpdateHomestayRelatives(Child2ID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
            }
            else if (FamilyName6 != "" && FirstName6 != "")
            {
                //Insert new relative
                intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
            }
        }
        //re-populate
        populateFamilyContacts();
    }
    protected void btnCmdUpdateMinorChild3_Click(object sender, EventArgs e)
    {
        // do validation
        isValidDate = false;
        isValidDate = checkDate(DOB7);
        if (!isValidDate)
        {
            eDOB7.InnerHtml = "Please enter a valid date of birth";
            DOB7.Focus();
            litResultMsg.Text = "<span class='divError'>Error with updating the 'minor child 3' information - please review.</span>";
            return;
        }
        else
            eDOB7.InnerHtml = "";
        //update
        Homestay hsInfo = new Homestay();
        FamilyName7 = familyName7.Text.Replace("'", "''");
        FirstName7 = firstName7.Text.Replace("'", "''");
        if (FamilyName7 != "" && FirstName7 != "")
        {
            DateOfBirth7 = DOB7.Text;
            Gender7 = rdoGender7.SelectedValue;
            //Call update function
            //Minor Child 3
            if (hdnMinorChild3ID.Value != null && hdnMinorChild3ID.Value != "")
            {
                Int32 Child3ID = Convert.ToInt32(hdnMinorChild3ID.Value);
                intResult = hsInfo.UpdateHomestayRelatives(Child3ID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
            }
            else if (FamilyName7 != "" && FirstName7 != "")
            {
                //Insert new relative
                intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
            }
        }
        //re-populate
        populateFamilyContacts();
    }// End update minor child 3
    protected void btnCmdUpdateMinorChild4_Click(object sender, EventArgs e)
    {
        //validate
        isValidDate = false;
        isValidDate = checkDate(DOB8);
        if (!isValidDate)
        {
            eDOB8.InnerHtml = "Please enter a valid date of birth";
            DOB8.Focus();
            litResultMsg.Text = "<span class='divError'>Error with updating the 'minor child 4' information - please review.</span>";
            return;
        }
        else
            eDOB8.InnerHtml = "";
        //update
        Homestay hsInfo = new Homestay();
        FamilyName8 = familyName8.Text.Replace("'", "''");
        FirstName8 = firstName8.Text.Replace("'", "''");
        if (FamilyName8 != "" && FirstName8 != "")
        {
            DateOfBirth8 = DOB8.Text;
            Gender8 = rdoGender8.SelectedValue;
            //Call update function
            //Minor Child 4
            if (hdnMinorChild4ID.Value != null && hdnMinorChild4ID.Value != "")
            {
                Int32 Child4ID = Convert.ToInt32(hdnMinorChild4ID.Value);
                intResult = hsInfo.UpdateHomestayRelatives(Child4ID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
            }
            else if (FamilyName8 != "" && FirstName8 != "")
            {
                //Insert new relative
                intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
            }
        }
        //re-populate
        populateFamilyContacts();
    }
}
