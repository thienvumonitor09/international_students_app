﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="Notes.aspx.cs" Inherits="Homestay_Admin_Notes" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<style type="text/css">
    p
    {
        padding:2px;
        margin:0px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">

<h2>Notes and Issues</h2>
<div id="divHome" runat="server" style="float:right;margin-right:30px;">
    <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;  
</div>
<div id="divHomeGrid" runat="server" style="float:right;margin-right:30px;">
     <asp:Button ID="btnCmdReturn" runat="server" Text="Return to Admin Home Page" OnClick="btnCmdReturn_Click" />&nbsp;&nbsp;
</div>
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnSearchType" runat="server" />
<asp:HiddenField ID="hdnKeepSearch" runat="server" />
<div style="float:left;width:30%;margin-bottom:10px;">
    <asp:RadioButtonList ID="rdoNoteView" AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
        <asp:ListItem Value="Add/Edit Notes" Selected="True"></asp:ListItem>
        <asp:ListItem Value="Grid View"></asp:ListItem>
    </asp:RadioButtonList>
</div>
<div id="divActivateButtons" runat="server" style="float:left;width:30%;margin-bottom:10px;">
    <input type="submit" name="btnSubmit" value="Update Status" />&nbsp;&nbsp;<input type="submit" name="btnSubmit" value="Activate/De-activate" />
</div>
<p style="clear:both;">&nbsp;</p>

<div id="divNote" runat="server">
    <fieldset id="fldNewNote" runat="server">
    <legend><asp:Literal ID="litFunction" runat="server"></asp:Literal></legend>
        <p>Select a Note</p>
        <asp:DropDownList ID="ddlNotes" runat="server" AutoPostBack="true"></asp:DropDownList>
        <p>&nbsp;</p>
        <div style="float:left;width:45%;margin-bottom:10px;">
            <label for="noteDate">Subject</label><br />
            <asp:TextBox ID="txtSubject" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvSubject" ControlToValidate="txtSubject" runat="server" ErrorMessage="A subject is required."></asp:RequiredFieldValidator>
        </div>
        <div style="float:left;width:45%;margin-bottom:10px;">
            <label for="noteType">Note Type</label><br />
            <asp:DropDownList ID="ddlNoteType" runat="server">
                <asp:ListItem Value="0" Text="-- Select One --"></asp:ListItem>
                <asp:ListItem Value="Matching" Text="Matching"></asp:ListItem>
                <asp:ListItem Value="Issue" Text="Issue"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <p style="clear:both;"><label for="Note">Concerning:</label></p>
        <div style="float:left;width:30%;margin-bottom:10px;">
            <p>A Student</p>
            <asp:DropDownList ID="ddlStudents" runat="server"></asp:DropDownList>
        </div>
        <div style="float:left;width:30%;margin-bottom:10px;">
            <p>A Family</p>
            <asp:DropDownList ID="ddlFamilies" runat="server"></asp:DropDownList>
        </div>
        <div style="float:left;width:30%;margin-bottom:10px;">
            <p>A Placement</p>
            <asp:DropDownList ID="ddlPlacements" runat="server"></asp:DropDownList>
        </div>
        <p style="clear:both;">
            <label for="Note">Note</label><br />
            <asp:TextBox ID="Note" runat="server" MaxLength="500" Rows="5" Columns="60" TextMode="MultiLine"></asp:TextBox>
        </p>
        <div style="float:left;width:30%;margin-bottom:10px;">
            <p>
                <label for="rdoActionRequired">Action Required?</label><br />
                <asp:RadioButtonList ID="rdoActionRequired" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </p>
        </div>
        <div id="divActionCompleted" style="float:left;width:30%;margin-bottom:10px;" runat="server">
            <p>
                <label for="rdoActionCompleted">Action Completed?</label><br />
                <asp:RadioButtonList ID="rdoActionCompleted" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </p>
        </div>

        <div style="clear:both;float:left;width:30%;margin-bottom:10px;">
        <p>
            <label for="noteDate">Date Created</label><br />
            <asp:TextBox ID="noteDate" runat="server" Width="100px" MaxLength="20"></asp:TextBox>
        </p>
        </div>
        <div id="divEditDate" style="float:left;width:30%;margin-bottom:10px;" runat="server">
            <p>
                <label for="noteDate">Date Last Edited</label><br />
                <asp:TextBox ID="editDate" runat="server" Width="100px" MaxLength="20"></asp:TextBox>
            </p>
        </div>
        <p style="clear:both;">
            <label for="StaffUserName">Created By</label><br />
            <asp:TextBox ID="StaffUserName" runat="server" Width="250px" MaxLength="20"></asp:TextBox>
        </p>
        <div id="divSave" runat="server">
            <p>
                <input type="submit" name="btnSubmit" value="Save" />
            </p>
        </div>
        <div id="divSaveChanges" runat="server">
            <p>
                <input type="submit" name="btnSubmit" value="Save Changes" />
            </p>
        </div>
    </fieldset>
</div>

<div id="divGridView" runat="server">
    <div class="noprint" style="padding:5px;border-radius:10px;border:1px solid Gray;">
        <fieldset id="fldViewNotes" runat="server">
        <legend>Select Notes to View</legend>
        <p><label for="Note">Concerning:</label></p>
        <div style="float:left;width:30%;margin-bottom:10px;">
            <p>Select a Student</p>
            <asp:DropDownList ID="ddlStudentSearch" runat="server" AutoPostBack="false"></asp:DropDownList>&nbsp;OR&nbsp;
            <div style="padding:5px;">
                <asp:Button ID="btnStudentSearch" OnClick="btnStudentSearch_Click" runat="server" Text="Search" CssClass="blueButton" /> 
            </div>
        </div>
        <div style="float:left;width:30%;margin-bottom:10px;">
            <p>Select a Family</p>
            <asp:DropDownList ID="ddlFamilySearch" AutoPostBack="false" runat="server"></asp:DropDownList>&nbsp;OR&nbsp;
            <div style="padding:5px;">
                <asp:Button ID="btnFamilySearch" OnClick="btnFamilySearch_Click" runat="server" Text="Search" CssClass="blueButton" /> 
            </div>
        </div>
        <div style="float:left;width:30%;margin-bottom:10px;">
            <p>A Placement</p>
            <asp:DropDownList ID="ddlPlacementSearch" runat="server"></asp:DropDownList>
            <div style="padding:5px;">
                <asp:Button ID="btnPlacementSearch" OnClick="btnPlacementSearch_Click" runat="server" Text="Search" CssClass="blueButton" /> 
            </div>
        </div>
        <div style="clear: both;width:100%;margin-top:5px;">
            Created/Modified dates between: 
            <telerik:RadDatePicker ID="dpStartDate" runat="server" AutoPostBack="False" Culture="en-US">
                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"></Calendar>
                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" LabelWidth="40%" AutoPostBack="True">
                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                    <FocusedStyle Resize="None"></FocusedStyle>
                    <DisabledStyle Resize="None"></DisabledStyle>
                    <InvalidStyle Resize="None"></InvalidStyle>
                    <HoveredStyle Resize="None"></HoveredStyle>
                    <EnabledStyle Resize="None"></EnabledStyle>
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDatePicker>
            and: 
            <telerik:RadDatePicker ID="dpEndDate" runat="server" AutoPostBack="False" Culture="en-US">
                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"></Calendar>
                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" LabelWidth="40%" AutoPostBack="True">
                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                    <FocusedStyle Resize="None"></FocusedStyle>
                    <DisabledStyle Resize="None"></DisabledStyle>
                    <InvalidStyle Resize="None"></InvalidStyle>
                    <HoveredStyle Resize="None"></HoveredStyle>
                    <EnabledStyle Resize="None"></EnabledStyle>
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDatePicker>
            <asp:Button ID="btnDateSearch" OnClick="btnDateSearch_Click" runat="server" Text="Search" CssClass="blueButton" /> 
        </div>
        <div style="width:100%; margin-top:5px;">
            Clear Search: <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="All Notes" CssClass="blueButton" />
        </div>
        </fieldset>
    </div>

    <div style="margin-top:25px;">
    <asp:GridView ID="gvNotes" runat="server" DataSourceID="AppsSqlDataSource" GridLines="Vertical" 
        AllowPaging="True" AllowSorting="True" PageSize="14" OnRowEditing="OnRowEditing_Click" OnRowUpdated="OnRowUpdated_Click"
        AutoGenerateColumns="False" DataKeyNames="id" Width="100%" EmptyDataText="There are no records to display." 
        >
        <AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
        <Columns>
            <asp:CommandField ShowEditButton="true" ControlStyle-CssClass="noprint" />
            <asp:BoundField DataField="id" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="studentName" HeaderText="Student" SortExpression="studentName" ReadOnly="true"  />
            <asp:BoundField DataField="homeName" HeaderText="Family" SortExpression="homeName" ReadOnly="true" />
            <asp:BoundField DataField="placementName" HeaderText="Placement" SortExpression="placementName" ReadOnly="true" />
            <asp:BoundField DataField="createDate" HeaderText="Date Created" SortExpression="createDate" ReadOnly="true" DataFormatString="{0:d}" />
            <asp:BoundField DataField="subject" HeaderText="Subject" SortExpression="subject" />
            <asp:TemplateField ControlStyle-CssClass="noprint" HeaderText="Note">
                <ItemTemplate>
                    <asp:Label ID="lblNote" runat="server"
                    Text='<%# Eval("Note") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtNote" runat="server"
                    TextMode="MultiLine" Text='<%# Bind("Note") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:CheckBoxField DataField="actionNeeded" HeaderText="Action Needed" SortExpression="actionNeeded" />
            <asp:CheckBoxField DataField="actionCompleted" HeaderText="Action Done" SortExpression="actionCompleted" />
            <asp:BoundField DataField="lastModifiedDate" HeaderText="Date Edited" SortExpression="lastModifiedDate" ReadOnly="true" DataFormatString="{0:d}" />
       </Columns>
        <FooterStyle BackColor="#448BC1" ForeColor="White" Font-Bold="True" Font-Size="Small" />
        <RowStyle BackColor="#E6E7E7" Font-Size="Small" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#448BC1" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#448BC1" Font-Bold="False" ForeColor="White" />
        <EditRowStyle BackColor="#FBF8F8" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    </div>

    
    <asp:SqlDataSource ID="AppsSqlDataSource" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConnectionString_CCSInternationalStudent %>" 
        SelectCommand="SELECT [familyID],[homeName],[applicantID],[studentName],[placementName],[id],[Note],[createDate],[actionNeeded],[actionCompleted],[subject],[lastModifiedDate] FROM [CCSInternationalStudent].[dbo].[vw_HomestayNotesExtended] ORDER BY [createDate] DESC"
        UpdateCommand="UPDATE [CCSInternationalStudent].[dbo].[HomestayNotes] SET [Note] = @Note,[actionNeeded] = @actionNeeded,[actionCompleted] = @actionCompleted,[subject] = @subject,[lastModifiedDate] = @lastModifiedDate WHERE [id] = @id"
        >
        <UpdateParameters>
            <asp:Parameter Name="Note" />
            <asp:Parameter Name="actionNeeded" />
            <asp:Parameter Name="actionCompleted" />
            <asp:Parameter Name="subject" />
            <asp:Parameter Name="lastModifiedDate" />
        </UpdateParameters>
    </asp:SqlDataSource>
</div>

</asp:Content>

