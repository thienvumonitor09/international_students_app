﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_ComparePreferences : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String strButtonFace = "";

            if (Request.Form["btnSubmit"] != null)
            {
                strButtonFace = Request.Form["btnSubmit"].ToString();
            }
            //strResultMsg += "Button Face: " + strButtonFace + "<br />";
            if (strButtonFace != "")
            {

                switch (strButtonFace)
                {
                    case "Return to Home Page":
                        Response.Redirect("/Homestay/Admin/Default.aspx");
                        break;
                }
            }
    }
}