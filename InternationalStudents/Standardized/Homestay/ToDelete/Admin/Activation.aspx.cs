﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Activation : System.Web.UI.Page
{
    String selectedStudent = "0";
    String selectedFamily = "0";
    String selectedStudentStatus = "";
    String selectedFamilyStatus = "";
    String currentStudentActive = "";
    String currentFamilyActive = "";
    String familyIsActive = "";
    String studentIsActive = "";
    String terminationDate = "";
    String deactivationDate = "";
    String strResultMsg = "";
    String postBackControlName = "";
    String strButtonFace = "";
    Int32 intResult = 0;
    DateTime outDate;
    Boolean dateSuccess = false;

    protected void Page_Load(object sender, EventArgs e)
    {

        litResultMsg.Text = "";

        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (strButtonFace == "Return to Admin Home Page")
        {
            Response.Redirect("/Homestay/Admin/Default.aspx");
        }

        Homestay hsInfo = new Homestay();
        DataTable dtStudents = hsInfo.GetHomestayInternationalStudents();
        DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDL");

        if (!Page.IsPostBack)
            postBackControlName = "";
        else
        {
            postBackControlName = Page.Request.Params["__EVENTTARGET"];
           // Response.Write("control name: " + postBackControlName + "<br />");
          //  Response.Write("postback? " + Page.IsPostBack.ToString() + "<br />");
            selectedStudent = ddlAllStudents.SelectedValue;
            selectedFamily = ddlAllFamilies.SelectedValue;
        }
        if (IsPostBack && postBackControlName.Trim().Contains("ddlAllStudents"))
        {
            // populate only the Student fields on the form
            DataTable dtStudentInfo = hsInfo.GetOneHomestayStudent(Convert.ToInt32(selectedStudent));
            if (dtStudentInfo != null)
            {
                if (dtStudentInfo.Rows.Count > 0)
                {
                    //Set active indicator
                    currentStudentActive = dtStudentInfo.Rows[0]["IsActive"].ToString();
                    try
                    {
                        rdoIsStudentActive.SelectedValue = currentStudentActive;
                    }
                    catch
                    {
                        strResultMsg = "Student is active? " + currentStudentActive;
                    }
                    //Set homestay status
                    homestayStatus.Text = dtStudentInfo.Rows[0]["homestayStatus"].ToString().Trim();
                    //deactivate date
                    if (DBNull.Value.Equals(dtStudentInfo.Rows[0]["dateInactive"]) || (dtStudentInfo.Rows[0]["dateInactive"].ToString().Contains("1/1/1900")))
                    {
                        dateInactive.Text = "";
                    }
                    else
                    {
                        dateInactive.Text = Convert.ToDateTime(dtStudentInfo.Rows[0]["dateInactive"]).ToShortDateString();
                    }

                }
                else { strResultMsg += "Error: dtStudentInfo has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtStudentInfo is null.<br />"; }
        }
        else if (IsPostBack && postBackControlName.Trim().Contains("ddlAllFamilies"))
        {
            // populate only the Family fields on the form
            DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(Convert.ToInt32(selectedFamily), "", "ID");
            if (dtFamilyInfo != null)
            {
                if (dtFamilyInfo.Rows.Count > 0)
                {
                    //Set active indicator
                    Boolean blIsFamilyActive = Convert.ToBoolean(dtFamilyInfo.Rows[0]["IsActive"]);
                    if (blIsFamilyActive) { currentFamilyActive = "1"; } else { currentFamilyActive = "0"; }
                    rdoIsFamilyActive.SelectedValue = currentFamilyActive;
                    //Terminate date
                    Response.Write("DATE: " + dtFamilyInfo.Rows[0]["dateTerminated"].ToString() + "<br />");
                    if (DBNull.Value.Equals(dtFamilyInfo.Rows[0]["dateTerminated"]) || (dtFamilyInfo.Rows[0]["dateTerminated"].ToString().Contains("1/1/1900")))
                    {
                        dateTerminated.Text = "";
                    }
                    else
                    {
                        dateTerminated.Text = Convert.ToDateTime(dtFamilyInfo.Rows[0]["dateTerminated"]).ToShortDateString();
                    }
                    //Set homestay status
                    familyStatus.Text = dtFamilyInfo.Rows[0]["familyStatus"].ToString().Trim();
                }
                else { strResultMsg += "Error: dtFamilyInfo has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtFamilyInfo is null.<br />"; }
        }
        else
        {
            // Is postback and updating either student or family
        }

        if (!IsPostBack)
        {
            //Load the DDLs
            ddlAllStudents.Items.Clear();
            ListItem LI;
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlAllStudents.Items.Add(LI);
            if (dtStudents != null)
            {
                if (dtStudents.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtStudents.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                        ddlAllStudents.Items.Add(LI);
                    }
                    ddlAllStudents.SelectedValue = selectedStudent;
                }
                else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtStudents is null.<br />"; }



            ddlAllFamilies.Items.Clear();
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlAllFamilies.Items.Add(LI);
            if (dtFamilies != null)
            {
                if (dtFamilies.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtFamilies.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["homeName"].ToString();
                        ddlAllFamilies.Items.Add(LI);
                    }
                    ddlAllFamilies.SelectedValue = selectedFamily;
                }
                else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtFamilies is null.<br />"; }

            DataTable dtStudentStatus = hsInfo.GetHomestayStatusValues("Student");
            ddlStudentStatus.Items.Clear();
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlStudentStatus.Items.Add(LI);
            if (dtStudentStatus != null)
            {
                if (dtStudentStatus.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtStudentStatus.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["statusDescription"].ToString().Trim();
                        ddlStudentStatus.Items.Add(LI);
                    }
                }
                else { strResultMsg += "Error: dtStudentStatus has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtStudentStatus is null.<br />"; }

            DataTable dtFamilyStatus = hsInfo.GetHomestayStatusValues("Family");
            ddlFamilyStatus.Items.Clear();
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlFamilyStatus.Items.Add(LI);
            if (dtFamilyStatus != null)
            {
                if (dtFamilyStatus.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtFamilyStatus.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["statusDescription"].ToString().Trim();
                        ddlFamilyStatus.Items.Add(LI);
                    }
                }
                else { strResultMsg += "Error: dtFamilyStatus has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtFamilyStatus is null.<br />"; }
        }// end NOT a postback - load the drop downs

        litResultMsg.Text += "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }// end page OnLoad

    protected void btnCmdUpdateFamily_Click(object sender, EventArgs e)
    {
        // validation
        if (ddlAllFamilies.SelectedIndex == -1)
        {
            eAllFamilies.InnerHtml = "Please select a family from the drop-down";
            ddlAllFamilies.Focus();
            return;
        }
        else
        {
            eAllFamilies.InnerHtml = "";
        }
        if (rdoIsFamilyActive.SelectedIndex == -1)
        {
            eFamilyActive.InnerHtml = "Please indicate whether the family is active or not";
            rdoIsFamilyActive.Focus();
            return;
        }
        else
            eFamilyActive.InnerHtml = "";
        if (ddlFamilyStatus.SelectedIndex == -1 || ddlFamilyStatus.SelectedValue == "0")
        {
            eFamilyStatus.InnerHtml = "Please select a new family status";
            ddlFamilyStatus.Focus();
            return;
        }
        else
        {
            eFamilyStatus.InnerHtml = "";
        }
        if (dateTerminated.Text.Trim() != "")
        {
            dateSuccess = DateTime.TryParse(dateTerminated.Text.Trim(), out outDate);
            if (!dateSuccess)
            {
                eDateTerminated.InnerHtml = "Please enter a valid date in the format mm/dd/yyyy";
                dateTerminated.Focus();
                return;
            }
            else
            {
                eDateTerminated.InnerHtml = "";
            }
        }
        if (rdoIsFamilyActive.SelectedValue == "1" && dateTerminated.Text.Trim() != "")
        {
            eDateTerminated.InnerHtml = "The family cannot be active and still have a termination date.";
            dateTerminated.Focus();
            return;
        }
        else
            eDateTerminated.InnerHtml = "";
      //  Response.Write("active: " + rdoIsFamilyActive.SelectedValue + "; date: " + dateTerminated.Text.Trim() + "<br/>");
        if (rdoIsFamilyActive.SelectedValue == "0" && dateTerminated.Text.Trim() == "")
        {
            eDateTerminated.InnerHtml = "The family cannot be inactive or terminated with no termination date.";
            dateTerminated.Focus();
            return;
        }
        else
            eDateTerminated.InnerHtml = "";

        //validation complete - do update
        selectedFamily = ddlAllFamilies.SelectedValue;
        familyIsActive = rdoIsFamilyActive.SelectedValue;
        selectedFamilyStatus = ddlFamilyStatus.SelectedItem.Text;
        if (dateTerminated.Text.Trim() == "")
            terminationDate = "";
        else
            terminationDate = outDate.ToShortDateString();
      //  Response.Write("family: " + selectedFamily + "; active: " + familyIsActive + "; status: " + selectedFamilyStatus + "; date: " + terminationDate.ToString() + "<br />");

        Homestay hsInfo = new Homestay();

        intResult = hsInfo.UpdateIsActiveBitFlagHomestayFamilyInfo(Convert.ToInt32(selectedFamily), familyIsActive);
        //Termination Date
        intResult = hsInfo.UpdateDateTerminatedHomestayFamilyInfo(Convert.ToInt32(selectedFamily), terminationDate);

        //Family Status
        intResult = hsInfo.UpdateFamilyStatus(Convert.ToInt32(selectedFamily), selectedFamilyStatus);

        //re-populate fields with new data from updated table
        DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(Convert.ToInt32(selectedFamily), "", "ID");
        if (dtFamilyInfo != null)
        {
            if (dtFamilyInfo.Rows.Count > 0)
            {
                //Set active indicator
                Boolean blIsFamilyActive = Convert.ToBoolean(dtFamilyInfo.Rows[0]["IsActive"]);
                if (blIsFamilyActive) { currentFamilyActive = "1"; } else { currentFamilyActive = "0"; }
                rdoIsFamilyActive.SelectedValue = currentFamilyActive;
                //Terminate date
                if (!DBNull.Value.Equals(dtFamilyInfo.Rows[0]["dateTerminated"]) || (dtFamilyInfo.Rows[0]["dateTerminated"].ToString().Contains("1/1/1900")))
                    dateTerminated.Text = "";
                else
                    dateTerminated.Text = Convert.ToDateTime(dtFamilyInfo.Rows[0]["dateTerminated"]).ToShortDateString();

                //Set homestay status
                familyStatus.Text = dtFamilyInfo.Rows[0]["familyStatus"].ToString().Trim();
            }
            else { strResultMsg += "Error: dtFamilyInfo has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtFamilyInfo is null.<br />"; }
    }// end btnCmdUpdateFamily

    protected void btnCmdUpdateStudent_Click(object sender, EventArgs e)
    {
        // validation
        if (ddlAllStudents.SelectedIndex == -1)
        {
            eAllStudents.InnerHtml = "Please select a student from the drop-down";
            ddlAllStudents.Focus();
            return;
        }
        else
        {
            eAllStudents.InnerHtml = "";
        }
        if (rdoIsStudentActive.SelectedIndex == -1)
        {
            eStudentActive.InnerHtml = "Please indicate whether the student is active or not.";
            rdoIsStudentActive.Focus();
            return;
        }
        else
        {
            eStudentActive.InnerHtml = "";
        }
        if (ddlStudentStatus.SelectedIndex == -1 || ddlStudentStatus.SelectedValue == "0")
        {
            eStudentStatus.InnerHtml = "Please select a new student status";
            ddlStudentStatus.Focus();
            return;
        }
        else
        {
            eStudentStatus.InnerHtml = "";
        }
        if (dateInactive.Text.Trim() != "")
        {
            dateSuccess = DateTime.TryParse(dateInactive.Text.Trim(), out outDate);
            if (!dateSuccess)
            {
                eDateInactive.InnerHtml = "Please enter a valid date in the format mm/dd/yyyy";
                dateInactive.Focus();
                return;
            }
            else
            {
                eDateInactive.InnerHtml = "";
            }
        }// end validate date inactive for student
        if (rdoIsStudentActive.SelectedValue == "1" && dateInactive.Text.Trim() != "") // Yes (Active) is selected AND deactivated date is populated
        {
            eDateInactive.InnerHtml = "The student cannot be active and still have a deactivation date.";
            dateInactive.Focus();
            return;
        }
        else
        {
            eDateInactive.InnerHtml = "";
        }
        if (rdoIsStudentActive.SelectedValue == "0" && dateInactive.Text.Trim() == "")            // No (Inactive) is selected AND deactivated date is blank
        {
            eDateInactive.InnerHtml = "The student cannot be inactive with no deactivation date.";
            dateInactive.Focus();
            return;
        }
        else
            eDateInactive.InnerHtml = "";
        // all validation passed
        selectedStudent = ddlAllStudents.SelectedValue;
        studentIsActive = rdoIsStudentActive.SelectedValue;
        selectedStudentStatus = ddlStudentStatus.SelectedItem.Text;
        if (dateInactive.Text.Trim() == "")
            deactivationDate = "";
        else
            deactivationDate = outDate.ToShortDateString();
      //  Response.Write("student: " + selectedStudent + "; active: " + studentIsActive + "; status: " + selectedStudentStatus + "; date: " + deactivationDate.ToString() + "<br />");
        Homestay hsInfo = new Homestay();

        strResultMsg += "Update Student Status value: " + rdoIsStudentActive.SelectedValue + "; Student: " + selectedStudent + "<br />";
        //Is Active?
        intResult = hsInfo.UpdateIsActiveBitFlagHomestayStudentInfo(Convert.ToInt32(selectedStudent), studentIsActive);

        //Deactivation Date
        intResult = hsInfo.UpdateDateInactiveHomestayStudentInfo(Convert.ToInt32(selectedStudent), deactivationDate);

        //Student Status
        intResult = hsInfo.UpdateHomestayStudentStatus(0, Convert.ToInt32(selectedStudent), selectedStudentStatus);

        //re-populate fields with new values from updated table
        DataTable dtStudentInfo = hsInfo.GetOneHomestayStudent(Convert.ToInt32(selectedStudent));
        if (dtStudentInfo != null)
        {
            if (dtStudentInfo.Rows.Count > 0)
            {
                //Set active indicator
                currentStudentActive = dtStudentInfo.Rows[0]["IsActive"].ToString();
                try
                {
                    rdoIsStudentActive.SelectedValue = currentStudentActive;
                }
                catch
                {
                    strResultMsg = "Student is active? " + currentStudentActive;
                }
                //Set homestay status
                homestayStatus.Text = dtStudentInfo.Rows[0]["homestayStatus"].ToString().Trim();
                //deactivate date
                if (DBNull.Value.Equals(dtStudentInfo.Rows[0]["dateInactive"]) || (dtStudentInfo.Rows[0]["dateInactive"].ToString() == "1/1/1900 12:00:00 AM"))
                {
                    dateInactive.Text = "";
                }
                else
                    dateInactive.Text = Convert.ToDateTime(dtStudentInfo.Rows[0]["dateInactive"]).ToShortDateString();

            }
            else { strResultMsg += "Error: dtStudentInfo has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudentInfo is null.<br />"; }
    }
}