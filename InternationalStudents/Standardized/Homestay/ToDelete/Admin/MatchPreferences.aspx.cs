﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_MatchPreferences : System.Web.UI.Page
{
    public String strResultMsg = "";
    const string smokes = "Smoker";
    const string nonSmokes = "Nonsmoker";
    const string occasional = "Occasional smoker";


    protected void Page_Load(object sender, EventArgs e)
    {
        String postBackControlName = "";
        String selectedStudent = "0";
        String selectedFamily = "0";
        String selectedGenderSearch = "";
        String selectedHomestaySearch = "";
        String selectedSmokingSearch = "";
        String selectedCollegeSearch = "";
        String strRelationship = "";
        String strChildren = "No minor children";
        String strChild1 = "";
        String strChild2 = "";
        String strChild3 = "";
        String strChild4 = "";
        String submitButton = "";
        String lastFamilyID = "";
        String strGender = "";
        String strOption = "";
        String strSmokeSelf = "";
        String strOtherSmokePreference = "";
        String strSQL = "";
        Int32 intApplicantID = 0;
        Int32 intFamilyID = 0;
        Int32 intResult = 0;
        //calculate age
        // Save today's date.
        DateTime dtToday = DateTime.Today;
        DateTime dtBirthday = DateTime.Today;
        Int32 intAge = 0;
        ListItem LI;

        litResultMsg.Text = "";
        Homestay hsInfo = new Homestay();

        //  TO DO:
        //  Wire up Notes for students and families
        //  Line up the top of each section with divs for easy visual comparisons
        //  Develop matching SQL


        if (IsPostBack)
        {
            DataTable dtStudentChoice;
            // see if user selected a student from the drop-down.
            // If so, get the school the student wishes to attend and assign value to rdoCollege
            postBackControlName = Page.Request.Params["__EVENTTARGET"];
            if (postBackControlName.Contains("ddlStudents"))
            {
                // reset the loopStatus values in the HomestayPreferenceSelecteions table. 
                // New student selected so new preferences
             //   intResult = hsInfo.ResetLoopStatus();
                //get the student ID
                selectedStudent = ddlStudents.SelectedValue;
                dtStudentChoice = hsInfo.GetStudentStudyInfo(Convert.ToInt32(selectedStudent));
                if (dtStudentChoice != null)
                {
                    if (dtStudentChoice.Rows.Count > 0) { }
                    rdoCollege.SelectedValue = dtStudentChoice.Rows[0]["studyWhere"].ToString();
                    hdnGender.Value = dtStudentChoice.Rows[0]["Gender"].ToString();
                    hdnOption.Value = dtStudentChoice.Rows[0]["homestayOption"].ToString();
                  //  Response.Write("value: " + dtStudentChoice.Rows[0]["agreeCCSConduct18"].ToString());
                    if (dtStudentChoice.Rows[0]["agreeCCSConduct18"].ToString() == "False")
                        hdnAge.Value = "UA";
                    else hdnAge.Value = "18";
                    hdnSmoke.Value = "";
                    foreach(DataRow dr in dtStudentChoice.Rows)
                        {
                            hdnSmoke.Value += dr["preferenceID"].ToString() + "|";
                        }
                    hdnSmoke.Value = hdnSmoke.Value.Substring(0,hdnSmoke.Value.Length-1);
                }
            }
            //Capture values
            selectedFamily = ddlFamilies.SelectedValue;
            selectedStudent = ddlStudents.SelectedValue;
            if (Request.Form["btnSubmit"] != null)
            {
                submitButton = Request.Form["btnSubmit"].ToString();
                //Process submit buttons
                switch (submitButton)
                {
                    case "Return to Admin Home Page":
                        Response.Redirect("/Homestay/Admin/");
                        break;
                    case "Search":
                        selectedHomestaySearch = rdoHomestay.SelectedValue;
                        selectedGenderSearch = rdoGender.SelectedValue;
                        selectedSmokingSearch = rdoSmoking.SelectedValue;
                        selectedCollegeSearch = rdoCollege.SelectedValue;
                        //Create query
                        Response.Write("gender: " + rdoGender.SelectedValue.ToString() + "; homestay: " + rdoHomestay.SelectedValue.ToString() + "; college: " + rdoCollege.SelectedValue.ToString() + "<br />");
                        if(rdoGender.SelectedValue == "Include")
                        {
                            if (hdnGender.Value.ToString() == "F")
                                strGender = "Female";
                            else if (hdnGender.Value.ToString() == "M")
                                strGender = "Male";
                            else
                                strGender = "Other";
                        }
                        if(rdoHomestay.SelectedValue == "Include")
                        {
                            if (hdnOption.Value.ToString() == "Either")
                                strOption = "NoPreference";
                            else if (hdnOption.Value.ToString() == "Without food")
                                strOption = "Shared18";
                            else if (hdnOption.Value.ToString() == "With food" && hdnAge.Value.ToString() == "18")
                                strOption = "Full18";
                            else
                                strOption = "FullUA";
                        }
                        if(rdoSmoking.SelectedValue == "Include")
                        {
                            string[] smokes = hdnSmoke.Value.ToString().Split('|');
                            foreach(string smokePref in smokes)
                            {
                                if (smokePref.Contains("Other"))
                                    strOtherSmokePreference = smokePref;
                                else
                                    strSmokeSelf = smokePref;
                            }
                            Response.Write(" other smoker preference: " + strOtherSmokePreference + "; self smoker: " + strSmokeSelf + "<br />");
                        }
                        
                        // get families if only college is selected - we are always using college
                        if((rdoGender.SelectedValue == "Omit" || rdoGender.SelectedValue == "") && (rdoHomestay.SelectedValue == "Omit" || rdoHomestay.SelectedValue == "") && (rdoSmoking.SelectedValue.ToString() == "Omit" || rdoSmoking.SelectedValue.ToString() == ""))
                        {
                            // college only
                            strSQL = String.Format("SELECT id, homeName FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo] " +
                             "WHERE collegeQualified = '{0}' OR collegeQualified = '{1}' order by homeName", rdoCollege.SelectedValue.ToString(), "Either");
                        } else if(rdoGender.SelectedValue == "Include" && (rdoHomestay.SelectedValue == "Omit" || rdoHomestay.SelectedValue == "") && (rdoSmoking.SelectedValue.ToString() == "Omit" || rdoSmoking.SelectedValue.ToString() == ""))
                        {
                            // college and gender
                            strSQL = String.Format("SELECT id, homeName, genderPreference FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo] " +
                             "WHERE (collegeQualified = '{0}' OR collegeQualified = '{1}') AND (genderPreference = '{2}' OR genderPreference = 'Either' OR genderPreference = 'Combination') " +
                             "order by homeName", rdoCollege.SelectedValue.ToString(), "Either", strGender);
                        }
                        else if (rdoHomestay.SelectedValue == "Include" && (rdoGender.SelectedValue == "Omit" || rdoGender.SelectedValue == "") && (rdoSmoking.SelectedValue == "Omit" || rdoSmoking.SelectedValue == ""))
                        {
                            // college and homestay option
                            strSQL = String.Format("SELECT id, homeName, genderPreference, homestayOption " +
                            "FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo] " +
                            "WHERE (collegeQualified = '{0}' OR collegeQualified = '{1}') AND (homestayOption LIKE '%{2}%' OR homestayOption = '{3}') " +
                            "order by homeName", rdoCollege.SelectedValue.ToString(), "Either", strOption, "NoPreference");
                        }
                        else if(rdoSmoking.SelectedValue == "Include" && (rdoGender.SelectedValue == "Omit" || rdoGender.SelectedValue == "") && (rdoHomestay.SelectedValue.ToString() == "Omit" || rdoHomestay.SelectedValue.ToString() == ""))
                        {
                            // college and smoking
                            if (strOtherSmokePreference.Contains("not OK") && strSmokeSelf == nonSmokes)
                            {
                                //student is non smoker and other smoker is NOT OK
                                strSQL = String.Format("SELECT hfi.id, hfi.homeName, hfi.genderPreference, hfi.homestayOption, hps.familyID " +
                                "FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo] AS hfi " +
                                "INNER JOIN [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections] AS hps ON hps.familyID = hfi.id " +
                                "WHERE (collegeQualified = '{0}' OR collegeQualified = '{1}') AND hps.preferenceID = '{2}' " +
                                "order by hfi.homeName", rdoCollege.SelectedValue.ToString(), "Either", nonSmokes);
                            }
                            else if ((strOtherSmokePreference.Contains("smoker OK") || strOtherSmokePreference.Contains("no preference")) && strSmokeSelf == nonSmokes)
                            {
                                // student is non smoker and other smokers ARE OK so family can have smokers, or not
                                strSQL = String.Format("SELECT hfi.id, hfi.homeName, hfi.genderPreference, hfi.homestayOption, hps.familyID " +
                                "FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo] AS hfi " +
                                "INNER JOIN [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections] AS hps ON hps.familyID = hfi.id " +
                                "WHERE (collegeQualified = '{0}' OR collegeQualified = '{1}') AND (hps.preferenceID = '{2}' OR hps.preferenceID = '{3}' OR hps.preferenceID = '{4}') " +
                                "order by hfi.homeName", rdoCollege.SelectedValue.ToString(), "Either", smokes, nonSmokes, occasional);
                            }
                            else if (strOtherSmokePreference.Contains("not OK") && (strSmokeSelf == smokes || strSmokeSelf == occasional))
                            {
                                // student is smoker/occasional but other smokers are NOT OK so family must be non smokers but be OK with a smoker
                                // Execute the stored procedure uspGetMatchNoSmokeButAllowOthers
                                intResult = hsInfo.GetMatchStuNoSmokeButAllowOthers();
                                // Run following sql to select the records where loopStatus = 1
                                // Be sure to ZERO OUT loopStatus whenever a new student is selected from ddlStudents!!!!
                                strSQL = String.Format("SELECT hfi.id, hfi.homeName, hfi.genderPreference, hfi.homestayOption, hps.familyID " +
                                "FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo] AS hfi " +
                                "INNER JOIN [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections] AS hps ON hps.familyID = hfi.id " +
                                "WHERE (collegeQualified = '{0}' OR collegeQualified = '{1}') AND hps.loopStatus = 1 " +
                                "order by hfi.homeName", rdoCollege.SelectedValue.ToString(), "Either");
                            } else
                            {
                                // student is smoker/occasional and others are OK or no preference so family can smoke or not and must allow other smokers in home
                                strSQL = String.Format("SELECT hfi.id, hfi.homeName, hfi.genderPreference, hfi.homestayOption, hps.familyID " +
                                "FROM [CCSInternationalStudent].[dbo].[HomestayFamilyInfo] AS hfi " +
                                "INNER JOIN [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections] AS hps ON hps.familyID = hfi.id " +
                                "WHERE (collegeQualified = '{0}' OR collegeQualified = '{1}') AND (hps.preferenceID = '{2}' OR hps.preferenceID = '{3}') " +
                                "order by hfi.homeName", rdoCollege.SelectedValue.ToString(), "Either", "Other smoker OK", "Other smoker no preference");
                            }
                        }
                        else if(rdoGender.SelectedValue == "Include" && rdoHomestay.SelectedValue == "Include" && (rdoSmoking.SelectedValue.ToString() == "Omit" || rdoSmoking.SelectedValue.ToString() == ""))
                        {
                            // college, gender and homestay option
                            strSQL = String.Format("SELECT id, homeName, genderPreference, homestayOption FROM[CCSInternationalStudent].[dbo].[HomestayFamilyInfo] " +
                                "WHERE (collegeQualified = '{0}' OR collegeQualified = '{1}') AND (genderPreference = '{2}' OR genderPreference = 'Either' OR genderPreference = 'Combination') " +
                                "AND (homestayOption LIKE '%{3}%' OR homestayOption = 'NoPreference') " +
                                "order by homeName", rdoCollege.SelectedValue.ToString(), "Either", strGender, strOption);
                        }
                        else if (rdoGender.SelectedValue == "Include" && rdoSmoking.SelectedValue == "Include" && (rdoHomestay.SelectedValue.ToString() == "Omit" || rdoHomestay.SelectedValue.ToString() == ""))
                        {
                            // college, gender and smoking
                        }

                        Response.Write("sql: " + strSQL + "<br />");

                        /*
                        if (rdoHomestay.SelectedValue == "Include")
                            strOption = hdnOption.Value.ToString();
                        
                        if(rdoSmoking.SelectedValue == "Include")
                        { strSQL = String.Format("SELECT studentPS.preferenceID, studentPS.applicantID, familyPS.familyID, hfi.homeName " +
                                "FROM [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections] AS studentPS " +
                                "INNER JOIN [CCSInternationalStudent].[dbo].[HomestayPreferenceSelections] AS familyPS ON familyPS.applicantID = 0 AND familyPS.preferenceID = studentPS.preferenceID " +
                                "INNER JOIN [CCSInternationalStudent].[dbo].[HomestayFamilyInfo] AS hfi ON hfi.id = familyPS.familyID " +
                                "WHERE studentPS.applicantID = {0} AND studentPS.preferenceID LIKE '%smoker%' AND hfi.collegeQualified = '{1}' " +
                                "order by hfi.homeName", Convert.ToInt32(ddlStudents.SelectedValue.ToString()), selectedCollegeSearch); }
                        // strSQL = "";
                        */
                        //Factor in selected criteria 

                        //Calculate number of rooms available > 0

                        //Run query to create family data table
                        DataTable dtFamilies = hsInfo.RunGetQuery(strSQL);
                       //Temporary for testing only
                        // DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
                        //Load family DDL
                        ddlFamilies.Items.Clear();
                        lastFamilyID = "";
                        LI = new ListItem();
                        LI.Value = "0";
                        LI.Text = "-- Select One --";
                        ddlFamilies.Items.Add(LI);
                        if (dtFamilies != null)
                        {
                            if (dtFamilies.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dtFamilies.Rows)
                                {
                                    if (lastFamilyID != dr["id"].ToString())
                                    {
                                        LI = new ListItem();
                                        LI.Value = dr["id"].ToString();
                                        LI.Text = dr["homeName"].ToString();
                                        ddlFamilies.Items.Add(LI);
                                        lastFamilyID = dr["id"].ToString();
                                    }
                                }
                                //if (selectedFamily != "") { ddlFamilies.SelectedValue = selectedFamily; }
                            }
                            else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
                        }
                        else { strResultMsg += "Error: dtFamilies is null.<br />"; }
                        intResult = hsInfo.ResetLoopStatus();
                        break;
                    case "Compare":
                        ResetFields();
                        //Reset matching selections
                        rdoGender.SelectedValue = selectedGenderSearch;
                        rdoCollege.SelectedValue = selectedCollegeSearch;
                        rdoSmoking.SelectedValue = selectedSmokingSearch;
                        rdoHomestay.SelectedValue = selectedHomestaySearch;

                        //Get selected student profiles and display
                        //LOAD STUDENT RECORD
                        if (selectedStudent != "" && selectedStudent != null) { intApplicantID = Convert.ToInt32(selectedStudent); }
                        DataTable dtInfo = hsInfo.GetOneHomestayStudent(intApplicantID);
                        if (dtInfo != null)
                        {
                            if (dtInfo.Rows.Count > 0)
                            {
                                traveledWhereStu.Text = dtInfo.Rows[0]["traveledOutsideCountry"].ToString().Trim();
                                smokingHabitsStu.Text = dtInfo.Rows[0]["smokingHabits"].ToString().Trim();
                                activitiesEnjoyedStu.Text = dtInfo.Rows[0]["activitiesEnjoyed"].ToString().Trim();
                                allergiesStu.Text = dtInfo.Rows[0]["allergies"].ToString().Trim();
                                homeEnvironmentPreferencesStu.Text = dtInfo.Rows[0]["homeEnvironmentPreferences"].ToString().Trim();
                                healthConditionsStu.Text = dtInfo.Rows[0]["healthConditions"].ToString().Trim();
                                //rdoHealthStatus.SelectedValue = dtInfo.Rows[0]["healthStatus"].ToString().Trim();
                                //Boolean blDriveCar = Convert.ToBoolean(dtInfo.Rows[0]["driveCar"]);
                                //if (blDriveCar)
                                //{
                                //    rdoDriveCar.SelectedValue = "1";
                                //}
                                //else
                                //{
                                //    rdoDriveCar.SelectedValue = "0";
                                //}
                                anythingElse.Text = dtInfo.Rows[0]["anythingElse"].ToString().Trim();
                            }
                        }
                        //Get STUDENT preferences
                        DataTable dtPref = hsInfo.GetHomestayPreferenceSelections(intApplicantID, 0);
                        if (dtPref != null)
                        {
                            if (dtPref.Rows.Count > 0)
                            {
                                foreach (DataRow dtPrefRow in dtPref.Rows)
                                {
                                    String objID = dtPrefRow["fieldID"].ToString().Trim() + "Stu";
                                    String objValue = dtPrefRow["fieldValue"].ToString().Trim();
                                    if (objID.Substring(0, 3).Contains("rdo"))
                                    {
                                        //Radio Button
                                        //declare specific type of control
                                        RadioButtonList myRDO = (RadioButtonList)Page.Master.FindControl("MainContent").FindControl("studentProfile").FindControl(objID);
                                        if (myRDO != null)
                                        {
                                            //set value
                                            myRDO.SelectedValue = objValue;
                                        }
                                        else { strResultMsg += "Radio button list " + objID + " with a value of " + objValue + " is null.<br />"; }
                                    }
                                    else
                                    {
                                        //Checkbox
                                        //declare specific type of control
                                        CheckBox myControl = (CheckBox)Page.Master.FindControl("MainContent").FindControl("studentProfile").FindControl(objID);
                                        if (myControl != null)
                                        {
                                            //set value
                                            myControl.Checked = true;
                                        }
                                        else { strResultMsg += "Checkbox " + objID + " with a value of " + objValue + " is null.<br />"; }
                                    }
                                }
                            }
                            else { strResultMsg += "Student Preference Selections returned no rows.<br />"; }
                        }
                        else { strResultMsg += "Student Preference Selections table is null<br />"; }

                        //Get FAMILY data
                        if (selectedFamily != "" && selectedFamily != null) { intFamilyID = Convert.ToInt32(selectedFamily); }
                        strResultMsg += "Selected family: " + intFamilyID + "<br />";
                        DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
                        if (dtFamilyInfo != null)
                        {
                            if (dtFamilyInfo.Rows.Count > 0)
                            {
                                traveledWhere.Text = dtFamilyInfo.Rows[0]["travelOutsideUS"].ToString();
                                smokingHabits.Text = dtFamilyInfo.Rows[0]["smokerInHome"].ToString();
                                smokingGuidelines.Text = dtFamilyInfo.Rows[0]["smokingGuidelines"].ToString();
                                activitiesEnjoyed.Text = dtFamilyInfo.Rows[0]["otherHobbies"].ToString();
                                homeEnvironmentPreferences.Text = dtFamilyInfo.Rows[0]["homeEnvironment"].ToString();
                                anythingElse.Text = dtFamilyInfo.Rows[0]["additionalPreferences"].ToString();
                                petsInHome.Text = dtFamilyInfo.Rows[0]["petsInHome"].ToString();
                            }
                        }
                        //Get family children's DOBs
                        DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
                        if (dtRelatives != null)
                        {
                            if (dtRelatives.Rows.Count > 0)
                            {
                                foreach (DataRow dtRow in dtRelatives.Rows)
                                {
                                    strRelationship = dtRow["relationship"].ToString().Trim();
                                    switch (strRelationship)
                                    {
                                        case "Minor Child 1":
                                            strChild1 = "1 - Name: " + dtRow["firstName"].ToString().Trim();
                                            dtBirthday = Convert.ToDateTime(dtRow["DOB"]);
                                            intAge = dtToday.Year - dtBirthday.Year;
                                            strChild1 += " Age: " + intAge;
                                            strChild1 += " Gender: " + dtRow["gender"].ToString().Trim();
                                            break;
                                        case "Minor Child 2":
                                            strChild2 = "; 2 - Name: " + dtRow["firstName"].ToString().Trim();
                                            dtBirthday = Convert.ToDateTime(dtRow["DOB"]);
                                            intAge = dtToday.Year - dtBirthday.Year;
                                            strChild2 += " Age: " + intAge;
                                            strChild2 += " Gender: " + dtRow["gender"].ToString().Trim();
                                            break;
                                        case "Minor Child 3":
                                            strChild3 = "; 3 - Name: " + dtRow["firstName"].ToString().Trim();
                                            dtBirthday = Convert.ToDateTime(dtRow["DOB"]);
                                            intAge = dtToday.Year - dtBirthday.Year;
                                            strChild3 += " Age: " + intAge;
                                            strChild3 += " Gender: " + dtRow["gender"].ToString().Trim();
                                            break;
                                        case "Minor Child 4":
                                            strChild4 = "; 4 - Name: " + dtRow["firstName"].ToString().Trim();
                                            dtBirthday = Convert.ToDateTime(dtRow["DOB"]);
                                            intAge = dtToday.Year - dtBirthday.Year;
                                            strChild4 += " Age: " + intAge;
                                            strChild4 += " Gender: " + dtRow["gender"].ToString().Trim();
                                            break;
                                    }
                                }
                                strChildren = strChild1 + strChild2 + strChild3 + strChild4;
                            }
                        }
                        SmallChildrenInHome.Text = strChildren;
                        //Get FAMILY preferences
                        DataTable dtFamPref = hsInfo.GetHomestayPreferenceSelections(0, intFamilyID);
                        if (dtFamPref != null)
                        {
                            if (dtFamPref.Rows.Count > 0)
                            {
                                foreach (DataRow dtPrefRow in dtFamPref.Rows)
                                {
                                    String objID = dtPrefRow["fieldID"].ToString().Trim();
                                    String objValue = dtPrefRow["fieldValue"].ToString().Trim();
                                    if (objID.Substring(0, 3).Contains("rdo"))
                                    {
                                        //Radio Button
                                        //declare specific type of control
                                        RadioButtonList myRDO = (RadioButtonList)Page.Master.FindControl("MainContent").FindControl("divFamilyProfile").FindControl(objID);
                                        if (myRDO != null)
                                        {
                                            //set value
                                            myRDO.SelectedValue = objValue;
                                        }
                                        else { strResultMsg += "Radio button list " + objID + " with a value of " + objValue + " is null.<br />"; }
                                    }
                                    else
                                    {
                                        //Checkbox
                                        //declare specific type of control
                                        CheckBox myControl = (CheckBox)Page.Master.FindControl("MainContent").FindControl("divFamilyProfile").FindControl(objID);
                                        if (myControl != null)
                                        {
                                            //set value
                                            myControl.Checked = true;
                                        }
                                        else { strResultMsg += "Checkbox " + objID + " with a value of " + objValue + " is null.<br />"; }
                                    }
                                }
                            }
                            else { strResultMsg += "Family Preference Selections returned no rows.<br />"; }
                        }
                        else { strResultMsg += "Family Preference Selections table is null<br />"; }

                        break;
                }

                if (selectedFamily != "" && selectedFamily != "0" && selectedFamily != null)
                {
                    //reset selected family, if any
                    ddlFamilies.SelectedValue = selectedFamily;
                }

            }
            litResultMsg.Text = strResultMsg;
        }

        //Load the student DDL
        DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
        ddlStudents.Items.Clear();
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlStudents.Items.Add(LI);
        if (dtStudents != null)
        {
            if (dtStudents.Rows.Count > 0)
            {
                foreach (DataRow dr in dtStudents.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["applicantID"].ToString();
                    LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                    ddlStudents.Items.Add(LI);
                }
                //if (selectedStudent != "") { ddlStudents.SelectedValue = selectedStudent; }
            }
            else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudents is null.<br />"; }
        if (selectedStudent != "" && selectedStudent != "0" && selectedStudent != null)
        {
            //reset selected Student, if any
            ddlStudents.SelectedValue = selectedStudent;
        }

    }

    void ClearInputs(ControlCollection ctrls)
    {
        foreach (Control ctrl in ctrls)
        {
            if (ctrl is TextBox)
            {
                ((TextBox)ctrl).Text = string.Empty;
                strResultMsg += "Textbox: " + ctrl.ID + "<br />";
            }
            else if (ctrl is RadioButtonList)
            {
                ((RadioButtonList)ctrl).ClearSelection();
                strResultMsg += "Radio Button List: " + ctrl.ID + "<br />";
            }
            else if (ctrl is CheckBox)
            {
                ((CheckBox)ctrl).Checked = false;
                strResultMsg += "Checkbox: " + ctrl.ID + "<br />";
            }
            //else { strResultMsg += "No match: " + ctrl.ID + "<br />"; }

            ClearInputs(ctrl.Controls);
        }
    }

    void ResetFields()
    {
        ViewState.Remove("rdoSmokerStu");
        ViewState.Remove("rdoOtherSmokerOKStu");
        ViewState.Remove("smokingHabitsStu");
        ViewState.Remove("rdoInteractionStu");
        ViewState.Remove("rdoHomeEnvironmentStu");
        ViewState.Remove("homeEnvironmentPreferencesStu");
        ViewState.Remove("rdoSmallChildrenStu");
        ViewState.Remove("VegetarianStu");
        ViewState.Remove("GlutenFreeStu");
        ViewState.Remove("DairyFreeStu");
        ViewState.Remove("OtherFoodAllergyStu");
        ViewState.Remove("HalalStu");
        ViewState.Remove("KosherStu");
        ViewState.Remove("NoSpecialDietStu");
        ViewState.Remove("rdoTraveledOutsideCountryStu");
        ViewState.Remove("MusicalInstrumentStu");
        ViewState.Remove("ArtStu");
        ViewState.Remove("TeamSportsStu");
        ViewState.Remove("IndividualSportsStu");
        ViewState.Remove("ListenMusicStu");
        ViewState.Remove("DramaStu");
        ViewState.Remove("WatchMoviesStu");
        ViewState.Remove("SingingStu");
        ViewState.Remove("ShoppingStu");
        ViewState.Remove("ReadBooksStu");
        ViewState.Remove("OutdoorsStu");
        ViewState.Remove("CookingStu");
        ViewState.Remove("PhotographyStu");
        ViewState.Remove("GamingStu");
        ViewState.Remove("activitiesEnjoyedStu");
        ViewState.Remove("anythingElseStu");
        ViewState.Remove("traveledWhereStu");
        ViewState.Remove("allergiesStu");
        ViewState.Remove("healthConditionsStu");

        ViewState.Remove("rdoSmoker");
        ViewState.Remove("rdoOtherSmokerOK");
        ViewState.Remove("smokingHabits");
        ViewState.Remove("rdoInteraction");
        ViewState.Remove("rdoHomeEnvironment");
        ViewState.Remove("SmallChildrenInHome");
        ViewState.Remove("Vegetarian");
        ViewState.Remove("GlutenFree");
        ViewState.Remove("DairyFree");
        ViewState.Remove("OtherFoodAllergy");
        ViewState.Remove("Halal");
        ViewState.Remove("Kosher");
        ViewState.Remove("NoSpecialDiet");
        ViewState.Remove("rdoTraveledOutsideCountry");
        ViewState.Remove("MusicalInstrument");
        ViewState.Remove("Art");
        ViewState.Remove("TeamSports");
        ViewState.Remove("IndividualSports");
        ViewState.Remove("ListenMusic");
        ViewState.Remove("Drama");
        ViewState.Remove("WatchMovies");
        ViewState.Remove("Singing");
        ViewState.Remove("Shopping");
        ViewState.Remove("ReadBooks");
        ViewState.Remove("Outdoors");
        ViewState.Remove("Cooking");
        ViewState.Remove("Photography");
        ViewState.Remove("Gaming");
        ViewState.Remove("activitiesEnjoyed");
        ViewState.Remove("anythingElse");
        ViewState.Remove("homeEnvironmentPreferences");
        ViewState.Remove("traveledWhere");
        ViewState.Remove("smokingGuidelines");
        ViewState.Remove("petsInHome");

        rdoSmokerStu.ClearSelection();
        rdoOtherSmokerOKStu.ClearSelection();
        smokingHabitsStu.Text = "";
        rdoInteractionStu.ClearSelection();
        rdoHomeEnvironmentStu.ClearSelection();
        homeEnvironmentPreferencesStu.Text = "";
        rdoSmallChildrenStu.ClearSelection();
        VegetarianStu.Checked = false;
        GlutenFreeStu.Checked = false;
        DairyFreeStu.Checked = false;
        OtherFoodAllergyStu.Checked = false;
        HalalStu.Checked = false;
        KosherStu.Checked = false;
        NoSpecialDietStu.Checked = false;
        rdoTraveledOutsideCountryStu.ClearSelection();
        MusicalInstrumentStu.Checked = false;
        ArtStu.Checked = false;
        TeamSportsStu.Checked = false;
        IndividualSportsStu.Checked = false;
        ListenMusicStu.Checked = false;
        DramaStu.Checked = false;
        WatchMoviesStu.Checked = false;
        SingingStu.Checked = false;
        ShoppingStu.Checked = false;
        ReadBooksStu.Checked = false;
        OutdoorsStu.Checked = false;
        CookingStu.Checked = false;
        PhotographyStu.Checked = false;
        GamingStu.Checked = false;
        activitiesEnjoyedStu.Text = "";
        anythingElseStu.Text = "";
        traveledWhereStu.Text = "";
        allergiesStu.Text = "";
        healthConditionsStu.Text = "";

        rdoSmoker.ClearSelection();
        rdoOtherSmokerOK.ClearSelection();
        smokingHabits.Text = "";
        rdoInteraction.ClearSelection();
        rdoHomeEnvironment.ClearSelection();
        SmallChildrenInHome.Text = "";
        Vegetarian.Checked = false;
        GlutenFree.Checked = false;
        DairyFree.Checked = false;
        OtherFoodAllergy.Checked = false;
        Halal.Checked = false;
        Kosher.Checked = false;
        NoSpecialDiet.Checked = false;
        rdoTraveledOutsideCountry.ClearSelection();
        MusicalInstrument.Checked = false;
        Art.Checked = false;
        TeamSports.Checked = false;
        IndividualSports.Checked = false;
        ListenMusic.Checked = false;
        Drama.Checked = false;
        WatchMovies.Checked = false;
        Singing.Checked = false;
        Shopping.Checked = false;
        ReadBooks.Checked = false;
        Outdoors.Checked = false;
        Cooking.Checked = false;
        Photography.Checked = false;
        Gaming.Checked = false;
        activitiesEnjoyed.Text = "";
        anythingElse.Text = "";
        homeEnvironmentPreferences.Text = "";
        traveledWhere.Text = "";
        smokingGuidelines.Text = "";
        petsInHome.Text = "";
    }
}