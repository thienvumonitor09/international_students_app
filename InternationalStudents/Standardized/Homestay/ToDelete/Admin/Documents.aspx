﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="Documents.aspx.cs" Inherits="Homestay_Admin_Documents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Document Submissions
</asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <div style="float:left;width:70%;">
        <asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
    </div>
    <div id="divHome" runat="server" style="float:right;margin-right:30px;">
        <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;
    </div>

    <fieldset id="fldStudent" runat="server" visible="false" style="clear:both;">
    <legend>View Student Documents</legend>
            <div style="float:left;width:30%;margin-bottom:10px;">
                <p>Students</p>
                <asp:DropDownList ID="ddlStudents" AutoPostBack="true" runat="server"></asp:DropDownList>
            </div>
    </fieldset>
    <fieldset id="fldFamily" runat="server" visible="false" style="clear:both;">
    <legend>View Family Documents</legend>
           <div style="float:left;width:30%;margin-bottom:10px;">
                <p>Families</p>
                <asp:DropDownList ID="ddlFamilies" AutoPostBack="true" runat="server"></asp:DropDownList>
            </div>
    </fieldset>
    <fieldset>
    <legend>Documents Submitted</legend>
    <div id="divStudentFiles" runat="server" visible="false">
        <p>Letters</p>
        <asp:Literal ID="litLetterFile" runat="server"></asp:Literal>
        <p>Photos</p>
        <asp:Literal ID="litPhotoFile" runat="server"></asp:Literal>
        <p>Agreements<br />
        <span style="font-size:smaller; font-style:italic">In case of multiple uploads the most recent file is listed first</span></p>
        <asp:Literal ID="litAgreementFile" runat="server"></asp:Literal>
    </div>
    <div id="divFamilyFiles" runat="server" visible="false">
    <p>Drivers Licenses</p>
    <asp:Literal ID="litLicenseFile" runat="server"></asp:Literal>
    <p>Waiver of Liability</p>
    <asp:Literal ID="litWaiverFile" runat="server"></asp:Literal>
    </div>
    </fieldset>

</asp:Content>

