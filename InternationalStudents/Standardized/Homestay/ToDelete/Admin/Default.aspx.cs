﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String strLogonUser = Request.ServerVariables["LOGON_USER"];
        Boolean blIsAdmin = false;
        
        if (strLogonUser != "" && strLogonUser != null)
        {
            //If user has not logged out of Family Portal, forms authentication considers the person logged in
            if (strLogonUser.Contains("ccs\\"))
            {
                blIsAdmin = true;
            }
        }
        if (!blIsAdmin)
        {
            Response.Redirect("/Login/Login.aspx?ReturnUrl=%2fHomestay%2fAdmin%2fDefault.aspx");
        }
    }
}