﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_kOriginals_UpdateFamily : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Int32 intResult = 0;
        Int32 intFamilyID = 0;
        String strButtonFace = "";
        String strResultMsg = "";
        String selectedFamily = "";
        String strRelationship = "";

        litResultMsg.Text = "";
        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (strButtonFace == "Return to Admin Home Page")
        {
            Response.Redirect("/Homestay/Admin/Default.aspx");
        }

        selectedFamily = ddlFamilies.SelectedValue;
        //strResultMsg += "Family ID: " + selectedFamily + "<br />";
        if (selectedFamily != "")
        {
            intFamilyID = Convert.ToInt32(selectedFamily);
        }
        Homestay hsInfo = new Homestay();

        if (intFamilyID > 0)
        {
            switch (strButtonFace)
            {
                case "Update Family Home":
                    //Insert HomestayFamilyInfo table
                    String strHomeName = homeName.Text.Replace("'", "''");
                    String strStreetAddress = streetAddress.Text.Replace("'", "''");
                    String strCity = city.Text.Replace("'", "''");
                    String strState = state.Text;
                    String strZIP = ZIP.Text;
                    String strLandLinePhone = landLinePhone.Text;
                    String strInsuranceCompany = insuranceCompanyName.Text.Replace("'", "''");
                    String strInsurancePolicyNumber = insurancePolicyNumber.Text.Replace("'", "''");
                    String strInsuranceAgentName = insuranceAgentName.Text.Replace("'", "''");
                    String strInsuranceAgentPhone = insuranceAgentPhone.Text.Replace("'", "''");
                    String strRoom1 = rdoRoom1.SelectedValue;
                    String strRoom1DateOpen = room1DateOpen.Text.Replace("'", "''");
                    String strRoom1DateClosed = room1DateClosed.Text.Replace("'", "''");
                    String strRoom1Bathroom = rdoRoom1Bathroom.SelectedValue;
                    String strRoom1Occupancy = rdoRoom1Occupancy.Text.Replace("'", "''");
                    String strRoom2 = rdoRoom2.SelectedValue;
                    String strRoom2DateOpen = room2DateOpen.Text.Replace("'", "''");
                    String strRoom2DateClosed = room2DateClosed.Text.Replace("'", "''");
                    String strRoom2Bathroom = rdoRoom2Bathroom.SelectedValue;
                    String strRoom2Occupancy = rdoRoom2Occupancy.Text.Replace("'", "''");
                    String strRoom3 = rdoRoom3.SelectedValue;
                    String strRoom3DateOpen = room3DateOpen.Text.Replace("'", "''");
                    String strRoom3DateClosed = room3DateClosed.Text.Replace("'", "''");
                    String strRoom3Bathroom = rdoRoom3Bathroom.SelectedValue;
                    String strRoom3Occupancy = rdoRoom3Occupancy.Text.Replace("'", "''");
                    String strRoom4 = rdoRoom4.SelectedValue;
                    String strRoom4DateOpen = room4DateOpen.Text.Replace("'", "''");
                    String strRoom4DateClosed = room4DateClosed.Text.Replace("'", "''");
                    String strRoom4Bathroom = rdoRoom4Bathroom.SelectedValue;
                    String strRoom4Occupancy = rdoRoom4Occupancy.Text.Replace("'", "''");
                    //Call update function
                    intResult = hsInfo.UpdateHomestayFamilyHome(intFamilyID, strHomeName, strStreetAddress, strCity, strState, strZIP, strLandLinePhone, 
                        strInsuranceCompany, strInsurancePolicyNumber, strInsuranceAgentName, strInsuranceAgentPhone,
                        strRoom1, strRoom1DateOpen, strRoom1DateClosed, strRoom1Bathroom, strRoom1Occupancy,  
                        strRoom2, strRoom2DateOpen, strRoom2DateClosed, strRoom2Bathroom, strRoom2Occupancy,  
                        strRoom3, strRoom3DateOpen, strRoom3DateClosed, strRoom3Bathroom, strRoom3Occupancy,  
                        strRoom4, strRoom4DateOpen, strRoom4DateClosed, strRoom4Bathroom, strRoom4Occupancy);
                    break;
                case "Update Primary Contact":
                    //Insert HomestayRelatives table
                    String FamilyName = familyName.Text.Replace("'", "''"); 
                    String FirstName = firstName.Text.Replace("'", "''");
                    String MiddleName = middleName.Text.Replace("'", "''");
                    String DateOfBirth = DOB.Text;
                    String Gender = rdoGender.SelectedValue;
                    String DriversLicenseNumber = driversLicenseNumber.Text.Replace("'", "''");
                    String Occupation = occupation.Text.Replace("'", "''");
                    String CellPhone = cellPhone.Text;
                    String WorkPhone = workPhone.Text;
                    String Email = email.Text;
                    //Call update function
                    //Primary Contact
                    if (hdnPrimaryContactID.Value != null && hdnPrimaryContactID.Value != "")
                    {
                        Int32 PrimaryID = Convert.ToInt32(hdnPrimaryContactID.Value);
                        intResult = hsInfo.UpdateHomestayRelatives(PrimaryID, FamilyName, FirstName, Occupation, DateOfBirth, Gender, "Primary Contact", Email, CellPhone, WorkPhone, DriversLicenseNumber, MiddleName);
                    }

                    break;
                case "Update Secondary Contact":

                    String FamilyName2 = familyName2.Text.Replace("'", "''");
                    String FirstName2 = firstName2.Text.Replace("'", "''");
                    if (FamilyName2 != "" && FirstName2 != "")
                    {
                        String MiddleName2 = middleName2.Text.Replace("'", "''");
                        String DateOfBirth2 = DOB2.Text.Replace("'", "''");
                        String Gender2 = rdoGender2.SelectedValue;
                        String DriversLicenseNumber2 = driversLicenseNumber2.Text.Replace("'", "''");
                        String Occupation2 = occupation2.Text.Replace("'", "''");
                        String CellPhone2 = cellPhone2.Text;
                        String WorkPhone2 = workPhone2.Text;
                        String Email2 = email2.Text;
                        //Secondary Contact
                        if (hdnSecondaryContactID.Value != null && hdnSecondaryContactID.Value != "")
                        {
                            Int32 SecondaryID = Convert.ToInt32(hdnSecondaryContactID.Value);
                            intResult = hsInfo.UpdateHomestayRelatives(SecondaryID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
                        }
                        else if (FamilyName2 != "" && FirstName2 != "")
                        {
                            //Insert new relative
                            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
                        }
                    }
                    //Call update function
                    break;
                case "Update Other Adult 1":
                    String FamilyName3 = familyName3.Text.Replace("'", "''");
                    String FirstName3 = firstName3.Text.Replace("'", "''");
                    if (FamilyName3 != "" && FirstName3 != "")
                    {
                        String MiddleName3 = middleName3.Text.Replace("'", "''");
                        String DateOfBirth3 = DOB3.Text.Replace("'", "''");
                        String Gender3 = rdoGender3.SelectedValue;
                        String DriversLicenseNumber3 = driversLicenseNumber3.Text.Replace("'", "''");
                        String Occupation3 = occupation3.Text.Replace("'", "''");
                        String CellPhone3 = cellPhone3.Text;
                        String WorkPhone3 = workPhone3.Text;
                        String Email3 = email3.Text;
                        //Adult 1
                        if (hdnOtherAdult1ID.Value != null && hdnOtherAdult1ID.Value != "")
                        {
                            Int32 Adult1ID = Convert.ToInt32(hdnOtherAdult1ID.Value);
                            intResult = hsInfo.UpdateHomestayRelatives(Adult1ID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
                        }
                        else if (FamilyName3 != "" && FirstName3 != "")
                        {
                            //Insert new relative
                            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
                        }
                    }
                    //Call update function

                    break;
                case "Update Other Adult 2":
                    String FamilyName4 = familyName4.Text;
                    String FirstName4 = firstName4.Text;
                    if (FamilyName4 != "" && FirstName4 != "")
                    {
                        String MiddleName4 = middleName4.Text;
                        String DateOfBirth4 = DOB4.Text;
                        String Gender4 = rdoGender4.SelectedValue;
                        String DriversLicenseNumber4 = driversLicenseNumber4.Text;
                        String Occupation4 = occupation4.Text;
                        String CellPhone4 = cellPhone4.Text;
                        String WorkPhone4 = workPhone4.Text;
                        String Email4 = email4.Text;
                        //Call update function
                        //Adult 2
                        if (hdnOtherAdult2ID.Value != null && hdnOtherAdult2ID.Value != "")
                        {
                            Int32 Adult2ID = Convert.ToInt32(hdnOtherAdult2ID.Value);
                            intResult = hsInfo.UpdateHomestayRelatives(Adult2ID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
                        }
                        else if (FamilyName4 != "" && FirstName4 != "")
                        {
                            //Insert new relative
                            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
                        }
                    }
                    break;
                case "Update Minor Child 1":
                    String FamilyName5 = familyName5.Text.Replace("'", "''");
                    String FirstName5 = firstName5.Text.Replace("'", "''");
                    if (FamilyName5 != "" && FirstName5 != "")
                    {
                        String DateOfBirth5 = DOB5.Text;
                        String Gender5 = rdoGender5.SelectedValue;
                        //Call update function
                        //Minor Child 1
                        if (hdnMinorChild1ID.Value != null && hdnMinorChild1ID.Value != "")
                        {
                            Int32 Child1ID = Convert.ToInt32(hdnMinorChild1ID.Value);
                            intResult = hsInfo.UpdateHomestayRelatives(Child1ID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
                        }
                        else if (FamilyName5 != "" && FirstName5 != "")
                        {
                            //Insert new relative
                            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
                        }
                    }

                    break;
                case "Update Minor Child 2":
                    String FamilyName6 = familyName6.Text.Replace("'", "''");
                    String FirstName6 = firstName6.Text.Replace("'", "''");
                    if (FamilyName6 != "" && FirstName6 != "")
                    {
                        String DateOfBirth6 = DOB6.Text;
                        String Gender6 = rdoGender6.SelectedValue;
                        //Call update function
                        //Minor Child 2
                        if (hdnMinorChild2ID.Value != null && hdnMinorChild2ID.Value != "")
                        {
                            Int32 Child2ID = Convert.ToInt32(hdnMinorChild2ID.Value);
                            intResult = hsInfo.UpdateHomestayRelatives(Child2ID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
                        }
                        else if (FamilyName6 != "" && FirstName6 != "")
                        {
                            //Insert new relative
                            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
                        }
                    }

                    break;
                case "Update Minor Child 3":
                    String FamilyName7 = familyName7.Text.Replace("'", "''");
                    String FirstName7 = firstName7.Text.Replace("'", "''");
                    if (FamilyName7 != "" && FirstName7 != "")
                    {
                        String DateOfBirth7 = DOB7.Text;
                        String Gender7 = rdoGender7.SelectedValue;
                        //Call update function
                        //Minor Child 3
                        if (hdnMinorChild3ID.Value != null && hdnMinorChild3ID.Value != "")
                        {
                            Int32 Child3ID = Convert.ToInt32(hdnMinorChild3ID.Value);
                            intResult = hsInfo.UpdateHomestayRelatives(Child3ID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
                        }
                        else if (FamilyName7 != "" && FirstName7 != "")
                        {
                            //Insert new relative
                            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
                        }
                    }

                    break;
                case "Update Minor Child 4":
                    String FamilyName8 = familyName8.Text.Replace("'", "''");
                    String FirstName8 = firstName8.Text.Replace("'", "''");
                    if (FamilyName8 != "" && FirstName8 != "")
                    {
                        String DateOfBirth8 = DOB8.Text;
                        String Gender8 = rdoGender8.SelectedValue;
                        //Call update function
                        //Minor Child 4
                        if (hdnMinorChild4ID.Value != null && hdnMinorChild4ID.Value != "")
                        {
                            Int32 Child4ID = Convert.ToInt32(hdnMinorChild4ID.Value);
                            intResult = hsInfo.UpdateHomestayRelatives(Child4ID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
                        }
                        else if (FamilyName8 != "" && FirstName8 != "")
                        {
                            //Insert new relative
                            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
                        }
                    }
                    break;
            }
            //strResultMsg += "intResult: " + intResult + "<br />";
            if (intResult != 0) { strResultMsg += "Your update was successful!<br />"; }
            ResetFields();
            //Populate fields
            //Base info
            DataTable dtFamily = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
            if (dtFamily != null)
            {
                if (dtFamily.Rows.Count > 0)
                {
                    homeName.Text = dtFamily.Rows[0]["homeName"].ToString();
                    streetAddress.Text = dtFamily.Rows[0]["streetAddress"].ToString();
                    city.Text = dtFamily.Rows[0]["city"].ToString();
                    state.Text = dtFamily.Rows[0]["state"].ToString();
                    ZIP.Text = dtFamily.Rows[0]["ZIP"].ToString();
                    landLinePhone.Text = dtFamily.Rows[0]["landlinePhone"].ToString();
                    insuranceCompanyName.Text = dtFamily.Rows[0]["insuranceCompanyName"].ToString();
                    insurancePolicyNumber.Text = dtFamily.Rows[0]["insurancePolicyNumber"].ToString();
                    insuranceAgentName.Text = dtFamily.Rows[0]["insuranceAgentName"].ToString();
                    insuranceAgentPhone.Text = dtFamily.Rows[0]["insuranceAgentPhone"].ToString();
                    rdoRoom1.SelectedValue = dtFamily.Rows[0]["room1Level"].ToString();
                    room1DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room1DateOpen"]).ToShortDateString();
                    room1DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room1DateClosed"]).ToShortDateString();
                    rdoRoom1Bathroom.SelectedValue = dtFamily.Rows[0]["room1Bath"].ToString();
                    rdoRoom1Occupancy.Text = dtFamily.Rows[0]["room1Occupancy"].ToString();
                    rdoRoom2.SelectedValue = dtFamily.Rows[0]["room2Level"].ToString();
                    room2DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room2DateOpen"]).ToShortDateString();
                    room2DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room2DateClosed"]).ToShortDateString();
                    rdoRoom2Bathroom.SelectedValue = dtFamily.Rows[0]["room2Bath"].ToString();
                    rdoRoom2Occupancy.Text = dtFamily.Rows[0]["room2Occupancy"].ToString();
                    rdoRoom3.SelectedValue = dtFamily.Rows[0]["room3Level"].ToString();
                    room3DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room3DateOpen"]).ToShortDateString();
                    room3DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room3DateClosed"]).ToShortDateString();
                    rdoRoom3Bathroom.SelectedValue = dtFamily.Rows[0]["room3Bath"].ToString();
                    rdoRoom3Occupancy.Text = dtFamily.Rows[0]["room3Occupancy"].ToString();
                    rdoRoom4.SelectedValue = dtFamily.Rows[0]["room4Level"].ToString();
                    room4DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room4DateOpen"]).ToShortDateString();
                    room4DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room4DateClosed"]).ToShortDateString();
                    rdoRoom4Bathroom.SelectedValue = dtFamily.Rows[0]["room4Bath"].ToString();
                    rdoRoom4Occupancy.Text = dtFamily.Rows[0]["room4Occupancy"].ToString();
                }
            }
            //Relatives
            DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
            if (dtRelatives != null)
            {
                if (dtRelatives.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in dtRelatives.Rows)
                    {
                        strRelationship = dtRow["relationship"].ToString().Trim();
                        //strResultMsg += "Relationship: " + strRelationship + "<br />";
                        switch (strRelationship)
                        {
                            case "Primary Contact":
                                familyName.Text = dtRow["familyName"].ToString().Trim();
                                firstName.Text = dtRow["firstName"].ToString().Trim();
                                middleName.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone.Text = dtRow["workPhone"].ToString().Trim();
                                email.Text = dtRow["email"].ToString().Trim();
                                hdnPrimaryContactID.Value = dtRow["id"].ToString();
                                break;
                            case "Secondary Contact":
                                familyName2.Text = dtRow["familyName"].ToString().Trim();
                                firstName2.Text = dtRow["firstName"].ToString().Trim();
                                middleName2.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender2.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB2.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber2.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation2.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone2.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone2.Text = dtRow["workPhone"].ToString().Trim();
                                email2.Text = dtRow["email"].ToString().Trim();
                                hdnSecondaryContactID.Value = dtRow["id"].ToString();
                                break;
                            case "Other Adult 1":
                                familyName3.Text = dtRow["familyName"].ToString().Trim();
                                firstName3.Text = dtRow["firstName"].ToString().Trim();
                                middleName3.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender3.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB3.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber3.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation3.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone3.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone3.Text = dtRow["workPhone"].ToString().Trim();
                                email3.Text = dtRow["email"].ToString().Trim();
                                hdnOtherAdult1ID.Value = dtRow["id"].ToString();
                                break;
                            case "Other Adult 2":
                                familyName4.Text = dtRow["familyName"].ToString().Trim();
                                firstName4.Text = dtRow["firstName"].ToString().Trim();
                                middleName4.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender4.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB4.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber4.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation4.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone4.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone4.Text = dtRow["workPhone"].ToString().Trim();
                                email4.Text = dtRow["email"].ToString().Trim();
                                hdnOtherAdult2ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 1":
                                familyName5.Text = dtRow["familyName"].ToString().Trim();
                                firstName5.Text = dtRow["firstName"].ToString().Trim();
                                DOB5.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender5.Text = dtRow["gender"].ToString().Trim();
                                hdnMinorChild1ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 2":
                                familyName6.Text = dtRow["familyName"].ToString().Trim();
                                firstName6.Text = dtRow["firstName"].ToString().Trim();
                                DOB6.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender6.Text = dtRow["gender"].ToString().Trim();
                                hdnMinorChild2ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 3":
                                familyName7.Text = dtRow["familyName"].ToString().Trim();
                                firstName7.Text = dtRow["firstName"].ToString().Trim();
                                DOB7.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender7.Text = dtRow["gender"].ToString().Trim();
                                hdnMinorChild3ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 4":
                                familyName8.Text = dtRow["familyName"].ToString().Trim();
                                firstName8.Text = dtRow["firstName"].ToString().Trim();
                                DOB8.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender8.Text = dtRow["gender"].ToString().Trim();
                                hdnMinorChild4ID.Value = dtRow["id"].ToString();
                                break;
                        }
                    }
                }
                else { strResultMsg += "No HomestayRelatives records returned.<br />"; }
            }
            else { strResultMsg += "No HomestayRelatives records returned.<br />"; }  
        }

      
        //Get families into a drop-down list; 
        DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
        if (dtFamilies != null)
        {
            if (dtFamilies.Rows.Count > 0)
            {
                ddlFamilies.Items.Clear();
                ListItem LI;
                LI = new ListItem();
                LI.Value = "0";
                LI.Text = "-- Select One --";
                ddlFamilies.Items.Add(LI);
                foreach (DataRow dr in dtFamilies.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["id"].ToString();
                    LI.Text = dr["homeName"].ToString();
                    ddlFamilies.Items.Add(LI);
                }
                if (selectedFamily != "") { ddlFamilies.SelectedValue = selectedFamily; }
            }
            else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtFamilies is null.<br />"; }

        litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";

    }

    private void ResetFields()
    {
        homeName.Text = "";
        streetAddress.Text = "";
        city.Text = "";
        state.Text = "";
        ZIP.Text = "";
        landLinePhone.Text = "";
        insuranceCompanyName.Text = "";
        insurancePolicyNumber.Text = "";
        insuranceAgentName.Text = "";
        insuranceAgentPhone.Text = "";
        familyName.Text = "";
        firstName.Text = "";
        middleName.Text = "";
        rdoGender.SelectedValue = "";
        DOB.Text = "";
        driversLicenseNumber.Text = "";
        occupation.Text = "";
        cellPhone.Text = "";
        workPhone.Text = "";
        email.Text = "";
        hdnPrimaryContactID.Value = "";
        familyName2.Text = "";
        firstName2.Text = "";
        middleName2.Text = "";
        rdoGender2.SelectedValue = "";
        DOB2.Text = "";
        driversLicenseNumber2.Text = "";
        occupation2.Text = "";
        cellPhone2.Text = "";
        workPhone2.Text = "";
        email2.Text = "";
        hdnSecondaryContactID.Value = "";
        familyName3.Text = "";
        firstName3.Text = "";
        middleName3.Text = "";
        rdoGender3.SelectedValue = "";
        DOB3.Text = "";
        driversLicenseNumber3.Text = "";
        occupation3.Text = "";
        cellPhone3.Text = "";
        workPhone3.Text = "";
        email3.Text = "";
        hdnOtherAdult1ID.Value = "";
        familyName4.Text = "";
        firstName4.Text = "";
        middleName4.Text = "";
        rdoGender4.SelectedValue = "";
        DOB4.Text = "";
        driversLicenseNumber4.Text = "";
        occupation4.Text = "";
        cellPhone4.Text = "";
        workPhone4.Text = "";
        email4.Text = "";
        hdnOtherAdult2ID.Value = "";
        familyName5.Text = "";
        firstName5.Text = "";
        DOB5.Text = "";
        rdoGender5.Text = "";
        hdnMinorChild1ID.Value = "";
        familyName6.Text = "";
        firstName6.Text = "";
        DOB6.Text = "";
        rdoGender6.Text = "";
        hdnMinorChild2ID.Value = "";
        familyName7.Text = "";
        firstName7.Text = "";
        DOB7.Text = "";
        rdoGender7.Text = "";
        hdnMinorChild3ID.Value = "";
        familyName8.Text = "";
        firstName8.Text = "";
        DOB8.Text = "";
        rdoGender8.Text = "";
        hdnMinorChild4ID.Value = "";
    }
}