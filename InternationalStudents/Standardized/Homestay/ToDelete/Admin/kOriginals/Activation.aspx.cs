﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_kOriginals_Activation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        String selectedStudent = "0";
        String selectedFamily = "0";
        String selectedStudentStatus = "";
        String selectedFamilyStatus = "";
        String currentStudentActive = "";
        String currentFamilyActive = "";
        String familyIsActive = "";
        String studentIsActive = "";
        String terminationDate = "";
        String deactivationDate = "";
        String strResultMsg = "";
        Int32 intResult = 0;
        litResultMsg.Text = "";

        Homestay hsInfo = new Homestay();
        DataTable dtStudents = hsInfo.GetHomestayInternationalStudents();
        DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDL");
        String strButtonFace = "";

        if (IsPostBack)
        {
            selectedStudent = ddlAllStudents.SelectedValue;
            selectedFamily = ddlAllFamilies.SelectedValue;
            selectedStudentStatus = ddlStudentStatus.SelectedItem.Text;
            studentIsActive = rdoIsStudentActive.SelectedValue;
            selectedFamilyStatus = ddlFamilyStatus.SelectedItem.Text;
            familyIsActive = rdoIsFamilyActive.SelectedValue;
            terminationDate = dateTerminated.Text;
            deactivationDate = dateInactive.Text;
            //Process request
            if (Request.Form["btnSubmit"] != null)
            {
                strButtonFace = Request.Form["btnSubmit"].ToString();
            }
            //strResultMsg += "Button Face: " + strButtonFace + "<br />";
            if (strButtonFace != "")
            {

                switch (strButtonFace)
                {
                    case "Return to Admin Home Page":
                        {
                            Response.Redirect("/Homestay/Admin/Default.aspx");
                            break;
                        }
                    case "Update Student":
                        strResultMsg += "Update Student Status value: " + rdoIsStudentActive.SelectedValue + "; Student: " + selectedStudent + "<br />";
                        //Is Active?
                        intResult = hsInfo.UpdateIsActiveBitFlagHomestayStudentInfo(Convert.ToInt32(selectedStudent), studentIsActive);
                        if (deactivationDate != "")
                        {
                            //Deactivation Date
                            intResult = hsInfo.UpdateDateInactiveHomestayStudentInfo(Convert.ToInt32(selectedStudent), deactivationDate);
                        }
                        //Student Status
                        intResult = hsInfo.UpdateHomestayStudentStatus(0, Convert.ToInt32(selectedStudent), selectedStudentStatus);
                        break;
                    case "Update Family":
                        intResult = hsInfo.UpdateIsActiveBitFlagHomestayFamilyInfo(Convert.ToInt32(selectedFamily), familyIsActive);
                        //Is Active?
                        if (terminationDate != "")
                        {
                            //Termination Date
                            intResult = hsInfo.UpdateDateTerminatedHomestayFamilyInfo(Convert.ToInt32(selectedFamily), terminationDate);
                        }
                        if (selectedFamilyStatus != "")
                        {
                            //Family Status
                            intResult = hsInfo.UpdateFamilyStatus(Convert.ToInt32(selectedFamily), selectedFamilyStatus);
                        }
                        break;
                }
                //need better success/failure messaging
                if (intResult != 0) { strResultMsg += "Your submission was successful!<br />"; }
            }

            //Fetch status info
            if (selectedStudent != "" && selectedStudent != null && selectedStudent != "0")
            {
                DataTable dtStudentInfo = hsInfo.GetOneHomestayStudent(Convert.ToInt32(selectedStudent));
                if (dtStudentInfo != null)
                {
                    if (dtStudentInfo.Rows.Count > 0)
                    {
                        //Set active indicator
                        currentStudentActive = dtStudentInfo.Rows[0]["IsActive"].ToString();
                        try
                        {
                            rdoIsStudentActive.SelectedValue = currentStudentActive;
                        }
                        catch
                        {
                            strResultMsg = "Student is active? " + currentStudentActive;
                        }
                        //Set homestay status
                        homestayStatus.Text = dtStudentInfo.Rows[0]["homestayStatus"].ToString().Trim();
                        //deactivate date
                        if (!DBNull.Value.Equals(dtStudentInfo.Rows[0]["dateInactive"]))
                        {
                            dateInactive.Text = Convert.ToDateTime(dtStudentInfo.Rows[0]["dateInactive"]).ToShortDateString();
                        }

                    }
                    else { strResultMsg += "Error: dtStudentInfo has no rows.<br />"; }
                }
                else { strResultMsg += "Error: dtStudentInfo is null.<br />"; }
            }

            if (selectedFamily != "" && selectedFamily != null && selectedFamily != "0")
            {
                DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(Convert.ToInt32(selectedFamily), "", "ID");
                if (dtFamilyInfo != null)
                {
                    if (dtFamilyInfo.Rows.Count > 0)
                    {
                        //Set active indicator
                        Boolean blIsFamilyActive = Convert.ToBoolean(dtFamilyInfo.Rows[0]["IsActive"]);
                        if (blIsFamilyActive) { currentFamilyActive = "1"; } else { currentFamilyActive = "0"; }
                        rdoIsFamilyActive.SelectedValue = currentFamilyActive;
                        //Terminate date
                        if (!DBNull.Value.Equals(dtFamilyInfo.Rows[0]["dateTerminated"]))
                        {
                            dateTerminated.Text = Convert.ToDateTime(dtFamilyInfo.Rows[0]["dateTerminated"]).ToShortDateString();
                        }
                        //Set homestay status
                        familyStatus.Text = dtFamilyInfo.Rows[0]["familyStatus"].ToString().Trim();
                    }
                    else { strResultMsg += "Error: dtFamilyInfo has no rows.<br />"; }
                }
                else { strResultMsg += "Error: dtFamilyInfo is null.<br />"; }
            }
        }
        //Load the DDLs
        ddlAllStudents.Items.Clear();
        ListItem LI;
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlAllStudents.Items.Add(LI);
        if (dtStudents != null)
        {
            if (dtStudents.Rows.Count > 0)
            {
                foreach (DataRow dr in dtStudents.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["id"].ToString();
                    LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                    ddlAllStudents.Items.Add(LI);
                }
                ddlAllStudents.SelectedValue = selectedStudent;
            }
            else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudents is null.<br />"; }



        ddlAllFamilies.Items.Clear();
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlAllFamilies.Items.Add(LI);
        if (dtFamilies != null)
        {
            if (dtFamilies.Rows.Count > 0)
            {
                foreach (DataRow dr in dtFamilies.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["id"].ToString();
                    LI.Text = dr["homeName"].ToString();
                    ddlAllFamilies.Items.Add(LI);
                }
                ddlAllFamilies.SelectedValue = selectedFamily;
            }
            else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtFamilies is null.<br />"; }

        DataTable dtStudentStatus = hsInfo.GetHomestayStatusValues("Student");
        ddlStudentStatus.Items.Clear();
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlStudentStatus.Items.Add(LI);
        if (dtStudentStatus != null)
        {
            if (dtStudentStatus.Rows.Count > 0)
            {
                foreach (DataRow dr in dtStudentStatus.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["id"].ToString();
                    LI.Text = dr["statusDescription"].ToString().Trim();
                    ddlStudentStatus.Items.Add(LI);
                }
            }
            else { strResultMsg += "Error: dtStudentStatus has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudentStatus is null.<br />"; }

        DataTable dtFamilyStatus = hsInfo.GetHomestayStatusValues("Family");
        ddlFamilyStatus.Items.Clear();
        LI = new ListItem();
        LI.Value = "0";
        LI.Text = "-- Select One --";
        ddlFamilyStatus.Items.Add(LI);
        if (dtFamilyStatus != null)
        {
            if (dtFamilyStatus.Rows.Count > 0)
            {
                foreach (DataRow dr in dtFamilyStatus.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["id"].ToString();
                    LI.Text = dr["statusDescription"].ToString().Trim();
                    ddlFamilyStatus.Items.Add(LI);
                }
            }
            else { strResultMsg += "Error: dtFamilyStatus has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtFamilyStatus is null.<br />"; }


        litResultMsg.Text += "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }
}