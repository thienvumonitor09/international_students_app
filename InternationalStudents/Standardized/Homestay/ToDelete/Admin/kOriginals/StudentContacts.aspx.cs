﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_kOriginals_UpdateStudent : System.Web.UI.Page
{
    /************************************************************************************
    *  address codes:  1:  student's permanent address
    *                  2:  student's parent's address (if different)
    *                  3:  student's agency address (if any)
    *                  4:  student's relative address (if in US)
    *                  5:  student's emergency contact address
    *  Use applicant ID to match to student
    * ********************************************************************************/

    protected void Page_Load(object sender, EventArgs e)
    {
        Int32 applicantID = 0;
        String strBlank = "";
        String strSubmitMsg = "";
        String strResultMsg = "";
        Boolean blSuccess = false;
        char speakEnglish = '0';
        String strSelectedStudent = "";
        String strButtonFace = "";

        Homestay hsInfo = new Homestay();
        litResultMsg.Text = "";
        //load page
        strSelectedStudent = ddlStudents.SelectedValue;
        //strResultMsg += "Selected student: " + strSelectedStudent + "<br />";
        if (strSelectedStudent != "")
        {
            applicantID = Convert.ToInt32(strSelectedStudent);
        }
        DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
        if (dtStudents != null)
        {
            if (dtStudents.Rows.Count > 0)
            {
                ddlStudents.Items.Clear();
                ListItem LI;
                LI = new ListItem();
                LI.Value = "0";
                LI.Text = "-- Select One --";
                ddlStudents.Items.Add(LI);
                foreach (DataRow dr in dtStudents.Rows)
                {
                    LI = new ListItem();
                    LI.Value = dr["applicantID"].ToString();
                    LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                    ddlStudents.Items.Add(LI);
                }
                if (strSelectedStudent != "") { ddlStudents.SelectedValue = strSelectedStudent; }
            }
            else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudents is null.<br />"; }
        if (Request.Form["btnSubmit"] != null) {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (strButtonFace == "Return to Admin Home Page")
        {
            Response.Redirect("/Homestay/Admin/Default.aspx");
        }

        InternationalStudent myIStudent = new InternationalStudent();
        if (applicantID != 0)
        {
            switch (strButtonFace)
            {
                case "Update Permanent Home":
                {
                    Int32 intStudentID = 0;
                    if (hdnStudentID.Value != null && hdnStudentID.Value != "") { intStudentID = Convert.ToInt32(hdnStudentID.Value); }
                    //Student contact information - address code 1
                    strSubmitMsg = myIStudent.UpdateContactInfo(intStudentID, txtStudentName.Text.Trim().Replace("'", "''"), strBlank, txtPermAddress.Text.Trim().Replace("'", "''"), txtPermCity.Text.Trim().Replace("'", "''"), txtPermStateP.Text.Trim().Replace("'", "''"),
                        txtPermZip.Text.Trim().Replace("'", "''"), txtPermHomeCountry.Text.Trim().Replace("'", "''"), txtPermPhone.Text.Trim().Replace("'", "''"), strBlank, txtPermEmail.Text.Trim().Replace("'", "''"), strBlank, speakEnglish);
                    if (strSubmitMsg == "OK contact info") { blSuccess = true; }
                    break;
                }
                case "Update Parents":
                {
                    Int32 intParentsID = 0;
                    if (hdnParentsID.Value != null && hdnParentsID.Value != "") { intParentsID = Convert.ToInt32(hdnParentsID.Value); }
                    if (intParentsID == 0)
                    {
                        //Insert new agent record - address code 3
                        strSubmitMsg = myIStudent.InsertContactInfo(applicantID, 2, txtParentsName.Text.Trim().Replace("'", "''"), strBlank, txtParentsStreet.Text.Trim().Replace("'", "''"), txtParentsCity.Text.Trim().Replace("'", "''"), txtParentsState.Text.Trim().Replace("'", "''"),
                            txtParentsZip.Text.Trim().Replace("'", "''"), txtParentsCountry.Text.Trim().Replace("'", "''"), txtParentsPhone.Text.Trim().Replace("'", "''"), strBlank, txtParentsEmail.Text.Trim().Replace("'", "''"), strBlank, speakEnglish);
                    }
                    else
                    {
                        // Parent contact information - address code 2
                        strSubmitMsg = myIStudent.UpdateContactInfo(intParentsID, txtParentsName.Text.Trim().Replace("'", "''"), strBlank, txtParentsStreet.Text.Trim().Replace("'", "''"), txtParentsCity.Text.Trim().Replace("'", "''"), txtParentsState.Text.Trim().Replace("'", "''"),
                            txtParentsZip.Text.Trim().Replace("'", "''"), txtParentsCountry.Text.Trim().Replace("'", "''"), txtParentsPhone.Text.Trim().Replace("'", "''"), strBlank, txtParentsEmail.Text.Trim().Replace("'", "''"), strBlank, speakEnglish);
                    }
                    if (strSubmitMsg == "OK contact info") { blSuccess = true; }
                    break;
                }
                case "Update Agent":
                {
                    Int32 intAgencyID = 0;
                    if (hdnAgencyID.Value != null && hdnAgencyID.Value != "") { intAgencyID = Convert.ToInt32(hdnAgencyID.Value); }
                    if (intAgencyID == 0)
                    {
                        //Insert new agent record - address code 3
                        strSubmitMsg = myIStudent.InsertContactInfo(applicantID, 3, txtAgencyContact.Text.Trim().Replace("'", "''"), txtAgencyName.Text.Trim().Replace("'", "''"), txtAgencyAddress.Text.Trim().Replace("'", "''"), txtAgencyCity.Text.Trim().Replace("'", "''"), txtAgencyState.Text.Trim().Replace("'", "''"),
                            txtAgencyZip.Text.Trim().Replace("'", "''"), txtAgencyCountry.Text.Trim().Replace("'", "''"), txtAgencyPhone.Text.Trim().Replace("'", "''"), strBlank, txtAgencyEmail.Text.Trim().Replace("'", "''"), strBlank, speakEnglish);
                    }
                    else
                    {
                        //Update existing agency address - address code 3
                        strSubmitMsg = myIStudent.UpdateContactInfo(intAgencyID, txtAgencyContact.Text.Trim().Replace("'", "''"), txtAgencyName.Text.Trim().Replace("'", "''"), txtAgencyAddress.Text.Trim().Replace("'", "''"), txtAgencyCity.Text.Trim().Replace("'", "''"), txtAgencyState.Text.Trim().Replace("'", "''"),
                            txtAgencyZip.Text.Trim().Replace("'", "''"), txtAgencyCountry.Text.Trim().Replace("'", "''"), txtAgencyPhone.Text.Trim().Replace("'", "''"), strBlank, txtAgencyEmail.Text.Trim().Replace("'", "''"), strBlank, speakEnglish);
                    }
                    if (strSubmitMsg == "OK contact info") { blSuccess = true; }
                    break;
                }
                case "Update Emergency Contact":
                {
                    Int32 intEmergencyID = 0;
                    if (hdnEmergencyID.Value != null && hdnEmergencyID.Value != "") { intEmergencyID = Convert.ToInt32(hdnEmergencyID.Value); }
                    if (chkSpeakEnglish.Checked) { speakEnglish = '1'; }
                    //Emergency contact information - address code 5
                    strSubmitMsg = myIStudent.UpdateContactInfo(intEmergencyID, txtEmergName.Text.Trim().Replace("'", "''"), strBlank, strBlank, strBlank, strBlank, strBlank, strBlank, txtEmergPhone.Text.Trim().Replace("'", "''"),
                        strBlank, txtEmergEmail.Text.Trim().Replace("'", "''"), txtEmergRelationship.Text.Trim().Replace("'", "''"), speakEnglish);
                    if (strSubmitMsg == "OK contact info") { blSuccess = true; }
                    break;
                }
            }
            if (blSuccess)
            {
                strResultMsg += "Your contact information has been successfully updated!<br />";
            }
            else if (strButtonFace != "")
            {
                strResultMsg += "Error " + strButtonFace + ": " + strSubmitMsg + "<br />";
            }
        }
        //Clear the decks - .Net is SO helpful...
        txtStudentName.Text = "";
        txtPermAddress.Text = "";
        txtPermCity.Text = "";
        txtPermStateP.Text = "";
        txtPermHomeCountry.Text = "";
        txtPermPhone.Text = "";
        txtPermEmail.Text = "";
        txtAgencyName.Text = "";
        txtAgencyContact.Text = "";
        txtAgencyAddress.Text = ""; 
        txtAgencyCity.Text = "";
        txtAgencyState.Text = "";
        txtAgencyCountry.Text = "";
        txtAgencyPhone.Text = "";
        txtAgencyEmail.Text = "";
        txtParentsName.Text = "";
        txtParentsStreet.Text = ""; 
        txtParentsCity.Text = "";
        txtParentsState.Text = "";
        txtParentsCountry.Text = "";
        txtParentsPhone.Text = "";
        txtParentsEmail.Text = "";
        txtEmergName.Text = "";
        txtEmergRelationship.Text = "";
        txtEmergPhone.Text = "";
        txtEmergEmail.Text = "";
        hdnAgencyID.Value = "";
        hdnEmergencyID.Value = "";
        hdnParentsID.Value = "";
        hdnStudentID.Value = "";
        chkSpeakEnglish.Checked = false;

        //Get Student contacts info and load form
        if (applicantID != 0)
        {
            DataTable dtStudent = hsInfo.GetOneInternationalStudent("", applicantID, "", "", "", "");
            if (dtStudent != null)
            {
                if (dtStudent.Rows.Count > 0)
                {
                    txtStudentName.Text = dtStudent.Rows[0]["addresseeName"].ToString();
                    txtPermAddress.Text = dtStudent.Rows[0]["Addr"].ToString();
                    txtPermCity.Text = dtStudent.Rows[0]["City"].ToString();
                    txtPermStateP.Text = dtStudent.Rows[0]["StateProv"].ToString();
                    txtPermHomeCountry.Text = dtStudent.Rows[0]["Country"].ToString();
                    txtPermPhone.Text = dtStudent.Rows[0]["Phone"].ToString();
                    txtPermEmail.Text = dtStudent.Rows[0]["Email"].ToString();
                    //Get the ContactInformation table id for this record from the extended view
                    hdnStudentID.Value = dtStudent.Rows[0]["contactInfoID"].ToString();
                    Boolean blUseAgency = Convert.ToBoolean(dtStudent.Rows[0]["useAgency"]);
                    if (blUseAgency)
                    {
                        DataTable dtAgency = hsInfo.GetOneContact(3, applicantID);
                        if (dtAgency != null)
                        {
                            if (dtAgency.Rows.Count > 0)
                            {
                                txtAgencyName.Text = dtAgency.Rows[0]["agencyName"].ToString();
                                txtAgencyContact.Text = dtAgency.Rows[0]["addresseeName"].ToString();
                                txtAgencyAddress.Text = dtAgency.Rows[0]["Addr"].ToString();
                                txtAgencyCity.Text = dtAgency.Rows[0]["City"].ToString();
                                txtAgencyState.Text = dtAgency.Rows[0]["StateProv"].ToString();
                                txtAgencyCountry.Text = dtAgency.Rows[0]["Country"].ToString();
                                txtAgencyPhone.Text = dtAgency.Rows[0]["Phone"].ToString();
                                txtAgencyEmail.Text = dtAgency.Rows[0]["Email"].ToString();
                                hdnAgencyID.Value = dtAgency.Rows[0]["id"].ToString();
                            }
                            else { hdnAgencyID.Value = "0"; }
                        }
                        else { hdnAgencyID.Value = "0"; }
                    }
                    else { hdnAgencyID.Value = "0"; }

                }
            }
            DataTable dtParents = hsInfo.GetOneContact(2, applicantID);
            if (dtParents != null)
            {
                if (dtParents.Rows.Count > 0)
                {
                    //Populate fields
                    txtParentsName.Text = dtParents.Rows[0]["addresseeName"].ToString();
                    txtParentsStreet.Text = dtParents.Rows[0]["Addr"].ToString();
                    txtParentsCity.Text = dtParents.Rows[0]["City"].ToString();
                    txtParentsState.Text = dtParents.Rows[0]["StateProv"].ToString();
                    txtParentsCountry.Text = dtParents.Rows[0]["Country"].ToString();
                    txtParentsPhone.Text = dtParents.Rows[0]["Phone"].ToString();
                    txtParentsEmail.Text = dtParents.Rows[0]["Email"].ToString();
                    hdnParentsID.Value = dtParents.Rows[0]["id"].ToString();
                }
                else { hdnParentsID.Value = "0"; }
            }
            else { hdnParentsID.Value = "0"; }

            DataTable dtEmergency = hsInfo.GetOneContact(5, applicantID);
            if (dtEmergency != null)
            {
                if (dtEmergency.Rows.Count > 0)
                {
                    //Populate fields
                    txtEmergName.Text = dtEmergency.Rows[0]["addresseeName"].ToString();
                    txtEmergRelationship.Text = dtEmergency.Rows[0]["Relationship"].ToString();
                    txtEmergPhone.Text = dtEmergency.Rows[0]["Phone"].ToString();
                    txtEmergEmail.Text = dtEmergency.Rows[0]["Email"].ToString();
                    hdnEmergencyID.Value = dtEmergency.Rows[0]["id"].ToString();
                    Boolean blSpeakEnglish = Convert.ToBoolean(dtEmergency.Rows[0]["speakEnglish"]);
                    if (blSpeakEnglish) { chkSpeakEnglish.Checked = true; }
                }
            }
        }
        litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }
}