﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="StudentContacts.aspx.cs" Inherits="Homestay_Admin_kOriginals_UpdateStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Manage Student Information
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Student Profile</h2>

<h2>Manage Student Contact Information</h2>
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
<h4>Select a Homestay Student</h4>
<div style="padding-bottom:50px;">
<div id="divHome" runat="server" style="float:right;margin-right:30px;">
    <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;
</div>
<asp:DropDownList ID="ddlStudents" AutoPostBack="true" runat="server"></asp:DropDownList>
</div>
<div id="accordion">
    <h3 class="accordion accordion-toggle default">Student</h3>
    <div class="panel accordion-content">
        <fieldset>
        <legend>Permanent Home Country Information</legend>
        <p>
            <label for="txtStudentName" class="fieldTitle">First and Last Names</label><br />
            <asp:TextBox Width="300px" ID="txtStudentName" runat="server"  class="required" MaxLength="50"></asp:TextBox>
        </p>
        <p>
            <label for="txtPermAddress" class="fieldTitle">Street Address</label><br />
            <asp:TextBox ID="txtPermAddress" runat="server" Width="400px"  MaxLength="75"></asp:TextBox>
        </p>
        <table style="width:100%;">
            <tr>
                <td><label for="txtPermCity" class="fieldTitle">City</label></td>
                <td><label for="txtPermStateP" class="fieldTitle">State/Province</label></td>
                <td><label for="txtPermZip" class="fieldTitle">Zip/Postal Code</label></td>
            </tr>
            <tr>
                <td><asp:TextBox ID="txtPermCity" runat="server" MaxLength="30" Width="250px"></asp:TextBox></td>
                <td><asp:TextBox ID="txtPermStateP" Width="250px" runat="server" MaxLength="30"></asp:TextBox></td>
                <td><asp:TextBox ID="txtPermZip" Width="100px" runat="server" MaxLength="15"></asp:TextBox></td>
            </tr>
        </table>
        <p>
            <label for="txtPermHomeCountry" class="fieldTitle">Home Country</label><br />
            <asp:TextBox ID="txtPermHomeCountry" runat="server" ></asp:TextBox>
        </p>
        <p>
            <label for="txtPermPhone" class="fieldTitle">Home Country Phone</label><br />
            <asp:TextBox ID="txtPermPhone" Width="150px" runat="server"></asp:TextBox>
        </p>
        <p>
            <label for="txtPermEmail" class="fieldTitle">Home Country Email</label><br />
            <asp:TextBox ID="txtPermEmail" Width="200px" runat="server" MaxLength="75"></asp:TextBox>
        </p>
        <div>
            <input type="submit" class="button" name="btnSubmit" id="btnPermHome" value="Update Permanent Home" />
            <asp:HiddenField ID="hdnStudentID" runat="server" />
        </div>

        </fieldset>
    </div>

    <h3 class="accordion accordion-toggle">Agent</h3>
    <div class="panel accordion-content">
        <p>
            <label for="txtAgencyName" class="fieldTitle">Agency Name</label><br />
            <asp:TextBox ID="txtAgencyName" runat="server" Width="300px" MaxLength="75"></asp:TextBox>
        </p>

        <p>
            <label for="txtAgencyContact" class="fieldTitle">Agency Contact Person</label><br />
            <asp:TextBox ID="txtAgencyContact" runat="server" Width="300px" MaxLength="75"></asp:TextBox>
        </p>

        <p>
            <label for="txtAgencyAddress" class="fieldTitle">Agency Street Address</label><br />
            <asp:TextBox ID="txtAgencyAddress" runat="server" Width="400px" MaxLength="75"></asp:TextBox>
        </p>

        <table style="width:100%;">
            <tr>
                <td><label for="txtAgencyCity" class="fieldTitle">Agency City</label></td>
                <td><label for="txtAgencyState" class="fieldTitle">Agency State/Province</label></td>
                <td><label for="txtAgencyZip" class="fieldTitle">Agency Zip/Postal Code</label></td>
            </tr>
            <tr>
                <td><asp:TextBox ID="txtAgencyCity" runat="server" Width="250px" MaxLength="30"></asp:TextBox></td>
                <td><asp:TextBox ID="txtAgencyState" Width="250px" runat="server" MaxLength="30"></asp:TextBox></td>
                <td><asp:TextBox ID="txtAgencyZip" Width="100px" runat="server" MaxLength="15"></asp:TextBox></td>
            </tr>
        </table>
        
        <p>
            <label for="ddAgencyCountry" class="fieldTitle">Agency Country</label><br />
            <asp:TextBox ID="txtAgencyCountry" runat="server"></asp:TextBox>
        </p>

        <p>
            <label for="txtAgencyPhone" class="fieldTitle">Agency Phone</label><br />
            <asp:TextBox ID="txtAgencyPhone" Width="150px" runat="server" onblur="ValidatePhone('AgencyPhone');"></asp:TextBox>
        </p>

        <p>
            <label for="txtAgencyEmail" class="fieldTitle">Agency Email</label><br />
            <asp:TextBox ID="txtAgencyEmail" Width="200px" runat="server" MaxLength="75"></asp:TextBox>
        </p>
        <div>
            <input type="submit" class="button" name="btnSubmit" id="UpdateAgent" value="Update Agent" />
            <asp:HiddenField ID="hdnAgencyID" runat="server" />
        </div>
    </div>

    <h3 class="accordion accordion-toggle">Parents</h3>
    <div class="panel accordion-content">
        <p>
            <label for="txtParentsName" class="fieldTitle">Parents' Names</label><br />
            <asp:TextBox ID="txtParentsName" runat="server" MaxLength="75" Width="400px"></asp:TextBox>
        </p>
        <p>
            <label for="txtParentsStreet" class="fieldTitle">Parents' Street Address</label><br />
            <asp:TextBox ID="txtParentsStreet" runat="server" Width="400px" MaxLength="75"></asp:TextBox>
        </p>
        <table style="width:100%;">
            <tr>
                <td><label for="txtParentsCity" class="fieldTitle">City</label></td>
                <td><label for="txtParentsState" class="fieldTitle">State/Province</label></td>
                <td><label for="txtParentsZip" class="fieldTitle">Zip/Postal Code</label></td>
            </tr>
            <tr>
                <td><asp:TextBox ID="txtParentsCity" runat="server" MaxLength="30" Width="250px"></asp:TextBox></td>
                <td><asp:TextBox ID="txtParentsState" Width="250px" runat="server" MaxLength="30"></asp:TextBox></td>
                <td><asp:TextBox ID="txtParentsZip" Width="100px" runat="server" MaxLength="15"></asp:TextBox></td>
            </tr>
        </table>
        <p>
            <label for="txtParentsCountry" class="fieldTitle">Parents' Country of Citizenship</label><br />
            <asp:TextBox ID="txtParentsCountry" runat="server"></asp:TextBox>
        </p>
        <p>
            <label for="txtParentsPhone" class="fieldTitle">Parents' Phone</label><br />
            <asp:TextBox ID="txtParentsPhone" Width="150px" runat="server" onblur="ValidatePhone('ParentsPhone');"></asp:TextBox>
        </p>
        <p>
            <label for="txtParentsEmail" class="fieldTitle">Parents' Email</label><br />
            <asp:TextBox ID="txtParentsEmail" Width="200px" runat="server" MaxLength="75"></asp:TextBox>
        </p>
        <div>
            <input type="submit" class="button" name="btnSubmit" id="UpdateParents" value="Update Parents" />
            <asp:HiddenField ID="hdnParentsID" runat="server" />
        </div>

    </div>

    <h3 class="accordion accordion-toggle">Emergency Contact</h3>
    <div class="panel accordion-content">
        <p>
            <label for="txtEmergName" class="fieldTitle">Emergency Contact Name</label><br />
            <asp:TextBox ID="txtEmergName" Width="200px" runat="server" MaxLength="75"></asp:TextBox>
        </p>

        <p>
            <label for="txtEmergRelationship" class="fieldTitle">Emergency Contact Relationship</label><br />
            <asp:TextBox ID="txtEmergRelationship" Width="200px" runat="server" MaxLength="30"></asp:TextBox>
        </p>

        <p>
            <label for="txtEmergPhone" class="fieldTitle">Emergency Contact Phone</label><br />
            <asp:TextBox ID="txtEmergPhone" Width="150px" runat="server" onblur="ValidatePhone('EmergPhone');"></asp:TextBox>
        </p>
        
        <p>
            <label for="txtEmergEmail" class="fieldTitle">Emergency Contact Email</label><br />
            <asp:TextBox ID="txtEmergEmail" Width="200px" runat="server" MaxLength="75"></asp:TextBox>
        </p>
        <p>
            This field is false/unchecked by default.  If you know this contact speaks English, check the box.<br />
            <asp:CheckBox ID="chkSpeakEnglish" runat="server" /> <label for="chkSpeakEnglish" class="fieldTitle">Speaks English</label>
        </p>
        <div>
            <input type="submit" class="button" name="btnSubmit" id="EmergencyContact" value="Update Emergency Contact" />
            <asp:HiddenField ID="hdnEmergencyID" runat="server" />
        </div>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function ($) {
        $('#accordion').find('.accordion-toggle').click(function () {

            //Expand or collapse this panel
            $(this).next().slideToggle('fast');

            //Hide the other panels
            $(".accordion-content").not($(this).next()).slideUp('fast');

        });
    });
</script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
        });
    }

</script>

</asp:Content>

