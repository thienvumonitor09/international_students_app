﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CCSApps.master" AutoEventWireup="true" CodeFile="MatchPreferences.aspx.cs" Inherits="Homestay_Admin_MatchPreferences" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Match Preferenes
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <div style="float:left;width:70%;">
        <asp:Literal ID="litResultMsg" runat=server></asp:Literal>
        </div>
    <div id="divHome" runat="server" style="float:right;margin-right:30px;">
        <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;
    </div>
    <div style="margin-bottom:10px;clear:both;">
        <h3>Select a Student</h3>
        <asp:DropDownList ID="ddlStudents" runat="server" autopostback="true"></asp:DropDownList>
    </div>
    <h3>Level of Matching</h3>
    <div style="float:left;width:25%;margin-bottom:10px;">
        <p> <label for="rdoGender">Gender</label>
            <asp:RadioButtonList ID="rdoGender" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Value="Include"></asp:ListItem>
                <asp:ListItem Value="Omit"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
    </div>
    <div style="float:left;width:25%;margin-bottom:10px;">
        <p> <label for="rdoHomestay">Homestay Preference</label>
            <asp:RadioButtonList ID="rdoHomestay" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Value="Include"></asp:ListItem>
                <asp:ListItem Value="Omit"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
    </div>
    <div style="float:left;width:25%;margin-bottom:10px;">
        <p> <label for="rdoCollege">College</label>
            <asp:RadioButtonList ID="rdoCollege" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Value="SCC"></asp:ListItem>
                <asp:ListItem Value="SFCC"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
    </div>
    <div style="float:left;width:25%;margin-bottom:10px;">
        <p> <label for="rdoSmoking">Smoking Habits</label>
            <asp:RadioButtonList ID="rdoSmoking" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Value="Include"></asp:ListItem>
                <asp:ListItem Value="Omit"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
    </div>
    <p>
        <input type="submit" name="btnSubmit" value="Search" />
    </p>
    <asp:HiddenField ID="hdnGender" runat="server" />
    <asp:HiddenField ID="hdnOption" runat="server" />
    <asp:HiddenField ID="hdnAge" runat="server" />
    <asp:HiddenField ID="hdnSmoke" runat="server" />
    <h3>Families that Match</h3>
    <div id="divFamilies" runat="server" style="margin-bottom:10px;">
        <asp:DropDownList ID="ddlFamilies" runat="server"></asp:DropDownList>
    </div>
    <p>
        <input type="submit" name="btnSubmit" value="Compare" />
    </p>


    <div id="studentProfile" runat="server" style="float:left;width:49%;margin-bottom:10px;">
        <h3>Student Profile</h3>
        <p>
            <label for="studentNotes">Notes</label><br />
            <asp:TextBox ID="studentNotes" runat="server" TextMode="MultiLine" Rows="5" Columns="40"></asp:TextBox>
        </p>
        <div style="height:275px;">
            <h4>Smoking Habits</h4>
                <p>
                <label for="rdoSmokerStu">Do you smoke?</label><br /> 
                <asp:RadioButtonList ID="rdoSmokerStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Smoker">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Occasional smoker">Sometimes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Nonsmoker">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="rdoOtherSmokerOKStu">Other smokers OK?</label><br />
                <asp:RadioButtonList ID="rdoOtherSmokerOKStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Other smoker OK">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker not OK">No&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker no preference">No Preference</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="smokingHabitsStu">Explain:</label><br />
                <asp:TextBox ID="smokingHabitsStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:250px;">
            <h4>Home Environment</h4>
            <p>
                <label for="rdoInteractionStu">Interaction and Conversation</label><br />
                <asp:RadioButtonList ID="rdoInteractionStu" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Interaction every day">Every Day</asp:ListItem>
                    <asp:ListItem Value="Interaction frequent">Frequent, but not every day</asp:ListItem>
                    <asp:ListItem Value="Interaction minimal">Minimal conversation is okay</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="rdoHomeEnvironmentStu">HomeEnvironment</label><br />
                <asp:RadioButtonList ID="rdoHomeEnvironmentStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Home environment quiet">Quiet&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Home busy and active">Busy and Active&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="homeEnvironmentPreferencesStu">Explain:</label><br />
                <asp:TextBox ID="homeEnvironmentPreferencesStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:125px;">
            <h4>Children</h4>
            <p>
                <label for="rdoSmallChildren">Preference</label><br />
                <asp:RadioButtonList ID="rdoSmallChildrenStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Young children">Young children&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Older children">Older children&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="No children">No children</asp:ListItem>
                    <asp:ListItem Value="Children no preference">No Preference</asp:ListItem>
                </asp:RadioButtonList>
            </p>
        </div>
        <div style="height:350px;">
            <h4>Lifestyle</h4>
            <b>Dietary needs:</b><br />
            <asp:CheckBox ID="VegetarianStu" runat="server" Text="Vegetarian" /><br />
            <asp:CheckBox ID="GlutenFreeStu" runat="server" Text="Gluten free" /><br />
            <asp:CheckBox ID="DairyFreeStu" runat="server" Text="Dairy free" /><br />
            <asp:CheckBox ID="OtherFoodAllergyStu" runat="server" Text="Other food allergy" /><br />
            <asp:CheckBox ID="HalalStu" runat="server" Text="Halal" />  (Food prepared as prescribed by Muslim law)<br />
            <asp:CheckBox ID="KosherStu" runat="server" Text="Kosher" /><br />
            <asp:CheckBox ID="NoSpecialDietStu" runat="server" Text="I have no special dietary needs" /><br />
            <p>
                <label for="rdoTraveledOutsideCountryStu">Have you traveled outside the U.S. before?</label><br />
                <asp:RadioButtonList ID="rdoTraveledOutsideCountryStu" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Traveled outside country">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Not traveled outside country">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="traveledWhere">Explain:</label><br />
                <asp:TextBox ID="traveledWhereStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:320px;">
            <p><b>Hobbies and activities:</b><br />
            <asp:CheckBox ID="MusicalInstrumentStu" runat="server" Text="Play a musical instrument" /><br />
            <asp:CheckBox ID="ArtStu" runat="server" Text="Drawing/painting" /><br />
            <asp:CheckBox ID="TeamSportsStu" runat="server" Text="Team sports" /><br />
            <asp:CheckBox ID="IndividualSportsStu" runat="server" Text="Individual sports" /><br />
            <asp:CheckBox ID="ListenMusicStu" runat="server" Text="Listen to music" /><br />
            <asp:CheckBox ID="DramaStu" runat="server" Text="Drama" /><br />
            <asp:CheckBox ID="WatchMoviesStu" runat="server" Text="Watch movies" /><br />
            <asp:CheckBox ID="SingingStu" runat="server" Text="Singing" /><br />
            <asp:CheckBox ID="ShoppingStu" runat="server" Text="Shopping" /><br />
            <asp:CheckBox ID="ReadBooksStu" runat="server" Text="Read books" /><br />
            <asp:CheckBox ID="OutdoorsStu" runat="server" Text="Hiking/camping/outdoors" /><br />
            <asp:CheckBox ID="CookingStu" runat="server" Text="Cooking" /><br />
            <asp:CheckBox ID="PhotographyStu" runat="server" Text="Photography" /><br />
            <asp:CheckBox ID="GamingStu" runat="server" Text="Gaming" />
            </p>
        </div>
        <p>
            <label for="activitiesEnjoyedStu">Other hobbies/activities you enjoy:</label><br />
            <asp:TextBox ID="activitiesEnjoyedStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
        </p>
        <p>
            <label for="allergies">Do you have any allergies to food, drugs, animals or anything else?</label><br />
            <asp:TextBox ID="allergiesStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
        </p>
        <p>
            <label for="healthConditions">Do you have any health conditions that would prevent you from participation in normal and regular physical activities?</label><br />
            <asp:TextBox ID="healthConditionsStu" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
        </p>
        <p>
            <label for="anythingElseStu">Is there anything else you'd like us to know about your family?</label><br />
            <asp:TextBox ID="anythingElseStu" runat="server" Width="98%" MaxLength="300"></asp:TextBox>
        </p>
    </div>
        
    <div id="divFamilyProfile" runat="server" style="float:left;width:49%;margin-bottom:10px;">
        <h3>Family Profile</h3>
        <p>
            <label for="familyNotes">Notes</label><br />
            <asp:TextBox ID="familyNotes" runat="server" TextMode="MultiLine" Rows="5" Columns="40"></asp:TextBox>
        </p>
        <div style="height:275px;">
            <h4>Smoking Habits</h4>
                <p>
                <label for="rdoSmoker">Do you smoke?</label><br /> 
                <asp:RadioButtonList ID="rdoSmoker" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Smoker">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Occasional smoker">Sometimes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Nonsmoker">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="rdoOtherSmokerOK">Other smokers OK?</label><br />
                <asp:RadioButtonList ID="rdoOtherSmokerOK" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Other smoker OK">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker not OK">No&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Other smoker no preference">No Preference</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="smokingHabits">Explain:</label><br />
                <asp:TextBox ID="smokingHabits" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
            <p>
                <label for="smokingHabits">Smoking Guidelines</label><br />
                <asp:TextBox ID="smokingGuidelines" runat="server" Width="98%" MaxLength="500"></asp:TextBox>
            </p>
        </div>
        <div style="height:250px;">
            <h4>Home Environment</h4>
            <p>
                <label for="rdoInteraction">Interaction and Conversation</label><br />
                <asp:RadioButtonList ID="rdoInteraction" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Interaction every day">Every Day</asp:ListItem>
                    <asp:ListItem Value="Interaction frequent">Frequent, but not every day</asp:ListItem>
                    <asp:ListItem Value="Interaction minimal">Minimal conversation is okay</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="rdoHomeEnvironment">HomeEnvironment</label><br />
                <asp:RadioButtonList ID="rdoHomeEnvironment" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Home quiet">Quiet&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Home busy and active">Busy and Active&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <label for="homeEnvironmentPreferences">Explain:</label><br />
                <asp:TextBox ID="homeEnvironmentPreferences" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:125px;">
            <h4>Minor children in home</h4>
            <p>
                <asp:TextBox ID="SmallChildrenInHome" runat="server" Width="98%"  Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
            </p>
        </div>
        <div style="height:350px;">
            <h4>Lifestyle</h4>
            <b>Willing to accommodate:</b><br />
            <asp:CheckBox ID="Vegetarian" runat="server" Text="Vegetarian" /><br />
            <asp:CheckBox ID="GlutenFree" runat="server" Text="Gluten free" /><br />
            <asp:CheckBox ID="DairyFree" runat="server" Text="Dairy free" /><br />
            <asp:CheckBox ID="OtherFoodAllergy" runat="server" Text="Other food allergy" /><br />
            <asp:CheckBox ID="Halal" runat="server" Text="Halal" />  (Food prepared as prescribed by Muslim law)<br />
            <asp:CheckBox ID="Kosher" runat="server" Text="Kosher" /><br />
            <asp:CheckBox ID="NoSpecialDiet" runat="server" Text="I prefer a student with no special dietary needs" /><br />
            <p>
                <label for="rdoTraveledOutsideCountry">Have you traveled outside the U.S. before?</label><br />
                <asp:RadioButtonList ID="rdoTraveledOutsideCountry" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                    <asp:ListItem Value="Traveled outside country">Yes&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Not traveled outside country">No&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </p>
             <p>
                <label for="traveledWhere">Explain:</label><br />
                <asp:TextBox ID="traveledWhere" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
            </p>
        </div>
        <div style="height:320px;">
           <p><b>Hobbies and activities:</b><br />
            <asp:CheckBox ID="MusicalInstrument" runat="server" Text="Play a musical instrument" /><br />
            <asp:CheckBox ID="Art" runat="server" Text="Drawing/painting" /><br />
            <asp:CheckBox ID="TeamSports" runat="server" Text="Team sports" /><br />
            <asp:CheckBox ID="IndividualSports" runat="server" Text="Individual sports" /><br />
            <asp:CheckBox ID="ListenMusic" runat="server" Text="Listen to music" /><br />
            <asp:CheckBox ID="Drama" runat="server" Text="Drama" /><br />
            <asp:CheckBox ID="WatchMovies" runat="server" Text="Watch movies" /><br />
            <asp:CheckBox ID="Singing" runat="server" Text="Singing" /><br />
            <asp:CheckBox ID="Shopping" runat="server" Text="Shopping" /><br />
            <asp:CheckBox ID="ReadBooks" runat="server" Text="Read books" /><br />
            <asp:CheckBox ID="Outdoors" runat="server" Text="Hiking/camping/outdoors" /><br />
            <asp:CheckBox ID="Cooking" runat="server" Text="Cooking" /><br />
            <asp:CheckBox ID="Photography" runat="server" Text="Photography" /><br />
            <asp:CheckBox ID="Gaming" runat="server" Text="Gaming" />
        </p>
        </div>
        <p>
            <label for="activitiesEnjoyed">Other hobbies/activities you enjoy:</label><br />
            <asp:TextBox ID="activitiesEnjoyed" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
        </p>
        <p>
            <label for="petsInHome">Do you have pets? What type and what areas of your home are they free to occupy?</label><br />
            <asp:TextBox ID="petsInHome" runat="server" MaxLength="100" Rows="3" Columns="40" TextMode="MultiLine"></asp:TextBox>
        </p>
        <p>
            <label for="anythingElse">Is there anything else you'd like us to know about your family?</label><br />
            <asp:TextBox ID="anythingElse" runat="server" Width="98%" MaxLength="300"></asp:TextBox>
        </p>
    </div>
</asp:Content>

