﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_ListActive : System.Web.UI.Page
{
    string strListType = "";
    string strResultMsg = "";
    DataTable dtFamilies;
    DataTable dtStudents;
    protected void Page_Load(object sender, EventArgs e)
    {
        Homestay hsInfo = new Homestay();

        strListType = Request.QueryString["type"];
        if (strListType == "F")
        {
            litType.Text = "Families";
            divFamilies.Visible = true;
            divStudents.Visible = false;
            dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
        }
        else
        {
            litType.Text = "Students";
            divStudents.Visible = true;
            divFamilies.Visible = false;
            dtStudents = hsInfo.GetHomestayStudents("Active", 0);
        }

        if (strListType == "F" && dtFamilies != null)
        {
            if (dtFamilies.Rows.Count > 0)
            {
                litList.Text = "";
                foreach (DataRow dr in dtFamilies.Rows)
                {
                    litList.Text += "<li>" + (dr["homeName"].ToString() + "</li>");
                  //  Response.Write(dr["id"].ToString() + "<br />");
                }
            }
            else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtFamilies is null.<br />"; }
        DateTime bDate = DateTime.Now;
        string strShortDate = "";
        if(strListType == "S" && dtStudents != null)
        {
            if (dtStudents.Rows.Count > 0)
            {
                litTable.Text = "";
                foreach (DataRow dr in dtStudents.Rows)
                {
                    if (dr["DOB"].ToString().Trim() != "")
                    {
                        bDate = Convert.ToDateTime(dr["DOB"].ToString().Trim());
                        strShortDate = bDate.ToShortDateString();
                    }
                    else
                        strShortDate = ""; ;
                    litTable.Text += "<tr><td style='width:50%'>" + dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " " + dr["middleNames"].ToString() + "</td><td style='align:left;width:50%'>" + strShortDate + "</td></tr>";
                }
            }
            else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
        }
        else { strResultMsg += "Error: dtStudents is null.<br />"; }
    }

    protected void linkBtnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }
}