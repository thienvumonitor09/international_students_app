﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Admin_Reports_PlacementForm : System.Web.UI.Page
{
    string strResultMsg = "";
    String selectedStudent = "0";
    DateTime dob;
    String arrivalDate;
    String shortDate = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Homestay hsInfo = new Homestay();
        if (IsPostBack)
        {
            selectedStudent = ddlStudent.SelectedValue;
            if (selectedStudent != "0")
            {
                // have a valid student selected so get extended information to fill out table
                DataTable dtStudentExtended = hsInfo.GetHomestayStudents("ID", Convert.ToInt32(selectedStudent));
                if (dtStudentExtended != null)
                {
                    if (dtStudentExtended.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtStudentExtended.Rows)
                        {
                            // convert the arrival date --> quarter arrive
                            arrivalDate = Convert.ToDateTime(dr["arrivalDate"].ToString()).ToShortDateString();
                           // litQtrArrive.Text = dr["arrivalDate"].ToString(); ;
                           // litQtrStart.Text = dr["quarterStartDate"].ToString();
                            litStudentName.Text = dr["firstName"].ToString().Trim().Replace("''", "'") + " " + dr["middleNames"].ToString().Trim().Replace("''", "'") + " " + dr["familyName"].ToString().Trim().Replace("''", "'");
                            litStudentGender.Text = dr["Gender"].ToString();
                            shortDate = Convert.ToDateTime(dr["DOB"].ToString()).ToShortDateString();
                            litDOB.Text = shortDate;
                            litCountry.Text = dr["countryOfCitizenship"].ToString();
                            //check status of other fields and tack on if entries exist
                            litOther.Text = dr["activitiesEnjoyed"].ToString().Trim().Replace("''", "'") + " " + dr["anythingElse"].ToString().Trim().Replace("''", "'");
                            litAirport.Text = "TBD: Please let usknow if you receive a flight itinerary from your student.";
                            litUrgentCare.Text = "$20 co-pay";
                        }
                        try
                        {
                            DataTable dtStudentMore = hsInfo.GetStudentStudyInfo(Convert.ToInt32(selectedStudent));
                            if (dtStudentMore != null)
                            {
                                if (dtStudentMore.Rows.Count == 1)
                                {
                                    if (dtStudentMore.Rows[0]["studyWhere"].ToString().Length == 3)
                                        litCampus.Text = "Spokane Community College - SCC";
                                    else
                                        litCampus.Text = "Spokane Falls Community College - SFCC";
                                    litProgramStudy.Text = dtStudentMore.Rows[0]["studyWhat"].ToString().Trim().Replace("''", "'") + " - " + dtStudentMore.Rows[0]["studyMajor"].ToString().Trim().Replace("''", "'");
                                    if (dtStudentMore.Rows[0]["studyMajorOther"].ToString().Trim().Length > 1)
                                        litProgramStudy.Text += dtStudentMore.Rows[0]["studyMajorOther"].ToString().Trim().Replace("''", "'");
                                    litStudentEmail.Text = dtStudentMore.Rows[0]["Email"].ToString().Trim().Replace("''", "'");
                                }
                                else if (dtStudentMore.Rows.Count > 1)
                                    strResultMsg += "Multiple records returned - please investigate!";
                                else
                                    strResultMsg += "No records returned!";
                            }
                        } catch (Exception ex)
                        {
                            Response.Write("Error: " + ex.Message + "<br />" + ex.StackTrace);
                        }
                        try
                        {
                            DataTable dtArriveQtr = hsInfo.GetArrivalQuarter(arrivalDate);                            
                            if (dtArriveQtr != null)
                            {
                                if (dtArriveQtr.Rows.Count > 0)
                                {
                                    litQtrArrive.Text = dtArriveQtr.Rows[0]["Name"].ToString() + " ";
                                    if (dtArriveQtr.Rows[0]["Name"].ToString().Trim() == "Fall")
                                        litQtrArrive.Text += dtArriveQtr.Rows[0]["SchoolYears"].ToString().Substring(0, 4);
                                    else
                                        litQtrArrive.Text += dtArriveQtr.Rows[0]["SchoolYears"].ToString().Substring(5, 4);
                                    litQtrArrive.Text += " - STRM: " + dtArriveQtr.Rows[0]["STRM"].ToString();
                                }
                            }
                            else
                                Response.Write("is null");
                                
                        } catch (Exception err)
                        {
                            Response.Write("Error: " + err.Message + "<br />" + err.StackTrace);
                        }
                    }// row count > 0
                }// dataset not null

                
            }// selctedStudent != 0
        }// is postback

        if (!IsPostBack)
        {
            DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);

            ddlStudent.Items.Clear();
            ListItem LI;
            LI = new ListItem();
            LI.Value = "0";
            LI.Text = "-- Select One --";
            ddlStudent.Items.Add(LI);
            if (dtStudents != null)
            {
                if (dtStudents.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtStudents.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["applicantID"].ToString();
                        LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                        ddlStudent.Items.Add(LI);
                    }
                }
                else { strResultMsg += "Error: dtStudents has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtStudents is null.<br />"; }

            litResutlMsg.Text = strResultMsg;
        }// NOT IsPostback

        
    }

    protected void linkBtnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }
}