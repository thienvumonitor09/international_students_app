﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="PlacementForm.aspx.cs" Inherits="Homestay_Admin_Reports_PlacementForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<style type="text/css">
    p
    {
        padding:2px;
        margin:0px;
    }
</style>
    <link href="/Homestay/_css/Print.css" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<div style="text-align:center"><h1>Global Education Homestay Program</h1>
<h2>New Student Placement</h2></div>
<div id="divHome" class="noPrint" runat="server" style="float:right;margin-right:30px;">
    <asp:LinkButton ID="linkBtnHome" runat="server" class="ButtonAdmin" OnClick="linkBtnHome_Click">Return to Admin Home Page</asp:LinkButton>&nbsp;&nbsp;
</div>
    <asp:Literal ID="litResutlMsg" runat="server"></asp:Literal>
    
    <div id="divStudentdd" class="noPrint" runat="server" visible="true">
        <h2>Select a Student</h2>
        <asp:DropDownList ID="ddlStudent" runat="server"  AutoPostBack="true" style="margin-top:5px;"></asp:DropDownList>
        <p>&nbsp;</p>
    </div>
    
<table style="width:95%;" cellpadding="5">
    <tr>
        <td style="width:30%;"><b>Campus<b></b></td>
        <td><asp:Literal ID="litCampus" runat="server"></asp:Literal></td>
        
    </tr>
    <tr>
        <td style="width:30%;"><b>Quarter of Arrival<b></b></td>
        <td><asp:Literal ID="litQtrArrive" runat="server"></asp:Literal></td>
        
    </tr>
    <tr>
        <td style="width:30%;"><b>Orientation Start Date<b></b></td>
        <td><asp:TextBox ID="txtOrientationStart" Width="80%" MaxLength="200" runat="server"></asp:TextBox></td>
        
    </tr>
    <tr>
        <td style="width:30%;"><b>Quarter Start Date<b></b></td>
        <td><asp:TextBox ID="txtQuarterStartDates" Width="80%" MaxLength="200" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td style="width:30%;"><b>Student Name<b></b></td>
        <td><asp:Literal ID="litStudentName" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td style="width:30%;"><b>Student Gender<b></b></td>
        <td><asp:Literal ID="litStudentGender" runat="server"></asp:Literal></td>
        
    </tr>
    <tr>
        <td style="width:30%;"><b>Date of Birth<b></b></td>
        <td><asp:Literal ID="litDOB" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td style="width:30%;"><b>Country<b></b></td>
        <td><asp:Literal ID="litCountry" runat="server"></asp:Literal></td>
        
    </tr>
    <tr>
        <td style="width:30%;"><b>Program of Study<b></b></td>
        <td><asp:Literal ID="litProgramStudy" runat="server"></asp:Literal></td>
        
    </tr>
    <tr>
        <td style="width:30%;"><b>Student Email address<b></b></td>
        <td><asp:Literal ID="litStudentEmail" runat="server"></asp:Literal></td>
        
    </tr>
    <tr>
        <td style="width:30%;"><b>Other<b></b></td>
        <td><asp:Literal ID="litOther" runat="server"></asp:Literal></td>
        
    </tr>
    <tr>
        <td style="width:30%;"><b>Airport Arrival Information<b></b></td>
        <td><asp:Literal ID="litAirport" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td style="width:30%;"><b>Urgent Care:<b>covered by Student Insurance (Lewermark)</b></td>
        <td style="vertical-align:top;"><asp:Literal ID="litUrgentCare" runat="server"></asp:Literal></td>
    </tr>
</table>
    <h3>Before your student arrives:</h3>
    <ul>
        <li>Review the Culturegram about the student's country and culture.</li>
        <li>Be sure the room has all required items, working smoke detectors, C0<sub>2</sub> monitors, fire ladders, etc.</li>
        <li>Attend the quarterly Family Meeting for Homestay Families.</li>
    </ul>
    <h3>After your student arrives:</h3>
    <ol>
        <li>Meet your student at the airport near Baggage Claim. Bring a sign with <b><i>all</i></b> of their names on it.</li>
        <li>Bring the student to school for their Orientation and or testing date(s).</li>
        <li>Ride the bus with them, at least once, to and from school. Inform them to carry cash for the bus fee until their ID card is active.</li>
        <li>Discuss the Homestay Student and Family Agreement and give the student a copy. Email a scanned coopy to the Homestay Team at 
            <a href="mailto:InternationalHomestay@ccs.spokane.edu">InternationalHomestay@ccs.spokane.edu</a>. Be sure the student puts your 
            phone numbers in their own phone for emergencies, or if the need to contact you arises.</li>
        <li>Hellp the student:
            <ul>
                <li>access your internet</li>
                <li>get a bank account and</li>
                <li>purchase a cell phone plan</li>
            </ul>
        </li>
        <li>Be sure student attends the Homestay Success Meeting at SFCC (approximately one week after the quarter starts).</li>
    </ol>
    <p><input type="button" name="btnPrint" class="noPrint" onclick="javascript:window.print();" value="Print" /></p>
</asp:Content>
