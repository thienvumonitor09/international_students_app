﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CCSApps.master" CodeFile="ListActive.aspx.cs" Inherits="Homestay_Admin_Reports_ListActive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<style type="text/css">
    p
    {
        padding:2px;
        margin:0px;
    }
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bannerH1Text" Runat="Server">
    Homestay Administration
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">

<h2>List of Active Homestay <asp:Literal ID="litType" runat="server"></asp:Literal></h2>
    <div id="divHome" runat="server" style="float:right;margin-right:30px;">
        <asp:LinkButton ID="linkBtnHome" runat="server" class="ButtonAdmin" OnClick="linkBtnHome_Click">Return to Admin Home Page</asp:LinkButton>&nbsp;&nbsp;
    </div>
<div id="divFamilies" runat="server" Visible="false">    
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
    <ul>
        <asp:Literal ID="litList" runat="server"></asp:Literal>
    </ul>
</div>
<div id="divStudents" runat="server" Visible="false">
    <table style="width:75%;">
        <tr>
            <th style="width:50%;text-align:left;">NAME</th><th style="text-align:left;">Date of Birth</th>
        </tr>
        <asp:Literal ID="litTable" runat="server"></asp:Literal>
    </table>
</div>
</asp:Content>
