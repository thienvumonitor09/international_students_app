﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_ApplicationForm : System.Web.UI.Page
{
    Boolean blIsAdmin = false;

    /* FIELD LIST from vw_StudentInfoExtended
        hsGrad, hsGradDate, studyWhat, studyWhere, 
        studyMajor, studyMajorOther, trSchools, studiesBegin, 
        id, familyName, firstName, middleNames, 
        fullName, DOB, Gender, studentStatus, 
        countryOfBirth, countryOfCitizenship, englishAbility, visaType, 
        numberDependents, Spouse, Children, inUS, 
        useAgency, sendI_20, releaseInformation, allTrue, 
        howHearCCS, dateSubmitted, SID, maritalStatus, 
        addressCode, addresseeName, agencyName, Addr, 
        City, StateProv, Zip, Country, Phone, 
        CellPhone, Email, Relationship, healthIns
     */

    /************************************************************************************
    *  address codes:  1:  student's permanent address
    *                  2:  student's parent's address (if different)
    *                  3:  student's agency address (if any)
    *                  4:  student's relative address (if in US)
    *                  5:  student's emergency contact address
    *  Use applicant ID to match to student
    * ********************************************************************************/

    /* FIELD LIST from ContactInformation
        applicantID, addressCode, addresseeName, agencyName
		Addr, City, StateProv, Zip, Country
		Phone, CellPhone, Email, Relationship, speakEnglish
     */
    /*
     * Student Status:
     * App Submitted
     * App Complete
     * Placed
     * Withdrew
     * Graduated
     * Visa Denied
     * Deferred
     * Moved Out
     */
    protected void Page_Load(object sender, EventArgs e)
    {
        String strDOB = "";
        String strFirstName = "";
        String strLastName = "";
        Int32 intApplicantID = 0;
        String strSID = "";
        Int32 intAgencyID = 0;
        String strGender = "";
        String strCountry = "";
        String strStudentEmail = "";
        String strArrivalDate = "";
        String strAgency = "";
        String strAgentName = "";
        String strAgentEmail = "";
        String strAgencyAddress = "";
        String strAgencyCity = "";
        String strAgencyState = "";
        String strAgencyCountry = "";
        String strAgencyZIP = "";
        String strAgencyPhone = "";
        Boolean blUseAgency = false;
        String strCollege = "";
        String strHomestayOption = "";
        String strPayMethod = "";
        Boolean blNoRecords = false;
        Int32 intIsActive = 1;
        String strResultMsg = "";
        String strButtonFace = "";
        DateTime dtDOB = System.DateTime.Today;
        String strYear = System.DateTime.Today.Year.ToString();
        DateTime dtFirstDayOfClass;
        DateTime dtLatestDOB;
        Boolean blUnderAge = false;
        String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
        String strUploadServer = "";
        String strFileName = "";
        String strQuarterStartDate = "";
        String strSelectedQuarter = "";
        Boolean blLetterSubmitted = false;
        Boolean blPhotoSubmitted = false;
        String studentStatus = "App Submitted";
        Int32 intNewIdentity = 0;
        Int32 intResult = 0;
        String strSubmitMsg = "";
        String strBlank = "";
        char speakEnglish = '0';
        Boolean blDuplicateBasicInfoRecords = false;
        Boolean blAddMode = true;
        String strAddMode = "";
        String selectedStudent = "";
        String applicantID = "";
        Boolean blIsAdmin = false;     
        String strLogonUser = Request.ServerVariables["LOGON_USER"];
        DateTime outDate;               // used in try/catch to validate dob on form
        Boolean dateSuccess = false;    // used in try/catch to validate dob on form

        litResultMsg.Text = "";
        //For testing: get security context
        //WindowsIdentity winID = WindowsIdentity.GetCurrent();
        //strResultMsg = "<p><b>Security context:</b> " + winID.Name + "</p>";

        if (strLogonUser != "" && strLogonUser != null) 
        { 
            //If user has not logged out of Family Portal, forms authentication considers the person logged in
            if (strLogonUser.Contains("ccs\\"))
            {
                blIsAdmin = true; 
            }
        }
        if (Request.QueryString["ID"] != null)
        {
            //page load
            applicantID = Request.QueryString["ID"].ToString();
        }
        if (applicantID != "")
        {
            //returning from Profile page
            selectedStudent = applicantID;
        }
        //strResultMsg += "applicantID: " + applicantID + "<br />";
        if (applicantID != "0" && applicantID != "" && applicantID != null)
        {
            intApplicantID = Convert.ToInt32(applicantID);
            hdnApplicantID.Value = applicantID;
        }
        Homestay hsInfo = new Homestay();
        //Not an ASP.Net control
        if (Request.Form["btnSubmit"] != null) { strButtonFace = Request.Form["btnSubmit"].ToString(); }
        //Redirect without processing
        if (strButtonFace == "Continue") { Response.Redirect("PlacementProfile.aspx?ID=" + hdnApplicantID.Value); }
        //Admin function only
        if (strButtonFace == "Reset Fields") { resetFields(); ddlStudents.SelectedValue = "0"; }
        if (strButtonFace == "Return to Admin Home Page" && blIsAdmin){ Response.Redirect("/Homestay/Admin/Default.aspx"); }
        //Visibility for initial page load
        divMorePersonalInfo.Visible = false;
        recruitingAgent.Visible = false;
        personalInformation.Visible = true;
        divSearchButton.Visible = true;
        homestay18.Visible = false;
        homestay1617.Visible = false;
        PhotoLetter.Visible = false;
        divSave.Visible = false;
        divNextPage.Visible = false;

        //strResultMsg += "Button: " + strButtonFace + "<br />";
        strSelectedQuarter = rdoQuarterStart.SelectedValue;
        if (blIsAdmin)
        {
            strResultMsg = "<p><b>strLogonUser:</b> " + strLogonUser + "; Is admin? " + blIsAdmin + "</p>";
            //Admin acess and tools
            if (ddlStudents.SelectedValue == "") 
            { 
                selectedStudent = applicantID;
            }
            else
            {
                //override any query string value
                selectedStudent = ddlStudents.SelectedValue;
            }
            if (selectedStudent != "0" && selectedStudent != "" && selectedStudent != null)
            {
                applicantID = selectedStudent;
                intApplicantID = Convert.ToInt32(applicantID);
                hdnApplicantID.Value = selectedStudent;
            }
            //Set visibility
            divAdmin.Visible = true;
            divHome.Visible = true;
            divNextPage.Visible = true;
            //Fill student drop-down
            DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
            if (dtStudents != null)
            {
                if (dtStudents.Rows.Count > 0)
                {
                    ddlStudents.Items.Clear();
                    ListItem LI;
                    LI = new ListItem();
                    LI.Value = "0";
                    LI.Text = "-- Add New Student --";
                    ddlStudents.Items.Add(LI);
                    foreach (DataRow dr in dtStudents.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["applicantID"].ToString();
                        LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                        ddlStudents.Items.Add(LI);
                    }
                    if (selectedStudent != "") { ddlStudents.SelectedValue = selectedStudent; }
                }
                else if (blIsAdmin) { strResultMsg += "Error: dtStudents has no rows.<br />"; }
            }
            else if (blIsAdmin) { strResultMsg += "Error: dtStudents is null.<br />"; }

        }
        else
        {
            //Student view
            divAdmin.Visible = false;
            divHome.Visible = false;
        }

        Quarter_MetaData QMD = new Quarter_MetaData();
        DataTable dtQuarters = QMD.GetRangeRows_Quarter_MetaData_Base(0, 3);
        ListItem LI2;
        if (dtQuarters != null)
        {
            if (dtQuarters.Rows.Count > 0)
            {
                rdoQuarterStart.Items.Clear();
                foreach (DataRow dr in dtQuarters.Rows)
                {
                    LI2 = new ListItem();
                    LI2.Value = Convert.ToDateTime(dr["FirstDayOfClass"]).ToShortDateString();
                    LI2.Text = dr["Name"].ToString() + " - " + Convert.ToDateTime(dr["FirstDayOfClass"]).ToShortDateString();
                    rdoQuarterStart.Items.Add(LI2);
                    //strResultMsg += "Item: " + dr["Name"].ToString() + " - " + Convert.ToDateTime(dr["FirstDayOfClass"]).ToShortDateString() + "<br />";
                }
                if (strSelectedQuarter != "") { rdoQuarterStart.SelectedValue = strSelectedQuarter; }
            }
            else if (blIsAdmin) { strResultMsg += "QMD has no rows.<br />"; }
        }
        else if (blIsAdmin) { strResultMsg += "QMD is null.<br />"; }

        //Set mode: insert or update
        strAddMode = hdnAddMode.Value;
        if (strAddMode.ToLower() == "false") { blAddMode = false; }
        //Collect data for insert/update 
        strFirstName = txtFirstName.Text;
        strLastName = txtFamilyName.Text;
        strDOB = txtDOB.Text;
        strSID = txtSID.Text;
        strArrivalDate = txtArrivalDate.Text;
        strAgency = txtAgency.Text.Trim().Replace("'", "''");
        strAgentName = txtAgentName.Text.Trim().Replace("'", "''");
        strAgentEmail = txtAgentEmail.Text.Trim().Replace("'", "''");
        strAgencyAddress = txtAgencyAddress.Text.Trim().Replace("'", "''");
        strAgencyCity = txtAgencyCity.Text.Trim().Replace("'", "''");
        strAgencyState = txtAgencyState.Text.Trim().Replace("'", "''");
        strAgencyZIP = txtAgencyZip.Text.Trim().Replace("'", "''");
        strAgencyCountry = txtAgencyCountry.Text.Trim().Replace("'", "''");
        strAgencyPhone = txtAgencyPhone.Text.Trim().Replace("'", "''");
        strHomestayOption = rdoHomestayOption.SelectedValue;
        strQuarterStartDate = rdoQuarterStart.SelectedValue;
        if (strButtonFace == "Save")
        {
            if (txtArrivalDate.Text.Trim() == "")
            {
                txtArrivalDate.Focus();  
            }
            if (rdoHomestayOption.SelectedValue == "")
            {
                rdoHomestayOption.Focus();
            }
            if (hdnApplicantID.Value != null && hdnApplicantID.Value != ""){ intApplicantID = Convert.ToInt32(hdnApplicantID.Value); }
            //1 = Update contct information for agent
            InternationalStudent myIStudent = new InternationalStudent();
            if (hdnAgencyID.Value != null && hdnAgencyID.Value != "") { intAgencyID = Convert.ToInt32(hdnAgencyID.Value); }
            if (intAgencyID == 0 && strAgentName.Trim() != "")
            {
                //Insert new agent record - address code 3
                strSubmitMsg = myIStudent.InsertContactInfo(intApplicantID, 3, strAgentName, strAgency, strAgencyAddress, strAgencyCity, strAgencyState,
                    strAgencyZIP, strAgencyCountry, strAgencyPhone, strBlank, strAgentEmail, strBlank, speakEnglish);
            }
            else if (strAgentName.Trim() != "")
            {
                //Update existing agency address - address code 3
                strSubmitMsg = myIStudent.UpdateContactInfo(intAgencyID, strAgentName, strAgency, strAgencyAddress, strAgencyCity, strAgencyState,
                    strAgencyZIP, strAgencyCountry, strAgencyPhone, strBlank, strAgentEmail, strBlank, speakEnglish);
            }
            if (strSubmitMsg == "OK contact info") { strResultMsg += "Your agent contact information was updated successfully!<br />"; }

            if (strHomestayOption == "")
            {
                if (blUnderAge)
                {
                    //16/17 year olds forced choice
                    strHomestayOption = "With food";
                }
                else
                {
                    strHomestayOption = "Either";
                }
            }
            //Upload the letter and photo
            if (uplLetterFile.HasFile)
            {
                strFileName = strLastName + "-" + intApplicantID + "-Letter-" + uplLetterFile.FileName;
                uplLetterFile.SaveAs(strUploadURL + strFileName);
                strResultMsg += "Your letter, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                //Insert record in UploadedFilesInformation table
                intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                blLetterSubmitted = true;
            }
            else if (hdnLetterSubmitted.Value.ToLower() == "true")
            {
                //previous submission exists
                blLetterSubmitted = true;
            }
            //strResultMsg += "Letter submitted? " + hdnLetterSubmitted.Value + "<br />";
            if (uplPhotoFile.HasFile)
            {
                strFileName = strLastName + "-" + intApplicantID + "-Photo-" + uplPhotoFile.FileName;
                uplPhotoFile.SaveAs(strUploadURL + strFileName);
                strResultMsg += "Your photo, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                //Insert record in UploadedFilesInformation table
                intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                blPhotoSubmitted = true;
            }
            else if (hdnPhotoSubmitted.Value.ToLower() == "true")
            {
                //previous submission exists
                blPhotoSubmitted = true;
            }
            //strResultMsg += "blAddMode 2? " + blAddMode + "<br />";
            //1 = insert/update HomestayStudentInfo
            intNewIdentity = hsInfo.InsertUpdateP1HomestayStudentInfo(intApplicantID, strSelectedQuarter, strArrivalDate, strHomestayOption, strPayMethod, blLetterSubmitted, blPhotoSubmitted, intIsActive, blAddMode);
            //strResultMsg += "intNewIdentity: " + intNewIdentity + "<br />";
            // If not automatically taken to next page, display result messages
            if (intNewIdentity > 0) 
            { 
                strResultMsg += "Your student information was inserted successfully!<br />"; 
            }
            else if (intNewIdentity == -1)
            {
                strResultMsg += "Your student information was updated successfully!<br />";
            } 
            if (blAddMode && intNewIdentity > 0)
            {
                //2 = set IsHomestay bitflag in ApplicantBasicInfo table; use applicant ID
                intResult = hsInfo.UpdateHomestayBitflagApplicantBasicInfo(intApplicantID, 1);
                //3 = Update studentStatus; use homestay ID
                intResult = hsInfo.UpdateHomestayStudentStatus(intNewIdentity, 0, studentStatus);
                //4 = Update SID, if provided
                if (hdnSID.Value != null && hdnSID.Value != "")
                {
                    //international student DB; use applicant ID
                    myIStudent.UpdateSID(intApplicantID, strSID);
                }
                //if (hsInfo.strResultMsg == "")
                //{
                    // Go to part 2
                    Response.Redirect("PlacementProfile.aspx?ID=" + intApplicantID);
                //}
            }
            else if (!blAddMode && intApplicantID > 0)
            {
                //2 = Update SID, if field was empty to start with
                if (strSID != "" && strSID != null && (hdnSID.Value == null || hdnSID.Value == ""))
                {
                    myIStudent.UpdateSID(intApplicantID, strSID);
                }
            }
            //Display any messages
            setVisibilityNoAccess();
            divNextPage.Visible = true;
        }
        //Get records by admin student selection; or user hit the back button from profile page
        if (selectedStudent != "" && selectedStudent != "0")
        {
            //set visibility of admin info for admins
            if (blIsAdmin) { 
                divAdmin.Visible = true;
                divHome.Visible = true;
            }
            intApplicantID = Convert.ToInt32(selectedStudent);
            hdnApplicantID.Value = intApplicantID.ToString();
            //do something
            DataTable dtStudent = hsInfo.GetOneInternationalStudent("", intApplicantID, "", "", "", "");
            if (dtStudent != null)
            {
                if (dtStudent.Rows.Count > 0)
                {
                    //clear any values from previous selection
                    resetFields();
                    if (dtStudent.Rows.Count > 1) { blDuplicateBasicInfoRecords = true; }
                    strFirstName = dtStudent.Rows[0]["firstName"].ToString();
                    txtFirstName.Text = strFirstName;
                    strLastName = dtStudent.Rows[0]["familyName"].ToString(); 
                    txtFamilyName.Text = strLastName; 
                    strGender = dtStudent.Rows[0]["Gender"].ToString();
                    strSID = dtStudent.Rows[0]["SID"].ToString();
                    hdnSID.Value = strSID;
                    txtSID.Text = strSID;
                    dtDOB = Convert.ToDateTime(dtStudent.Rows[0]["DOB"]);
                    txtDOB.Text = dtDOB.ToShortDateString();
                    if (strQuarterStartDate != "")
                    {
                        dtFirstDayOfClass = Convert.ToDateTime(strQuarterStartDate);
                    }
                    else
                    {
                        strQuarterStartDate = QMD.NextQtrFirstClass.ToShortDateString();
                        dtFirstDayOfClass = QMD.NextQtrFirstClass;
                    }
                    dtLatestDOB = dtFirstDayOfClass.AddYears(-18);
                    if (dtLatestDOB < dtDOB)
                    {
                        blUnderAge = true;
                    }
                    //strResultMsg += "DOB threshold: " + dtLatestDOB.ToShortDateString() + "; underage? " + blUnderAge + "<br />";
                    strCountry = dtStudent.Rows[0]["Country"].ToString();
                    strStudentEmail = dtStudent.Rows[0]["Email"].ToString();
                    blUseAgency = Convert.ToBoolean(dtStudent.Rows[0]["useAgency"]);
                    //Check with Homestay if they want this behavior
                    if (blUseAgency)
                    {
                        recruitingAgent.Visible = true;
                        DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
                        if (dtContact != null)
                        {
                            if (dtContact.Rows.Count > 0)
                            {
                                strAgency = dtContact.Rows[0]["agencyName"].ToString();
                                txtAgency.Text = strAgency;
                                strAgentName = dtContact.Rows[0]["addresseeName"].ToString();
                                txtAgentName.Text = strAgentName;
                                strAgentEmail = dtContact.Rows[0]["Email"].ToString();
                                txtAgentEmail.Text = strAgentEmail;
                                hdnAgencyID.Value = dtContact.Rows[0]["id"].ToString();
                            }
                        }
                    }
                    else
                    {
                        recruitingAgent.Visible = false;
                    }
                    strCollege = dtStudent.Rows[0]["studyWhere"].ToString();
                    if (strCollege == "Pullman") { strCollege = "SFCC"; }
                    if (blIsAdmin)
                    {
                        //radio buttons only available to staff
                        rdoWhereStudy.SelectedValue = strCollege;
                    }
                    rdoGender.SelectedValue = strGender;
                    txtCountry.Text = strCountry;
                    txtPermEmail.Text = strStudentEmail;
                }
            }
        }
        //Get records by student search
        if (strButtonFace == "Next")
        {
            //Initial set of mode: true at this point
            hdnAddMode.Value = blAddMode.ToString();
            //set visibility for admins
            if (blIsAdmin && selectedStudent != "" && selectedStudent != "0") { divAdmin.Visible = true; }
            //Find the student and populate the info

            //make sure the name fields have been entered
            if (txtFamilyName.Text.Trim() == "")
            {
               // blNoRecords = true;
                strResultMsg += "<span style=\"color:Red;font-weight:bold\"> Error:  You must enter your family name. <br />";
               // strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"/Homestay/Default.aspx\">Return to Homestay Homepage</a></p>";
            }
            if (txtFirstName.Text.Trim() == "")
            {
                //blNoRecords = true;
                strResultMsg += "<span style=\"color:Red;font-weight:bold\"> Error:  You must enter your first name. <br />";
               /*
                if (!strResultMsg.Contains("Return"))
                {
                    strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"/Homestay/Default.aspx\">Return to Homestay Homepage</a></p>";
                }
                */ 
            }
            if (rdoQuarterStart.SelectedValue == "")
            {
               // blNoRecords = true;
                strResultMsg += "<span style=\"color:Red;font-weight:bold\"> Error:  You must select a quarter start date. <br />";
            }

            //Do some data calculation to determine age
            // test if valid date
            dateSuccess = DateTime.TryParse(strDOB, out outDate);
            if (strDOB != "" && dateSuccess)
            {
                dtDOB = Convert.ToDateTime(strDOB);
                if (strQuarterStartDate != "")
                {
                    dtFirstDayOfClass = Convert.ToDateTime(strQuarterStartDate);
                }
                else
                {
                    strQuarterStartDate = QMD.NextQtrFirstClass.ToShortDateString();
                    dtFirstDayOfClass = QMD.NextQtrFirstClass;
                }
                dtLatestDOB = dtFirstDayOfClass.AddYears(-18);
                if (dtLatestDOB < dtDOB) 
                { 
                    blUnderAge = true; 
                }
                //strResultMsg += "DOB threshold: " + dtLatestDOB.ToShortDateString() + "; underage? " + blUnderAge + "<br />";
                DataTable dtInfo = hsInfo.GetOneInternationalStudent("DOB", 0, strDOB, strFirstName, strLastName, "");
                if (dtInfo != null)
                {
                    if (dtInfo.Rows.Count > 0)
                    {
                        if (dtInfo.Rows.Count > 1) { blDuplicateBasicInfoRecords = true; }
                        intApplicantID = Convert.ToInt32(dtInfo.Rows[0]["id"]);
                        hdnApplicantID.Value = intApplicantID.ToString();
                        strGender = dtInfo.Rows[0]["Gender"].ToString();
                        strCountry = dtInfo.Rows[0]["Country"].ToString();
                        strStudentEmail = dtInfo.Rows[0]["Email"].ToString();
                        blUseAgency = Convert.ToBoolean(dtInfo.Rows[0]["useAgency"]);
                        //Check with Homestay if they want this behavior
                        if (blUseAgency)
                        {
                            recruitingAgent.Visible = true;
                            DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
                            if (dtContact != null)
                            {
                                if (dtContact.Rows.Count > 0)
                                {
                                    strAgency = dtContact.Rows[0]["agencyName"].ToString();
                                    txtAgency.Text = strAgency;
                                    strAgentName = dtContact.Rows[0]["addresseeName"].ToString();
                                    txtAgentName.Text = strAgentName;
                                    strAgentEmail = dtContact.Rows[0]["Email"].ToString();
                                    txtAgentEmail.Text = strAgentEmail;
                                    hdnAgencyID.Value = dtContact.Rows[0]["id"].ToString();
                                }
                            }
                        }
                        else
                        {
                            recruitingAgent.Visible = false;
                        }
                        strCollege = dtInfo.Rows[0]["studyWhere"].ToString();
                        if (strCollege == "Pullman") { strCollege = "SFCC"; }
                        if (blIsAdmin)
                        {
                            rdoWhereStudy.SelectedValue = strCollege;
                        }
                        rdoGender.SelectedValue = strGender;
                        txtCountry.Text = strCountry;
                        txtPermEmail.Text = strStudentEmail;
                    }
                    else { blNoRecords = true; }
                }
                else { blNoRecords = true; }
            }
            else 
            { 
               // blNoRecords = true;
                strResultMsg += "<span style=\"color:Red;font-weight:bold\"> Error:  You must submit your date of birth in correct date format - mm/dd/yyyy. <br />";
                if(strResultMsg.Contains("Error"))
                {
                    strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"/Homestay/Default.aspx\">Return to Homestay Homepage</a></p>";
                }
            }
           
            if(!strResultMsg.Contains("Error"))
            {
                strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"/Homestay/Default.aspx\">Return to Homestay Homepage</a></p>";
            }
            
        }

        if (intApplicantID > 0 && !blDuplicateBasicInfoRecords && !strResultMsg.Contains("Error"))
        {
            //Set visibility
            personalInformation.Visible = true;
            divMorePersonalInfo.Visible = true;
            divSearchButton.Visible = false;
            if (blUnderAge)
            {
                homestay18.Visible = false;
                homestay1617.Visible = true;
            }
            else
            {
                homestay18.Visible = true;
                homestay1617.Visible = false;
            }
            PhotoLetter.Visible = true;
            divSave.Visible = true;
            if (blIsAdmin)
            {
                divNextPage.Visible = true;
            }

            //Check for existing HomestayStudentInfo record
            DataTable dtHSStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtHSStudent != null)
            {
                if (dtHSStudent.Rows.Count > 0)
                {
                    blAddMode = false;
                    hdnAddMode.Value = blAddMode.ToString();
                    //strResultMsg += "blAddMode 1? " + blAddMode + "<br />";
                    Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                    AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                    strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                    foreach (DataRow drHSStudent in dtHSStudent.Rows)
                    {
                        blLetterSubmitted = Convert.ToBoolean(drHSStudent["letterSubmitted"]);
                        hdnLetterSubmitted.Value = blLetterSubmitted.ToString();
                        blPhotoSubmitted = Convert.ToBoolean(drHSStudent["photoSubmitted"]);
                        hdnPhotoSubmitted.Value = blPhotoSubmitted.ToString();
                        if (blLetterSubmitted || blPhotoSubmitted)
                        {
                            litLetterFile.Text = "";
                            litPhotoFile.Text = "";
                            DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        strFileName = dr["fileName"].ToString();
                                        if (strFileName.Contains("Letter"))
                                        {
                                            //get the file name
                                            litLetterFile.Text += "<br /><b>Letter submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                        if (strFileName.Contains("Photo"))
                                        {
                                            //Get the file name
                                            litPhotoFile.Text += "<br /><b>Photo submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                    }
                                }
                            }
                        }
                        //strResultMsg += "Letter submitted: " + Convert.ToBoolean(drHSStudent["letterSubmitted"]) + "<br />";
                        //strResultMsg += "Start Date: " + Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() + "<br />";
                        if (Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() != "1/1/1900")
                        {
                            try
                            {
                                rdoQuarterStart.SelectedValue = Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString();
                            }
                            catch
                            {
                                //do nothing
                            }
                        }
                        txtArrivalDate.Text = Convert.ToDateTime(drHSStudent["arrivalDate"]).ToShortDateString();
                        if (blIsAdmin)
                        {
                            txtDateApplied.Text = Convert.ToDateTime(drHSStudent["dateApplied"]).ToShortDateString();
                        }
                        if (!blUnderAge)
                        {
                            rdoHomestayOption.SelectedValue = drHSStudent["homestayOption"].ToString().Trim();
                        }
                    }
                    if (blIsAdmin)
                    {
                        strResultMsg += "A Homestay student application exists.  You may review the information here.<br />";
                    }
                    else
                    {
                        //setVisibilityNoAccess();
                        if (strButtonFace != "Save")
                        {
                            strResultMsg += "Your Homestay application has already been submitted.</p>";
                            litAllRequired.Text = "";
                            PhotoLetter.Visible = true;
                            divUpload.Visible = true;
                            divSave.Visible = false;
                        }
                        divNextPage.Visible = true;
                    }
                }
                else { 
                    strResultMsg = "Please complete the application form below.  The application consists of multiple pages.<br />";
                    litAllRequired.Text = "";
                }
            }
            else { 
                strResultMsg = "Please complete the application below.  The application consists of multiple pages.<br />";
                litAllRequired.Text = "";
            }
        }
        if (blNoRecords)
        {
            setVisibilityNoAccess();
            if(!strResultMsg.Contains("Return")) {
                strResultMsg += "<span style=\"color:Red;font-weight:bold\">No records were returned.  You must complete the <a href=\"https://portal.ccs.spokane.edu/_netapps/internationalsa/InternationalStudents/Standardized/applicationForm.aspx?site=ccs\">International Student application</a> before applying for Homestay.</span><br />";
                strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"/Homestay/Default.aspx\">Return to the Homestay Homepage</a><br />";
            }
        }
        if (blDuplicateBasicInfoRecords) 
        {
            setVisibilityNoAccess();
            if (!strResultMsg.Contains("Return"))
            {
                strResultMsg += "<span style=\"color:Red;font-weight:bold\">Warning: multiple records exist with this last name, first name and date of birth.</span>  Please contact the Homestay staff to resolve this issue before continuing with the application process.<br />";
                strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"/Homestay/Default.aspx\">Return to the Homestay Homepage</a><br />";
            }    
        }

        litResultMsg.Text = "<p>" + hsInfo.strResultMsg + strResultMsg +  "</p>";
    }

    void setVisibilityNoAccess()
    {
        //VISIBILITY when not redirecting
        personalInformation.Visible = false;
        divMorePersonalInfo.Visible = false;
        divSearchButton.Visible = false;
        recruitingAgent.Visible = false;
        homestay18.Visible = false;
        homestay1617.Visible = false;
        PhotoLetter.Visible = false;
        divSave.Visible = false;
        divNextPage.Visible = false;
    }

    void resetFields()
    {
        //Stupid viewstate...
        txtFamilyName.Text = "";
        txtFirstName.Text = "";
        txtDOB.Text = "";
        txtDateApplied.Text = "";
        txtArrivalDate.Text = "";
        rdoHomestayOption.ClearSelection();
        rdoQuarterStart.ClearSelection();
        if (blIsAdmin) { rdoWhereStudy.ClearSelection(); }
        txtAgencyAddress.Text = "";
        txtAgencyCity.Text = "";
        txtAgencyCountry.Text = "";
        txtAgencyPhone.Text = "";
        txtAgencyState.Text = "";
        txtAgencyZip.Text = "";
        txtAgentEmail.Text = "";
        txtAgentName.Text = "";
        litLetterFile.Text = "";
        litPhotoFile.Text = "";
    }
}