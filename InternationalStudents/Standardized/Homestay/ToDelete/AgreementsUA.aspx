﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgreementsUA.aspx.cs" Inherits="Homestay_Students_Agreements" %>
<!DOCTYPE html>

<html lang="en-us" >

<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>CCS Homestay Program</title>
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" />
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />         
    <link href="/Homestay/_css/Print.css" rel="stylesheet" media="print" type="text/css" />  
    <meta http-equiv="X-UA-Compatible" content="IE=9" />        
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>
</head>

<body>
<form id="form1" runat="server">
<div id="divInstructions" runat="server">
    <h2>Instructions</h2>
    <p>Please read, print and sign the following agreement and submit a scanned copy using the File Upload below</p>
    <asp:HiddenField ID="hdnApplicantID" runat="server" />
    <asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
</div>
<div id="div1" runat="server">
    <h3>AGREEMENT BETWEEN COMMUNITY COLLEGES OF SPOKANE (CCS) AND PARENTS/GUARDIANS </h3>
    <p>Student's parents/guardians agree to the following terms and
    conditions:</p>
    <ol>
    <li>I understand and agree
    that the assignment of Homestay families may be changed prior to or after my
    child's arrival in the United States, and placement with more than one Homestay
    family may occur during my child's stay, and placement of my child may be
    declined if an appropriate Homestay family is not available. </li>

    <li>I agree to purchase and
    maintain adequate medical insurance and personal liability insurance on my
    child's personal belongings while they are in attendance at CCS and
    participating in the Homestay program. I understand and agree that CCS assumes
    no responsibility for verifying the medical insurance standards of coverage if
    purchased outside the College's recommended plan or the adequacy of the
    personal liability coverage. </li>

    <li>I grant CCS and the
    Homestay family, the authority to take action they believe is reasonably
    warranted in the event my child experiences health or safety issues during
    his/her stay. This authority and permission includes, but is not limited to,
    seeking emergency medical intervention, care, and treatment from a licensed
    health care provider. For the purposes of this consent emergency medical
    intervention would apply to circumstances where my child is injured and
    circumstances where my child is suffering from an illness that poses a risk to
    his/her health and safety. </li>

    <li>I hereby grant permission
    for licensed medical and mental health care providers to provide medical care
    for my child in the event he/she experiences health or safety issues during
    his/her attendance at CCS and participation in the Homestay program. This
    includes consent and authorization to render or order medical treatment; to
    prescribe and to give medication; to order x-rays, to provide anesthesia, and
    to complete any emergent medical intervention including, but not limited to
    surgery and hospital care as deemed medically necessary. </li>

    <li>I agree to the release of
    medical information regarding my child to the CCS Homestay family(ies), health
    care providers and facilities responsible for examining and providing medical
    treatment to my child including: prescribing or giving medication, x-rays,
    anesthetic, medical or surgical diagnosis or treatment or hospital care, if and
    as deemed necessary based on standard health care practices. </li>

    <li>I agree that CCS and the
    Homestay family shall not be liable or responsible for any costs or aspects of
    medical treatment. </li>

    <li>I agree to be financially
    responsible for all medical care and treatment obtained or provided to my child
    during his or her homestay and attendance at CCS. </li>

    <li>I agree to reimburse the
    Homestay family for the repair or other costs incurred by my child or his/her
    guests in the event of damage to the Homestay family home or property. </li>

    <li>I understand and agree
    that CCS shall not be responsible for negligence on the part of the Homestay
    family or during the Homestay including but not limited to any loss or damage
    to property, and any injuries or death. </li>

    <li>I agree to pay the Homestay fee on the first day of
    each month. I understand there are <b>NO refunds</b> if my child leaves without 30 days
    written notice. If my child arrived mid-month, I will pay the pro-rated amount
    due the month of arrival and on the first of each subsequent month. </li>

    <li>I agree to ensure that
    my child complies with all CCS Student Standards of Conduct, Policies and
    Procedures including the guidelines of the Homestay Family and the provisions
    included in the Agreement between the Homestay and Student on page three of this
    application. I further agree and understand that in the event my child fails to
    comply with CCS Student Standards of Conduct, Policies and Procedures, including
    the guidelines of the Homestay Family and the provisions included in the
    Agreement between the Homestay and Student on page three of this application,
    he/she may be dismissed from the CCS International Student Program and required
    to return home at my expense. </li>
    </ol>

    <p>&nbsp;</p>
    <p>Parent/Guardian's Name (please print): ______________________________________________________ </p>

    <p>&nbsp;</p>
    <p>Parent/Guardian's (signature): ______________________________________________________ </p>

    <p>&nbsp;</p>
    <p>Date: ____________________ </p>

    <p>&nbsp;</p>
    <p>Parent/Guardian's Email:  ____________________________________________ </p>

    <p>&nbsp;</p>
    <p>Parent/Guardian's Phone Number: ____________________________________ </p>
    <p>&nbsp;</p>
    
    <div id="divWaiveLiabilityParentUA" style="border: 1px solid Gray;width:30%;padding:15px;" runat="server">
        <p>
            <label for="uplWaiveLiabilityParentUAFile">Document Upload:</label>&nbsp;
            <asp:FileUpload ID="uplWaiveLiabilityParentUAFile" runat="server" Size="70" /> <asp:Literal ID="litWaiveLiabilityParentUAFile" runat="server"></asp:Literal>
            <asp:HiddenField ID="hdnWaiveLiabilityParentUASubmitted" runat="server" /><br />
          <!--  <input type="submit" name="btnSubmit" id="Submit3" value="Upload File" /> -->
            <asp:Button ID="btnUploadWaiveLiabParentUA" runat="server" Text="Upload File" OnClick="btnUploadWaiveLiabParentUA_Click" />
        </p>
    </div>
    <h4 class="noPrint">Acknowledgement</h4>
    <p class="noPrint">
        <asp:CheckBox ID="chkWaiveLiabilityParentUA" runat="server" Text="I understand that my parents or guardians must print and sign this agreement." /><label for="chkPrint">
        to complete my application process.</label>
    </p>

</div>


<div id="div2" runat="server">
    <h3>STUDENT AGREEMENT </h3>

    <p>Student agrees to the following terms and conditions: </p>
    <p>I understand and agree that in consideration for
    participation in the Homestay program, I am responsible for following all of
    the following including:</p>

    <ol>
    <li>The CCS Standards of Conduct for Students WAC 132Q-10,
    which is available at <a href="http://apps.leg.wa.gov/wac/default.aspx?cite=132Q-10/" target="_blank"> 
    http://apps.leg.wa.gov/wac/default.aspx?cite=132Q-10/</a> or <a href="http://catalog.spokane.edu/StudentRights.aspx">CCS Student Rights and Responsibilities</a> 
    or by contacting Homestay staff members.</li>


    <li>The CCS Policies and Standards of Conduct for Students
    which prohibits my use or possession of alcohol, unauthorized prescription
    drugs, narcotics, controlled substances, including marijuana, and drug
    paraphernalia at all times during the Homestay program. CCS complies with the
    federal Drug Free Workplace Act and the Drug Free Schools and Community Act as
    a federal grant recipient. </li>

    <li>Respect &amp; follow guidelines of my Homestay family. </li>

    <li>Comply with state and federal laws and regulations and
    abstain from illegal activities at all times during my stay. </li>

    <li>If I have a problem or conflict, talk with CCS
    International Staff and/or my Homestay parents, including Manager of Immigration and Student Success. </li>

    <li>Inform my Homestay family where I am at all times, advise
    Homestay family of my daily schedule, and communicate to Homestay family if I
    will not be home for dinner or arriving late and specifically where I will be
    an when I will arrive home. </li>

    <li>Introduce my friends to my Homestay family, and provide
    their names, cell phone, e-mail and address of residence. </li>

    <li>Attend classes at SFCC or SCC regularly. </li>

    <li>I shall refrain from causing any damage to the home or
    property of my Homestay family and ensure my guests do the same. </li>

    <li>Arrange for a personal cell phone and share my cell phone
    number with my homestay family. I agree to respond quickly to my Homestay
    Family phone calls, texts and e-mails to me. </li>
    </ol>

    <p>I have read and understand the above "Student Agreement." I
    agree to comply with these terms and conditions. </p>

    <p>I understand that if I fail to comply with these terms and
    provisions that I may be dismissed from the CCS International Program,
    terminated from the Homestay Program, and returned to my home at my parent's
    expense. </p>

    <p>&nbsp;</p>
    <p>Student (print name):  ______________________________________________________ </p>

    <p>&nbsp;</p>
    <p>Student's (signature): ______________________________________________________</p>

    <p>&nbsp;</p>
    <p>Date: ____________________ </p>

    <p>&nbsp;</p>
    <div id="divCCSConductUA" class="noPrint" style="border: 1px solid Gray;width:30%;padding:15px;" runat="server">
        <p>
            <label for="uplCCSConductUAFile">Document Upload:</label>&nbsp;
            <asp:FileUpload ID="uplCCSConductUAFile" runat="server" Size="70" /> <asp:Literal ID="litCCSConductUAFile" runat="server"></asp:Literal>
            <asp:HiddenField ID="hdnCCSConductUASubmitted" runat="server" /><br />
            <input type="submit" name="btnSubmit" id="Submit1" value="Upload File" />
        </p>
    </div>
    <h4 class="noPrint">Acknowledgement</h4>
    <p class="noPrint">
        <asp:CheckBox ID="chkCCSConductUA" runat="server" Text="I acknowledge that I must print this agreement." /><label for="chkPrint">&nbsp;I understand that I must have a parent/guardian sign and submit this agreement
        to complete my application process.</label>
    </p>

</div>

<div id="div3" runat="server">
    <h3>Notices and Waivers</h3>
    <p><b>PLEASE READ AND UNDERSTAND THE FOLLOWING BEFORE SIGNING. IT INCLUDES CONTRACTUAL PROVISIONS.</b></p>

    <p>&nbsp;</p>
    <p>Print Name of Student: ____________________________________________ </p>

    <p>&nbsp;</p>
    <p>Date of Birth: ______________________ </p>

    <p>&nbsp;</p>

    <p >Print Names of Student's Parents/Guardians (hereinafter referred to as "Parents"):</p>

    <p>&nbsp;</p>
    <p>______________________________________________________________________________________</p>
    <p>&nbsp;</p>
    <p>______________________________________________________________________________________</p>

    <p>&nbsp;</p>
    <ol>
    <li>
    <p><b>WARNING: NOTICE OF HAZARDS AND RISKS FOR TRAVEL AND LIVING IN THE UNITED STATES</b></p>

    <p>The Community
    Colleges of Spokane's (CCS) Homestay Program is designed to provide Student
    with educational opportunities and cultural experience in the United States. </p>

    <p>Due to the
    structure and design of the program, many services and activities including, but
    not limited to, transportation, recreational activities and entertainment are
    provided by third parties and not under the supervision or control of CCS. The
    Homestay placement is a placement in a private family home. It includes a
    private room, bathing facilities, and a place to study. The Homestay family
    provides necessary household items such as linens and towels. The Student's
    room is furnished, including a bed/bedding, desk or table, lamp dresser, and
    closet. In Homestay the family is responsible for providing food for meals and
    snacks seven days a week. Students are required to follow the rules of the
    Homestay family. </p>

    <p>All members of
    the Homestay family will be proficient in English and will only speak English
    when the student is present. The primary form of transportation for the student
    is the city bus; however, the Homestay family may occasionally offer student a
    ride in their personal vehicles. Additionally, Homestay family may offer
    student the opportunity to participate in family activities. Homestay family
    activities are diverse based on the interests and hobbies of the Homestay
    family. Activities may include, but are not limited to, shopping, commuting
    to/from school, dining out, community and religious events (e.g. movies,
    concerts, art exhibits, sporting events, conferences, rodeos, etc.), family
    recreational and vacation activities (e.g. camping, fishing, water sports,
    skiing, skating, picnics, travel, hiking, visits to theme parks, bicycling,
    horse back riding, etc.), exercise (e.g. work outs, playing recreational
    sports, swimming, exercise classes) and transportation in family member
    vehicles. There may be modifications in the Homestay placement for the student.
    </p>

    <p>Participation
    in any international study program may result in injury, illness, disability,
    accident, death, property damage, property loss, delay or expense caused by:
    vehicles/transportation; activities involving recreation or entertainment
    outside the academic curriculum whether coordinated by CCS, a third party, or
    Student on his/her own behalf; activities involving residing in a home-stay
    family, or other accommodations; consumption of unsafe water or food from
    restaurants, vendors, stores, or other source; adverse weather conditions,
    natural disasters; sickness, disease, illnesses, quarantine; terrorist
    activities; strikes, social or labor unrest; criminal activities; construction
    activities; government restrictions or regulations; safety hazards; and other
    acts or conditions outside the CCS control by other university/colleges, agencies
    (government or private), companies and/or individuals. </p>

    <p style='page-break-before:always'>The U.S. Department of State, World Health Organization, and
    Centers for Disease Control provide information and recommendations concerning
    immunizations, medicines, and travel warnings which are available as follows: </p>

    <p>The US
    Department of State, which issues Travel Warnings, Travel Alerts and Country
    Specific Information at <a href="http://travel.state.gov/" target="_blank">http://travel.state.gov/</a> </p>

    <p>The World Health Organization, <a href="http://www.who.int/csr/alertresponse/en" target="_blank">http://www.who.int/csr/alertresponse/en</a>; and </p>

    <p>The Centers
    for Disease Control, via the International Travelers Hotline at 1-877-FYI-TRIP
    (1-877-394-8747) or at <a href="http://wwwn.cdc.gov/travel" target="_blank">http://wwwn.cdc.gov/travel</a>. </p>

    <p>The purpose of this WARNING
    is: 1) to bring Parents' and Student's attention to the existence of potential
    dangers associated with Student's participation in the CCS Homestay Program and
    to advise that there are additionally always the risk of other types of
    injuries or death resulting from other causes not specified above; 2) to aid
    Parents and Student in making an informed decision as to whether Student should
    participate in this program; and 3) to make Parents aware that it is their
    responsibility as Parents of Student to learn about and/or inquire of faculty,
    staff, health care providers, or other knowledgeable persons about any concerns
    that Parents might have regarding Student's health, safety and welfare related
    to participation in this program. </p>
</li>
<li>
    <p><b>PARENTAL  ASSUMPTION OF RISK, RELEASE OF CLAIMS AND AGREEMENT TO INDEMNIFY</b></p>

    <p>We, the
    parent(s), identified above, have been warned of the hazards and risks involved
    with Student's participation in the Homestay Program. We voluntarily agree to
    assume these hazards and risks and to waive and release any and all claims we
    may have against the State of Washington, CCS, SFCC, SCC, and their officers,
    agents, employees, and departments and the host institutions abroad including
    but not limited to claims, demands obligations, actions, causes of action
    regarding Student for any injury, illness, disability, accident, wrongful
    death, loss of services and support, loss of consortium, property damage,
    and/or property loss from Student's participation in the Homestay Program and
    related activities. For the purposes of this Release of Claims, activities
    include, but are not limited to: activities involving recreation or
    entertainment outside the academic curriculum whether coordinated by CCS, the
    Homestay family, a third party or Student on his/her own behalf; activities
    involving travel via any form including cars, buses, trains, taxis, planes,
    four wheelers, motorcycles, boat, or jet ski; consumption of beverages or food
    from restaurants, vendors, stores or other sources; and activities outside of
    the Homestay Program's control such as adverse weather conditions, natural
    disasters; quarantine; terrorist activities; travel delays; strikes, social or
    labor unrest; criminal activities; construction activities; government
    restrictions or regulations; and safety hazards. </p>

    <p>We, the
    parent(s), identified above also agree to indemnify the State of Washington,
    CCS, SFCC and SCC, and their officers, agents employees, departments and the
    Homestay family with regard to any financial obligations, liabilities, or
    expenses that may be incurred as a result of any injury, illness, disability,
    accident; wrongful death, loss of services and support, loss of consortium;
    property damage; and/or property loss including but not limited to, emergency
    transport; emergency medical services; medical treatment; rehabilitation; lost
    wages; lost or damaged property and any financial obligations, liabilities or
    expenses that may be incurred as a result of any damage or loss to person(s) or
    property caused by the Student. </p>

    <p>We, the
    parent(s) identified above, have reviewed and understand the hazards and risks
    detailed above and have advised Student to take appropriate actions and to
    govern him/herself accordingly. We are also aware that certain insurance
    companies offer insurance against some or many of the perils noted regarding
    loss and/or damage to property and are aware that the US Department of State
    has a specific requirement for F1-1 students to have full financial capacity to
    prevent them becoming a burden of the state. Due to this financial requirement,
    we acknowledge and agree that we are required to obtain medical insurance
    coverage consistent with the Affordable Care Act requirements and Section 4.6
    below. </p>
    <p>STUDENT ACKNOWLEDGMENT OF HAZARDS AND RISKS </p>

    <p>I, the Student
    identified above, have reviewed the hazards and risks detailed above with my
    Parents and understand these hazards and risks. I have also discussed with my
    Parents my responsibility to take appropriate actions and to govern myself
    accordingly. </p>
</li>
<li>

    <p><b>PERSONAL AND FINANCIAL OBLIGATIONS AND RESPONSIBILITIES</b></p>

    <p>4.1 Compliance
    with Student Standards of Conduct, Policies, Procedures, and Guidelines: </p>

    <p>Student and
    Parents understand and agree that pursuant to the CCS Standards of Conduct for
    Students, as a participant in the CCS Homestay program, Student shall comply
    with the following requirements and Parents shall ensure such compliance: </p>

    <p>a. The CCS
    Standards of Conduct for Students, Chapter 132Q-10 WAC, which is available at
    <a href="http://apps.leg.wa.gov/wac/default.aspx?cite=132Q-10" target="_blank">
    http://apps.leg.wa.gov/wac/default.aspx?cite=132Q-10</a> or by contacting CCS
    Global Education Office/Teresa Gay at Teresa.Gay@ccs.spokane.edu. </p>

    <p>b. The CCS
    Policies and Standards of Conduct for Students which prohibits my use or
    possession of alcohol, unauthorized prescription drugs, narcotics, controlled
    substances, including marijuana, and drug paraphernalia at all times during the
    Homestay program. CCS complies with the federal Drug Free Workplace Act and the
    Drug Free Schools and Community Act as a federal grant recipient. </p>

    <p>c. Respect and
    follow the guidelines and rules of Homestay family, </p>

    <p>d. Comply with
    state and federal laws and regulations and abstain from illegal activities at
    all times during my stay, </p>

    <p>e. Try to
    resolve conflict with assigned Homestay family by talking with International
    Homestay Manager: Teresa Gay, </p>

    <p>f. Inform my
    Homestay family where I am at all times, advise my Homestay family of my daily
    schedule and communicate to my Homestay family if I will not be home for dinner
    or arriving late, and when I will arrive home. </p>

    <p>g. Introduce
    friends to Homestay family and provide their names, cell phone, e-mail and
    address of residence. </p>

    <p>h. Attend
    classes at SFCC or SCC regularly. </p>

    <p>i. Refrain
    from causing any damage to the home or property of my Homestay family and
    ensure my guests do the same. </p>

    <p>j. Arrange for
    a personal cell phone and share my cell phone number with my Homestay family. I
    agree to respond quickly to my Homestay family phone calls, texts and e-mails
    to me. </p>

    <p><b>4.2 Consequences
    of Failure to comply with Standards of Conduct, Policies, Procedures, and
    Guidelines</b>: </p>

    <p>Student and
    Parents agree and understand that CCS has the right to enforce the terms of the
    Student Standards of Conduct, Policies, Procedures, and Guidelines. If Student
    fails to comply with any of the Standards of Conduct, Policy or Guideline
    requirements detailed above, CCS has the right to immediately terminate
    Student's participation in the program with no refund of moneys paid or costs
    incurred. In the event of termination, Student and Parents agree and understand
    that the Student will be sent home at Parents' expense. </p>

    <p><b>4.3 Responsibility
    for Health and Safety</b>: </p>

    <p>a. Student and
    Parents acknowledge and agree that Student is personally responsible for
    his/her own conduct during the Homestay Program. Parents and Student are responsible
    for ensuring </p>

    <p>Student is aware of the risks
    and dangers that he/she may encounter including, but not limited to risks and
    dangers to his/her person and property, and Student is responsible for
    exercising reasonable care to avoid or minimize those risks and dangers. Prior
    to participation in the CCS Homestay program, parents and students are
    encouraged to consult with a physician or other trained licensed medical
    professional to confirm Student's fitness for participation in this activity.
    Parents and Student certify that Student is in good health and has no physical,
    medical, mental, or emotional impairments, conditions, or concerns that might
    jeopardize or affect Student's safety, or the safety of others, related to
    his/her participation in the Homestay Program. </p>

    <p>b. If Student has a
    prescription for medications or is taking over the counter medications, Parents
    and Student should confirm with Student's medical provider whether the
    medications will impact his/her participation in the Homestay Program. Parents
    and Student understand and agree that Student shall not participate in
    activities while under the influence of any medication that may impact his/her
    ability to safely participate. </p>

    <p><b>4.4 Independent Travel:</b> </p>

    <p>Parents and
    Student understand that if Student engages in independent travel, such travel
    would be at the expense of Student's Parents and would not include any
    supervision by the Homestay Program or CCS. Parents and Student understand and
    agree that Parents and Student would be responsible for arranging such travel,
    separate from the CCS academic program and the Homestay program. Parents and
    Student further understand and agree that CCS, SFCC, SCC and their officers,
    agents, employees, and departments are not responsible either for any injury,
    accident, damage, loss, or expense suffered by Student during periods of
    independent travel or during any absence from the academic program of CCS,
    SFCC, or SCC. </p>

    <p>4.5 <b>Financial
    Obligations for Homestay Expenses: </b></p>

    <p>a. Student and
    Parents shall pay all personal bills and obligations of student. </p>

    <p>b. Student and
    Parents shall reimburse Homestay Family for any damage student or his/her
    guests cause to Homestay home and/or property, </p>

    <p>c. Student
    shall use a prepaid phone card or personal cell phone and not charge calls on
    Homestay phone, </p>

    <p>d. Student and
    Parents shall notify family and International Office in writing 30 days prior
    to moving out, and only when moving out occurs due to completion of program of
    study, official authorized transfer to another school or return to home
    country. </p>

    <p>e. Student and
    Parents shall pay Homestay fee on the first of
    each month.  <b>NO refunds</b> if student leaves without providing 30 days notice. </p>

    <p>f. In the
    event Student is terminated from the Homestay Program for failure to comply
    with the Student Standards of Conduct, Policies, Procedures and Guidelines as
    specified above, Parents and Student agree that Student will be sent home at
    Parents' expense. </p>

    <p><b>4.6 Financial
    Obligations and Responsibilities for Insurance and Student Medical and Dental
    Expenses</b></p>

    <p>Student and
    Parents agree to purchase and maintain adequate medical and personal liability
    insurance on my child's personal belongings while they are in attendance at CCS
    and participating in the Homestay program. I understand and agree that CCS
    assumes no responsibility for verifying the medical insurance standards of coverage if purchased outside
    the College's recommended plan or the adequacy of the personal liability
    coverage for the student. </p>
</li>
<li>
    <p><b>PUBLICITY RELEASE AND MISCELLANEOUS PROVISIONS</b></p>

    <p><b>5.1 PUBLICITY RELEASE</b></p>

    <p>Parents and
    Student grant permission to CCS and the host institutions to photograph Student
    during his/her participation in the International Program and consent to the
    use of any photograph likenesses of Student or comments made by Student during
    his/her participation in all forms and media and in all manners including
    composites for publicity material. Further, Parents and Student waive any right
    to inspect or approve the finished version(s), including written copy that may
    be created and appear in connection therewith. </p>

    <p><b>5.2 PROGRAM CHANGES </b></p>

    <p>Parents and
    Student understand that CCS and its agents reserve the right to make changes in
    initial campus assignments, academic center, etc., and to make alterations in
    programs, itineraries, schedules and academic calendar they deem appropriate. </p>

    <p><b>5.3 REFERENCED INDIVIDUALS AND DOCUMENTS</b></p>

    <p>All references in this form to CCS
    shall include: State of Washington, CCS, SFCC, SCC and all their officers,
    directors, staff members, campus directors, chaperones, group leaders, faculty
    members, administrators, advisors and agents, and Home stay families. </p>

    <p>All references to the "Parents" of
    the Student shall include the person or persons who are parents or legal
    guardians as specified on the top of this document and who are responsible for
    the Student until he/she turns 18 years of age. </p>

    <p><b>STUDENT:</b>All references to "Student" shall
    include the child or ward of the Parent(s)/Guardian(s). </p>

    <p><b>NOTICE OF
    HAZARDS AND RISKS, PARENTAL ASSUMPTION OF RISK, RELEASE OF CLAIMS AND AGREEMENT
    TO INDEMNIFY, STUDENT ACKNOWLEDGMENT OF HAZARDS AND RISKS, PERSONAL AND FINANCIAL OBLIGATIONS
    AND RESPONSIBILITIES,
    PUBLICITY RELEASE AND MISCELLANEOUS PROVISIONS:</b> All references to "Agreement" shall
    include the Notice of Hazards and Risks, Parental Assumption of Risk, Release
    of Claims and Agreement to Indemnify, Student Acknowledgement of Hazards and
    Risks, Personal and Financial Obligations and Responsibilities, Publicity
    Release and Miscellaneous Provisions. </p>

    <p><b>5.4 APPLICABLE LAW </b></p>

    <p>Parents and
    Student understand and agree that this Agreement shall be governed by and
    interpreted pursuant to the laws of the State of Washington. Venue for any
    dispute under this Agreement shall be in Spokane County, Washington. </p>

    <p><b>BY SIGNING
    THIS NOTICE OF
    HAZARDS AND RISKS, PARENTAL ASSUMPTION OF RISK, RELEASE OF CLAIMS AND AGREEMENT
    TO INDEMNIFY, STUDENT ACKNOWLEDGMENT OF HAZARDS AND RISKS, PERSONAL AND FINANCIAL OBLIGATIONS
    AND RESPONSIBILITIES,
    PUBLICITY RELEASE AND MISCELLANEOUS PROVISIONS, I CERTIFY I AM THE STUDENT APPLICANT
    IDENTIFIED AT THE TOP OF THIS DOCUMENT. I HAVE READ, UNDERSTAND, AND VOLUNTARILY
    AGREE TO ALL OF THE TERMS AND CONDITIONS SET FORTH IN THIS AGREEMENT AND CHOOSE TO PARTICIPATE IN THE
    PROGRAM.</b> </p>

</li>
</ol>
    <p><b>FURTHER UNDERSTAND THAT THIS AGREEMENT SHALL TAKE
    FORCE ONLY UPON MY ACCEPTANCE INTO THE STUDY ABROAD PROGRAM.</b></p>

    <p>Name of
    Student (please print): _______________________________________________________________ </p>
    <p>&nbsp;</p>
    <p>Signature of Student: ______________________________________________________________
    <p>&nbsp;</p>
    <p>Date_____________________________ </p>

    <p><b>BY SIGNING
    THIS NOTICE OF HAZARDS AND RISKS, PARENTAL ASSUMPTION OF RISK, RELEASE OF CLAIMS
    AND AGREEMENT TO INDEMNIFY, STUDENT ACKNOWLEDGMENT OF HAZARDS AND RISKS, PERSONAL AND FINANCIAL OBLIGATIONS
    AND RESPONSIBILITIES,
    PUBLICITY RELEASE AND MISCELLANEOUS PROVISIONS, I/WE CERTIFY THAT I/WE ARE THE
    PARENT(S) IDENTIFIED AT THE TOP OF THIS DOCUMENT. I/WE HAVE READ, UNDERSTAND, AND
    VOLUNTARILY AGREE TO ALL OF THE TERMS AND CONDITIONS SET FORTH IN THE
    DESCRIPTIVE INFORMATION AND IN THIS INFORMED ACKNOWLEDGMENT OF HAZARDS AND RISKS, STUDENT CONDUCT
    REQUIREMENTS, PUBLICITY RELEASE AND PARENTAL RELEASE OF LIABILITY AND AGREEMENT
    TO INDEMNIFY FOR STUDY ABROAD PROGRAM. I/WE FURTHER UNDERSTAND THAT THIS
    AGREEMENT SHALL TAKE FORCE ONLY UPON MY/OUR CHILD'S ACCEPTANCE INTO THE CCS
    STUDY ABROAD PROGRAM.</b></p>
    <p>&nbsp;</p>

    <p>Name of Parent/Guardian (please print): ______________________________________________________ </p>

    <p>&nbsp;</p>
    <p>Signature of Parent/Guardian: _________________________________________________________ </p>

    <p>&nbsp;</p>
    <p>Date_____________________________</p>

    <p>&nbsp;</p>
    <p>Name of  Parent/Guardian (please print): _________________________________________________ </p>

    <p>&nbsp;</p>
    <p>Signature of Parent/Guardian: _________________________________________________________ </p>

    <p>&nbsp;</p>
    <p>Date_____________________________</p>
    
    <p>&nbsp;</p>
    <div id="divNoticesWaiverParentUA" class="noPrint" style="border: 1px solid Gray;width:30%;padding:15px;" runat="server">
        <p>
            <label for="uplNoticesWaiverParentUAFile">Document Upload:</label>&nbsp;
            <asp:FileUpload ID="uplNoticesWaiverParentUAFile" runat="server" Size="70" /> <asp:Literal ID="litNoticesWaiverParentUAFile" runat="server"></asp:Literal>
            <asp:HiddenField ID="hdnNoticesWaiverParentUASubmitted" runat="server" /><br />
            <input type="submit" name="btnSubmit" id="Submit2" value="Upload Waiver" />
        </p>
    </div>
    <h4>Acknowledgement</h4>
    <p>
        <asp:CheckBox ID="chkNoticesWaiverParentUA" runat="server" Text="I understand what is required of me during my stay with a Homestay Host Family." />
        I understand I must print, sign, and submit all agreements to the International Student Office.
    </p>
</div>


<div id="divConfirmation" runat="server">
    <h2>Thank your for your interest in the CCS Homestay Program!</h2>
    <p>Someone from the Homestay Office will be in contact with you soon.</p>
    <p>In the meantime, you can browse the information below to prepare for your stay:</p>
    <ul>
        <li><a href="/Homestay/Students/PDF/2017-18StudentPlanSummary.pdf">Student Health Insurance Plan</a></li>
        <li><a href="/Homestay/Students/PDF/HouseGuidelinesEnglish-2016.pdf">House Guidelines</a></li>
        <li><a href="/Homestay/Students/PDF/MentalHealth-2017.pdf">Mental Health</a></li>
    </ul>
    <p>&nbsp;</p>
</div>
<p>&nbsp;</p>
<div id="divButtons" runat="server">
    <div id="divUploadWaiveLiabilityParentUA" runat="server" style="float:left;" visible="false">
        <input type="submit" name="btnSubmit" value="Save Parent Agreement" />&nbsp;&nbsp;
    </div>
    <div id="btnContinue2" runat="server" style="float:left;">
        <input type="submit" name="btnSubmit" value="Continue Part 2"/>&nbsp;&nbsp;
    </div>
    <div id="divUploadCCSConductUA" runat="server" style="float:left;" visible="false">
        <input type="submit" name="btnSubmit" value="Save Student Agreement" />&nbsp;&nbsp;
    </div>
    <div id="btnContinue3" runat="server" style="float:left;">
        <input type="submit" name="btnSubmit" value="Continue Part 3"/>&nbsp;&nbsp;
    </div>
    <div id="divUploadNoticesWaiverParentUA" runat="server" style="float:left;" visible="false">
        <input type="submit" name="btnSubmit" value="Save Notices and Waiver" />&nbsp;&nbsp;
    </div>
    <div id="btnPrevious" runat="server" style="float:left;">
        <input type="submit" name="btnSubmit" value="Back" />&nbsp;&nbsp;
    </div>
    <div id="btnDone" runat="server" style="float:left;">
        <input type="submit" name="btnSubmit" value="Save" />&nbsp;&nbsp;
    </div>
    <div id="divPrint" class="noPrint" style="float:left;margin-left:30px;">
        <input type="button" name="btnPrint" onclick="javascript:window.print();" value="Print" />&nbsp;&nbsp;
    </div>
        <div id="divHomepage" runat="server" class="noPrint" style="float:left;margin-left:30px;" visible="false">
        <input style="float:left;margin-left:30px;" type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;
    </div>
</div>
</form>
</body>
</html>
