﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Families_Archive_PlacementProfile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String strButtonFace = "";
        String strAppPart = "";
        String strResultMsg = "";
        Int32 intResult = 0;
        Int32 intFamilyID = 0;
        String FamilyID = "";
        String strFamilyStatus = "App Submitted";
        String appStatus = "";
        Boolean blWaiverSubmitted = false;
        String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
        String strFileName = "";
        String strUploadServer = "";
        String strHomeName = "";
        String strPrintPage = "false";
        Boolean blIsAdmin = false;

        Homestay hsInfo = new Homestay();

        litPageTitle.Text = "Family Profile";
        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (Request.Form["part"] != null)
        {
            strAppPart = Request.Form["part"].ToString();
            if (strAppPart == "3") { strButtonFace = "Done"; }
        }
        if (Request.ServerVariables["LOGON_USER"] != null)
        {
            //set admin status for visibility control
            blIsAdmin = true;
        }
        if (Request.QueryString["admin"] != null)
        {
            blIsAdmin = Convert.ToBoolean(Request.QueryString["admin"].ToString());
        }
        if (blIsAdmin)
        {
            divHome.Visible = true;
        }
        //set up page for printing
        if (Request.QueryString["print"] != null)
        {
            divWaiver.Visible = true;
            divConfirmation.Visible = false;
            btnDone.Visible = false;
            btnSave.Visible = false;
            btnPrevious.Visible = false;
            fldProfile.Visible = false;
            strPrintPage = "true";
            divPrint.Visible = true;
            divPrintWaiver.Visible = false;
            divNextPage.Visible = false;
            divUpload.Visible = false;
            divHome.Visible = false;
            divCheckbox.Visible = false;
            hdnAppStatus.Value = "Printable Waiver";
        }
        //track app status
        appStatus = hdnAppStatus.Value;
        if (strButtonFace != "") { appStatus = strButtonFace; }
        if (appStatus == "") { appStatus = "default"; }
        hdnAppStatus.Value = appStatus;
        //strResultMsg += "Button Face: " + strButtonFace + "; App Status: " + appStatus + "; Print? " + strPrintPage + "<br />";

        if (Request.QueryString["ID"] != null)
        {
            //page load
            FamilyID = Request.QueryString["ID"].ToString();
            hdnFamilyID.Value = FamilyID;
        }
        //Set default visibility
        if (appStatus == "default")
        {
            divConfirmation.Visible = false;
            btnDone.Visible = false;
            divNextPage.Visible = true;
            btnPrevious.Visible = true;
            fldProfile.Visible = true;
            divWaiver.Visible = false;
        }
        if (FamilyID == "")
        {
            //after postback
            FamilyID = hdnFamilyID.Value;
        }
        //strResultMsg += "Family ID: " + FamilyID + "<br />";
        if (FamilyID != "0" && FamilyID != "" && FamilyID != null)
        {
            intFamilyID = Convert.ToInt32(FamilyID);
        }
        if (intFamilyID > 0)
        {
            switch (strButtonFace)
            {
                case "Return to Admin Home Page":
                    Response.Redirect("/Homestay/Admin/Default.aspx");
                    break;
                case "Save and Continue":
                    //process submission
                    divConfirmation.Visible = false;
                    btnDone.Visible = true;
                    btnSave.Visible = false;
                    btnPrevious.Visible = false;
                    fldProfile.Visible = false;
                    divWaiver.Visible = true;
                    divPrintWaiver.Visible = true;
                    litPageTitle.Text = "Sign Form";
                    String TraveledWhere = traveledWhere.Text.Replace("'", "''");
                    String SmokingHabits = smokingHabits.Text.Replace("'", "''");
                    String HomeEnvironmentPreferences = homeEnvironmentPreferences.Text.Replace("'", "''");
                    String ActivitiesEnjoyed = activitiesEnjoyed.Text.Replace("'", "''");
                    String AnythingElse = anythingElse.Text.Replace("'", "''");
                    String HomestayOption = rdoHomestayOption.SelectedValue;
                    String GenderPreference = rdoGenderPreference.SelectedValue;
                    //Update HomestayFamilyInfo table
                    intResult = hsInfo.UpdateP2HomestayFamilyInfo(intFamilyID, TraveledWhere, SmokingHabits, ActivitiesEnjoyed, HomeEnvironmentPreferences, AnythingElse, HomestayOption, GenderPreference);

                    //Delete all previous selections; then re-add new ones
                    intResult = hsInfo.DeleteHomestayPreferenceSelections(0, intFamilyID);

                    //Insert into HomestayPreferenceSelections table
                    String SmokersOK = rdoOtherSmokerOK.SelectedValue;
                    if (SmokersOK != "")
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(SmokersOK, 0, intFamilyID);
                    }
                    String StudentSmokes = rdoSmoker.SelectedValue;
                    if (StudentSmokes != "")
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(StudentSmokes, 0, intFamilyID);
                    }
                    String selectedTraveledOutsideCountry = rdoTraveledOutsideCountry.SelectedValue;
                    if (selectedTraveledOutsideCountry != "")
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedTraveledOutsideCountry, 0, intFamilyID);
                    }
                    if (MusicalInstrument.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("MusicalInstrument", 0, intFamilyID);
                    }
                    if (Art.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Art", 0, intFamilyID);
                    }
                    if (TeamSports.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("TeamSports", 0, intFamilyID);
                    }
                    if (IndividualSports.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("IndividualSports", 0, intFamilyID);
                    }
                    if (ListenMusic.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("ListenMusic", 0, intFamilyID);
                    }
                    if (Drama.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Drama", 0, intFamilyID);
                    }
                    if (WatchMovies.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("WatchMovies", 0, intFamilyID);
                    }
                    if (Singing.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Singing", 0, intFamilyID);
                    }
                    if (Shopping.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Shopping", 0, intFamilyID);
                    }
                    if (ReadBooks.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("ReadBooks", 0, intFamilyID);
                    }
                    if (Outdoors.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Outdoors", 0, intFamilyID);
                    }
                    if (Cooking.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Cooking", 0, intFamilyID);
                    }
                    if (Photography.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Photography", 0, intFamilyID);
                    }
                    if (Gaming.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Gaming", 0, intFamilyID);
                    }
                    String selectedInteraction = rdoInteraction.SelectedValue;
                    if (selectedInteraction != "")
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedInteraction, 0, intFamilyID);
                    }
                    String selectedHomeEnvironment = rdoHomeEnvironment.SelectedValue;
                    if (selectedHomeEnvironment != "")
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedHomeEnvironment, 0, intFamilyID);
                    }
                    if (Vegetarian.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Vegetarian", 0, intFamilyID);
                    }
                    if (GlutenFree.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("GlutenFree", 0, intFamilyID);
                    }
                    if (DairyFree.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("DairyFree", 0, intFamilyID);
                    }
                    if (OtherFoodAllergy.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("OtherFoodAllergy", 0, intFamilyID);
                    }
                    if (Halal.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Halal", 0, intFamilyID);
                    }
                    if (Kosher.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Kosher", 0, intFamilyID);
                    }
                    if (NoSpecialDiet.Checked)
                    {
                        intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("NoSpecialDiet", 0, intFamilyID);
                    }


                    break;
                case "Save":
                    //Upload waiver
                    if (uplWaiverFile.HasFile)
                    {
                        strHomeName = hdnHomeName.Value;
                        strFileName = strHomeName + "-" + intFamilyID + "-Waiver-" + uplWaiverFile.FileName;
                        uplWaiverFile.SaveAs(strUploadURL + strFileName);
                        strResultMsg += "Your file, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                        //Insert record in UploadedFilesInformation table
                        intResult = hsInfo.InsertUploadedFilesInformation(intFamilyID, strHomeName, strFileName, strUploadURL);
                        blWaiverSubmitted = true;
                        intResult = hsInfo.UpdateWaiverSubmittedBitflagHomestayFamilyInfo(intFamilyID, 1);
                    }
                    else if (hdnWaiverSubmitted.Value.ToLower() == "true")
                    {
                        //previous submission exists
                        blWaiverSubmitted = true;
                    }
                    //Process acknowledgement
                    if (chkAcknowledgeUnderstand.Checked)
                    {
                        intResult = hsInfo.UpdateUnderstandAgreeHomestayFamilyInfo(intFamilyID, 1);
                        //app now complete; update status
                        intResult = hsInfo.UpdateFamilyStatus(intFamilyID, strFamilyStatus);
                    }
                    //Display Waiver
                    divConfirmation.Visible = true;
                    divWaiver.Visible = false;
                    divButtons.Visible = false;
                    litPageTitle.Text = "Confirmation";
                    break;
                case "Printable Waiver":
                    Response.Redirect("/Homestay/Families/PlacementProfile.aspx?ID=" + intFamilyID + "&print=true");
                    break;
                case "Back":
                    Response.Redirect("/Homestay/Families/ApplicationForm.aspx?ID=" + intFamilyID);
                    break;
                case "Continue":
                    divConfirmation.Visible = false;
                    btnDone.Visible = true;
                    btnSave.Visible = false;
                    divNextPage.Visible = false;
                    btnPrevious.Visible = true;
                    fldProfile.Visible = false;
                    divWaiver.Visible = true;
                    divPrintWaiver.Visible = true;
                    litPageTitle.Text = "Sign Form";

                    break;
            }
        }
        //strResultMsg += "Button Face: " + strButtonFace + "<br />";
        //Display data
        DataTable dtFamilyInfo = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
        if (dtFamilyInfo != null)
        {
            if (dtFamilyInfo.Rows.Count > 0)
            {
                traveledWhere.Text = dtFamilyInfo.Rows[0]["travelOutsideUS"].ToString();
                smokingHabits.Text = dtFamilyInfo.Rows[0]["smokerInHome"].ToString();
                activitiesEnjoyed.Text = dtFamilyInfo.Rows[0]["otherHobbies"].ToString();
                homeEnvironmentPreferences.Text = dtFamilyInfo.Rows[0]["homeEnvironment"].ToString();
                anythingElse.Text = dtFamilyInfo.Rows[0]["additionalPreferences"].ToString();
                rdoHomestayOption.SelectedValue = dtFamilyInfo.Rows[0]["homestayOption"].ToString();
                rdoGenderPreference.SelectedValue = dtFamilyInfo.Rows[0]["genderPreference"].ToString();
                blWaiverSubmitted = Convert.ToBoolean(dtFamilyInfo.Rows[0]["waiverSubmitted"]);
                strHomeName = dtFamilyInfo.Rows[0]["homeName"].ToString();
                hdnHomeName.Value = strHomeName;
                hdnWaiverSubmitted.Value = blWaiverSubmitted.ToString();
                if (blWaiverSubmitted)
                {
                    Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                    AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                    strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                    litWaiverFile.Text = "";
                    DataTable dt = hsInfo.GetUploadedFiles(intFamilyID);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                strFileName = dr["fileName"].ToString();
                                if (strFileName.Contains("Waiver"))
                                {
                                    //get the file name
                                    litWaiverFile.Text += "<br /><b>Waiver submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>"; ;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (intFamilyID != 0)
        {
            DataTable dtFamPref = hsInfo.GetHomestayPreferenceSelections(0, intFamilyID);
            if (dtFamPref != null)
            {
                if (dtFamPref.Rows.Count > 0)
                {
                    foreach (DataRow dtFamPrefRow in dtFamPref.Rows)
                    {
                        String objID = dtFamPrefRow["fieldID"].ToString().Trim();
                        String objValue = dtFamPrefRow["fieldValue"].ToString().Trim();
                        //strResultMsg += "Object ID; " + objID + " Value: " + objValue + "<br />"; 
                        if (objID.Substring(0, 3).Contains("rdo"))
                        {
                            //Radio Button
                            //declare specific type of control
                            try
                            {
                                //RadioButtonList myRDO = (RadioButtonList)Page.Master.FindControl("MainContent").FindControl("fldProfile").FindControl(objID);
                                RadioButtonList myRDO = (RadioButtonList)FindControl("fldProfile").FindControl(objID);
                                if (myRDO != null)
                                {
                                    //set value
                                    myRDO.SelectedValue = objValue;
                                }
                            } catch { 
                                //strResultMsg += "Radio button list " + objID + " with a value of " + objValue + " is null.<br />"; 
                            }
                        }
                        else
                        {
                            //Checkbox
                            //declare specific type of control
                            try
                            {
                                //CheckBox myControl = (CheckBox)Page.Master.FindControl("MainContent").FindControl("fldProfile").FindControl(objID);
                                CheckBox myControl = (CheckBox)FindControl("fldProfile").FindControl(objID);
                                if (myControl != null)
                                {
                                    //set value
                                    myControl.Checked = true;
                                }
                            } catch { 
                                //strResultMsg += "Checkbox " + objID + " with a value of " + objValue + " is null.<br />"; 
                            }
                        }
                    }
                }
                else { strResultMsg += "Preference Selections returned no rows.<br />"; }
            }
            else { strResultMsg += "Preference Selections table is null<br />"; }
        }
        litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }
}