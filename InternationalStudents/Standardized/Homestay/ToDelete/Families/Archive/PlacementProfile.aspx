﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PlacementProfile.aspx.cs" Inherits="Homestay_Families_Archive_PlacementProfile" %>
<!DOCTYPE html>

<html lang="en-us" >

<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>CCS Homestay Program</title>
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" />
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />         
    <link href="/Homestay/_css/Print.css" rel="stylesheet" type="text/css" media="print" />  
    <meta http-equiv="X-UA-Compatible" content="IE=9" />        
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>
</head>

<body>
<h1>CCS Homestay Program</h1>
<form id="form1" runat="server">
<h2 class="noPrint"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></h2>
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnFamilyID" runat="server" /><asp:HiddenField ID="hdnAppStatus" runat="server" /><asp:HiddenField ID="hdnHomeName" runat="server" />
<fieldset id="fldProfile" runat="server">
<legend>Preferences</legend>
    <h4>Homestay Preferences</h4>
    <p>
        <label for="rdoHomestayOption">Homestay Program</label><br />
        <asp:RadioButtonList ID="rdoHomestayOption" runat="server" RepeatDirection="Vertical">
             <asp:ListItem Value="Full18" Text="Full - 18+ Room and Board, $700" />
             <asp:ListItem Value="Shared18" Text="Shared - 18+ Room Only, $400" />
             <asp:ListItem Value="FullUA" Text="Full Minors - Ages 16/17, Room and Board, $850" />
             <asp:ListItem Value="NoPreference" Text="No preference" />
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="rdoHomestayOption" CssClass="redText" runat="server" ErrorMessage="Please indicate your family's general lifestyle."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="rdoGenderPreference">Gender of Student(s)</label><br />
        <asp:RadioButtonList ID="rdoGenderPreference" runat="server" RepeatDirection="Horizontal">
             <asp:ListItem Value="Male" Text="Males Only" />
             <asp:ListItem Value="Female" Text="Females Only" />
             <asp:ListItem Value="Either" Text="Either" />
             <asp:ListItem Value="Combination" Text="Combination - separate bathrooms available" />
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="rdoGenderPreference" CssClass="redText" runat="server" ErrorMessage="Please indicate your family's general lifestyle."></asp:RequiredFieldValidator>
    </p>
    <h4>Personal Preferences and Activities</h4>
    <p>
        <label for="rdoTraveledOutsideCountry">Have you traveled outside the U.S. before?</label><br />
        <asp:RadioButtonList ID="rdoTraveledOutsideCountry" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Traveled outside country">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Not traveled outside country">No&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="rdoTraveledOutsideCountry" CssClass="redText" runat="server" ErrorMessage="Please indicate your family's general lifestyle."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="traveledWhere">Explain:</label><br />
        <asp:TextBox ID="traveledWhere" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        <label for="rdoSmoker">Does anyone living in your home smoke?</label><br />
        <asp:RadioButtonList ID="rdoSmoker" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Smoker">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Occasional smoker">Sometimes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Nonsmoker">No&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="rdoSmoker" CssClass="redText" runat="server" ErrorMessage="Please indicate your family's general lifestyle."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="smokingHabits">Explain:</label><br />
        <asp:TextBox ID="smokingHabits" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        <label for="rdoOtherSmokerOK">Is it okay if your homestay student smokes?</label><br />
        <asp:RadioButtonList ID="rdoOtherSmokerOK" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Other smoker okay">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Other smoker not okay">No&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Other smoker no preference">No Preference</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="rdoOtherSmokerOK" CssClass="redText" runat="server" ErrorMessage="Please indicate your family's general lifestyle."></asp:RequiredFieldValidator>
    </p>
    <p>
        What hobbies, sports or other activities do you or other members of your household enjoy?<br />
        <asp:CheckBox ID="MusicalInstrument" runat="server" Text="Play a musical instrument" /><br />
        <asp:CheckBox ID="Art" runat="server" Text="Drawing/painting" /><br />
        <asp:CheckBox ID="TeamSports" runat="server" Text="Team sports" /><br />
        <asp:CheckBox ID="IndividualSports" runat="server" Text="Individual sports" /><br />
        <asp:CheckBox ID="ListenMusic" runat="server" Text="Listen to music" /><br />
        <asp:CheckBox ID="Drama" runat="server" Text="Drama" /><br />
        <asp:CheckBox ID="WatchMovies" runat="server" Text="Watch movies" /><br />
        <asp:CheckBox ID="Singing" runat="server" Text="Singing" /><br />
        <asp:CheckBox ID="Shopping" runat="server" Text="Shopping" /><br />
        <asp:CheckBox ID="ReadBooks" runat="server" Text="Read books" /><br />
        <asp:CheckBox ID="Outdoors" runat="server" Text="Hiking/camping/outdoors" /><br />
        <asp:CheckBox ID="Cooking" runat="server" Text="Cooking" /><br />
        <asp:CheckBox ID="Photography" runat="server" Text="Photography" /><br />
        <asp:CheckBox ID="Gaming" runat="server" Text="Gaming" />
    </p>
    <p>
        <label for="activitiesEnjoyed">Other hobbies/activities you enjoy:</label><br />
        <asp:TextBox ID="activitiesEnjoyed" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        <label for="rdoInteraction">How much interaction and conversation with your student do you hope to have each day/week?</label><br />
        <asp:RadioButtonList ID="rdoInteraction" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Interaction every day">Every Day</asp:ListItem>
            <asp:ListItem Value="Interaction frequent">Frequent, but not every day</asp:ListItem>
            <asp:ListItem Value="Interaction minimal">Minimal conversation is okay</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="rdoInteraction" CssClass="redText" runat="server" ErrorMessage="Please indicate your family's general lifestyle."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="rdoHomeEnvironment">I would describe my home as more:</label><br />
        <asp:RadioButtonList ID="rdoHomeEnvironment" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Home quiet">Quiet&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Home busy and active">Busy and Active&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="rdoHomeEnvironment" CssClass="redText" runat="server" ErrorMessage="Please indicate your family's general lifestyle."></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="homeEnvironmentPreferences">Explain:</label><br />
        <asp:TextBox ID="homeEnvironmentPreferences" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
    </p>
    <p>
        I am willing to accommodate the following dietary needs for students:<br />
        <asp:CheckBox ID="Vegetarian" runat="server" Text="Vegetarian" /><br />
        <asp:CheckBox ID="GlutenFree" runat="server" Text="Gluten free" /><br />
        <asp:CheckBox ID="DairyFree" runat="server" Text="Dairy free" /><br />
        <asp:CheckBox ID="OtherFoodAllergy" runat="server" Text="Other food allergy" /><br />
        <asp:CheckBox ID="Halal" runat="server" Text="Halal" />  (Food prepared as prescribed by Muslim law)<br />
        <asp:CheckBox ID="Kosher" runat="server" Text="Kosher" /><br />
        <asp:CheckBox ID="NoSpecialDiet" runat="server" Text="None (I am not able to accommodate a specific diet)" /><br />
    </p>
    <p>
        <label for="anythingElse">Is there anything else you'd like us to know about your family?</label><br />
        <asp:TextBox ID="anythingElse" runat="server" Width="500px" BackColor="ControlLight" MaxLength="300"></asp:TextBox>
    </p>

</fieldset>
<div id="divWaiver" runat="server">
    <div id="divPrint" runat="server" visible="false" class="noPrint" style="float:right;margin-right:30px;">
        <input type="button" name="btnPrint" onclick="javascript:window.print();" value="Print" />
    </div>
    <h2>Instructions</h2>
    <p>Please read, print and sign the following waiver and submit.</p>
    <ul>
        <li>Upload a scanned copy using the File Upload below.</li>
        <li>Or mail to:
        <address>
            International Homestay<br />
            Global Education<br />
            Spokane Falls Community College<br />
            3410 West Fort George Wright Drive MS 3011<br />
            Spokane, WA 99224-5288
        </address> 
        </li>
    </ul>
    <div id="divUpload" class="noPrint" style="border: 1px solid Gray;width:60%;padding:15px;" runat="server">
        <p>
            <label for="uplWaiverFile">Document Upload:</label>&nbsp;
            <asp:FileUpload ID="uplWaiverFile" runat="server" Size="70" /> <asp:Literal ID="litWaiverFile" runat="server"></asp:Literal>
            <asp:HiddenField ID="hdnWaiverSubmitted" runat="server" /><br />
        </p>
    </div>
    <p><u><b>WAIVER AND RELEASE OF LIABILITY:</b></u> In consideration of being permitted to participate in the Homestay Program, we hereby release the State of Washington, the Board of Trustees of the Community Colleges of Spokane, the Community Colleges of Spokane, and any other subdivision or unit of the Community Colleges of Spokane, its officers, employees, representatives and agents (“Releasees”), from any and all liability, claims costs expenses, injuries, and/or losses I may sustain, including any actions or causes of action wither at law or in equity, including without limitation liability for property damage or loss, and liability for debts, conduct or actions of the student(s) assigned to our home. We understand that the student will assume responsibility for their own debts, conduct, and actions while in the Homestay Program.</p>
    <p><u><b>EMPLOYEES OF THE COMMUNITY COLLEGES OF SPOKANE</b></u> As an employee of the Community Colleges of Spokane, I acknowledges that I participate in this program as a volunteer in my personal capacity, and not in the course and scope of my employment with the Community Colleges of Spokane. As such, I agree to the general Waiver and Release of Liability as outlined above.</p>
    <p><b>BY SIGNING BELOW, I ACKNOWLEDGE THAT I HAVE BOTH READ AND UNDERSTOOD THE ABOVE STATED WAIVER AND RELEASE OF LIABILITY AND GUIDELINES FOR EMPLOYEES OF THE COMMUNITY COLLEGES OF SPOKANE AND AGREE TO ACCEPT THEM AS A CONDITION OF OUR PARTICIPATION IN THE PROGRAM.</b></p>
    <p>&nbsp;</p>
    <div style="width:49%;float:left;padding-right:5px;">
        <p>Signature:&nbsp;_________________________________</p>
        <p>Legal name (print):&nbsp;___________________________________</p>
    </div>
    <div style="width:49%;float:left;">
        <p>Signature:&nbsp;________________________________ </p>
        <p>Legal name (print):&nbsp;___________________________________</p>
    </div>
    <div id="divCheckbox" runat="server" visible="true">
        <p>&nbsp;</p>
        <p>
            <asp:CheckBox ID="chkAcknowledgeUnderstand" runat="server" Text="&nbsp;I acknowledge that I have
            read this waiver and release of liability." /><label for="chkAcknowledgeUnderstand">&nbsp;I understand what is required of me as a Homestay Host Family.</label>
        </p>
    </div>
</div>
<div id="divConfirmation" runat="server">
    <h2>Thank you for your interest in the CCS Homestay Program!</h2>
    <p>When the review of your application is complete, 
    you will be contacted for a home visit to view the rooms available and to answer any questions regarding our program.</p>
    <p>In the meantime, please review the information below in preparation for receiving your student.</p>
    <ul>
        <li><a href="_PDF/2017-SafetyChecklist.pdf">Safety Checklist</a></li>
        <li><a href="_PDF/2017-18-StudentPlanSummary.pdf">Student Insurance Plan Summary</a></li>
        <li><a href="_PDF/UAHomestayFamilyResponsibilities-2017-2018.pdf">Homestay Family Responsibilities</a></li>
        <li><a href="_PDF/CellPhonesUA-2017.pdf">Cellphones</a></li>
        <li><a href="_PDF/MentalHealth-2017.pdf">Mental Health</a></li>
        <li><a href="_PDF/SCCFall17WelcomeWeek.pdf">International Student Welcome Week</a></li>
    </ul>
    <p>&nbsp;</p>
</div>
<div id="divButtons" runat="server">
    <div id="btnSave" runat="server" style="float:left;">
        <input type="submit" name="btnSubmit" value="Save and Continue"/>&nbsp;&nbsp;
    </div>
    <div id="btnDone" runat="server" style="float:left;margin-left:30px;">
        <input type="submit" name="btnSubmit" value="Save" />&nbsp;&nbsp;
    </div>
    <div id="divNextPage" runat="server" style="float:left;margin-left:30px;">
        <input type="submit" name="btnSubmit" id="Next" value="Continue"/>&nbsp;&nbsp;
    </div>
    <div id="btnPrevious" runat="server" style="float:left;margin-left:30px;">
        <input type="submit" name="btnSubmit" value="Back" />
    </div>
    <div id="divPrintWaiver" runat="server" visible="false" style="float:left;margin-left:30px;">
        <input type="submit" name="btnSubmit" value="Printable Waiver" />
    </div>
    <div id="divHome" runat="server" style="float:left;margin-left:30px;">
        <input style="float:left;margin-left:30px;" type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;
    </div>

</div>
</form>
</body>
</html>




