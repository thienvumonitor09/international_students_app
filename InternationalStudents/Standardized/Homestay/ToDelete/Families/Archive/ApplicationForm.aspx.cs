﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Families_Archive_ApplicationForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String strButtonFace = "";
        String strResultMsg = "";
        Int32 intResult = 0;
        Int32 intFamilyID = 0;
        Int32 intMainLevelRooms = 0;
        Int32 intUpstairsRooms = 0;
        Int32 intBasementRooms = 0;
        String FamilyID = "";
        String strRelationship = "";
        Boolean blLicenseSubmitted = false;
        String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
        String strFileName = "";
        String strUploadServer = "";
        Boolean blAddMode = true;
        String selectedFamily = "";
        String strUserName = ""; //Ideally this is inserted by family after logging in when applying
        String strHomeName = "";
        Boolean blIsAdmin = false;

        Homestay hsInfo = new Homestay();
        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (Request.QueryString["ID"] != null)
        {
            //page load
            FamilyID = Request.QueryString["ID"].ToString();
            hdnFamilyID.Value = FamilyID;
        }
        if (FamilyID == "")
        {
            //after postback
            FamilyID = hdnFamilyID.Value;
        }
        strResultMsg += "FamilyID: " + FamilyID + "<br />";
        if (FamilyID != "" && FamilyID != null)
        {
            intFamilyID = Convert.ToInt32(FamilyID);
            blAddMode = false;
        }
        //search for an existing application with this family's last name and first name of primary contact.
        selectedFamily = ddlFamilies.SelectedValue;
        if (selectedFamily != "" && selectedFamily != "0" && selectedFamily != null)
        {
            blAddMode = false;
            intFamilyID = Convert.ToInt32(selectedFamily);  //overrides any previous value        
        }
        if (FamilyID == "0")
            blAddMode = true;
        //This only happens when the back button is hit
        if (selectedFamily == "" && intFamilyID > 0)
        {
            selectedFamily = intFamilyID.ToString();
        }
        //strResultMsg += "Selected Family: " + selectedFamily + "<br />";
        strResultMsg += "Add mode? " + blAddMode + "<br />";
        //Get UserID if family applying
        if (Request.QueryString["UID"] != null)
        {
            strUserName = Request.QueryString["UID"].ToString();

        }
        else if (Request.ServerVariables["LOGON_USER"] != null)
        {
            //set admin status for visibility control
            blIsAdmin = true;
        }
        if (Request.QueryString["admin"] != null)
        {
            blIsAdmin = Convert.ToBoolean(Request.QueryString["admin"].ToString());
        }
        //strResultMsg += "Family User ID: " + strUserName + "; Is admin? " + blIsAdmin + "<br />";
        //Process submissions
        if (strButtonFace != "" && IsPostBack)
        {
            if (strButtonFace == "Continue")
            {
                Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID);
            }
            if (strButtonFace == "Return to Admin Home Page")
            {
                Response.Redirect("/Homestay/Admin/Default.aspx");
            }
            //Admin only fields
            String strWalkingBusStopMinutes = "";
            String strWalkingBusStopMiles = "";
            String strWalkingBusStopBlocks = "";
            String strBusMinutesSCC = "";
            String strBusMinutesSFCC = "";
            String strCollegeQualified = "";
            if (blIsAdmin && !blAddMode)
            {
                strWalkingBusStopMinutes = walkingBusStopMinutes.Text.Replace("'", "''");
                strWalkingBusStopMiles = walkingBusStopMiles.Text.Replace("'", "''");
                strWalkingBusStopBlocks = walkingBusStopBlocks.Text.Replace("'", "''");
                strBusMinutesSCC = busMinutesSCC.Text.Replace("'", "''");
                strBusMinutesSFCC = busMinutesSFCC.Text.Replace("'", "''");
                strCollegeQualified = collegeQualified.SelectedValue;
            }
            //Home information 
            String strStreetAddress = streetAddress.Text.Replace("'", "''");
            String strCity = city.Text.Replace("'", "''");
            String strState = state.Text;
            String strZIP = ZIP.Text;
            String strLandLinePhone = landLinePhone.Text;
            String strHowReferred = howReferred.Text.Replace("'", "''");
            String strRoom1 = rdoRoom1.SelectedValue;
            String strRoom1DateOpen = room1DateOpen.Text.Replace("'", "''");
            String strRoom1DateClosed = room1DateClosed.Text.Replace("'", "''");
            String strRoom1Bathroom = rdoRoom1Bathroom.SelectedValue;
            String strRoom1Occupancy = rdoRoom1Occupancy.Text.Replace("'", "''");
            String strRoom2 = rdoRoom2.SelectedValue;
            String strRoom2DateOpen = room2DateOpen.Text.Replace("'", "''");
            String strRoom2DateClosed = room2DateClosed.Text.Replace("'", "''");
            String strRoom2Bathroom = rdoRoom2Bathroom.SelectedValue;
            String strRoom2Occupancy = rdoRoom2Occupancy.Text.Replace("'", "''");
            String strRoom3 = rdoRoom3.SelectedValue;
            String strRoom3DateOpen = room3DateOpen.Text.Replace("'", "''");
            String strRoom3DateClosed = room3DateClosed.Text.Replace("'", "''");
            String strRoom3Bathroom = rdoRoom3Bathroom.SelectedValue;
            String strRoom3Occupancy = rdoRoom3Occupancy.Text.Replace("'", "''");
            String strRoom4 = rdoRoom4.SelectedValue;
            String strRoom4DateOpen = room4DateOpen.Text.Replace("'", "''");
            String strRoom4DateClosed = room4DateClosed.Text.Replace("'", "''");
            String strRoom4Bathroom = rdoRoom4Bathroom.SelectedValue;
            String strRoom4Occupancy = rdoRoom4Occupancy.Text.Replace("'", "''");
            String strInsuranceCompany = insuranceCompanyName.Text.Replace("'", "''");
            String strInsurancePolicyNumber = insurancePolicyNumber.Text.Replace("'", "''");
            String strInsuranceAgentName = insuranceAgentName.Text.Replace("'", "''");
            String strInsuranceAgentPhone = insuranceAgentPhone.Text.Replace("'", "''");
            String strWhyHost = whyHost.Text.Replace("'", "''");
            String strHostExperience = hostExperience.Text.Replace("'", "''");
            String strPrimaryLanguage = primaryLanguage.Text.Replace("'", "''");
            String strOtherLanguages = otherLanguages.Text.Replace("'", "''");
            String strReligiousActivities = religiousActivities.Text.Replace("'", "''");
            String strSmokingGuidelines = smokingGuidelines.Text.Replace("'", "''");
            String strPetsInHome = petsInHome.Text.Replace("'", "''");
            String strGeneralLifestyle = generalLifestyle.Text.Replace("'", "''");
            String strOtherSchool = otherSchool.Text.Replace("'", "''");
            String strAdditionalExpectations = additionalExpectations.Text.Replace("'", "''");
            //Primary Contact
            String FamilyName = familyName.Text.Replace("'", "''");
            String FirstName = firstName.Text.Replace("'", "''");
            String MiddleName = middleName.Text.Replace("'", "''");
            String DateOfBirth = DOB.Text;
            String Gender = rdoGender.SelectedValue;
            String DriversLicenseNumber = driversLicenseNumber.Text;
            String Occupation = occupation.Text.Replace("'", "''");
            String CellPhone = cellPhone.Text;
            String WorkPhone = workPhone.Text;
            String Email = email.Text;
            //Secondary Contact
            String FamilyName2 = familyName2.Text.Replace("'", "''");
            String FirstName2 = firstName2.Text.Replace("'", "''");
            String MiddleName2 = middleName2.Text.Replace("'", "''");
            String DateOfBirth2 = DOB2.Text;
            String Gender2 = rdoGender2.SelectedValue;
            String DriversLicenseNumber2 = driversLicenseNumber2.Text;
            String Occupation2 = occupation2.Text.Replace("'", "''");
            String CellPhone2 = cellPhone2.Text;
            String WorkPhone2 = workPhone2.Text;
            String Email2 = email2.Text;
            //Other Adult 1
            String FamilyName3 = familyName3.Text.Replace("'", "''");
            String FirstName3 = firstName3.Text.Replace("'", "''");
            String MiddleName3 = middleName3.Text.Replace("'", "''");
            String DateOfBirth3 = DOB3.Text;
            String Gender3 = rdoGender3.SelectedValue;
            String DriversLicenseNumber3 = driversLicenseNumber3.Text;
            String Occupation3 = occupation3.Text.Replace("'", "''");
            String CellPhone3 = cellPhone3.Text;
            String WorkPhone3 = workPhone3.Text;
            String Email3 = email3.Text;
            //Other Adult 2
            String FamilyName4 = familyName4.Text.Replace("'", "''");
            String FirstName4 = firstName4.Text.Replace("'", "''");
            String MiddleName4 = middleName4.Text.Replace("'", "''");
            String DateOfBirth4 = DOB4.Text;
            String Gender4 = rdoGender4.SelectedValue;
            String DriversLicenseNumber4 = driversLicenseNumber4.Text;
            String Occupation4 = occupation4.Text.Replace("'", "''");
            String CellPhone4 = cellPhone4.Text;
            String WorkPhone4 = workPhone4.Text;
            String Email4 = email4.Text;
            //Minor Child 1
            String FamilyName5 = familyName5.Text.Replace("'", "''");
            String FirstName5 = firstName5.Text.Replace("'", "''");
            String DateOfBirth5 = DOB5.Text;
            String Gender5 = rdoGender5.SelectedValue;
            //Minor Child 2
            String FamilyName6 = familyName6.Text.Replace("'", "''");
            String FirstName6 = firstName6.Text.Replace("'", "''");
            String DateOfBirth6 = DOB6.Text;
            String Gender6 = rdoGender6.SelectedValue;
            //Minor Child 3
            String FamilyName7 = familyName7.Text.Replace("'", "''");
            String FirstName7 = firstName7.Text.Replace("'", "''");
            String DateOfBirth7 = DOB7.Text;
            String Gender7 = rdoGender7.SelectedValue;
            //Minor Child 4
            String FamilyName8 = familyName8.Text.Replace("'", "''");
            String FirstName8 = firstName8.Text.Replace("'", "''");
            String DateOfBirth8 = DOB8.Text;
            String Gender8 = rdoGender8.SelectedValue;
            if (strButtonFace == "Save and Continue")
            {
                //Upload drivers license copies, if any
                if (uplLicenseFile.HasFile)
                {
                    strFileName = strHomeName + "-" + intFamilyID + "-License-" + uplLicenseFile.FileName;
                    uplLicenseFile.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your file, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    //Insert record in UploadedFilesInformation table
                    intResult = hsInfo.InsertUploadedFilesInformation(intFamilyID, strHomeName, strFileName, strUploadURL);
                    blLicenseSubmitted = true;
                }
                else if (hdnLicenseSubmitted.Value.ToLower() == "true")
                {
                    //previous submission exists
                    blLicenseSubmitted = true;
                }
                //Insert HomestayFamilyInfo table
                //Concatenate home name
                strHomeName = familyName.Text + ", " + firstName.Text;
                if (firstName2.Text != "") { strHomeName += " and " + firstName2.Text; }

                //Retrieve family ID with insert
                intFamilyID = hsInfo.InsertHomestayFamilyInfo(strUserName, strHomeName, strStreetAddress, strCity, strState, strZIP, strLandLinePhone, 
                    strInsuranceCompany, strInsurancePolicyNumber, strInsuranceAgentName, strInsuranceAgentPhone, strWhyHost, strHostExperience, strPrimaryLanguage, strOtherLanguages,
                    strReligiousActivities, strSmokingGuidelines, strPetsInHome, strGeneralLifestyle, strOtherSchool, strAdditionalExpectations, blLicenseSubmitted, strHowReferred,
                    strRoom1, strRoom1DateOpen, strRoom1DateClosed, strRoom1Bathroom, strRoom1Occupancy,  
                    strRoom2, strRoom2DateOpen, strRoom2DateClosed, strRoom2Bathroom, strRoom2Occupancy,  
                    strRoom3, strRoom3DateOpen, strRoom3DateClosed, strRoom3Bathroom, strRoom3Occupancy,  
                    strRoom4, strRoom4DateOpen, strRoom4DateClosed, strRoom4Bathroom, strRoom4Occupancy);

                //Insert Primary Contact into HomestayRelatives table
                intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName, FirstName, Occupation, DateOfBirth, Gender, "Primary Contact", Email, CellPhone, WorkPhone, DriversLicenseNumber, MiddleName);
                //Remainder of relatives, if any
                if (FamilyName2 != "" && FirstName2 != "")
                {
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
                }
                if (FamilyName3 != "" && FirstName3 != "")
                {
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
                }
                if (FamilyName4 != "" && FirstName4 != "")
                {
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
                }
                if (FamilyName5 != "" && FirstName5 != "")
                {
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
                }
                if (FamilyName6 != "" && FirstName6 != "")
                {
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
                }
                if (FamilyName7 != "" && FirstName7 != "")
                {
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
                }
                if (FamilyName8 != "" && FirstName8 != "")
                {
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
                }
                if (hsInfo.strResultMsg == "") { Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID); }
            }
            if (strButtonFace == "Update and Continue")
            {
                //process updates
                strHomeName = homeName.Text;
                strUserName = userName.Text;
                //Upload drivers license copies
                if (uplLicenseFile.HasFile)
                {
                    strFileName = strHomeName + "-" + intFamilyID + "-License-" + uplLicenseFile.FileName;
                    uplLicenseFile.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your file, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    //Insert record in UploadedFilesInformation table
                    intResult = hsInfo.InsertUploadedFilesInformation(intFamilyID, strHomeName, strFileName, strUploadURL);
                    blLicenseSubmitted = true;
                }
                else if (hdnLicenseSubmitted.Value.ToLower() == "true")
                {
                    //previous submission exists
                    blLicenseSubmitted = true;
                }
                intResult = hsInfo.UpdateP1HomestayFamilyInfo(intFamilyID, strUserName, strHomeName, strStreetAddress, strCity, strState, strZIP, strLandLinePhone, intMainLevelRooms, intUpstairsRooms, intBasementRooms,
                    strInsuranceCompany, strInsurancePolicyNumber, strInsuranceAgentName, strInsuranceAgentPhone, strWhyHost, strHostExperience, strPrimaryLanguage, strOtherLanguages,
                    strReligiousActivities, strSmokingGuidelines, strPetsInHome, strGeneralLifestyle, strOtherSchool, strAdditionalExpectations, blLicenseSubmitted, strHowReferred,
                    strRoom1, strRoom1DateOpen, strRoom1DateClosed, strRoom1Bathroom, strRoom1Occupancy,  
                    strRoom2, strRoom2DateOpen, strRoom2DateClosed, strRoom2Bathroom, strRoom2Occupancy,  
                    strRoom3, strRoom3DateOpen, strRoom3DateClosed, strRoom3Bathroom, strRoom3Occupancy,  
                    strRoom4, strRoom4DateOpen, strRoom4DateClosed, strRoom4Bathroom, strRoom4Occupancy);
                if (blIsAdmin)
                {
                    //update admin only fields
                    intResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID, strWalkingBusStopMinutes, strWalkingBusStopMiles, strWalkingBusStopBlocks, strBusMinutesSCC, strBusMinutesSFCC, strCollegeQualified);
                }
                
                //Primary Contact
                if (hdnPrimaryID.Value != null && hdnPrimaryID.Value != "")
                {
                    Int32 PrimaryID = Convert.ToInt32(hdnPrimaryID.Value);
                    intResult = hsInfo.UpdateHomestayRelatives(PrimaryID,FamilyName, FirstName, Occupation, DateOfBirth, Gender, "Primary Contact", Email, CellPhone, WorkPhone, DriversLicenseNumber, MiddleName);
                }
                //Secondary Contact
                if (hdnSecondaryID.Value != null && hdnSecondaryID.Value != "")
                {
                    Int32 SecondaryID = Convert.ToInt32(hdnSecondaryID.Value);
                    intResult = hsInfo.UpdateHomestayRelatives(SecondaryID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
                }
                else if (FamilyName2 != "" && FirstName2 != "")
                {
                    //Insert new relative
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
                }
                //Adult 1
                if (hdnAdult1ID.Value != null && hdnAdult1ID.Value != "")
                {
                    Int32 Adult1ID = Convert.ToInt32(hdnAdult1ID.Value);
                    intResult = hsInfo.UpdateHomestayRelatives(Adult1ID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
                }
                else if (FamilyName3 != "" && FirstName3 != "")
                {
                    //Insert new relative
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
                }
                //Adult 2
                if (hdnAdult2ID.Value != null && hdnAdult2ID.Value != "")
                {
                    Int32 Adult2ID = Convert.ToInt32(hdnAdult2ID.Value);
                    intResult = hsInfo.UpdateHomestayRelatives(Adult2ID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
                }
                else if (FamilyName4 != "" && FirstName4 != "")
                {
                    //Insert new relative
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
                }

                //Minor Child 1
                if (hdnChild1ID.Value != null && hdnChild1ID.Value != "")
                {
                    Int32 Child1ID = Convert.ToInt32(hdnChild1ID.Value);
                    intResult = hsInfo.UpdateHomestayRelatives(Child1ID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
                }
                else if (FamilyName5 != "" && FirstName5 != "")
                {
                    //Insert new relative
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
                }
                //Minor Child 2
                if (hdnChild2ID.Value != null && hdnChild2ID.Value != "")
                {
                    Int32 Child2ID = Convert.ToInt32(hdnChild2ID.Value);
                    intResult = hsInfo.UpdateHomestayRelatives(Child2ID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
                }
                else if (FamilyName6 != "" && FirstName6 != "")
                {
                    //Insert new relative
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
                }
                //Minor Child 3
                if (hdnChild3ID.Value != null && hdnChild3ID.Value != "")
                {
                    Int32 Child3ID = Convert.ToInt32(hdnChild3ID.Value);
                    intResult = hsInfo.UpdateHomestayRelatives(Child3ID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
                }
                else if (FamilyName7 != "" && FirstName7 != "")
                {
                    //Insert new relative
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
                }
                //Minor Child 4
                if (hdnChild4ID.Value != null && hdnChild4ID.Value != "")
                {
                    Int32 Child4ID = Convert.ToInt32(hdnChild4ID.Value);
                    intResult = hsInfo.UpdateHomestayRelatives(Child4ID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
                }
                else if (FamilyName8 != "" && FirstName8 != "")
                {
                    //Insert new relative
                    intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
                }
            }
            if (hsInfo.strResultMsg == "") { Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID); }
        }
        if (!blAddMode && intFamilyID > 0)
        {
            //Populate fields
            //Base info
            DataTable dtFamily = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
            if (dtFamily != null)
            {
                if (dtFamily.Rows.Count > 0)
                {
                    userName.Text = dtFamily.Rows[0]["userName"].ToString().Trim();
                    homeName.Text = dtFamily.Rows[0]["homeName"].ToString().Trim();
                    streetAddress.Text = dtFamily.Rows[0]["streetAddress"].ToString().Trim();
                    city.Text = dtFamily.Rows[0]["city"].ToString().Trim();
                    state.Text = dtFamily.Rows[0]["state"].ToString().Trim();
                    ZIP.Text = dtFamily.Rows[0]["ZIP"].ToString().Trim();
                    landLinePhone.Text = dtFamily.Rows[0]["landlinePhone"].ToString().Trim();
                    insuranceCompanyName.Text = dtFamily.Rows[0]["insuranceCompanyName"].ToString().Trim();
                    insurancePolicyNumber.Text = dtFamily.Rows[0]["insurancePolicyNumber"].ToString().Trim();
                    insuranceAgentName.Text = dtFamily.Rows[0]["insuranceAgentName"].ToString().Trim();
                    insuranceAgentPhone.Text = dtFamily.Rows[0]["insuranceAgentPhone"].ToString().Trim();
                    whyHost.Text = dtFamily.Rows[0]["whyHost"].ToString().Trim();
                    hostExperience.Text = dtFamily.Rows[0]["hostExperience"].ToString().Trim();
                    primaryLanguage.Text = dtFamily.Rows[0]["primaryLanguage"].ToString().Trim();
                    otherLanguages.Text = dtFamily.Rows[0]["otherLanguages"].ToString().Trim();
                    religiousActivities.Text = dtFamily.Rows[0]["religiousActivities"].ToString().Trim();
                    smokingGuidelines.Text = dtFamily.Rows[0]["smokingGuidelines"].ToString().Trim();
                    petsInHome.Text = dtFamily.Rows[0]["petsInHome"].ToString().Trim();
                    generalLifestyle.Text = dtFamily.Rows[0]["generalLifestyle"].ToString().Trim();
                    otherSchool.Text = dtFamily.Rows[0]["otherSchool"].ToString().Trim();
                    additionalExpectations.Text = dtFamily.Rows[0]["additionalExpectations"].ToString().Trim();
                    Boolean blIsActive = Convert.ToBoolean(dtFamily.Rows[0]["IsActive"]);
                    if (blIsActive) { rdoIsActive.SelectedValue = "1"; } else { rdoIsActive.SelectedValue = "0"; }
                    familyStatus.Text = dtFamily.Rows[0]["familyStatus"].ToString().Trim();
                    blLicenseSubmitted = Convert.ToBoolean(dtFamily.Rows[0]["licenseCopiesSubmitted"]);
                    hdnLicenseSubmitted.Value = blLicenseSubmitted.ToString();
                    if (blLicenseSubmitted )
                    {
                        Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                        AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                        strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                        litLicenseFile.Text = "";
                        DataTable dt = hsInfo.GetUploadedFiles(intFamilyID);
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    strFileName = dr["fileName"].ToString();
                                    if (strFileName.Contains("License"))
                                    {
                                        //get the file name
                                        litLicenseFile.Text += "<br /><b>License copies submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                    }
                                }
                            }
                        }
                    }
                    howReferred.Text = dtFamily.Rows[0]["howHearHomestay"].ToString();
                    rdoRoom1.SelectedValue = dtFamily.Rows[0]["room1Level"].ToString();
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room1StartDate"]))
                    {
                        room1DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room1EndDate"]).ToShortDateString();
                    }
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room1StartDate"]))
                    {
                        room1DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room1EndDate"]).ToShortDateString();
                    }
                    rdoRoom1Bathroom.SelectedValue = dtFamily.Rows[0]["room1Bath"].ToString();
                    rdoRoom1Occupancy.Text = dtFamily.Rows[0]["room1Occupancy"].ToString();
                    rdoRoom2.SelectedValue = dtFamily.Rows[0]["room2Level"].ToString();
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room2StartDate"]))
                    {
                        room1DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room2EndDate"]).ToShortDateString();
                    }
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room2StartDate"]))
                    {
                        room1DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room2EndDate"]).ToShortDateString();
                    }
                    rdoRoom2Bathroom.SelectedValue = dtFamily.Rows[0]["room2Bath"].ToString();
                    rdoRoom2Occupancy.Text = dtFamily.Rows[0]["room2Occupancy"].ToString();
                    rdoRoom3.SelectedValue = dtFamily.Rows[0]["room3Level"].ToString();
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room3StartDate"]))
                    {
                        room1DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room3EndDate"]).ToShortDateString();
                    }
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room3StartDate"]))
                    {
                        room1DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room3EndDate"]).ToShortDateString();
                    }
                    rdoRoom3Bathroom.SelectedValue = dtFamily.Rows[0]["room3Bath"].ToString();
                    rdoRoom3Occupancy.Text = dtFamily.Rows[0]["room3Occupancy"].ToString();
                    rdoRoom4.SelectedValue = dtFamily.Rows[0]["room4Level"].ToString();
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room4StartDate"]))
                    {
                        room1DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room4EndDate"]).ToShortDateString();
                    }
                    if (!DBNull.Value.Equals(dtFamily.Rows[0]["room4StartDate"]))
                    {
                        room1DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room4EndDate"]).ToShortDateString();
                    }
                    rdoRoom4Bathroom.SelectedValue = dtFamily.Rows[0]["room4Bath"].ToString();
                    rdoRoom4Occupancy.Text = dtFamily.Rows[0]["room4Occupancy"].ToString();
                }
            }
            //Relatives
            DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
            if (dtRelatives != null)
            {
                if (dtRelatives.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in dtRelatives.Rows)
                    {
                        strRelationship = dtRow["relationship"].ToString().Trim();
                        //strResultMsg += "Relationship: " + strRelationship + "<br />";
                        switch (strRelationship)
                        {
                            case "Primary Contact":
                                familyName.Text = dtRow["familyName"].ToString().Trim();
                                firstName.Text = dtRow["firstName"].ToString().Trim();
                                middleName.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone.Text = dtRow["workPhone"].ToString().Trim();
                                email.Text = dtRow["email"].ToString().Trim();
                                hdnPrimaryID.Value = dtRow["id"].ToString();
                                break;
                            case "Secondary Contact":
                                familyName2.Text = dtRow["familyName"].ToString().Trim();
                                firstName2.Text = dtRow["firstName"].ToString().Trim();
                                middleName2.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender2.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB2.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber2.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation2.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone2.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone2.Text = dtRow["workPhone"].ToString().Trim();
                                email2.Text = dtRow["email"].ToString().Trim();
                                hdnSecondaryID.Value = dtRow["id"].ToString();
                                break;
                            case "Other Adult 1":
                                familyName3.Text = dtRow["familyName"].ToString().Trim();
                                firstName3.Text = dtRow["firstName"].ToString().Trim();
                                middleName3.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender3.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB3.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber3.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation3.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone3.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone3.Text = dtRow["workPhone"].ToString().Trim();
                                email3.Text = dtRow["email"].ToString().Trim();
                                hdnAdult1ID.Value = dtRow["id"].ToString();
                                break;
                            case "Other Adult 2":
                                familyName4.Text = dtRow["familyName"].ToString().Trim();
                                firstName4.Text = dtRow["firstName"].ToString().Trim();
                                middleName4.Text = dtRow["middleName"].ToString().Trim();
                                rdoGender4.SelectedValue = dtRow["gender"].ToString().Trim();
                                DOB4.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                driversLicenseNumber4.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                occupation4.Text = dtRow["occupation"].ToString().Trim();
                                cellPhone4.Text = dtRow["cellPhone"].ToString().Trim();
                                workPhone4.Text = dtRow["workPhone"].ToString().Trim();
                                email4.Text = dtRow["email"].ToString().Trim();
                                hdnAdult2ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 1":
                                familyName5.Text = dtRow["familyName"].ToString().Trim();
                                firstName5.Text = dtRow["firstName"].ToString().Trim();
                                DOB5.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender5.Text = dtRow["gender"].ToString().Trim();
                                hdnChild1ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 2":
                                familyName6.Text = dtRow["familyName"].ToString().Trim();
                                firstName6.Text = dtRow["firstName"].ToString().Trim();
                                DOB6.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender6.Text = dtRow["gender"].ToString().Trim();
                                hdnChild1ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 3":
                                familyName7.Text = dtRow["familyName"].ToString().Trim();
                                firstName7.Text = dtRow["firstName"].ToString().Trim();
                                DOB7.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender7.Text = dtRow["gender"].ToString().Trim();
                                hdnChild1ID.Value = dtRow["id"].ToString();
                                break;
                            case "Minor Child 4":
                                familyName8.Text = dtRow["familyName"].ToString().Trim();
                                firstName8.Text = dtRow["firstName"].ToString().Trim();
                                DOB8.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                rdoGender8.Text = dtRow["gender"].ToString().Trim();
                                hdnChild1ID.Value = dtRow["id"].ToString();
                                break;
                        }
                    }
                }
                else { strResultMsg += "No HomestayRelatives records returned.<br />"; }
            }
            else { strResultMsg += "No HomestayRelatives records returned.<br />"; }

        }
        //Set visibility of buttons
        if (blAddMode)
        {
            //Set visibility of home name field
            divHomeName.Visible = false;
            divSaveButton.Visible = true;
            divUpdateButton.Visible = false;
            divNextPage.Visible = false;
        }
        else
        {
            divHomeName.Visible = true;
            divSaveButton.Visible = false;
            divUpdateButton.Visible = true;
            divNextPage.Visible = true;
        }
        //Get families into a drop-down list; 
        if (blIsAdmin)
        {
            //Travel time fields update only
            if (!blAddMode) { divTravelTime.Visible = true; }
            divSelector.Visible = true;
            divHomeName.Visible = true;
            divHome.Visible = true;
            divTravelTime.Visible = true;
            DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
            if (dtFamilies != null)
            {
                if (dtFamilies.Rows.Count > 0)
                {
                    ddlFamilies.Items.Clear();
                    ListItem LI;
                    LI = new ListItem();
                    LI.Value = "0";
                    LI.Text = "-- Add New Family --";
                    ddlFamilies.Items.Add(LI);
                    foreach (DataRow dr in dtFamilies.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["homeName"].ToString();
                        ddlFamilies.Items.Add(LI);
                    }
                    if (selectedFamily != "") { ddlFamilies.SelectedValue = selectedFamily; }
                }
                else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtFamilies is null.<br />"; }
        }
        else 
        { 
            divSelector.Visible = false;
            divHomeName.Visible = false;
            divHome.Visible = false;
            divTravelTime.Visible = false;
        }
        litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }

    protected void btnCmdSaveAndContinue_Click(object sender, EventArgs e)
    {
        if (streetAddress.Text.Trim() == "")
        {
            eStreetAddress.InnerHtml = "Street Address is required.";
            eStreetAddress.Focus();
            return;
        }
        else
            eStreetAddress.InnerHtml = "";
        if (city.Text.Trim() == "")
        {
            eCity.InnerHtml = "City is required.";
            city.Focus();
            return;
        }
        else
            eCity.InnerHtml = "";

    }
}