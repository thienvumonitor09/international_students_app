﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PlacementProfile.aspx.cs" Inherits="Homestay_Families_PlacementProfile" %>
<!DOCTYPE html>

<html lang="en-us" >

<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>CCS Homestay Program</title>
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" />
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />         
    <link href="/Homestay/_css/Print.css" rel="stylesheet" type="text/css" media="print" />  
    <meta http-equiv="X-UA-Compatible" content="IE=9" />   
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>     
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>    
</head>

<body>
<h1>CCS Homestay Program</h1>
<form id="form1" runat="server">
<h2 class="noPrint"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></h2>
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnFamilyID" runat="server" /><asp:HiddenField ID="hdnAppStatus" runat="server" /><asp:HiddenField ID="hdnHomeName" runat="server" />
    <asp:HiddenField ID="hdnMailSent" runat="server" />
<fieldset id="fldProfile" runat="server">
<legend>Preferences</legend>
    <h4>Homestay Preferences</h4>
    <div id="eProgramPref" runat="server" class="divError"></div>
    <p>
        <!--<label for="rdoHomestayOption">Homestay Program</label><br />
        <asp:RadioButtonList ID="rdoHomestayOption" runat="server" RepeatDirection="Vertical">
             <asp:ListItem Value="Full18" Text="Full - 18+ Room and Board, $700" />
             <asp:ListItem Value="Shared18" Text="Shared - 18+ Room Only, $400" />
             <asp:ListItem Value="FullUA" Text="Full Minors - Ages 16/17, Room and Board, $850" />
             <asp:ListItem Value="NoPreference" Text="No preference" />
        </asp:RadioButtonList>
        -->
        Homestay Program - Select all that apply<br />
        <asp:CheckBox ID="Full18" runat="server" Text="Full - 18+ Room and Board, $700" /><br />
        <asp:CheckBox ID="Shared18" runat="server" Text="Shared - 18+ Room Only, $400" /><br />
        <asp:CheckBox ID="FullUA" runat="server" Text="Full Minors - Ages 16/17, Room and Board, $850" /><br />
        <asp:CheckBox ID="NoPreference" runat="server" Text="No preference" /><br />
    </p>
    <div id="eGenderPref" runat="server" class="divError"></div>
    <p>
        <label for="rdoGenderPreference">Gender of Student(s)</label><br />
        <asp:RadioButtonList ID="rdoGenderPreference" runat="server" RepeatDirection="Horizontal">
             <asp:ListItem Value="Male" Text="Males Only" />
             <asp:ListItem Value="Female" Text="Females Only" />
             <asp:ListItem Value="Either" Text="Either" />
             <asp:ListItem Value="Combination" Text="Combination - separate bathrooms available" />
        </asp:RadioButtonList>
    </p>
    
    <h4>Personal Preferences and Activities</h4><br />
    <div class="divError">Note: the following text boxes allow 300 characters maximum.</div>
    <div id="eTravel" runat="server" class="divError"></div>
    <p>
        <label for="rdoTraveledOutsideCountry">Have you traveled outside the U.S. before?</label><br />
        <asp:RadioButtonList ID="rdoTraveledOutsideCountry" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Traveled outside country">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Not traveled outside country">No&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
    </p>    
    <p>
        <label for="traveledWhere">OPTIIONAL Which countries?</label><br />
        <asp:TextBox ID="traveledWhere" runat="server" Width="500px" BackColor="ControlLight" MaxLength="300"></asp:TextBox>
    </p>
    <div id="eSmokers" runat="server" class="divError"></div>
    <p>
        <label for="rdoSmoker">Does anyone living in your home smoke?</label><br />
        <asp:RadioButtonList ID="rdoSmoker" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Smoker">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Occasional smoker">Sometimes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Nonsmoker">No&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
    </p>   
    <p>
        <label for="smokingHabits">OPTIONAL Explain:</label><br />
        <asp:TextBox ID="smokingHabits" runat="server" Width="500px" BackColor="ControlLight" MaxLength="300"></asp:TextBox>
    </p>
    <div id="eStudentSmokeOK" runat="server" class="divError"></div>
    <p>
        <label for="rdoOtherSmokerOK">Is it okay if your homestay student smokes?</label><br />
        <asp:RadioButtonList ID="rdoOtherSmokerOK" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Other smoker OK">Yes&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Other smoker not OK">No&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Other smoker no preference">No Preference</asp:ListItem>
        </asp:RadioButtonList>
    </p>    
    <p>
        What hobbies, sports or other activities do you or other members of your household enjoy?<br />
        <asp:CheckBox ID="MusicalInstrument" runat="server" Text="Play a musical instrument" /><br />
        <asp:CheckBox ID="Art" runat="server" Text="Drawing/painting" /><br />
        <asp:CheckBox ID="TeamSports" runat="server" Text="Team sports" /><br />
        <asp:CheckBox ID="IndividualSports" runat="server" Text="Individual sports" /><br />
        <asp:CheckBox ID="ListenMusic" runat="server" Text="Listen to music" /><br />
        <asp:CheckBox ID="Drama" runat="server" Text="Drama" /><br />
        <asp:CheckBox ID="WatchMovies" runat="server" Text="Watch movies" /><br />
        <asp:CheckBox ID="Singing" runat="server" Text="Singing" /><br />
        <asp:CheckBox ID="Shopping" runat="server" Text="Shopping" /><br />
        <asp:CheckBox ID="ReadBooks" runat="server" Text="Read books" /><br />
        <asp:CheckBox ID="Outdoors" runat="server" Text="Hiking/camping/outdoors" /><br />
        <asp:CheckBox ID="Cooking" runat="server" Text="Cooking" /><br />
        <asp:CheckBox ID="Photography" runat="server" Text="Photography" /><br />
        <asp:CheckBox ID="Gaming" runat="server" Text="Gaming" />
    </p>
    <p>
        <label for="activitiesEnjoyed">OPTIONAL Other hobbies/activities you enjoy:</label><br />
        <asp:TextBox ID="activitiesEnjoyed" runat="server" Width="500px" BackColor="ControlLight" MaxLength="300"></asp:TextBox>
    </p>
    <div id="eInteraction" runat="server" class="divError"></div>
    <p>
        <label for="rdoInteraction">How much interaction and conversation with your student do you hope to have each day/week?</label><br />
        <asp:RadioButtonList ID="rdoInteraction" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Interaction every day">Every Day</asp:ListItem>
            <asp:ListItem Value="Interaction frequent">Frequent, but not every day</asp:ListItem>
            <asp:ListItem Value="Interaction minimal">Minimal conversation is okay</asp:ListItem>
        </asp:RadioButtonList>
    </p>
    <div id="eEnvironment" runat="server" class="divError"></div>
    <p>
        <label for="rdoHomeEnvironment">I would describe my home as more:</label><br />
        <asp:RadioButtonList ID="rdoHomeEnvironment" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="Home quiet">Quiet&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="Home busy and active">Busy and Active&nbsp;&nbsp;</asp:ListItem>
        </asp:RadioButtonList>
    </p>    
    <p>
        <label for="homeEnvironmentPreferences">OPTIONAL Explain:</label><br />
        <asp:TextBox ID="homeEnvironmentPreferences" runat="server" Width="500px" BackColor="ControlLight" MaxLength="300"></asp:TextBox>
    </p>
    <p>
        I am willing to accommodate the following dietary needs for students:<br />
        <asp:CheckBox ID="Vegetarian" runat="server" Text="Vegetarian" /><br />
        <asp:CheckBox ID="GlutenFree" runat="server" Text="Gluten free" /><br />
        <asp:CheckBox ID="DairyFree" runat="server" Text="Dairy free" /><br />
        <asp:CheckBox ID="OtherFoodAllergy" runat="server" Text="Other food allergy" /><br />
        <asp:CheckBox ID="Halal" runat="server" Text="Halal" />  (Food prepared as prescribed by Muslim law)<br />
        <asp:CheckBox ID="Kosher" runat="server" Text="Kosher" /><br />
        <asp:CheckBox ID="NoSpecialDiet" runat="server" Text="None (I am not able to accommodate a specific diet)" /><br />
    </p>
    <p>
        <label for="anythingElse">OPTIONAL Is there anything else you'd like us to know about your family?</label><br />
        <asp:TextBox ID="anythingElse" runat="server" Width="500px" BackColor="ControlLight" MaxLength="300"></asp:TextBox>
    </p>
</fieldset>
<div id="divWaiver" runat="server">
    <div id="divPrint" runat="server" visible="false" class="noPrint" style="float:right;margin-right:30px;">
        <input type="button" name="btnPrint" onclick="javascript:window.print();" value="Print" />
    </div>
    <h2>Instructions</h2>
    <p>Please read, print and sign the following waiver and submit.</p>
    <div class="divError">You must check the box indicating you have read and understand the waiver before you can proceed to the next page.</div>
    <ul>
        <li>Upload a scanned copy using the File Upload below.</li>
        <li>Or mail to:
        <address>
            International Homestay<br />
            Global Education<br />
            Spokane Falls Community College<br />
            3410 West Fort George Wright Drive MS 3011<br />
            Spokane, WA 99224-5288
        </address> 
        </li>
    </ul>
    <p><u><b>WAIVER AND RELEASE OF LIABILITY:</b></u> In consideration of being permitted to participate in the Homestay Program, we hereby release the State of Washington, the Board of Trustees of the Community Colleges of Spokane, the Community Colleges of Spokane, and any other subdivision or unit of the Community Colleges of Spokane, its officers, employees, representatives and agents (“Releasees”), from any and all liability, claims costs expenses, injuries, and/or losses I may sustain, including any actions or causes of action wither at law or in equity, including without limitation liability for property damage or loss, and liability for debts, conduct or actions of the student(s) assigned to our home. We understand that the student will assume responsibility for their own debts, conduct, and actions while in the Homestay Program.</p>
    <p><u><b>EMPLOYEES OF THE COMMUNITY COLLEGES OF SPOKANE</b></u> As an employee of the Community Colleges of Spokane, I acknowledges that I participate in this program as a volunteer in my personal capacity, and not in the course and scope of my employment with the Community Colleges of Spokane. As such, I agree to the general Waiver and Release of Liability as outlined above.</p>
    <p><b>BY SIGNING BELOW, I ACKNOWLEDGE THAT I HAVE BOTH READ AND UNDERSTOOD THE ABOVE STATED WAIVER AND RELEASE OF LIABILITY AND GUIDELINES FOR EMPLOYEES OF THE COMMUNITY COLLEGES OF SPOKANE AND AGREE TO ACCEPT THEM AS A CONDITION OF OUR PARTICIPATION IN THE PROGRAM.</b></p>
    <p>&nbsp;</p>
    <div style="width:49%;float:left;padding-right:5px;">
        <p>Signature:&nbsp;_________________________________</p>
        <p>Legal name (print):&nbsp;___________________________________</p>
    </div>
    <div style="width:49%;float:left;">
        <p>Signature:&nbsp;________________________________ </p>
        <p>Legal name (print):&nbsp;___________________________________</p>
    </div>
    <div id="divCheckbox" runat="server" visible="true">
        <p>&nbsp;</p>
        <p class="noPrint"><span style="color:#DD0000;font-style:italic;">&nbsp;* required field</span><br />
        <div class="divError">You must check the box indicating you have read and understand the waiver before you can proceed to the next page.</div></p>
        <div id="eAcknowledge" runat="server" class="divError"></div>
        <p>
            <asp:CheckBox ID="chkAcknowledgeUnderstand" runat="server" Text="&nbsp;I acknowledge that I have
            read this waiver and release of liability." /><label for="chkAcknowledgeUnderstand">&nbsp;I understand what is required of me as a Homestay Host Family.</label>
        </p>        
    </div>
    <br />
    <div id="divUpload" class="noPrint" style="border: 1px solid Gray;width:60%;padding:15px;" runat="server">
        <p>
            <label for="uplWaiverFile">Document Upload:</label>&nbsp;
            <asp:FileUpload ID="uplWaiverFile" runat="server" Size="70" /> <asp:Literal ID="litWaiverFile" runat="server"></asp:Literal>
            <asp:HiddenField ID="hdnWaiverSubmitted" runat="server" /><br />
        </p>
    </div>
</div>
    <br />   
<div id="divConfirmation" runat="server">
    <h2>Thank you for your interest in the CCS Homestay Program!</h2>
    <p>When the review of your application is complete, 
    you will be contacted for a home visit to view the rooms available and to answer any questions regarding our program.</p>
    <p>In the meantime, please review the information below in preparation for receiving your student.</p>
    <ul>
        <li><a href="_PDF/ccs40-330-SafetyChecklist.pdf" target="_blank">Home Visit Safety Check</a></li>
        <li><a href="_PDF/2018-19-Lewermark.pdf" target="_blank">Insurance Plan 2018-19</a></li>
        <li><a href="_PDF/HomestayFamiliesResponsibilities16-17accessible.pdf" target="_blank">Homestay Family Responsibilities for ages 16-17</a></li>
        <li><a href="_PDF/HomestayFamiliesResponsibilities18accessible.pdf" target="_blank">Homestay Family Responsibilities for age 18+</a></li>
        <li><a href="_PDF/CellPhonesUA-2017.pdf" target="_blank">Cellphones</a></li>
        <!-- <li><a href="_PDF/MentalHealth-2017.pdf" target="_blank">Mental Health Resources</a></li> -->
        <li><a href="_PDF/MorneauShepell-studentCounseling.pdf" target="_blank">Student Counseling and Support Service-Morneau Shepell 2018</a></li>
        <li><a href="_PDF/SCC-ACA-Winter-2019WelcomeWeek.pdf" target="_blank">SCC Orientation</a></li>
        <li><a href="_PDF/SFCC-ACA-Winter-2019WelcomeWeek.pdf" target="_blank">SFCC Orientation for Academic Students</a></li>
        <li><a href="_PDF/SFCC-IELP-Winter-2019WelcomeWeek.pdf" target="_blank">SFCC Orientation for IELP Students </a></li>
    </ul>
    <p>&nbsp;</p>
    <asp:Literal ID="litMissingUploads" runat="server"></asp:Literal>
</div>
<div id="divButtons" runat="server">
    <div id="btnSave" runat="server" style="float:left;">
      <!--  <input type="submit" name="btnSubmit" value="Save and Continue"/>&nbsp;&nbsp; -->
        <asp:Button ID="btnCmdSaveAndContinue" runat="server" Text="Save and Continue" OnClick="btnCmdSaveAndContinue_Click" />&nbsp;&nbsp;
    </div>
    <div id="btnDone" runat="server" style="float:left;margin-left:30px;">
      <!--  <input type="submit" name="btnSubmit" value="Save" />&nbsp;&nbsp; -->
        <asp:Button ID="btnCmdSave" runat="server" Text="Save" OnClick="btnCmdSave_Click" />&nbsp;&nbsp;
    </div>
    <div id="divNextPage" runat="server" style="float:left;margin-left:30px;">
       <!-- <input type="submit" name="btnSubmit" id="Next" value="Continue"/>&nbsp;&nbsp; -->
        <asp:Button ID="btnCmdContinue" runat="server" Text="Continue" OnClick="btnCmdContinue_Click" />&nbsp;&nbsp;
    </div>
    <div id="btnPrevious" runat="server" style="float:left;margin-left:30px;">
       <!-- <input type="submit" name="btnSubmit" value="Back" /> -->
        <asp:Button ID="btnCmdBack" runat="server" Text="Back" OnClick="btnCmdBack_Click" />
    </div>
    <div id="divPrintWaiver" runat="server" visible="false" style="float:left;margin-left:30px;">
       <!-- <input type="submit" name="btnSubmit" value="Printable Waiver" /> -->
        <asp:Button ID="btnCmdPrintWaiver" runat="server" Text="Printable Waiver" OnClick="btnCmdPrintWaiver_Click" />
    </div>
    <div id="divHome" runat="server" style="float:left;margin-left:30px;">
      <!--  <input style="float:left;margin-left:30px;" type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp; -->
        <asp:Button ID="btnCmdReturnHome" runat="server" Text="Return to Admin Home Page" OnClick="btnCmdReturnHome_Click" />&nbsp;&nbsp;
    </div>
<br />
</div>
</form>
</body>
</html>




