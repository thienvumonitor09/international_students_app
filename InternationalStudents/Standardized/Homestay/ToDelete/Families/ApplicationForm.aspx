﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ApplicationForm.aspx.cs" Inherits="Homestay_Families_ApplicationForm" %>
<!DOCTYPE html>

<html lang="en-us" >

<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>CCS Homestay Program</title>
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" />
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />         
    <meta http-equiv="X-UA-Compatible" content="IE=9" />  
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>      
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>   
</head>

<body>
<form id="form1" runat="server">
<div id="divSelector" runat="server" style="padding-bottom:10px;">
    <h3>Application Form - Host Families</h3>
    <h4>Select a Homestay Family</h4>
    <asp:DropDownList ID="ddlFamilies" AutoPostBack="true" runat="server"></asp:DropDownList>
</div>
<asp:HiddenField ID="hdnFamilyID" runat="server" />
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
<fieldset id="PersonalInfo" runat="server">
<legend>Family Home</legend>
    <div id="divHomeName" runat="server">
        <p>
            <label for="homeName">Home Name</label><br />
            <asp:TextBox ID="homeName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="rdoIsActive">Homestay Program Status</label><br />
            <asp:RadioButtonList ID="rdoIsActive" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="1">Active&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="0">Inactive&nbsp;&nbsp;</asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <p>
            <label for="userName">Account Username for Primary Contact </label><br />
            <asp:TextBox ID="userName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="userName">Family Status</label><br />
            <asp:TextBox ID="familyStatus" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </p>
    </div>
    <div id="eStreetAddress" class="divError" runat="server"></div>
    <p>
        <label for="streetAddress">Street Address</label><br />
        <asp:TextBox ID="streetAddress" runat="server" Width="300px" MaxLength="100"></asp:TextBox>
    </p>
    <div id="eCity" class="divError" runat="server"></div>
    <p>        
        <label for="city">City</label><br />
        <asp:TextBox ID="city" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
    <div id="eState" class="divError" runat="server"></div>
    <p>
        <label for="state">State</label><br />
        <asp:TextBox ID="state" runat="server" Width="40px" MaxLength="2"></asp:TextBox>
    </p>
     <div id="eZip" class="divError" runat="server"></div>
    <p>
        <label for="ZIP">ZIP Code</label><br />
        <asp:TextBox ID="ZIP" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
    </p>
    <div id="divTravelTime" runat="server" visible="false"> 
        <div id="eTravelErr" class="divError" runat="server"></div>
        <label for="state">Walking distance to bus stop</label><br />
        <div style="padding: 2px 0px;"><asp:TextBox ID="walkingBusStopMinutes" runat="server" Width="50px" MaxLength="5" onblur="convertNum('walkingBusStopMinutes');"></asp:TextBox> Minutes</div>        
        <div style="padding: 2px 0px;"><asp:TextBox ID="walkingBusStopMiles" runat="server" Width="50px" MaxLength="5" onblur="convertNum('walkingBusStopMiles');"></asp:TextBox> Miles</div>
        <div style="padding: 2px 0px;"><asp:TextBox ID="walkingBusStopBlocks" runat="server" Width="50px" MaxLength="5" onvlur="convertNum('walkingBusStopBlocks');"></asp:TextBox> Blocks</div>       
        <p>
            <label for="state">Bus travel to SCC</label><br />
            <asp:TextBox ID="busMinutesSCC" runat="server" Width="50px" MaxLength="5" onblur="convertNum('busMinutesSCC');"></asp:TextBox> Minutes<br />
            <label for="state">Bus travel to SFCC</label><br />
            <asp:TextBox ID="busMinutesSFCC" runat="server" Width="50px" MaxLength="5" onblur="convertNum('busMinutesSFCC')"></asp:TextBox> Minutes<br />
        </p>
        <p>
            <label for="state">College(s) family qualifies for</label><br />
            <asp:RadioButtonList ID="collegeQualified" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="SCC" Text="SCC"></asp:ListItem>
                <asp:ListItem Value="SFCC" Text="SFCC"></asp:ListItem>
                <asp:ListItem Value="Either" Text="Either College"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <asp:Button ID="btnCmdUpdateTravel" runat="server" Text="Update Travel Information" OnClick="btnCmdUpdateTravel_Click" />
    </div>
    <div id="ePhone" class="divError" runat="server"></div>
    <p>
        <label for="landLinePhone">Landline Phone/Primary Contact Phone</label><br />
        <asp:TextBox ID="landLinePhone" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
    </p>
        <h3>Please describe the bedrooms you have available for students and the room status.</h3>
    <h4>Room 1</h4> 
    <div id="eRoom1Level" class="divError" runat="server"></div>
    <p>Level in home: <asp:RadioButtonList ID="rdoRoom1" runat="server" RepeatDirection="Horizontal"><asp:ListItem Value="Main" Text="Main Level" /><asp:ListItem Value="Upper" Text="Upper Level" /><asp:ListItem Value="Lower" Text="Lower Level" /></asp:RadioButtonList>  
    </p>
    <div id="eRoom1Date" runat="server" class="divError"></div>
    <p>Available dates: <asp:TextBox ID="room1DateOpen" runat="server" /> to <asp:TextBox ID="room1DateClosed" runat="server" /> 
    </p>
    <div id="eRoom1Bathroom" class="divError" runat="server"></div>
    <p>Bathing facilities:  <asp:RadioButtonList ID="rdoRoom1Bathroom" runat="server" RepeatDirection="Horizontal"><asp:ListItem Text="Private Room, Shared Bathroom" Value="Shared" /><asp:ListItem Text="Private Room, Private Bathroom" Value="Private" /></asp:RadioButtonList>
    </p>
    <div id="eRoom1Occupancy" class="divError" runat="server"></div>
    <p>Occupancy: <asp:RadioButtonList ID="rdoRoom1Occupancy" runat="server"><asp:ListItem Value="Open" Text="Available now"></asp:ListItem><asp:ListItem Value="Planning" Text="Open soon, subject to above dates"></asp:ListItem><asp:ListItem Value="CCS" Text="Occupied by CCS Student"></asp:ListItem><asp:ListItem Value="Non-CCS" Text="Occupied by Student from another program"></asp:ListItem></asp:RadioButtonList>
    </p>
    
    <h4>Room 2</h4> 
    <p>Level in home: <asp:RadioButtonList ID="rdoRoom2" runat="server" RepeatDirection="Horizontal"><asp:ListItem Value="Main" Text="Main Level" /><asp:ListItem Value="Upper" Text="Upper Level" /><asp:ListItem Value="Lower" Text="Lower Level" /></asp:RadioButtonList>  </p>
    <div id="eRoom2Date" runat="server" class="divError"></div>
    <p>Available dates: <asp:TextBox ID="room2DateOpen" runat="server" /> to <asp:TextBox ID="room2DateClosed" runat="server" /> </p>    
    <p>Bathing facilities:  <asp:RadioButtonList ID="rdoRoom2Bathroom" runat="server" RepeatDirection="Horizontal"><asp:ListItem Text="Private Room, Shared Bathroom" Value="Shared" /><asp:ListItem Text="Private Room, Private Bathroom" Value="Private" /></asp:RadioButtonList></p>
    <p>Occupancy: <asp:RadioButtonList ID="rdoRoom2Occupancy" runat="server"><asp:ListItem Value="Open" Text="Available now"></asp:ListItem><asp:ListItem Value="Planning" Text="Open soon, subject to above dates"></asp:ListItem><asp:ListItem Value="CCS" Text="Occupied by CCS Student"></asp:ListItem><asp:ListItem Value="Non-CCS" Text="Occupied by Student from another program"></asp:ListItem></asp:RadioButtonList></p>
    <h4>Room 3</h4> 
    <p>Level in home: <asp:RadioButtonList ID="rdoRoom3" runat="server" RepeatDirection="Horizontal"><asp:ListItem Value="Main" Text="Main Level" /><asp:ListItem Value="Upper" Text="Upper Level" /><asp:ListItem Value="Lower" Text="Lower Level" /></asp:RadioButtonList>  </p>
    <div id="eRoom3Date" runat="server" class="divError"></div>
    <p>Available dates: <asp:TextBox ID="room3DateOpen" runat="server" /> to <asp:TextBox ID="room3DateClosed" runat="server" /> </p>    
    <p>Bathing facilities:  <asp:RadioButtonList ID="rdoRoom3Bathroom" runat="server" RepeatDirection="Horizontal"><asp:ListItem Text="Private Room, Shared Bathroom" Value="Shared" /><asp:ListItem Text="Private Room, Private Bathroom" Value="Private" /></asp:RadioButtonList></p>
    <p>Occupancy: <asp:RadioButtonList ID="rdoRoom3Occupancy" runat="server"><asp:ListItem Value="Open" Text="Available now"></asp:ListItem><asp:ListItem Value="Planning" Text="Open soon, subject to above dates"></asp:ListItem><asp:ListItem Value="CCS" Text="Occupied by CCS Student"></asp:ListItem><asp:ListItem Value="Non-CCS" Text="Occupied by Student from another program"></asp:ListItem></asp:RadioButtonList></p>
    <h4>Room 4</h4> 
    <p>Level in home: <asp:RadioButtonList ID="rdoRoom4" runat="server" RepeatDirection="Horizontal"><asp:ListItem Value="Main" Text="Main Level" /><asp:ListItem Value="Upper" Text="Upper Level" /><asp:ListItem Value="Lower" Text="Lower Level" /></asp:RadioButtonList>  </p>
    <div id="eRoom4Date" runat="server" class="divError"></div>
    <p>Available dates: <asp:TextBox ID="room4DateOpen" runat="server" /> to <asp:TextBox ID="room4DateClosed" runat="server" /> </p>    
    <p>Bathing facilities:  <asp:RadioButtonList ID="rdoRoom4Bathroom" runat="server" RepeatDirection="Horizontal"><asp:ListItem Text="Private Room, Shared Bathroom" Value="Shared" /><asp:ListItem Text="Private Room, Private Bathroom" Value="Private" /></asp:RadioButtonList></p>
    <p>Occupancy: <asp:RadioButtonList ID="rdoRoom4Occupancy" runat="server"><asp:ListItem Value="Open" Text="Available now"></asp:ListItem><asp:ListItem Value="Planning" Text="Open soon, subject to above dates"></asp:ListItem><asp:ListItem Value="CCS" Text="Occupied by CCS Student"></asp:ListItem><asp:ListItem Value="Non-CCS" Text="Occupied by Student from another program"></asp:ListItem></asp:RadioButtonList></p>
</fieldset>
<fieldset>
<legend>Homeowner's Insurance Policy</legend>
    <p>
        <label for="insuranceCompanyName">Company Name</label><br />
        <asp:TextBox ID="insuranceCompanyName" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
    </p>
    <p>
        <label for="insurancePolicyNumber">Policy Number</label><br />
        <asp:TextBox ID="insurancePolicyNumber" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
    </p>
    <p>
        <label for="insuranceAgentName">Agent Name</label><br />
        <asp:TextBox ID="insuranceAgentName" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
    </p>
    <p>
        <label for="insuranceAgentPhone">Agent Phone Number</label><br />
        <asp:TextBox ID="insuranceAgentPhone" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
    </p>

</fieldset>
<fieldset id="Contacts" runat="server">
<legend>Host Family Contacts</legend>
<p>Please provide the information below for yourself and anyone 18 years or older who resides in your home.</p>
    
<div id="accordion">
    <h3 class="accordion accordion-toggle">Primary Host Family Contact</h3>
    <div class="panel accordion-content default">
        <i><span style="color:#dd0000">Unless indicated as "optional", all fields for the Primary Host Family Contact are required</span></i>.
        <h4>Full Legal Name</h4>
        <div id="eFamilyName" class="divError" runat="server"></div>
        <p>
            <label for="familyName">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName" runat="server" class="required" MaxLength="30"></asp:TextBox>
        </p>
        <div id="eFirstName" class="divError" runat="server"></div>
        <p>
            <label for="firstName">First Name</label><br />
            <asp:TextBox ID="firstName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="middleName">Middle Name (optional)</label><br />
            <asp:TextBox ID="middleName" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <div id="eGender" class="divError" runat="server"></div>
        <p>
            <label for="rdoGender">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>
        </p>        
        <h4>Personal Information</h4>
        <div id="eDOB" class="divError" runat="server"></div>
        <p>
            <label for="DOB">Date of Birth</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
        <div id="eLicense" class="divError" runat="server"></div>
        <p>
            <label for="driversLicenseNumber">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber" runat="server" Width="300px" MaxLength="20"></asp:TextBox> 
        </p>
        <div id="eOccupation" class="divError" runat="server"></div>
        <p>
            <label for="occupation">Occupation/Retired</label><br />
            <asp:TextBox ID="occupation" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p><i><span style="color:#dd0000">Please enter at least one phone number - either cell phone or work phone</span></i>.</p>
        <p>
            <label for="cellPhone">Cell Phone</label><br />
            <asp:TextBox ID="cellPhone" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="workPhone">Work Phone</label><br />
            <asp:TextBox ID="workPhone" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <div id="eEmail" class="divError" runat="server"></div>
        <p>
            <label for="email">Email</label><br />
            <asp:TextBox ID="email" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
            <asp:HiddenField ID="hdnPrimaryID" runat="server" />
        </p>        
   </div>
   <h3 class="accordion accordion-toggle">Secondary Host Family Contact</h3>
   <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <p>
            <label for="familyName2">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName2" runat="server" class="required" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="firstName2">First Name</label><br />
            <asp:TextBox ID="firstName2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="middleName2">Middle Name</label><br />
            <asp:TextBox ID="middleName2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="rdoGender2">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>

        </p>
        <h4>Personal Information</h4>
       <div id="eDOB2" class="divError" runat="server"></div> 
       <p>
            <label for="DOB2">Date of Birth</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB2" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>
       
        <p>
            <label for="driversLicenseNumber2">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="occupation2">Occupation/Retired</label><br />
            <asp:TextBox ID="occupation2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="cellPhone2">Cell Phone</label><br />
            <asp:TextBox ID="cellPhone2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="workPhone2">Work Phone</label><br />
            <asp:TextBox ID="workPhone2" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="email2">Email</label><br />
            <asp:TextBox ID="email2" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
            <asp:HiddenField ID="hdnSecondaryID" runat="server" />
        </p>   
    </div>
    <h3 class="accordion accordion-toggle">Other Adult Occupant 1</h3>
    <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <p>
            <label for="familyName3">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName3" runat="server" class="required" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="firstName3">First Name</label><br />
            <asp:TextBox ID="firstName3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="middleName3">Middle Name</label><br />
            <asp:TextBox ID="middleName3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="rdoGender3">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender3" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>

        </p>
        <h4>Personal Information</h4>
        <div id="eDOB3" class="divError" runat="server"></div>
        <p>
            <label for="DOB3">Date of Birth</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB3" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>        
        <p>
            <label for="driversLicenseNumber3">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="occupation3">Occupation/Retired</label><br />
            <asp:TextBox ID="occupation3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="cellPhone3">Cell Phone</label><br />
            <asp:TextBox ID="cellPhone3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="workPhone3">Work Phone</label><br />
            <asp:TextBox ID="workPhone3" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="email3">Email</label><br />
            <asp:TextBox ID="email3" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
            <asp:HiddenField ID="hdnAdult1ID" runat="server" />
        </p>  
    </div>

    <h3 class="accordion accordion-toggle">Other Adult Occupant 2</h3>
    <div class="panel accordion-content">
        <h4>Full Legal Name</h4>
        <p>
            <label for="familyName4">Family Name</label><br />
            <asp:TextBox Width="300px" ID="familyName4" runat="server" class="required" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="firstName4">First Name</label><br />
            <asp:TextBox ID="firstName4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="middleName4">Middle Name</label><br />
            <asp:TextBox ID="middleName4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="rdoGender4">Gender</label><br />
            <asp:RadioButtonList ID="rdoGender4" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                <asp:ListItem Value="O">Other</asp:ListItem>
            </asp:RadioButtonList>

        </p>
        <h4>Personal Information</h4>
        <div id="eDOB4" class="divError" runat="server"></div>
        <p>
            <label for="DOB4">Date of Birth</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
            <asp:TextBox ID="DOB4" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
        </p>        
        <p>
            <label for="driversLicenseNumber4">Driver's License Number</label><br />
            <asp:TextBox ID="driversLicenseNumber4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="occupation4">Occupation/Retired</label><br />
            <asp:TextBox ID="occupation4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="cellPhone4">Cell Phone</label><br />
            <asp:TextBox ID="cellPhone4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="workPhone4">Work Phone</label><br />
            <asp:TextBox ID="workPhone4" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
        </p>
        <p>
            <label for="email4">Email</label><br />
            <asp:TextBox ID="email4" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
            <asp:HiddenField ID="hdnAdult2ID" runat="server" />
        </p>     
    </div>
    <h3 class="accordion accordion-toggle">Minor Child 1</h3>
    <div class="panel accordion-content">
    <p>
        <label for="familyName5">Family Name</label><br />
        <asp:TextBox ID="familyName5" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstName5">First Name</label><br />
        <asp:TextBox ID="firstName5" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
        <div id="eDOB5" class="divError" runat="server"></div>
    <p>
        <label for="DOB5">Date Of Birth</label><br />
        <asp:TextBox ID="DOB5" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>        
    <p>
        <label for="rdoGender5">Gender</label><br />
        <asp:RadioButtonList ID="rdoGender5" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnChild1ID" runat="server" />
    </p>
   </div>
    <h3 class="accordion accordion-toggle">Minor Child 2</h3>
    <div class="panel accordion-content">
    <p>
        <label for="familyName6">Family Name</label><br />
        <asp:TextBox ID="familyName6" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstName6">First Name</label><br />
        <asp:TextBox ID="firstName6" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
        <div id="eDOB6" class="divError" runat="server"></div>
    <p>
        <label for="DOB6">Date Of Birth</label><br />
        <asp:TextBox ID="DOB6" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>      
    <p>
        <label for="rdoGender6">Gender</label><br />
        <asp:RadioButtonList ID="rdoGender6" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnChild2ID" runat="server" />
    </p>
   </div>
    <h3 class="accordion accordion-toggle">Minor Child 3</h3>
    <div class="panel accordion-content">
    <p>
        <label for="familyName7">Family Name</label><br />
        <asp:TextBox ID="familyName7" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstName7">First Name</label><br />
        <asp:TextBox ID="firstName7" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
        <div id="eDOB7" class="divError" runat="server"></div>
    <p>
        <label for="DOB7">Date Of Birth</label><br />
        <asp:TextBox ID="DOB7" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>        
    <p>
        <label for="rdoGender7">Gender</label><br />
        <asp:RadioButtonList ID="rdoGender7" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnChild3ID" runat="server" />
    </p>
   </div>
    <h3 class="accordion accordion-toggle">Minor Child 4</h3>
    <div class="panel accordion-content">
    <p>
        <label for="familyName8">Family Name</label><br />
        <asp:TextBox ID="familyName8" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <p>
        <label for="firstName8">First Name</label><br />
        <asp:TextBox ID="firstName8" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>
    <div id="eDOB8" class="divError" runat="server"></div>
    <p>
        <label for="DOB8">Date Of Birth</label><br />
        <asp:TextBox ID="DOB8" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
    </p>        
    <p>
        <label for="rdoGender8">Gender</label><br />
        <asp:RadioButtonList ID="rdoGender8" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
            <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdnChild4ID" runat="server" />
    </p
   </div>
</div> 
    <h3>Upload Copies of Drivers' Licenses</h3>
    <p>Please upload a scanned copy of your driver's license and of anyone 18 years or older who resides in your home.  If you prefer not 
    to mail copies of driver licenses, you may present them at the home visit for visual verification.  This information 
    is needed for conducting WSP Criminal History Requests for each homestay applicant and anyone 18 years of age or older who resides 
    with the homestay family.</p>
    
    <div id="upLoadOneFile" runat="server" visible="true">
    <p>
        <asp:FileUpload ID="uplLicenseFile" runat="server" Size="70" /> <asp:Literal ID="litLicenseFile" runat="server"></asp:Literal>
        <asp:HiddenField ID="hdnLicenseSubmitted" runat="server" />        
        <p><asp:Button ID="btnUploadLicenseFile" runat="server" Text="Upload Driver's License" OnClick="btnUploadLicenseFile_Click" /></p>
    </p>
    </div>
    <div id="uploadMultipleFiles" runat="server" visible="true">
        <p><i><b>NOTE:</b>You can upload a single file or multiple files at once. If uploading multiple files, please make sure they are all in the same location on your computer so you can select them after clicking the "Browse" button.  For example: <br />
            <img src="../images/multipleFiles.png" alt="selecting multiple files for file upload" /><br />
            The files will be saved when you complete this page and click the "Save and Continue" button at the bottom.</i>
        </p>
    <p>
        <asp:FileUpload ID="uplMultiLicenseFiles" runat="server" Size="70" Multiple="Multiple" /> <asp:Literal ID="litMultiLicenseFiles" runat="server"></asp:Literal>
        <asp:HiddenField ID="hndMultiLicenseSubmitted" runat="server" />
        <div id="hdnMultiUpload" runat="server" visible="true">
            <p><asp:Button ID="btnUploadMultiLicenseFiles" runat="server" Text="Upload All Driver's License(s)" OnClick="btnUploadMultiLicenseFile_Click" /></p>
        </div>
    </p>
    </div>
</fieldset>
<fieldset id="Characteristics" runat="server">
<legend>Family Characteristics</legend>
    <div class="divError">Note: Unless stated otherwise, the following text boxes allow 800 characters maximum.</div>
    <div id="eHowReferred" class="divError" runat="server"></div>
    <p>
        <label for="howReferred">How did you hear about the Homestay program? If someone referred you, please provide their name.</label><br />
        <asp:TextBox ID="howReferred" runat="server" MaxLength="800" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p>
    <div id="eWhyHost" class="divError" runat="server"></div>
    <p>
        <label for="whyHost">What interests you in being a homestay family?</label><br />
        <asp:TextBox ID="whyHost" runat="server" MaxLength="800" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p>   
    <p>
        <label for="hostExperience">OPTIONAL:  Please describe any prior experience as a homestay family or with international students.</label><br />
        <asp:TextBox ID="hostExperience" runat="server" MaxLength="800" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p>
    <div id="ePrimaryLanguage" class="divError" runat="server"></div>
    <p>
        <label for="primaryLanguage">Primary language spoken in the home:</label><br />
        <asp:TextBox ID="primaryLanguage" runat="server" MaxLength="20"></asp:TextBox>
    </p>    
    <p>
        <label for="otherLanguages">OPTIONAL:  Other languages spoken (maximum character limit is 100):</label><br />
        <asp:TextBox ID="otherLanguages" runat="server" MaxLength="100" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p>
    <p>
        <label for="religiousActivities">OPTIONAL: Describe your expectations regarding your international student's participation 
        in your religious activities? Occasionally a student will request or avoid placement  
        with a family of a specific faith. Most students are comfortable in any home regardless of the 
        family's faith if they are treated with respect.</label><br />
        <asp:TextBox ID="religiousActivities" runat="server" MaxLength="800" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p>
    <div id="eSmoking" class="divError" runat="server"></div>
    <p>
        <label for="smokingGuidelines">What are your guidelines regarding student use of alcohol or cigarettes while living with you?</label><br />
        <asp:TextBox ID="smokingGuidelines" runat="server" MaxLength="800" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p> 
    <div id="ePets" class="divError" runat="server"></div>
    <p>
        <label for="petsInHome">Do you have pets? What type and what areas of your home are they free to occupy?</label><br />
        <asp:TextBox ID="petsInHome" runat="server" MaxLength="800" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p>
    <div id="eGeneralLifestyle" class="divError" runat="server"></div>
    <p>
        <label for="generalLifestyle">Please describe your family's general lifestyle, schedule, 
        hobbies, sports, or other interests of family members.
        In what family activities do you hope your student(s) will participate?</label><br />
        <asp:TextBox ID="generalLifestyle" runat="server" MaxLength="800" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p>    
    <p>
        <label for="otherSchool">OPTIONAL:  Do you host students for any other school? If so, describe.</label><br />
        <asp:TextBox ID="otherSchool" runat="server" MaxLength="800" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p>
    <p>
        <label for="additionalExpectations">OPTIONAL:  Additional information about your expectations or your family situation:</label><br />
        <asp:TextBox ID="additionalExpectations" runat="server" MaxLength="800" Rows="3" Columns="60" TextMode="MultiLine"></asp:TextBox>
    </p>
</fieldset>
<div id="divSaveButton" runat="server" style="float:left;">
  <!--  <input type="submit" name="btnSubmit" id="btnSave" value="Save and Continue" /> -->
    <asp:Button ID="btnCmdSaveAndContinue" runat="server" Text="Save and Continue" OnClick="btnCmdSaveAndContinue_Click" />&nbsp;&nbsp;
</div>
<div id="divUpdateButton" runat="server" style="float:left;">
  <!--  <input type="submit" name="btnSubmit" id="btnUpdate" value="Update and Continue" /> -->
    <asp:Button ID="btnCmdUpdateAndContinue" runat="server" Text="Update and Continue" OnClick="btnCmdUpdateAndContinue_Click" />&nbsp;&nbsp;
</div>
<div id="divNextPage" runat="server" style="float:left;margin-left:30px;">
  <!--  <input type="submit" name="btnSubmit" id="btnContinue" value="Continue"/>&nbsp;&nbsp; -->
    <asp:Button ID="btnCmdContinue" runat="server" Text="Continue" OnClick="btnCmdContinue_Click" />&nbsp;&nbsp;
</div>
<div id="divHome" runat="server" style="float:left;margin-left:30px;">
   <!-- <input style="float:left;margin-left:30px;" type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp; -->
    <asp:Button ID="btnCmdReturnToAdmin" runat="server" Text="Return to Admin Home Page" OnClick="btnCmdReturnToAdmin_Click" />&nbsp;&nbsp;
</div>
<script type="text/javascript">
    $(document).ready(function ($) {
        $('#accordion').find('.accordion-toggle').click(function () {

            //Expand or collapse this panel
            $(this).next().slideToggle('fast');

            //Hide the other panels
            $(".accordion-content").not($(this).next()).slideUp('fast');

        });
    });
</script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
        });
    }
</script>
<script type="text/javascript">
    function convertNum(objSender) {
        var errMsg = document.getElementById("eTravelErr");
        var txtBox = document.getElementById(objSender);        
        if ($(txtBox).val() == "") {
            errMsg.innerHTML = "";
        } else {
            var intVal = parseInt($(txtBox).val(), 10);
            if (isNaN(intVal)) {
                errMsg.innerHTML = "Value must be a number, no characters allowed.";
                txtBox.focus();
            } else {
                errMsg.innerHTML = "";
            }
        }
    }
</script>
</form>
</body>
</html>
