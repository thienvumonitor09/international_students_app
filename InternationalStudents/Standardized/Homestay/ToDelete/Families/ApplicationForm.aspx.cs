﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_Families_ApplicationForm : System.Web.UI.Page
{
    String strButtonFace = "";
    String strResultMsg = "";
    Int32 intResult = 0;
    Int32 intFamilyID = 0;
    Int32 intMainLevelRooms = 0;
    Int32 intUpstairsRooms = 0;
    Int32 intBasementRooms = 0;
    String FamilyID = "";
    String strRelationship = "";
    Boolean blLicenseSubmitted = false;
    String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
    String strFileName = "";
    String strUploadServer = "";
    Boolean blAddMode = true;
    String selectedFamily = "";
    String strUserName = ""; //Ideally this is inserted by family after logging in when applying
    String strHomeName = "";
    Boolean blIsAdmin = false;

    //Home information 
    String strStreetAddress;
    String strCity;
    String strState;
    String strZIP;
    String strLandLinePhone;
    String strHowReferred;
    String strRoom1;
    String strRoom1DateOpen;
    String strRoom1DateClosed;
    String strRoom1Bathroom;
    String strRoom1Occupancy;
    String strRoom2;
    String strRoom2DateOpen;
    String strRoom2DateClosed;
    String strRoom2Bathroom;
    String strRoom2Occupancy;
    String strRoom3;
    String strRoom3DateOpen;
    String strRoom3DateClosed;
    String strRoom3Bathroom;
    String strRoom3Occupancy;
    String strRoom4;
    String strRoom4DateOpen;
    String strRoom4DateClosed;
    String strRoom4Bathroom;
    String strRoom4Occupancy;
    String strInsuranceCompany;
    String strInsurancePolicyNumber;
    String strInsuranceAgentName;
    String strInsuranceAgentPhone;
    String strWhyHost;
    String strHostExperience;
    String strPrimaryLanguage;
    String strOtherLanguages;
    String strReligiousActivities;
    String strSmokingGuidelines;
    String strPetsInHome;
    String strGeneralLifestyle;
    String strOtherSchool;
    String strAdditionalExpectations;
    //Primary Contact
    String FamilyName;
    String FirstName;
    String MiddleName;
    String DateOfBirth;
    String Gender;
    String DriversLicenseNumber;
    String Occupation;
    String CellPhone;
    String WorkPhone;
    String Email;
    //Secondary Contact
    String FamilyName2;
    String FirstName2;
    String MiddleName2;
    String DateOfBirth2;
    String Gender2;
    String DriversLicenseNumber2;
    String Occupation2;
    String CellPhone2;
    String WorkPhone2;
    String Email2;
    //Other Adult 1
    String FamilyName3;
    String FirstName3;
    String MiddleName3;
    String DateOfBirth3;
    String Gender3;
    String DriversLicenseNumber3;
    String Occupation3;
    String CellPhone3;
    String WorkPhone3;
    String Email3;
    //Other Adult 2
    String FamilyName4;
    String FirstName4;
    String MiddleName4;
    String DateOfBirth4;
    String Gender4;
    String DriversLicenseNumber4;
    String Occupation4;
    String CellPhone4;
    String WorkPhone4;
    String Email4;
    //Minor Child 1
    String FamilyName5;
    String FirstName5;
    String DateOfBirth5;
    String Gender5;
    //Minor Child 2
    String FamilyName6;
    String FirstName6;
    String DateOfBirth6;
    String Gender6;
    //Minor Child 3
    String FamilyName7;
    String FirstName7;
    String DateOfBirth7;
    String Gender7;
    //Minor Child 4
    String FamilyName8;
    String FirstName8;
    String DateOfBirth8;
    String Gender8;

    //Admin only fields
    String strWalkingBusStopMinutes = "";
    String strWalkingBusStopMiles = "";
    String strWalkingBusStopBlocks = "";
    String strBusMinutesSCC = "";
    String strBusMinutesSFCC = "";
    String strCollegeQualified = "";

    DateTime outDate;               // used in try/catch to validate dob on form
    Boolean dateSuccess = false;    // used in try/catch to validate dob on form

    String postBackControlName = "";

    Homestay hsInfo = new Homestay();

    protected void Page_Load(object sender, EventArgs e)
    {
        

       // Homestay hsInfo = new Homestay();
        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (Request.QueryString["ID"] != null)
        {
            //page load
            FamilyID = Request.QueryString["ID"].ToString();
            hdnFamilyID.Value = FamilyID;
        }
        if (FamilyID == "")
        {
            //after postback
            FamilyID = hdnFamilyID.Value;
        }
        //strResultMsg += "FamilyID: " + FamilyID + "<br />";
        if (FamilyID != "" && FamilyID != null)
        {
            intFamilyID = Convert.ToInt32(FamilyID);
            blAddMode = false;
        }
        //search for an existing application with this family's last name and first name of primary contact.
        selectedFamily = ddlFamilies.SelectedValue;
        if (selectedFamily != "" && selectedFamily != "0" && selectedFamily != null)
        {
            blAddMode = false;
            intFamilyID = Convert.ToInt32(selectedFamily);  //overrides any previous value  
            // reset all text fields
        //    resetFields();
        }
        
        if (FamilyID == "0")
        {
            blAddMode = true;
            upLoadOneFile.Visible = false;
            uploadMultipleFiles.Visible = true;
            hdnMultiUpload.Visible = false;
        }
        else {
            uploadMultipleFiles.Visible = false;
            upLoadOneFile.Visible = true;
        }
        //This only happens when the back button is hit
        if (selectedFamily == "" && intFamilyID > 0)
        {
           // selectedFamily = intFamilyID.ToString();
            selectedFamily = "0";
        }
        //strResultMsg += "Selected Family: " + selectedFamily + "<br />";
        //strResultMsg += "Add mode? " + blAddMode + "<br />";
        //Get UserID if family applying
        if (Request.QueryString["UID"] != null)
        {
            strUserName = Request.QueryString["UID"].ToString();
        }
        else if (Request.ServerVariables["LOGON_USER"] != null)
        {
            //set admin status for visibility control
            blIsAdmin = true;
        }
        if (Request.QueryString["admin"] != null)
        {
            blIsAdmin = Convert.ToBoolean(Request.QueryString["admin"].ToString());
        }
        strResultMsg += "Family User ID: " + strUserName + "; Is admin? " + blIsAdmin + "<br />";
        //Process submissions
        if (!Page.IsPostBack)
            postBackControlName = "";
        else
        {
            postBackControlName = Page.Request.Params["__EVENTTARGET"];
          //  Response.Write("control name: " + postBackControlName + "<br />");
        }
        if(strUserName == "")
        {
            if (IsPostBack && postBackControlName.Trim().Contains("ddlFamilies"))
            {
                if (blIsAdmin && !blAddMode)
                {
                    strWalkingBusStopMinutes = walkingBusStopMinutes.Text.Replace("'", "''");
                    strWalkingBusStopMiles = walkingBusStopMiles.Text.Replace("'", "''");
                    strWalkingBusStopBlocks = walkingBusStopBlocks.Text.Replace("'", "''");
                    strBusMinutesSCC = busMinutesSCC.Text.Replace("'", "''");
                    strBusMinutesSFCC = busMinutesSFCC.Text.Replace("'", "''");
                    strCollegeQualified = collegeQualified.SelectedValue;
                }
                getValues("load");
            
                if (hsInfo.strResultMsg == "" && !blIsAdmin) { Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID); }
            }// end postback and global ed admin adding times and dist. from home to school
        }// NOT logged in family submitter
        //if family logged in AND is a postback, they are updating or continuing - don't want to populate from tables, want to update tables
        if ((strUserName != "" && !IsPostBack) || (strUserName == "" && blIsAdmin && IsPostBack))
        {
            if (strUserName != "" || (strUserName == "" && !blAddMode && postBackControlName.Trim().Contains("ddlFamilies")))
            {
                //Populate fields
                //Base info
                DataTable dtFamily = hsInfo.GetHomestayFamilyInfo(intFamilyID, "", "ID");
                if (dtFamily != null)
                {
                    if (dtFamily.Rows.Count > 0)
                    {
                        userName.Text = dtFamily.Rows[0]["userName"].ToString().Trim();
                        homeName.Text = dtFamily.Rows[0]["homeName"].ToString().Trim();
                        streetAddress.Text = dtFamily.Rows[0]["streetAddress"].ToString().Trim();
                        city.Text = dtFamily.Rows[0]["city"].ToString().Trim();
                        state.Text = dtFamily.Rows[0]["state"].ToString().Trim();
                        ZIP.Text = dtFamily.Rows[0]["ZIP"].ToString().Trim();
                        walkingBusStopMinutes.Text = dtFamily.Rows[0]["walkingBusStopMinutes"].ToString().Trim();
                        walkingBusStopMiles.Text = dtFamily.Rows[0]["walkingBusStopMiles"].ToString().Trim();
                        walkingBusStopBlocks.Text = dtFamily.Rows[0]["walkingBusStopBlocks"].ToString().Trim();
                        busMinutesSCC.Text = dtFamily.Rows[0]["busMinutesSCC"].ToString().Trim();
                        busMinutesSFCC.Text = dtFamily.Rows[0]["busMinutesSFCC"].ToString().Trim();
                        if (dtFamily.Rows[0]["collegeQualified"].ToString().Trim() == "")
                            collegeQualified.SelectedIndex = -1;
                        else
                            collegeQualified.SelectedValue = dtFamily.Rows[0]["collegeQualified"].ToString().Trim();
                        collegeQualified.SelectedValue = dtFamily.Rows[0]["collegeQualified"].ToString().Trim();
                        landLinePhone.Text = dtFamily.Rows[0]["landlinePhone"].ToString().Trim();
                        insuranceCompanyName.Text = dtFamily.Rows[0]["insuranceCompanyName"].ToString().Trim();
                        insurancePolicyNumber.Text = dtFamily.Rows[0]["insurancePolicyNumber"].ToString().Trim();
                        insuranceAgentName.Text = dtFamily.Rows[0]["insuranceAgentName"].ToString().Trim();
                        insuranceAgentPhone.Text = dtFamily.Rows[0]["insuranceAgentPhone"].ToString().Trim();
                        whyHost.Text = dtFamily.Rows[0]["whyHost"].ToString().Trim();
                        hostExperience.Text = dtFamily.Rows[0]["hostExperience"].ToString().Trim();
                        primaryLanguage.Text = dtFamily.Rows[0]["primaryLanguage"].ToString().Trim();
                        otherLanguages.Text = dtFamily.Rows[0]["otherLanguages"].ToString().Trim();
                        religiousActivities.Text = dtFamily.Rows[0]["religiousActivities"].ToString().Trim();
                        smokingGuidelines.Text = dtFamily.Rows[0]["smokingGuidelines"].ToString().Trim();
                        petsInHome.Text = dtFamily.Rows[0]["petsInHome"].ToString().Trim();
                        generalLifestyle.Text = dtFamily.Rows[0]["generalLifestyle"].ToString().Trim();
                        otherSchool.Text = dtFamily.Rows[0]["otherSchool"].ToString().Trim();
                        additionalExpectations.Text = dtFamily.Rows[0]["additionalExpectations"].ToString().Trim();
                        Boolean blIsActive = Convert.ToBoolean(dtFamily.Rows[0]["IsActive"]);
                        if (blIsActive) { rdoIsActive.SelectedValue = "1"; } else { rdoIsActive.SelectedValue = "0"; }
                        familyStatus.Text = dtFamily.Rows[0]["familyStatus"].ToString().Trim();
                        blLicenseSubmitted = Convert.ToBoolean(dtFamily.Rows[0]["licenseCopiesSubmitted"]);
                        hdnLicenseSubmitted.Value = blLicenseSubmitted.ToString();
                        litLicenseFile.Text = "";
                        if (blLicenseSubmitted)
                        {
                            Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                            AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                            strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                            litLicenseFile.Text = "";
                            DataTable dt = hsInfo.GetUploadedFiles(intFamilyID);
                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        strFileName = dr["fileName"].ToString();
                                        if (strFileName.Contains("License"))
                                        {
                                            //get the file name
                                            litLicenseFile.Text += "<br /><b>License copies submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                    }
                                }
                            }
                        }
                        howReferred.Text = dtFamily.Rows[0]["howHearHomestay"].ToString();
                        rdoRoom1.SelectedIndex = -1;
                        rdoRoom1.SelectedValue = dtFamily.Rows[0]["room1Level"].ToString();
                        room1DateOpen.Text = "";
                        if (!DBNull.Value.Equals(dtFamily.Rows[0]["room1StartDate"]))
                        {
                            room1DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room1StartDate"]).ToShortDateString();
                            if (room1DateOpen.Text == "1/1/1900")
                                room1DateOpen.Text = "";
                        }
                        room1DateClosed.Text = "";
                        if (!DBNull.Value.Equals(dtFamily.Rows[0]["room1EndDate"]))
                        {
                            room1DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room1EndDate"]).ToShortDateString();
                            if (room1DateClosed.Text == "1/1/1900")
                                room1DateClosed.Text = "";
                        }
                        rdoRoom1Bathroom.SelectedIndex = -1;
                        rdoRoom1Occupancy.SelectedIndex = -1;
                        rdoRoom1Bathroom.SelectedValue = dtFamily.Rows[0]["room1Bath"].ToString();
                        rdoRoom1Occupancy.Text = dtFamily.Rows[0]["room1Occupancy"].ToString();

                        rdoRoom2.SelectedIndex = -1;
                        rdoRoom2.SelectedValue = dtFamily.Rows[0]["room2Level"].ToString();
                        room2DateOpen.Text = "";
                        if (!DBNull.Value.Equals(dtFamily.Rows[0]["room2StartDate"]))
                        {
                            room2DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room2StartDate"]).ToShortDateString();
                            if (room2DateOpen.Text == "1/1/1900")
                                room2DateOpen.Text = "";
                        }
                        room2DateClosed.Text = "";
                        if (!DBNull.Value.Equals(dtFamily.Rows[0]["room2EndDate"]))
                        {
                            room2DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room2EndDate"]).ToShortDateString();
                            if (room2DateClosed.Text == "1/1/1900")
                                room2DateClosed.Text = "";
                        }
                        rdoRoom2Bathroom.SelectedIndex = -1;
                        rdoRoom2Bathroom.SelectedValue = dtFamily.Rows[0]["room2Bath"].ToString();
                        rdoRoom2Occupancy.SelectedIndex = -1;
                        rdoRoom2Occupancy.Text = dtFamily.Rows[0]["room2Occupancy"].ToString();

                        rdoRoom3.SelectedIndex = -1;
                        rdoRoom3.SelectedValue = dtFamily.Rows[0]["room3Level"].ToString();
                        room3DateOpen.Text = "";
                        if (!DBNull.Value.Equals(dtFamily.Rows[0]["room3StartDate"]))
                        {
                            room3DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room3StartDate"]).ToShortDateString();
                            if (room3DateOpen.Text == "1/1/1900")
                                room3DateOpen.Text = "";
                        }
                        room3DateClosed.Text = "";
                        if (!DBNull.Value.Equals(dtFamily.Rows[0]["room3EndDate"]))
                        {
                            room3DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room3EndDate"]).ToShortDateString();
                            if (room3DateClosed.Text == "1/1/1900")
                                room3DateClosed.Text = "";
                        }
                        rdoRoom3Bathroom.SelectedIndex = -1;
                        rdoRoom3Bathroom.SelectedValue = dtFamily.Rows[0]["room3Bath"].ToString();
                        rdoRoom3Occupancy.SelectedIndex = -1;
                        rdoRoom3Occupancy.Text = dtFamily.Rows[0]["room3Occupancy"].ToString();
                        rdoRoom4.SelectedIndex = -1;
                        rdoRoom4.SelectedValue = dtFamily.Rows[0]["room4Level"].ToString();
                        room4DateOpen.Text = "";
                        if (!DBNull.Value.Equals(dtFamily.Rows[0]["room4StartDate"]))
                        {
                            room4DateOpen.Text = Convert.ToDateTime(dtFamily.Rows[0]["room4StartDate"]).ToShortDateString();
                            if (room4DateOpen.Text == "1/1/1900")
                                room4DateOpen.Text = "";
                        }
                        room4DateClosed.Text = "";
                        if (!DBNull.Value.Equals(dtFamily.Rows[0]["room4EndDate"]))
                        {
                            room4DateClosed.Text = Convert.ToDateTime(dtFamily.Rows[0]["room4EndDate"]).ToShortDateString();
                            if (room4DateClosed.Text == "1/1/1900")
                                room4DateClosed.Text = "";
                        }
                        rdoRoom4Bathroom.SelectedIndex = -1;
                        rdoRoom4Occupancy.SelectedIndex = -1;
                        rdoRoom4Bathroom.SelectedValue = dtFamily.Rows[0]["room4Bath"].ToString();
                        rdoRoom4Occupancy.Text = dtFamily.Rows[0]["room4Occupancy"].ToString();
                    }                
                }
                //Relatives
                //clear the fields - primary contact
                familyName.Text = "";
                firstName.Text = "";
                middleName.Text = "";
                rdoGender.SelectedIndex = -1;
                rdoGender.Text = "";
                DOB.Text = "";
                driversLicenseNumber.Text = "";
                occupation.Text = "";
                cellPhone.Text = "";
                workPhone.Text = "";
                email.Text = "";
                hdnPrimaryID.Value = "";
                //clear the fields - secondary contact
                familyName2.Text = "";
                firstName2.Text = "";
                middleName2.Text = "";
                rdoGender2.SelectedIndex = -1;
                rdoGender2.Text = "";
                DOB2.Text = "";
                driversLicenseNumber2.Text = "";
                occupation2.Text = "";
                cellPhone2.Text = "";
                workPhone2.Text = "";
                email2.Text = "";
                hdnSecondaryID.Value = "";
                //clear the fields - Other adult 1
                familyName3.Text = "";
                firstName3.Text = "";
                middleName3.Text = "";
                rdoGender3.SelectedIndex = -1;
                rdoGender3.Text = "";
                DOB3.Text = "";
                driversLicenseNumber3.Text = "";
                occupation3.Text = "";
                cellPhone3.Text = "";
                workPhone3.Text = "";
                email3.Text = "";
                hdnAdult1ID.Value = "";
                //clear the fields - Other adult 2
                familyName4.Text = "";
                firstName4.Text = "";
                middleName4.Text = "";
                rdoGender4.SelectedIndex = -1;
                rdoGender4.Text = "";
                DOB4.Text = "";
                driversLicenseNumber4.Text = "";
                occupation4.Text = "";
                cellPhone4.Text = "";
                workPhone4.Text = "";
                email4.Text = "";
                hdnAdult2ID.Value = "";
                //clear the fields - minor child 1
                familyName5.Text = "";
                firstName5.Text = "";
                DOB5.Text = "";
                rdoGender5.SelectedIndex = -1;
                rdoGender5.Text = "";
                hdnChild1ID.Value = "";
                //clear the fields - minor child 2
                familyName6.Text = "";
                firstName6.Text = "";
                DOB6.Text = "";
                rdoGender6.SelectedIndex = -1;
                rdoGender6.Text = "";
                hdnChild2ID.Value = "";
                //clear the fields - minor child 3
                familyName7.Text = "";
                firstName7.Text = "";
                DOB7.Text = "";
                rdoGender7.SelectedIndex = -1;
                rdoGender7.Text = "";
                hdnChild3ID.Value = "";
                //clear the fields - minor child 4
                familyName8.Text = "";
                firstName8.Text = "";
                DOB8.Text = "";
                rdoGender8.Text = "";
                rdoGender8.SelectedIndex = -1;
                hdnChild4ID.Value = "";

                DataTable dtRelatives = hsInfo.GetHomestayRelatives(0, intFamilyID);
                if (dtRelatives != null)
                {
                    if (dtRelatives.Rows.Count > 0)
                    {
                        foreach (DataRow dtRow in dtRelatives.Rows)
                        {
                            strRelationship = dtRow["relationship"].ToString().Trim();
                            //strResultMsg += "Relationship: " + strRelationship + "<br />";
                            switch (strRelationship)
                            {
                                case "Primary Contact":
                                    familyName.Text = dtRow["familyName"].ToString().Trim();
                                    firstName.Text = dtRow["firstName"].ToString().Trim();
                                    middleName.Text = dtRow["middleName"].ToString().Trim();
                                    rdoGender.SelectedValue = dtRow["gender"].ToString().Trim();
                                    DOB.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                    driversLicenseNumber.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                    occupation.Text = dtRow["occupation"].ToString().Trim();
                                    cellPhone.Text = dtRow["cellPhone"].ToString().Trim();
                                    workPhone.Text = dtRow["workPhone"].ToString().Trim();
                                    email.Text = dtRow["email"].ToString().Trim();
                                    hdnPrimaryID.Value = dtRow["id"].ToString();
                                    break;
                                case "Secondary Contact":
                                    familyName2.Text = dtRow["familyName"].ToString().Trim();
                                    firstName2.Text = dtRow["firstName"].ToString().Trim();
                                    middleName2.Text = dtRow["middleName"].ToString().Trim();
                                    rdoGender2.SelectedValue = dtRow["gender"].ToString().Trim();
                                    DOB2.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                    driversLicenseNumber2.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                    occupation2.Text = dtRow["occupation"].ToString().Trim();
                                    cellPhone2.Text = dtRow["cellPhone"].ToString().Trim();
                                    workPhone2.Text = dtRow["workPhone"].ToString().Trim();
                                    email2.Text = dtRow["email"].ToString().Trim();
                                    hdnSecondaryID.Value = dtRow["id"].ToString();
                                    break;
                                case "Other Adult 1":
                                    familyName3.Text = dtRow["familyName"].ToString().Trim();
                                    firstName3.Text = dtRow["firstName"].ToString().Trim();
                                    middleName3.Text = dtRow["middleName"].ToString().Trim();
                                    rdoGender3.SelectedValue = dtRow["gender"].ToString().Trim();
                                    DOB3.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                    driversLicenseNumber3.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                    occupation3.Text = dtRow["occupation"].ToString().Trim();
                                    cellPhone3.Text = dtRow["cellPhone"].ToString().Trim();
                                    workPhone3.Text = dtRow["workPhone"].ToString().Trim();
                                    email3.Text = dtRow["email"].ToString().Trim();
                                    hdnAdult1ID.Value = dtRow["id"].ToString();
                                    break;
                                case "Other Adult 2":
                                    familyName4.Text = dtRow["familyName"].ToString().Trim();
                                    firstName4.Text = dtRow["firstName"].ToString().Trim();
                                    middleName4.Text = dtRow["middleName"].ToString().Trim();
                                    rdoGender4.SelectedValue = dtRow["gender"].ToString().Trim();
                                    DOB4.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                    driversLicenseNumber4.Text = dtRow["driversLicenseNumber"].ToString().Trim();
                                    occupation4.Text = dtRow["occupation"].ToString().Trim();
                                    cellPhone4.Text = dtRow["cellPhone"].ToString().Trim();
                                    workPhone4.Text = dtRow["workPhone"].ToString().Trim();
                                    email4.Text = dtRow["email"].ToString().Trim();
                                    hdnAdult2ID.Value = dtRow["id"].ToString();
                                    break;
                                case "Minor Child 1":
                                    familyName5.Text = dtRow["familyName"].ToString().Trim();
                                    firstName5.Text = dtRow["firstName"].ToString().Trim();
                                    DOB5.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                    rdoGender5.Text = dtRow["gender"].ToString().Trim();
                                    hdnChild1ID.Value = dtRow["id"].ToString();
                                    break;
                                case "Minor Child 2":
                                    familyName6.Text = dtRow["familyName"].ToString().Trim();
                                    firstName6.Text = dtRow["firstName"].ToString().Trim();
                                    DOB6.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                    rdoGender6.Text = dtRow["gender"].ToString().Trim();
                                    hdnChild2ID.Value = dtRow["id"].ToString();
                                    break;
                                case "Minor Child 3":
                                    familyName7.Text = dtRow["familyName"].ToString().Trim();
                                    firstName7.Text = dtRow["firstName"].ToString().Trim();
                                    DOB7.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                    rdoGender7.Text = dtRow["gender"].ToString().Trim();
                                    hdnChild3ID.Value = dtRow["id"].ToString();
                                    break;
                                case "Minor Child 4":
                                    familyName8.Text = dtRow["familyName"].ToString().Trim();
                                    firstName8.Text = dtRow["firstName"].ToString().Trim();
                                    DOB8.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                    rdoGender8.Text = dtRow["gender"].ToString().Trim();
                                    hdnChild4ID.Value = dtRow["id"].ToString();
                                    break;
                            }
                        }
                    }
                    else { strResultMsg += "No HomestayRelatives records returned.<br />"; }
                }
                else { strResultMsg += "No HomestayRelatives records returned.<br />"; }

            }
        }// end family logged on and Is a postback - don't want to populate fields with table values
        //Set visibility of buttons
        if (blAddMode)
        {
            //Set visibility of home name field
            divHomeName.Visible = false;
            divSaveButton.Visible = true;
            divUpdateButton.Visible = false;
            divNextPage.Visible = false;
        }
        else
        {
            if(blIsAdmin)
                divHomeName.Visible = true;
            divSaveButton.Visible = false;
            divUpdateButton.Visible = true;
            divNextPage.Visible = true;
        }
        //Get families into a drop-down list; 
//        Response.Write("visibility: " + blIsAdmin.ToString() + "<br />");
        if (blIsAdmin)
        {
            //Travel time fields update only
            if (!blAddMode) { divTravelTime.Visible = true; }
            divSelector.Visible = true;
            divHomeName.Visible = true;
            divHome.Visible = true;
            divTravelTime.Visible = true;
            DataTable dtFamilies = hsInfo.GetHomestayFamilyInfo(0, "", "DDLactive");
            if (dtFamilies != null)
            {
                if (dtFamilies.Rows.Count > 0)
                {
                    ddlFamilies.Items.Clear();
                    ListItem LI;
                    LI = new ListItem();
                    LI.Value = "0";
                    LI.Text = "-- Add New Family --";
                    ddlFamilies.Items.Add(LI);
                    foreach (DataRow dr in dtFamilies.Rows)
                    {
                        LI = new ListItem();
                        LI.Value = dr["id"].ToString();
                        LI.Text = dr["homeName"].ToString();
                        ddlFamilies.Items.Add(LI);
                    }
                    if (selectedFamily != "") { ddlFamilies.SelectedValue = selectedFamily; }
                }
                else { strResultMsg += "Error: dtFamilies has no rows.<br />"; }
            }
            else { strResultMsg += "Error: dtFamilies is null.<br />"; }
        }
        else 
        { 
            divSelector.Visible = false;
            divHomeName.Visible = false;
            divHome.Visible = false;
            divTravelTime.Visible = false;
        }
        //litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
       // litResultMsg.Text = "<p>" + hsInfo.strResultMsg + "</p>";
    }

    protected void btnCmdSaveAndContinue_Click(object sender, EventArgs e)
    {
        Boolean numberSuccess = false;
        Double testNumber;
        if (blIsAdmin)
        {
            //admin adding record so check travel times and make sure they are numbers
            if (walkingBusStopMinutes.Text.Trim() != "")
            {
                numberSuccess = double.TryParse(walkingBusStopMinutes.Text.Trim(), out testNumber);
                if (!numberSuccess)
                {
                    eTravelErr.InnerHtml = "Minutes to the bus stop must be a number, no characters allowed.";
                    walkingBusStopMinutes.Focus();
                }
                else
                    eTravelErr.InnerHtml = "";
            }
            if (walkingBusStopMiles.Text.Trim() != "")
            {
                numberSuccess = double.TryParse(walkingBusStopMiles.Text.Trim(), out testNumber);
                if (!numberSuccess)
                {
                    eTravelErr.InnerHtml = "Miles to the bus stop must be a number, no characters allowed.";
                    walkingBusStopMiles.Focus();
                }
                else
                    eTravelErr.InnerHtml = "";
            }
            if (walkingBusStopBlocks.Text.Trim() != "")
            {
                numberSuccess = double.TryParse(walkingBusStopBlocks.Text.Trim(), out testNumber);
                if (!numberSuccess)
                {
                    eTravelErr.InnerHtml = "Blocks to the bus stop must be a number, no characters allowed.";
                    walkingBusStopBlocks.Focus();
                }
                else
                    eTravelErr.InnerHtml = "";
            }
            if (busMinutesSCC.Text.Trim() != "")
            {
                numberSuccess = double.TryParse(busMinutesSCC.Text.Trim(), out testNumber);
                if (!numberSuccess)
                {
                    eTravelErr.InnerHtml = "Travel time to SCC must be a number, no characters allowed.";
                    busMinutesSCC.Focus();
                }
                else
                    eTravelErr.InnerHtml = "";
            }
            if (busMinutesSFCC.Text.Trim() != "")
            {
                numberSuccess = double.TryParse(busMinutesSFCC.Text.Trim(), out testNumber);
                if (!numberSuccess)
                {
                    eTravelErr.InnerHtml = "Travel time to SCC must be a number, no characters allowed.";
                    busMinutesSFCC.Focus();
                }
                else
                    eTravelErr.InnerHtml = "";
            }
        }// end is admin and validating travel times
        if (streetAddress.Text.Trim() == "")
        {
            eStreetAddress.InnerHtml = "Street Address is required.";
            eStreetAddress.Focus();
            return;
        }
        else
            eStreetAddress.InnerHtml = "";
        if (city.Text.Trim() == "")
        {
            eCity.InnerHtml = "City is required.";
            city.Focus();
            return;
        }
        else
            eCity.InnerHtml = "";
        if (state.Text.Trim() == "")
        {
            eState.InnerHtml = "State is required.";
            state.Focus();
            return;
        }
        else
            eState.InnerHtml = "";
        if (ZIP.Text.Trim() == "")
        {
            eZip.InnerHtml = "Zip is required.";
            ZIP.Focus();
            return;
        }
        else
            eZip.InnerHtml = "";
        if (landLinePhone.Text.Trim() == "")
        {
            ePhone.InnerHtml = "Contact phone is required.";
            landLinePhone.Focus();
            return;
        }
        else
            ePhone.InnerHtml = "";
        if (rdoRoom1.SelectedIndex == -1)
        {
            eRoom1Level.InnerHtml = "Room 1 level is required.";
            rdoRoom1.Focus();
            return;
        }
        else
            eRoom1Level.InnerHtml = "";
        if (room1DateOpen.Text.Trim() == "")
        {
            eRoom1Date.InnerHtml = "Room 1 availability date is required.";
            room1DateOpen.Focus();
            return;
        }
        else
            eRoom1Date.InnerHtml = "";
        // see if valid date entered
        Boolean isValidDate = false;
        isValidDate = checkDate(room1DateOpen);
        if (!isValidDate)
        {
            eRoom1Date.InnerHtml = "Please enter a valid date for the availablility of Room 1";
            return;
        }
        else
            eRoom1Date.InnerHtml = "";

        if (room1DateClosed.Text.Trim() != "")
        {
            isValidDate = checkDate(room1DateClosed);
            if (!isValidDate)
            {
                eRoom1Date.InnerHtml = "Please enter a valid date for the 'To' field from Room 1.";
                return;
            }
            else
                eRoom1Date.InnerHtml = "";
        }// entry in room 1 Date Closed

        if (rdoRoom1Bathroom.SelectedIndex == -1)
        {
            eRoom1Bathroom.InnerHtml = "Please selecte Room 1 bathing facilities.";
            rdoRoom1Bathroom.Focus();
            return;
        }
        else
            eRoom1Bathroom.InnerHtml = "";
        if (rdoRoom1Occupancy.SelectedIndex == -1)
        {
            eRoom1Occupancy.InnerHtml = "Please select Room 1 occupancy.";
            rdoRoom1Occupancy.Focus();
            return;
        }
        else
            eRoom1Occupancy.InnerHtml = "";

        // Determine validity of other date fields.  Although not required, if a value is there, it must resolve to a valid date.
        // If it is blank, it will be assigned the value '01-01-1900' so the sql will execute on the record insert/update.  
        // This will be done when the text box values are assigned to the variables
        if (room2DateOpen.Text.Trim() != "")
        {
            isValidDate = checkDate(room2DateOpen);
            if (!isValidDate)
            {
                eRoom2Date.InnerHtml = "Please enter a valid date for the availability field from Room 2.";
                return;
            }
            else
                eRoom2Date.InnerHtml = "";
        }// entry in room 2 Date Open
        if (room2DateClosed.Text.Trim() != "")
        {
            isValidDate = checkDate(room2DateClosed);
            if (!isValidDate)
            {
                eRoom2Date.InnerHtml = "Please enter a valid date for the 'To' field from Room 2.";
                return;
            }
            else
                eRoom2Date.InnerHtml = "";
        }// entry in room 2 Date Closed
        if (room3DateOpen.Text.Trim() != "")
        {
            isValidDate = checkDate(room3DateOpen);
            if (!isValidDate)
            {
                eRoom3Date.InnerHtml = "Please enter a valid date for the availability field from Room 3.";
                return;
            }
            else
                eRoom3Date.InnerHtml = "";
        }// entry in room 3 Date Open
        if (room3DateClosed.Text.Trim() != "")
        {
            isValidDate = checkDate(room3DateClosed);
            if (!isValidDate)
            {
                eRoom3Date.InnerHtml = "Please enter a valid date for the 'To' field from Room 3.";
                return;
            }
            else
                eRoom3Date.InnerHtml = "";
        }// entry in room 3 Date Closed
        if (room4DateOpen.Text.Trim() != "")
        {
            isValidDate = checkDate(room4DateOpen);
            if (!isValidDate)
            {
                eRoom4Date.InnerHtml = "Please enter a valid date for the availability field from Room 4.";
                return;
            }
            else
                eRoom4Date.InnerHtml = "";
        }// entry in room 4 Date Open
        if (room4DateClosed.Text.Trim() != "")
        {
            isValidDate = checkDate(room4DateClosed);
            if (!isValidDate)
            {
                eRoom4Date.InnerHtml = "Please enter a valid date for the 'To' field from Room 4.";
                return;
            }
            else
                eRoom4Date.InnerHtml = "";
        }// entry in room 4 Date Closed

        if (familyName.Text.Trim() == "")
        {
            eFamilyName.InnerHtml = "Primary contact family (last) name is required.";
            litResultMsg.Text = "<span style='color:#dd0000'>Primary contact family (last) name is required</span>.";
            familyName.Focus();
            return;
        }
        else { 
            eFamilyName.InnerHtml = "";
            litResultMsg.Text = "";
        }
        if (firstName.Text.Trim() == "")
        {
            eFirstName.InnerHtml = "Primary contact first name is required.";
            litResultMsg.Text = "<span style='color:#dd0000'>Primary contact first name is requiredM</span>.";
            firstName.Focus();
            return;
        }
        else
        {
            eFirstName.InnerHtml = "";
            litResultMsg.Text = "";
        }
        if (rdoGender.SelectedIndex == -1)
        {
            eGender.InnerHtml = "Please indicate gender of primary contact.";
            litResultMsg.Text = "<span style='color:#dd0000'>Please indicate gender of primary contact</span>.";
            rdoGender.Focus();
            return;
        }
        else
        {
            eGender.InnerHtml = "";
            litResultMsg.Text = "";
        }
        if (DOB.Text.Trim() == "")
        {
            eDOB.InnerHtml = "Please indicate date of birth of primary contact.";
            litResultMsg.Text = "<span style='color:#dd0000'>Please indicate date of birth of primary contact</span>.";
            DOB.Focus();
            return;
        }
        else
        {
            eDOB.InnerHtml = "";
            litResultMsg.Text = "";
        }
        isValidDate = checkDate(DOB);
        if (!isValidDate)
        {
            eDOB.InnerHtml = "Please enter a valid date of birth for the primary contact";
            litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for the primary contact<span>.";
            DOB.Focus();
            return;
        }
        else
        {
            eDOB.InnerHtml = "";
            litResultMsg.Text = "";
        }

        if (driversLicenseNumber.Text.Trim() == "")
        {
            eLicense.InnerHtml = "Please provide driver's license number of primary contact.";
            litResultMsg.Text = "<span style='color:#dd0000'>Please provide driver's license number of primary contact</span>.";
            return;
        }
        else
        {
            eLicense.InnerHtml = "";
            litResultMsg.Text = "";
        }
        if (occupation.Text.Trim() == "")
        {
            eOccupation.InnerHtml = "Please indicate occupation of primary contact.";
            litResultMsg.Text = "<span style='color:#dd0000'>Please indicate occupation of primary contact</span>.";
            occupation.Focus();
            return;
        }
        else
        {
            eOccupation.InnerHtml = "";
            litResultMsg.Text = "";
        }
        if (email.Text.Trim() == "")
        {
            eEmail.InnerHtml = "Please indicate email of primary contact.";
            litResultMsg.Text = "<span style='color:#dd0000'>Please indicate email of primary contact</span>.";
            email.Focus();
            return;
        }
        else
        {
            eEmail.InnerHtml = "";
            litResultMsg.Text = "";
        }
        // check the non-required fields for DOB of other family members and relatives
        // If they are not blank, validate here
        // if blank, a default value will be added when the variables are assigned - like the room availability dates
        if (DOB2.Text.Trim() != "")
        {
            isValidDate = checkDate(DOB2);
            if (!isValidDate)
            {
                eDOB2.InnerHtml = "Please enter a valid date of birth.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for the Secondary Host Family Contact</span>.";
                return;
            }
            else
            {
                eDOB2.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in DOB2
        if (DOB3.Text.Trim() != "")
        {
            isValidDate = checkDate(DOB3);
            if (!isValidDate)
            {
                eDOB3.InnerHtml = "Please enter a valid date of birth.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for the Other Adult Occupant 1 information</span>.";
                return;
            }
            else
            {
                eDOB3.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in DOB3
        if (DOB4.Text.Trim() != "")
        {
            isValidDate = checkDate(DOB4);
            if (!isValidDate)
            {
                eDOB4.InnerHtml = "Please enter a valid date of birth.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for the Other Adult Occupant 2 information</span>.";
                return;
            }
            else
            {
                eDOB4.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in DOB4
        if (DOB5.Text.Trim() != "")
        {
            isValidDate = checkDate(DOB5);
            if (!isValidDate)
            {
                eDOB5.InnerHtml = "Please enter a valid date of birth.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for the Minor Child 1 information</span>.";
                return;
            }
            else
            {
                eDOB5.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in DOB5
        if (DOB6.Text.Trim() != "")
        {
            isValidDate = checkDate(DOB6);
            if (!isValidDate)
            {
                eDOB6.InnerHtml = "Please enter a valid date of birth.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for the Minor Child 2 information</span>.";
                return;
            }
            else
            {
                eDOB6.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in DOB6
        if (DOB7.Text.Trim() != "")
        {
            isValidDate = checkDate(DOB7);
            if (!isValidDate)
            {
                eDOB7.InnerHtml = "Please enter a valid date of birth.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for the Minor Child 3 information</span>.";
                return;
            }
            else
            {
                eDOB7.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in DOB7
        if (DOB8.Text.Trim() != "")
        {
            isValidDate = checkDate(DOB8);
            if (!isValidDate)
            {
                eDOB8.InnerHtml = "Please enter a valid date of birth.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for the Minor Child 4 information</span>.";
                return;
            }
            else
            {
                eDOB8.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in DOB8

        if (howReferred.Text.Trim() == "")
        {
            eHowReferred.InnerHtml = "Please indicate how you heard about the Homestay program.";
            howReferred.Focus();
            return;
        } else
            eHowReferred.InnerHtml = "";
        if (whyHost.Text.Trim() == "")
        {
            eWhyHost.InnerHtml = "Please indicate what interests you in being a homestay family.";
            whyHost.Focus();
            return;
        }
        else
            eWhyHost.InnerHtml = "";
        if (primaryLanguage.Text.Trim() == "")
        {
            ePrimaryLanguage.InnerHtml = "Please indicate primary language spoken in the home.";
            primaryLanguage.Focus();
            return;
        } else
            ePrimaryLanguage.InnerHtml = "";
        if (smokingGuidelines.Text.Trim() == "")
        {
            eSmoking.InnerHtml = "Please indicate your guidelines regarding student use of alcohol or cigarettes.";
            smokingGuidelines.Focus();
            return;
        }
        else
            eSmoking.InnerHtml = "";
        if (petsInHome.Text.Trim() == "")
        {
            ePets.InnerHtml = "Please indicate whether or  not your household has pets.";
            petsInHome.Focus();
            return;
        }
        else
            ePets.InnerHtml = "";
        if (generalLifestyle.Text.Trim() == "")
        {
            eGeneralLifestyle.InnerHtml = "Please indicate your family's general lifestyle.";
            generalLifestyle.Focus();
            return;
        }
        else
            eGeneralLifestyle.InnerHtml = "";

        
//        Response.Write("get values in btn click<br />");
        getValues("save");

        // check to see if a file has been added - it will be uploaded after the FamilyInfo table is updated since it needs the ID generated from the FamilyInfo table insert.
        HttpFileCollection uploadedFiles = Request.Files;
        if (uplLicenseFile.HasFile || uploadedFiles.Count > 0)
            blLicenseSubmitted = true;

        //Insert HomestayFamilyInfo table
        //Concatenate home name
        strHomeName = familyName.Text + ", " + firstName.Text;
//        Response.Write("ready to SAVE. Home name: " + strHomeName + "<br />");

        if (firstName2.Text != "") { strHomeName += " and " + firstName2.Text; }
//        Response.Write(" Home name2: " + strHomeName + "<br />");
//        Response.Write(" username: " + strUserName + "<br />");
//        Response.Write(" main: " + intMainLevelRooms.ToString() + "; up: " + intUpstairsRooms.ToString() + "; basement: " + intBasementRooms.ToString() + "<br />");
        //Retrieve family ID with insert
        //String resultMsg;
        intFamilyID = hsInfo.InsertHomestayFamilyInfo(strUserName, strHomeName, strStreetAddress, strCity, strState, strZIP, strLandLinePhone,
            strInsuranceCompany, strInsurancePolicyNumber, strInsuranceAgentName, strInsuranceAgentPhone, strWhyHost, strHostExperience, strPrimaryLanguage, strOtherLanguages,
            strReligiousActivities, strSmokingGuidelines, strPetsInHome, strGeneralLifestyle, strOtherSchool, strAdditionalExpectations, blLicenseSubmitted, strHowReferred,
            strRoom1, strRoom1DateOpen, strRoom1DateClosed, strRoom1Bathroom, strRoom1Occupancy,
            strRoom2, strRoom2DateOpen, strRoom2DateClosed, strRoom2Bathroom, strRoom2Occupancy,
            strRoom3, strRoom3DateOpen, strRoom3DateClosed, strRoom3Bathroom, strRoom3Occupancy,
            strRoom4, strRoom4DateOpen, strRoom4DateClosed, strRoom4Bathroom, strRoom4Occupancy,
            intMainLevelRooms, intUpstairsRooms, intBasementRooms);

//        Response.Write("new identity: " + intFamilyID.ToString() + "<br />");
       // Response.Write("msg: " + resultMsg + "<br />");
        //Insert Primary Contact into HomestayRelatives table
        intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName, FirstName, Occupation, DateOfBirth, Gender, "Primary Contact", Email, CellPhone, WorkPhone, DriversLicenseNumber, MiddleName);
        //Remainder of relatives, if any
        if (FamilyName2 != "" && FirstName2 != "")
        {
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
        }
        if (FamilyName3 != "" && FirstName3 != "")
        {
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
        }
        if (FamilyName4 != "" && FirstName4 != "")
        {
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
        }
        if (FamilyName5 != "" && FirstName5 != "")
        {
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
        }
        if (FamilyName6 != "" && FirstName6 != "")
        {
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
        }
        if (FamilyName7 != "" && FirstName7 != "")
        {
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
        }
        if (FamilyName8 != "" && FirstName8 != "")
        {
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
        }

        if (blIsAdmin)
        {
            //update admin only fields
            // Response.Write("travel: " + intFamilyID.ToString() + "; " + strWalkingBusStopMinutes + "; " + strWalkingBusStopMiles + "; " + strWalkingBusStopBlocks + "; " + strBusMinutesSCC + "; " + strBusMinutesSFCC + "; " + strCollegeQualified + "<br />"); 
            // intResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID, strWalkingBusStopMinutes, strWalkingBusStopMiles, strWalkingBusStopBlocks, strBusMinutesSCC, strBusMinutesSFCC, strCollegeQualified);
            decimal decMiles = Convert.ToDecimal(walkingBusStopMiles.Text.Trim().Replace("'", "''"));
            string sResult = "";
            sResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID, strWalkingBusStopMinutes, decMiles, strWalkingBusStopBlocks, strBusMinutesSCC, strBusMinutesSFCC, strCollegeQualified);
        }

        //Upload drivers license copies, if any
        for (int i = 0; i < uploadedFiles.Count; i++)
        {
            HttpPostedFile userPostedFile = uploadedFiles[i];
            try
            {
                if (userPostedFile.ContentLength > 0)
                {
                    strFileName = strHomeName + "-" + intFamilyID + "-License-" + userPostedFile.FileName;
                    userPostedFile.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your file, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    intResult = hsInfo.InsertUploadedFilesInformation(intFamilyID, strHomeName, strFileName, strUploadURL);
                    blLicenseSubmitted = true;
                }
            }
            catch (Exception ex) { Response.Write("Error: " + ex.Message + "<br />" + ex.StackTrace + "<br />"); }
        }
        if (hdnLicenseSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blLicenseSubmitted = true;
        }

        if (hsInfo.strResultMsg == "") { Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID); }
//        Response.Write("result btn click: " + strResultMsg + "<br />");
       // Response.Write("familyID btn click: " + intFamilyID.ToString() + "<br />");
    }

    protected void btnCmdUpdateAndContinue_Click(object sender, EventArgs e)
    {
        getValues("update");
        //process updates
        strHomeName = homeName.Text;
        strUserName = userName.Text;
        //Upload drivers license copies
        if (uplLicenseFile.HasFile)
        {
            strFileName = strHomeName + "-" + intFamilyID + "-License-" + uplLicenseFile.FileName;
            uplLicenseFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your file, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intFamilyID, strHomeName, strFileName, strUploadURL);
            blLicenseSubmitted = true;
        }
        else if (hdnLicenseSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blLicenseSubmitted = true;
        }
       // intResult string strMsg
        intResult = hsInfo.UpdateP1HomestayFamilyInfo(intFamilyID, strUserName, strHomeName, strStreetAddress, strCity, strState, strZIP, strLandLinePhone, 
            strInsuranceCompany, strInsurancePolicyNumber, strInsuranceAgentName, strInsuranceAgentPhone, strWhyHost, strHostExperience, strPrimaryLanguage, strOtherLanguages,
            strReligiousActivities, strSmokingGuidelines, strPetsInHome, strGeneralLifestyle, strOtherSchool, strAdditionalExpectations, blLicenseSubmitted, strHowReferred,
            strRoom1, strRoom1DateOpen, strRoom1DateClosed, strRoom1Bathroom, strRoom1Occupancy,
            strRoom2, strRoom2DateOpen, strRoom2DateClosed, strRoom2Bathroom, strRoom2Occupancy,
            strRoom3, strRoom3DateOpen, strRoom3DateClosed, strRoom3Bathroom, strRoom3Occupancy,
            strRoom4, strRoom4DateOpen, strRoom4DateClosed, strRoom4Bathroom, strRoom4Occupancy,
            intMainLevelRooms, intUpstairsRooms, intBasementRooms);
 //           Response.Write("result familyInfo: " + intResult.ToString() + "<br />pets in home: " + strPetsInHome + "<br />");
        if (blIsAdmin)
        {
            //update admin only fields
            //intResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID, strWalkingBusStopMinutes, strWalkingBusStopMiles, strWalkingBusStopBlocks, strBusMinutesSCC, strBusMinutesSFCC, strCollegeQualified);
            decimal decMiles = Convert.ToDecimal(walkingBusStopMiles.Text.Trim().Replace("'", "''"));
            string sResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID, strWalkingBusStopMinutes, decMiles, strWalkingBusStopBlocks, strBusMinutesSCC, strBusMinutesSFCC, strCollegeQualified);
        }

        //Primary Contact
        if (hdnPrimaryID.Value != null && hdnPrimaryID.Value != "")
        {
            Int32 PrimaryID = Convert.ToInt32(hdnPrimaryID.Value);
            intResult = hsInfo.UpdateHomestayRelatives(PrimaryID, FamilyName, FirstName, Occupation, DateOfBirth, Gender, "Primary Contact", Email, CellPhone, WorkPhone, DriversLicenseNumber, MiddleName);
        //    Response.Write("result update primary contact: " + intResult.ToString() + "<br />");
        }
        //Secondary Contact
        if (hdnSecondaryID.Value != null && hdnSecondaryID.Value != "")
        {
            Int32 SecondaryID = Convert.ToInt32(hdnSecondaryID.Value);
            intResult = hsInfo.UpdateHomestayRelatives(SecondaryID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
        //    Response.Write("result update secondary contact: " + intResult.ToString() + "<br />");
        }
        else if (FamilyName2 != "" && FirstName2 != "")
        {
            //Insert new relative
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName2, FirstName2, Occupation2, DateOfBirth2, Gender2, "Secondary Contact", Email2, CellPhone2, WorkPhone2, DriversLicenseNumber2, MiddleName2);
        //    Response.Write("result insert new 2: " + intResult.ToString() + "<br />");
        }
        //Adult 1
        if (hdnAdult1ID.Value != null && hdnAdult1ID.Value != "")
        {
            Int32 Adult1ID = Convert.ToInt32(hdnAdult1ID.Value);
            intResult = hsInfo.UpdateHomestayRelatives(Adult1ID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
        //    Response.Write("result update Adult 1: " + intResult.ToString() + "<br />");
        }
        else if (FamilyName3 != "" && FirstName3 != "")
        {
            //Insert new relative
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName3, FirstName3, Occupation3, DateOfBirth3, Gender3, "Other Adult 1", Email3, CellPhone3, WorkPhone3, DriversLicenseNumber3, MiddleName3);
        //    Response.Write("result insert new Adult 1: " + intResult.ToString() + "<br />");
        }
        //Adult 2
        if (hdnAdult2ID.Value != null && hdnAdult2ID.Value != "")
        {
            Int32 Adult2ID = Convert.ToInt32(hdnAdult2ID.Value);
            intResult = hsInfo.UpdateHomestayRelatives(Adult2ID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
        //    Response.Write("result update adult 2: " + intResult.ToString() + "<br />");
        }
        else if (FamilyName4 != "" && FirstName4 != "")
        {
            //Insert new relative
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName4, FirstName4, Occupation4, DateOfBirth4, Gender4, "Other Adult 2", Email4, CellPhone4, WorkPhone4, DriversLicenseNumber4, MiddleName4);
        //    Response.Write("result insert new adult 2: " + intResult.ToString() + "<br />");
        }

        //Minor Child 1
        if (hdnChild1ID.Value != null && hdnChild1ID.Value != "")
        {
            Int32 Child1ID = Convert.ToInt32(hdnChild1ID.Value);
            intResult = hsInfo.UpdateHomestayRelatives(Child1ID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
        //    Response.Write("result update child 1: " + intResult.ToString() + "<br />");
        }
        else if (FamilyName5 != "" && FirstName5 != "")
        {
            //Insert new relative
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName5, FirstName5, "", DateOfBirth5, Gender5, "Minor Child 1", "", "", "", "", "");
        //    Response.Write("result insert child 1: " + intResult.ToString() + "<br />");
        }
        //Minor Child 2
        if (hdnChild2ID.Value != null && hdnChild2ID.Value != "")
        {
            Int32 Child2ID = Convert.ToInt32(hdnChild2ID.Value);
            intResult = hsInfo.UpdateHomestayRelatives(Child2ID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
        //    Response.Write("result update chile 2: " + intResult.ToString() + "<br />");
        }
        else if (FamilyName6 != "" && FirstName6 != "")
        {
            //Insert new relative
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName6, FirstName6, "", DateOfBirth6, Gender6, "Minor Child 2", "", "", "", "", "");
         //   Response.Write("result insert child 2: " + intResult.ToString() + "<br />");
        }
        //Minor Child 3
        if (hdnChild3ID.Value != null && hdnChild3ID.Value != "")
        {
            Int32 Child3ID = Convert.ToInt32(hdnChild3ID.Value);
            intResult = hsInfo.UpdateHomestayRelatives(Child3ID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
         //   Response.Write("result update child 3: " + intResult.ToString() + "<br />");
        }
        else if (FamilyName7 != "" && FirstName7 != "")
        {
            //Insert new relative
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName7, FirstName7, "", DateOfBirth7, Gender7, "Minor Child 3", "", "", "", "", "");
         //   Response.Write("result insert child 3: " + intResult.ToString() + "<br />");
        }
        //Minor Child 4
        if (hdnChild4ID.Value != null && hdnChild4ID.Value != "")
        {
            Int32 Child4ID = Convert.ToInt32(hdnChild4ID.Value);
            intResult = hsInfo.UpdateHomestayRelatives(Child4ID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
        //    Response.Write("result update child 4: " + intResult.ToString() + "<br />");
        }
        else if (FamilyName8 != "" && FirstName8 != "")
        {
            //Insert new relative
            intResult = hsInfo.InsertHomestayRelatives(0, intFamilyID, FamilyName8, FirstName8, "", DateOfBirth8, Gender8, "Minor Child 4", "", "", "", "", "");
       //     Response.Write("result insert child 4: " + intResult.ToString() + "<br />");
        }

        if (!blIsAdmin)
            Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID + "&UID=" + strUserName + "&admin=False");
        else
            Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID);
    }

    protected void btnCmdContinue_Click(object sender, EventArgs e)
    {
        if(!blIsAdmin)
        Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID + "&UID="+strUserName+"&admin=False");
        else
            Response.Redirect("PlacementProfile.aspx?ID=" + intFamilyID);
    }

    protected void btnCmdReturnToAdmin_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin/Default.aspx");
    }
    protected void btnUploadLicenseFile_Click(object sender, EventArgs e)
    {
        strHomeName = familyName.Text + ", " + firstName.Text;
        if (firstName2.Text != "") { strHomeName += " and " + firstName2.Text; }
//        Response.Write("File upload. Home name: " + strHomeName + "<br />");

        if (uplLicenseFile.HasFile)
        {
            strFileName = strHomeName + "-" + intFamilyID + "-License-" + uplLicenseFile.FileName;
            uplLicenseFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your file, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intFamilyID, strHomeName, strFileName, strUploadURL);
  //          Response.Write("upload: " + intResult.ToString() + "<br />");
            blLicenseSubmitted = true;
            hdnLicenseSubmitted.Value = "true";
        } else if (hdnLicenseSubmitted.Value.ToLower() == "true")
        {
            // indicates a previous submittion
            blLicenseSubmitted = true;
        }
        litLicenseFile.Text = "";
        DataTable dt = hsInfo.GetUploadedFiles(intFamilyID);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strFileName = dr["fileName"].ToString();
                    if (strFileName.Contains("License"))
                    {
                        // get the file name and display
                        litLicenseFile.Text += "<br /><b>License submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                    }
                }// end of foreach
            }// end rowcount > 0
        }// end not null
    }// end btnUPloadLicenseFile

    protected void btnUploadMultiLicenseFile_Click(object sender, EventArgs e)
    {
        strHomeName = familyName.Text + ", " + firstName.Text;
        if (firstName2.Text != "") { strHomeName += " and " + firstName2.Text; }
        

        HttpFileCollection uploadedFiles = Request.Files;
        for (int i = 1; i < uploadedFiles.Count; i++)
            {
                HttpPostedFile userPostedFile = uploadedFiles[i];
                try
                {
                    if(userPostedFile.ContentLength > 0)
                    {
                    Response.Write("file number: " + (i).ToString() + ";  content type: " + userPostedFile.ContentType + "file name: " + userPostedFile.FileName + "<br />");
                    }
                } catch(Exception ex) { Response.Write("Error: " + ex.Message + "<br />" + ex.StackTrace + "<br />"); }
            }
        strFileName = strHomeName + "-" + intFamilyID + "-License-" + uplLicenseFile.FileName;
        blLicenseSubmitted = true;
        hdnLicenseSubmitted.Value = "true";
        if (hdnLicenseSubmitted.Value.ToLower() == "true")
        {
            // indicates a previous submittion
            blLicenseSubmitted = true;
        }

    }
    protected void btnCmdUpdateTravel_Click(object sender, EventArgs e)
    {
        if (walkingBusStopMiles.Text.Trim() == "")
            walkingBusStopMiles.Text = "0";
        decimal decMiles = Convert.ToDecimal(walkingBusStopMiles.Text.Trim().Replace("'", "''"));
        /**********************************************************/
        /*  CONVERT REST TO INTS!!! */
        string sResult = "";
        Response.Write("familyID: " + intFamilyID.ToString() + "; walking miles: " + walkingBusStopMiles.Text + "; SCC bus minutes: " + busMinutesSCC.Text + "<br />");
        if (walkingBusStopMinutes.Text.Trim() == "")
            walkingBusStopMinutes.Text = "0";
        if (walkingBusStopBlocks.Text.Trim() == "")
            walkingBusStopBlocks.Text = "0";
        if (busMinutesSCC.Text.Trim() == "")
            busMinutesSCC.Text = "0";
        if (busMinutesSFCC.Text.Trim() == "")
            busMinutesSFCC.Text = "0";
        sResult = hsInfo.UpdateTravelTimeHomestayFamilyInfo(intFamilyID, walkingBusStopMinutes.Text.Trim().Replace("'", "''"), decMiles, walkingBusStopBlocks.Text.Trim().Replace("'", "''"), busMinutesSCC.Text.Trim().Replace("'", "''"), busMinutesSFCC.Text.Trim().Replace("'", "''"), collegeQualified.SelectedValue);
        Response.Write("<br />btnUpdateTravel result: " + sResult + "<br />");
    }
    protected void getValues(string fromWhere)
    {
        //used to count each type of room
        intUpstairsRooms = 0;
        intBasementRooms = 0;
        intMainLevelRooms = 0;
        rdoIsActive.SelectedIndex = -1;
        userName.Text = "";
        //uplLicenseFile.
        //Home information 
        strStreetAddress = streetAddress.Text.Replace("'", "''");
        strCity = city.Text.Replace("'", "''");
        strState = state.Text;
        strZIP = ZIP.Text;
        strLandLinePhone = landLinePhone.Text;
 //       Response.Write("in getValues - phone: " + strLandLinePhone + "<br />");

        strWalkingBusStopMinutes = walkingBusStopMinutes.Text.Trim().Replace("'", "''");
        if (strWalkingBusStopMinutes == "")
            strWalkingBusStopMinutes = "0";
        strWalkingBusStopMiles = walkingBusStopMiles.Text.Trim().Replace("'", "''");
        if (strWalkingBusStopMiles == "")
            strWalkingBusStopMiles = "0";
        strWalkingBusStopBlocks = walkingBusStopBlocks.Text.Trim().Replace("'", "''");
        if (strWalkingBusStopBlocks == "")
            strWalkingBusStopBlocks = "0";
        strBusMinutesSCC = busMinutesSCC.Text.Trim().Replace("'", "''");
        if (strBusMinutesSCC == "")
            strBusMinutesSCC = "0";
        strBusMinutesSFCC = busMinutesSFCC.Text.Trim().Replace("'", "''");
        if (strBusMinutesSFCC == "")
            strBusMinutesSFCC = "0";
        strCollegeQualified = collegeQualified.SelectedValue;
    //    Response.Write("in getValues and fromWhere value: " + fromWhere + "<br />");
        //if(fromWhere == "save")
        //    strUserName = userName.Text.Trim().Replace("'", "''");
        if(blIsAdmin)
            strUserName = userName.Text.Trim().Replace("'", "''");
        //       Response.Write("in getValues - walkingBusMin: " + strWalkingBusStopMinutes + "<br />");
        //       Response.Write("in getValues - BusMinSCC: " + strBusMinutesSCC + "<br />");
        //       Response.Write("in getValues - College: " + strCollegeQualified + "<br />");
        //Response.Write("in getValues - username: " + strUserName + "<br />");
        
        strHowReferred = howReferred.Text.Replace("'", "''");
        strRoom1 = rdoRoom1.SelectedValue;
        if(rdoRoom1.SelectedIndex != -1)
            addRoom(strRoom1);
        strRoom1DateOpen = room1DateOpen.Text.Replace("'", "''");
        if (room1DateClosed.Text.Trim() == "")
            strRoom1DateClosed = "1900-01-01";
        else
            strRoom1DateClosed = room1DateClosed.Text.Replace("'", "''");
        strRoom1Bathroom = rdoRoom1Bathroom.SelectedValue;
        strRoom1Occupancy = rdoRoom1Occupancy.Text.Replace("'", "''");

        strRoom2 = rdoRoom2.SelectedValue;
        if (rdoRoom2.SelectedIndex != -1)
            addRoom(strRoom2);
        if (room2DateOpen.Text.Trim() == "")
            strRoom2DateOpen = "1900-01-01";
        else
            strRoom2DateOpen = room2DateOpen.Text.Replace("'", "''");
        if (room2DateClosed.Text.Trim() == "")
            strRoom2DateClosed = "1900-01-01";
        else
            strRoom2DateClosed = room2DateClosed.Text.Replace("'", "''");        
        strRoom2Bathroom = rdoRoom2Bathroom.SelectedValue;
        strRoom2Occupancy = rdoRoom2Occupancy.Text.Replace("'", "''");

        strRoom3 = rdoRoom3.SelectedValue;
        if (rdoRoom3.SelectedIndex != -1)
            addRoom(strRoom3);
        if (room3DateOpen.Text.Trim() == "")
            strRoom3DateOpen = "1900-01-01";
        else
            strRoom3DateOpen = room3DateOpen.Text.Replace("'", "''");
        if (room3DateClosed.Text.Trim() == "")
            strRoom3DateClosed = "1900-01-01";
        else
            strRoom3DateClosed = room3DateClosed.Text.Replace("'", "''");
        strRoom3Bathroom = rdoRoom3Bathroom.SelectedValue;
        strRoom3Occupancy = rdoRoom3Occupancy.Text.Replace("'", "''");

        strRoom4 = rdoRoom4.SelectedValue;
        if (rdoRoom4.SelectedIndex != -1)
            addRoom(strRoom4);
        if (room4DateOpen.Text.Trim() == "")
            strRoom4DateOpen = "1900-01-01";
        else
            strRoom4DateOpen = room4DateOpen.Text.Replace("'", "''");
        if (room4DateClosed.Text.Trim() == "")
            strRoom4DateClosed = "1900-01-01";
        else
            strRoom4DateClosed = room4DateClosed.Text.Replace("'", "''");
        strRoom4Bathroom = rdoRoom4Bathroom.SelectedValue;
        strRoom4Occupancy = rdoRoom4Occupancy.Text.Replace("'", "''");

        strInsuranceCompany = insuranceCompanyName.Text.Replace("'", "''");
        strInsurancePolicyNumber = insurancePolicyNumber.Text.Replace("'", "''");
        strInsuranceAgentName = insuranceAgentName.Text.Replace("'", "''");
        strInsuranceAgentPhone = insuranceAgentPhone.Text.Replace("'", "''");
        strWhyHost = whyHost.Text.Replace("'", "''");
        strHostExperience = hostExperience.Text.Replace("'", "''");
        strPrimaryLanguage = primaryLanguage.Text.Replace("'", "''");
        strOtherLanguages = otherLanguages.Text.Replace("'", "''");
        strReligiousActivities = religiousActivities.Text.Replace("'", "''");
        strSmokingGuidelines = smokingGuidelines.Text.Replace("'", "''");
        strPetsInHome = petsInHome.Text.Replace("'", "''");
//        Response.Write("in getValues - pets: " + strPetsInHome + "<br />");
        strGeneralLifestyle = generalLifestyle.Text.Replace("'", "''");
        strOtherSchool = otherSchool.Text.Replace("'", "''");
        strAdditionalExpectations = additionalExpectations.Text.Replace("'", "''");
        //Primary Contact
        FamilyName = familyName.Text.Replace("'", "''");
        FirstName = firstName.Text.Replace("'", "''");
        MiddleName = middleName.Text.Replace("'", "''");
        DateOfBirth = DOB.Text;
        Gender = rdoGender.SelectedValue;
        DriversLicenseNumber = driversLicenseNumber.Text;
        Occupation = occupation.Text.Replace("'", "''");
        CellPhone = cellPhone.Text;
        WorkPhone = workPhone.Text;
        Email = email.Text;
        //Secondary Contact
        FamilyName2 = familyName2.Text.Replace("'", "''");
        FirstName2 = firstName2.Text.Replace("'", "''");
        MiddleName2 = middleName2.Text.Replace("'", "''");
        if (DOB2.Text.Trim() == "")
            DateOfBirth2 = "1900-01-01";
        else
            DateOfBirth2 = DOB2.Text;
        Gender2 = rdoGender2.SelectedValue;
        DriversLicenseNumber2 = driversLicenseNumber2.Text;
        Occupation2 = occupation2.Text.Replace("'", "''");
        CellPhone2 = cellPhone2.Text;
        WorkPhone2 = workPhone2.Text;
        Email2 = email2.Text;
        //Other Adult 1
        FamilyName3 = familyName3.Text.Replace("'", "''");
        FirstName3 = firstName3.Text.Replace("'", "''");
        MiddleName3 = middleName3.Text.Replace("'", "''");
        if (DOB3.Text.Trim() == "")
            DateOfBirth3 = "1900-01-01";
        else
            DateOfBirth3 = DOB3.Text;
        Gender3 = rdoGender3.SelectedValue;
        DriversLicenseNumber3 = driversLicenseNumber3.Text;
        Occupation3 = occupation3.Text.Replace("'", "''");
        CellPhone3 = cellPhone3.Text;
        WorkPhone3 = workPhone3.Text;
        Email3 = email3.Text;
        //Other Adult 2
        FamilyName4 = familyName4.Text.Replace("'", "''");
        FirstName4 = firstName4.Text.Replace("'", "''");
        MiddleName4 = middleName4.Text.Replace("'", "''");
        if (DOB4.Text.Trim() == "")
            DateOfBirth4 = "1900-01-01";
        else
            DateOfBirth4 = DOB4.Text;
        Gender4 = rdoGender4.SelectedValue;
        DriversLicenseNumber4 = driversLicenseNumber4.Text;
        Occupation4 = occupation4.Text.Replace("'", "''");
        CellPhone4 = cellPhone4.Text;
        WorkPhone4 = workPhone4.Text;
        Email4 = email4.Text;
        //Minor Child 1
        FamilyName5 = familyName5.Text.Replace("'", "''");
        FirstName5 = firstName5.Text.Replace("'", "''");
        if (DOB5.Text.Trim() == "")
            DateOfBirth5 = "1900-01-01";
        else
            DateOfBirth5 = DOB5.Text;
        Gender5 = rdoGender5.SelectedValue;
        //Minor Child 2
        FamilyName6 = familyName6.Text.Replace("'", "''");
        FirstName6 = firstName6.Text.Replace("'", "''");
        if (DOB6.Text.Trim() == "")
            DateOfBirth6 = "1900-01-01";
        else
            DateOfBirth6 = DOB6.Text;
        Gender6 = rdoGender6.SelectedValue;
        //Minor Child 3
        FamilyName7 = familyName7.Text.Replace("'", "''");
        FirstName7 = firstName7.Text.Replace("'", "''");
        if (DOB7.Text.Trim() == "")
            DateOfBirth7 = "1900-01-01";
        else
            DateOfBirth7 = DOB7.Text;
        Gender7 = rdoGender7.SelectedValue;
        //Minor Child 4
        FamilyName8 = familyName8.Text.Replace("'", "''");
        FirstName8 = firstName8.Text.Replace("'", "''");
        if (DOB8.Text.Trim() == "")
            DateOfBirth8 = "1900-01-01";
        else
            DateOfBirth8 = DOB8.Text;
        Gender8 = rdoGender8.SelectedValue;
    }

    Boolean checkDate(TextBox txtBox)
    {
        dateSuccess = DateTime.TryParse(txtBox.Text.Trim(), out outDate);
        if (dateSuccess)
            txtBox.Text = outDate.ToShortDateString();
        else
            txtBox.Focus();
        return dateSuccess;
    }

    protected void addRoom(string strRoomLevel)
    {
        switch (strRoomLevel)
        {
            case "Upper":
                intUpstairsRooms++;
                break;
            case "Main":
                intMainLevelRooms++;
                break;
            case "Lower":
                intBasementRooms++;
                break;
            default:
               break;
        }
    }
    protected void resetFields()
    {
        intUpstairsRooms = 0;
        intBasementRooms = 0;
        intMainLevelRooms = 0;
        //Home information 
        streetAddress.Text = "";
        city.Text = "";
        state.Text = "";
        ZIP.Text = "";
        landLinePhone.Text = "";
        walkingBusStopMinutes.Text = "";
        walkingBusStopMiles.Text = "";
        walkingBusStopBlocks.Text = "";
        busMinutesSCC.Text = "";
        busMinutesSFCC.Text = "";
        collegeQualified.SelectedIndex = -1;

        howReferred.Text = "";
        rdoRoom1.SelectedIndex = -1;
        room1DateOpen.Text = "";
        rdoRoom1Bathroom.SelectedIndex = -1;
        rdoRoom1Occupancy.Text = "";
        rdoRoom2.SelectedIndex = -1;
        room2DateOpen.Text = "";
        room2DateClosed.Text = "";
        rdoRoom2Bathroom.SelectedIndex = -1;
        rdoRoom2Occupancy.Text = "";
        rdoRoom3.SelectedIndex = -1;
        room3DateOpen.Text = "";
        room3DateClosed.Text = "";
        rdoRoom3Bathroom.SelectedIndex = -1;
        rdoRoom3Occupancy.Text = "";
        rdoRoom4.SelectedIndex = -1;
        room4DateOpen.Text = "";
        room4DateClosed.Text = "";
        rdoRoom4Bathroom.SelectedIndex = -1;
        rdoRoom4Occupancy.Text = "";

        insuranceCompanyName.Text = "";
        insurancePolicyNumber.Text = "";
        insuranceAgentName.Text = "";
        insuranceAgentPhone.Text = "";
        whyHost.Text = "";
        hostExperience.Text = "";
        primaryLanguage.Text = "";
        otherLanguages.Text = "";
        religiousActivities.Text = "";
        smokingGuidelines.Text = "";
        petsInHome.Text = "";
        generalLifestyle.Text = "";
        otherSchool.Text = "";
        additionalExpectations.Text = "";

        //Relatives
        //clear the fields - primary contact
        familyName.Text = "";
        firstName.Text = "";
        middleName.Text = "";
        rdoGender.SelectedIndex = -1;
        rdoGender.Text = "";
        DOB.Text = "";
        driversLicenseNumber.Text = "";
        occupation.Text = "";
        cellPhone.Text = "";
        workPhone.Text = "";
        email.Text = "";
        hdnPrimaryID.Value = "";
        //clear the fields - secondary contact
        familyName2.Text = "";
        firstName2.Text = "";
        middleName2.Text = "";
        rdoGender2.SelectedIndex = -1;
        rdoGender2.Text = "";
        DOB2.Text = "";
        driversLicenseNumber2.Text = "";
        occupation2.Text = "";
        cellPhone2.Text = "";
        workPhone2.Text = "";
        email2.Text = "";
        hdnSecondaryID.Value = "";
        //clear the fields - Other adult 1
        familyName3.Text = "";
        firstName3.Text = "";
        middleName3.Text = "";
        rdoGender3.SelectedIndex = -1;
        rdoGender3.Text = "";
        DOB3.Text = "";
        driversLicenseNumber3.Text = "";
        occupation3.Text = "";
        cellPhone3.Text = "";
        workPhone3.Text = "";
        email3.Text = "";
        hdnAdult1ID.Value = "";
        //clear the fields - Other adult 2
        familyName4.Text = "";
        firstName4.Text = "";
        middleName4.Text = "";
        rdoGender4.SelectedIndex = -1;
        rdoGender4.Text = "";
        DOB4.Text = "";
        driversLicenseNumber4.Text = "";
        occupation4.Text = "";
        cellPhone4.Text = "";
        workPhone4.Text = "";
        email4.Text = "";
        hdnAdult2ID.Value = "";
        //clear the fields - minor child 1
        familyName5.Text = "";
        firstName5.Text = "";
        DOB5.Text = "";
        rdoGender5.SelectedIndex = -1;
        rdoGender5.Text = "";
        hdnChild1ID.Value = "";
        //clear the fields - minor child 2
        familyName6.Text = "";
        firstName6.Text = "";
        DOB6.Text = "";
        rdoGender6.SelectedIndex = -1;
        rdoGender6.Text = "";
        hdnChild2ID.Value = "";
        //clear the fields - minor child 3
        familyName7.Text = "";
        firstName7.Text = "";
        DOB7.Text = "";
        rdoGender7.SelectedIndex = -1;
        rdoGender7.Text = "";
        hdnChild3ID.Value = "";
        //clear the fields - minor child 4
        familyName8.Text = "";
        firstName8.Text = "";
        DOB8.Text = "";
        rdoGender8.Text = "";
        rdoGender8.SelectedIndex = -1;
        hdnChild4ID.Value = "";
        litLicenseFile.Text = "";
    }
}