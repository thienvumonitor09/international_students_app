﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
public partial class Homestay_Students_AgreementsDev : System.Web.UI.Page
{
    String strButtonFace = "";
    String strAppPart = "";
    String applicantID = "";
    String strUA = "";
    bool blNoButtons = false;
    Int32 intApplicantID = 0;
    String strResultMsg = "";
    Int32 intResult = 0;
    Boolean blIsAdmin = false;
 //   String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
    String strUploadURL = @"\\dist17-ccsnet\InternetContent\CCS\Homestay\";
    String strFileName = "";
    String strLastName = "";
    String strFirstName = "";
    DataTable dtStudent;
    DataTable dtHomestayStudent;
    Boolean blWaiveLiability18Submitted = false;
    Boolean blAgreeCCSConduct18 = false;
    Boolean blAgreeWaiveLiability18 = false;
    Boolean blEmailSent = false;

    public static String intlEmail = "globalprograms@ccs.spokane.edu";
    string sendTo = "";
    string sentFrom = "Internationalhomestay@ccs.spokane.edu";
    string strCCaddresses = "Internationalhomestay@ccs.spokane.edu;amy.cosgrove@ccs.spokane.edu;aadil.refaey@ccs.spokane.edu;laura.padden@ccs.spokane.edu";
    string strSubject = "International Homestay Student Application Submission";
    string strEmailResult = "";
    string strBody = "";

    Homestay hsInfo = new Homestay();
   
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnPartNo.Value = "1";
        }
        
        String strLogonUser = Request.ServerVariables["LOGON_USER"];

        
        if (strLogonUser != "" && strLogonUser != null)
        {
            //If user has not logged out of Family Portal, forms authentication considers the person logged in
            if (strLogonUser.Contains("ccs\\"))
            {
                blIsAdmin = true;
            }
        }
        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (Request.QueryString["part"] != null)
        {
            strAppPart = Request.QueryString["part"].ToString();
            if (strAppPart == "2") { strButtonFace = "Save"; }
            blNoButtons = true;
        }
        if (Request.QueryString["UA"] != null)
        {
            //page load
            strUA = Request.QueryString["UA"].ToString();
            hdnUA.Value = strUA;
        }
        if (strUA == "")
        {
            //after postback
            strUA = hdnUA.Value;
        }
        if (Request.QueryString["ID"] != null)
        {
            //page load
            applicantID = Request.QueryString["ID"].ToString();
            hdnApplicantID.Value = applicantID;
        }
        if (applicantID == "")
        {
            //after postback
            applicantID = hdnApplicantID.Value;
        }
        if (applicantID != "" && applicantID != null && applicantID != "0")
        {
            intApplicantID = Convert.ToInt32(applicantID);
        }
        if (intApplicantID != 0)
        {
            dtStudent = hsInfo.GetOneInternationalStudent("", intApplicantID, "", "", "", "");
            if (dtStudent != null)
            {
                if (dtStudent.Rows.Count > 0)
                {
                    strFirstName = dtStudent.Rows[0]["firstName"].ToString();
                    strLastName = dtStudent.Rows[0]["familyName"].ToString();
                    sendTo = dtStudent.Rows[0]["Email"].ToString();
                }
            }
            dtHomestayStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtHomestayStudent != null)
            {
                if (dtHomestayStudent.Rows.Count > 0)
                {
                    blWaiveLiability18Submitted = Convert.ToBoolean(dtHomestayStudent.Rows[0]["WaiveLiability18Submitted"]);
                    blAgreeCCSConduct18 = Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeCCSConduct18"]);
                    blAgreeWaiveLiability18 = Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeWaiveLiability18"]);
                    blEmailSent = Convert.ToBoolean(dtHomestayStudent.Rows[0]["SubmittedEmailSent"]);
                }
            }

            //Add agent's email
            DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
            if (dtContact != null)
            {
                if (dtContact.Rows.Count > 0)
                {

                    string strAgentEmail = dtContact.Rows[0]["Addr"].ToString();
                    strCCaddresses += ";" + strAgentEmail;
                }
            }

        }
        

        if (blWaiveLiability18Submitted)
        {
            getWaiveLiability18();
        }

        switch (strButtonFace)
        {
            case "Return to Admin Home Page":
                Response.Redirect("/Homestay/Admin/Default.aspx");
                break;
            case "Save and Continue":
                //  strResultMsg += "CCSConduct18 Checked? " + chkCCSConduct18.Checked + "<br />";
                if (chkCCSConduct18.Checked)
                {
                    intResult = hsInfo.UpdateUnderstandAgreeHomestayStudentInfo(intApplicantID, 1, "CCSConduct18");
                    eConduct18.InnerHtml = "";
                    //Display Waiver and upload button
                    if (blAgreeWaiveLiability18)
                    {
                        //Set visibility
                        divSaveAgreement.Visible = true;
                        divSave.Visible = false;
                    }
                    else
                    {
                        divSaveAgreement.Visible = false;
                        divSave.Visible = true;
                    }
                    divConfirmation.Visible = false;
                    divSaveContinue.Visible = false;
                    divPrevious.Visible = true;
                    div1.Visible = false;
                    div2.Visible = true;
                    hdnPartNo.Value = "2";

                }
                else
                {
                    //Get CCSConduct18 agreement value
                    //Set checkbox
                    eConduct18.InnerHtml = "You must show you understand your rights and responsibilities when living with a Homestay Host Family. Please check the box.";
                    chkCCSConduct18.Focus();
                }
                break;
            case "Back":
                if (hdnPartNo.Value.Equals("2"))
                {
                    Response.Redirect("Agreements18.aspx?ID=" + applicantID + "&site=" + Request.QueryString["site"]);
                }
                else if(hdnPartNo.Value.Equals("1"))
                {
                    Response.Redirect("PlacementProfile_L.aspx?ID=" + applicantID + "&UA=" + strUA + "&site=" + Request.QueryString["site"]);
                }
                break;
            case "Save Agreement and Continue":
                if (uplWaiveLiability18File.HasFile)
                {
                    strFileName = strLastName + "-" + intApplicantID + "-WaiveLiability18-" + uplWaiveLiability18File.FileName;
                    //Must replace illegal chars with "_" to prevent error when uploading
                    strFileName = Utility.replaceIllegalChars(strFileName);
                    uplWaiveLiability18File.SaveAs(strUploadURL + strFileName);
                    strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                    //Insert record in UploadedFilesInformation table
                    intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                    blWaiveLiability18Submitted = true;
                    intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "WaiveLiability18Submitted");
                }
                divConfirmation.Visible = true;
                div1.Visible = false;
                div2.Visible = false;
                divButtons.Visible = false;
                if (blIsAdmin)
                {
                    divButtons.Visible = true;
                    divSave.Visible = false;
                    divPrevious.Visible = true;
                    divSaveAgreement.Visible = false;
                    divPrint.Visible = false;
                    divHomepage.Visible = true;
                }
                break;
            default:
                //Visibility control
                if (blAgreeCCSConduct18)
                {
                    //Set visibility
                }
                divConfirmation.Visible = false;
                divSaveAgreement.Visible = false;
                divButtons.Visible = true;
                div1.Visible = true;
                div2.Visible = false;
                divSave.Visible = false;
                divPrint.Visible = true;
                if (blAgreeCCSConduct18 && !blAgreeWaiveLiability18 && IsPostBack)
                {
                    divConfirmation.Visible = false;
                    divSaveAgreement.Visible = false;
                    divButtons.Visible = true;
                    div1.Visible = false;
                    div2.Visible = true;
                    divSave.Visible = true;
                    divPrint.Visible = true;
                }
                break;
        }
        if (blNoButtons) { divButtons.Visible = false; }
        if (intApplicantID != 0)
        {
            //After processing, set checkboxes
            dtHomestayStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtHomestayStudent != null)
            {
                if (dtHomestayStudent.Rows.Count > 0)
                {
                    //Set checkboxes
                    if (Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeCCSConduct18"]) == true)
                    {
                        chkCCSConduct18.Checked = true;
                    }
                    if (Convert.ToBoolean(dtHomestayStudent.Rows[0]["agreeWaiveLiability18"]) == true)
                    {
                        chkWaiveLiability18.Checked = true;
                    }
                }
            }
        }
        litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }

    protected void btnCmdSaveAndContinue_Click(object sender, EventArgs e)
    {
        if (!chkCCSConduct18.Checked)
        {
            eConduct18.InnerHtml = "You must check the box indicating you understand your rights and resposibilities regarding the Homestay Program";
            chkCCSConduct18.Focus();
            return;
        }
    }

    protected void btnCmdSave_Click(object sender, EventArgs e)
    {
        // process submission
        strResultMsg += "WaiveLiability18 Checked? " + chkWaiveLiability18.Checked + "<br />";
        if (!chkWaiveLiability18.Checked)
        {
            eWaiveLiability18.InnerHtml = "You must indicate you have read the waiver and agree to it.  Please check the box.";
            chkWaiveLiability18.Focus();
            eConduct18.InnerHtml = "";
            //Display Waiver and upload button
            if (blAgreeWaiveLiability18)
            {
                //Set visibility
                divSaveAgreement.Visible = true;
                divSave.Visible = false;
            }
            else
            {
                divSaveAgreement.Visible = false;
                divSave.Visible = true;
            }
            divConfirmation.Visible = false;
            divSaveContinue.Visible = false;
            divPrevious.Visible = true;
            div1.Visible = false;
            div2.Visible = true;
            hdnPartNo.Value = "2";
            return;
        } else if (chkWaiveLiability18.Checked)
        {
            eWaiveLiability18.InnerHtml = "";
            intResult = hsInfo.UpdateUnderstandAgreeHomestayStudentInfo(intApplicantID, 1, "WaiveLiability18");
            if (intResult > 0) { strResultMsg += "Your Homestay Application form is submitted!<br />"; }
            if (uplWaiveLiability18File.HasFile)
            {
                strFileName = strLastName + "-" + intApplicantID + "-WaiveLiability18-" + uplWaiveLiability18File.FileName;
                //Must replace illegal chars with "_" to prevent error when uploading
                strFileName = Utility.replaceIllegalChars(strFileName);
                uplWaiveLiability18File.SaveAs(strUploadURL + strFileName);
                strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                //Insert record in UploadedFilesInformation table
                intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                blWaiveLiability18Submitted = true;
                intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "WaiveLiability18");
            }
            else if (hdnWaiveLiability18Submitted.Value.ToLower() == "true")
            {
                //previous submission exists
                blWaiveLiability18Submitted = true;
            }
            //Determine any missing file uploads then generate message in both email and confirmation fieldset
            // verify upload of files
            string strMissingUploads = "";
            blWaiveLiability18Submitted = false;
            
            DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        strFileName = dr["fileName"].ToString();
                        if (strFileName.Contains("-WaiveLiability18-"))
                            blWaiveLiability18Submitted = true;
                    }// end foreach
                }// end row count > 0
            }// end dt not null
             // if flags end up still false, populate the missing files string (strMissingUploads)
            if (!blWaiveLiability18Submitted)
            {
                litMissingUploads.Text = "<p>The Waiver of Liability form must be signed, scanned and uploaded into your application or emailed to <a href='mailto:Internationalhomestay@ccs.spokane.edu'>Internationalhomestay@ccs.spokane.edu</a> before your application can be fully processed.</p>";
            }
            // generate the email to the student
            Utility uEmail = new Utility();
            if (sendTo != "" && !blEmailSent)
            {
                
                // prepare the body of the email
                strBody = "";
                strBody = "<p>Dear " + strFirstName + " " + strLastName + ", </p>";
                strBody += "<p>Thank you for applying to the CCS Homestay Program!  We will review your application and begin the process of matching a family to your profile.  ";
                strBody += "Please feel free to learn more about the Homestay Program at our website:<br />";
                strBody += "<a href='https://sfcc.spokane.edu/Become-a-Student/I-am-an-International-Student/After-I-Have-Been-Admitted/Housing-Options/Homestay' target='_blank'>Homestay Program information</a></p>";
                strBody += "<p>Please take note of the following details and recommendations:<br />";
                strBody += "<ul><li>Student applications must be submitted 30 days prior to arrival.</li>";
                if (!blWaiveLiability18Submitted) { 
                    strBody += "<li>The Waiver of Liability form must be signed, scanned and uploaded into your application or emailed to <a href='mailto:Internationalhomestay@ccs.spokane.edu'>Internationalhomestay@ccs.spokane.edu</a>.</li>";
                }
                strBody += "<li>Housing Placements will be released to the agent/student after the student visa is approved.</li>";
                strBody += "<li>Minors (students ages 16-17) are REQUIRED to be in Homestay.</li>";
                strBody += "<li>Housing is not guaranteed for students over the age of 18. The application fee will be refunded if no suitable family is available.</li>";
                strBody += "<li>Preferences indicated on the Homestay Application will guide placements, but preferences cannot be guaranteed. </li>";
                strBody += "<li>After placement is confirmed, please contact your host family right away by email. They are looking forward to connecting with you and making arrangements for your arrival!  Please ask your host family about arrival dates prior to booking airline tickets to Spokane. They will be meeting you at the airport, but would like to coordinate a suitable date.  </li>";
                strBody += "<li>Once your airline flight has been arranged, please send a copy of your flight itinerary to the Homestay Office:  InternationalHomestay@ccs.spokane.edu so that our staff can prepare arrangements with your host family for airport pick-up. Please ensure that flights terminate before 9:00 p.m. in Spokane.</li>";
                strBody += "<li>We look forward to meeting you at the Homestay Success Meeting!  This mandatory meeting will take place about one week after school starts.  Please be sure you attend so that we can help you develop a positive experience in your new homestay family.</li></ul></p>";
                strBody += "<p>Please contact the Community Colleges of Spokane - International Homestay Program at the following email address if you have further questions:<br />";
                strBody += "<a href='mailto:Internationalhomestay@ccs.spokane.edu'>Internationalhomestay@ccs.spokane.edu</a></p>";
                /* RESTORE WHEN NO LONGER MANUALLY ENTERING APPLICATIONS!!! */
                //strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);
                strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, "");
                if (strEmailResult == "Success")
                {
                    // update HomestayStudentInfo table submittedEmailSent field --> true
    /* RESTORE WHEN NO LONGER MANUALLY ENTERING APPLICATIONS!!! */
                    intResult = hsInfo.UpdateMailSentStudent(intApplicantID, 1);
                    // Display confirmation
                } else
                {
                    // error sending email so let user know
                    litResultMsg.Text = strEmailResult;
                    //return;
                }
            }
            else if (sendTo == "" && !blEmailSent) // no send to email address and SubmittedEmailSent = false so want to send // end have email address
            {
             //   sendTo = "Internationalhomestay@ccs.spokane.edu";
                sendTo = "laura.padden@ccs.spokane.edu";
                strSubject = "Problem sending homestay student applicaiton form submission email";
                strBody = "";
                strBody = "<p>Submission for " + strFirstName + " " + strLastName + " contained no email address. </p>";
                strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);

                if (strEmailResult == "Success")
                {
                    // Display confirmation
                }
                else
                {
                    // error sending email so let user know
                    litResultMsg.Text = strEmailResult;
                    //return;
                }
            }
                
            divConfirmation.Visible = true;
            div1.Visible = false;
            div2.Visible = false;
            divButtons.Visible = false;
            if (blIsAdmin)
            {
                divButtons.Visible = true;
                divSave.Visible = false;
                divPrevious.Visible = true;
                divSaveAgreement.Visible = false;
                divPrint.Visible = false;
                divHomepage.Visible = true;
            }
        }// end agree waiver checked
        
    }

    protected void btnCmdUploadWaiver_Click(object sender, EventArgs e)
    {
        eWaiveLiability18File.InnerText = "";
        if (uplWaiveLiability18File.HasFile)
        { 
            if (Utility.ContainsNonEnglishChars(uplWaiveLiability18File.FileName))
            {
                eWaiveLiability18File.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
            }
            else
            {
                eUploadWaiver.InnerHtml = "";
                strFileName = strLastName + "-" + intApplicantID + "-WaiveLiability18-" + uplWaiveLiability18File.FileName;

                //Must replace illegal chars with "_" to prevent error when uploading
                strFileName = Utility.replaceIllegalChars(strFileName);
                uplWaiveLiability18File.SaveAs(strUploadURL + strFileName);
                strResultMsg += "Your agreement, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
                //Insert record in UploadedFilesInformation table
                intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
                blWaiveLiability18Submitted = true;
                intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "WaiveLiability18");
                //Response.Write("file uploaded<br />");
                getWaiveLiability18();
                btnCmdSave.Focus();
            }
        }
        else
        {
            eUploadWaiver.InnerHtml = "You must select a file to upload.";
            btnCmdUploadWaiver.Focus();
        }
        setVisible();
        //divSaveAgreement.Visible = true;
        //divSave.Visible = false;
        //return;  
    }

    protected void setVisible()
    {
        if (blIsAdmin)
        {
            divButtons.Visible = true;
            divSave.Visible = false;
            divPrevious.Visible = true;
            divSaveAgreement.Visible = false;
            divPrint.Visible = false;
            divHomepage.Visible = true;
        }
        else
        {
            div1.Visible = false;
            div2.Visible = true;
            divSave.Visible = true;
            divPrint.Visible = true;
        }
    }

    protected void getWaiveLiability18()
    {
        Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
        AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
        String strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
        litWaiveLiability18File.Text = "";
        DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strFileName = dr["fileName"].ToString();
                    string filePath = dr["filePath"].ToString();
                    string uploadServer = "";
                    if (filePath.Contains("ccs-internet"))
                    {
                        uploadServer += @"http:\\apps.spokane.edu\InternetContent\Homestay\";
                    }
                    else
                    {
                        uploadServer += @"http:\\portal.ccs.spokane.edu\InternetContent\Homestay\";
                    }
                    if (strFileName.Contains("WaiveLiability18"))
                    {
                        //get the file name
                        litWaiveLiability18File.Text += "<br /><b>Waiver of Liability 18+ submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                    }
                }
            }
        }
    }

    

  
}