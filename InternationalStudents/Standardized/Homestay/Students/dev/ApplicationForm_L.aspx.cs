﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Homestay_ApplicationForm_L : System.Web.UI.Page
{
 

    /* FIELD LIST from vw_StudentInfoExtended
        hsGrad, hsGradDate, studyWhat, studyWhere, 
        studyMajor, studyMajorOther, trSchools, studiesBegin, 
        id, familyName, firstName, middleNames, 
        fullName, DOB, Gender, studentStatus, 
        countryOfBirth, countryOfCitizenship, englishAbility, visaType, 
        numberDependents, Spouse, Children, inUS, 
        useAgency, sendI_20, releaseInformation, allTrue, 
        howHearCCS, dateSubmitted, SID, maritalStatus, 
        addressCode, addresseeName, agencyName, Addr, 
        City, StateProv, Zip, Country, Phone, 
        CellPhone, Email, Relationship, healthIns
     */

    /************************************************************************************
    *  address codes:  1:  student's permanent address
    *                  2:  student's parent's address (if different)
    *                  3:  student's agency address (if any)
    *                  4:  student's relative address (if in US)
    *                  5:  student's emergency contact address
    *  Use applicant ID to match to student
    * ********************************************************************************/

    /* FIELD LIST from ContactInformation
        applicantID, addressCode, addresseeName, agencyName
		Addr, City, StateProv, Zip, Country
		Phone, CellPhone, Email, Relationship, speakEnglish
     */
    /*
     * Student Status:
     * App Submitted
     * App Complete
     * Placed
     * Withdrew
     * Graduated
     * Visa Denied
     * Deferred
     * Moved Out
     */
    Boolean blIsAdmin = false;
    String strDOB = "";
    String strFirstName = "";
    String strLastName = "";
    Int32 intApplicantID = 0;
    String strSID = "";
    Int32 intAgencyID = 0;
    String strGender = "";
    String strCountry = "";
    String strStudentEmail = "";
    String strArrivalDate = "";
    String strAgency = "";
    String strAgentName = "";
    String strAgentEmail = "";
    String strAgencyAddress = "";
    String strAgencyCity = "";
    String strAgencyState = "";
    String strAgencyCountry = "";
    String strAgencyZIP = "";
    String strAgencyPhone = "";
    Boolean blUseAgency = false;
    String strCollege = "";
    String strHomestayOption = "";
    String strPayMethod = "";
    Boolean blNoRecords = false;
    Int32 intIsActive = 1;
    String strResultMsg = "";
    String strButtonFace = "";
    DateTime dtDOB = System.DateTime.Today;
    String strYear = System.DateTime.Today.Year.ToString();
    DateTime dtFirstDayOfClass;
    DateTime dtLatestDOB;
    Boolean blUnderAge = false;
    //String strUploadURL = @"\\ccs-internet\InternetContent\CCS\Homestay\";
    String strUploadURL = @"\\dist17-ccsnet\InternetContent\CCS\Homestay\";
    String strUploadServer = "";
    String strFileName = "";
    String strQuarterStartDate = "";
    String strSelectedQuarter = "";
    Boolean blLetterSubmitted = false;
    Boolean blPhotoSubmitted = false;
    String studentStatus = "App Submitted";
    Int32 intNewIdentity = 0;
    Int32 intResult = 0;
    String strSubmitMsg = "";
    String strBlank = "";
    char speakEnglish = '0';
    Boolean blDuplicateBasicInfoRecords = false;
    Boolean blAddMode = true;
    String strAddMode = "";
    String selectedStudent = "";
    String applicantID = "";
    
    DateTime outDate;               // used in try/catch to validate dob on form
    Boolean dateSuccess = false;    // used in try/catch to validate dob on form
    Boolean validateError = false;

    String postBackControlName = "";

    Homestay hsInfo = new Homestay();
    Quarter_MetaData QMD = new Quarter_MetaData();
    Regex regexUplFile = new Regex(@"[^ -~]+");

    protected void Page_Load(object sender, EventArgs e)
    {
        String strLogonUser = Request.ServerVariables["LOGON_USER"];

        litResultMsg.Text = "";
        //For testing: get security context
        //WindowsIdentity winID = WindowsIdentity.GetCurrent();
        //strResultMsg = "<p><b>Security context:</b> " + winID.Name + "</p>";

        if (strLogonUser != "" && strLogonUser != null) 
        { 
            //If user has not logged out of Family Portal, forms authentication considers the person logged in
            if (strLogonUser.Contains("ccs\\"))
            {
                blIsAdmin = true; 
            }
        }

       if (IsPostBack)
        {
            applicantID = hdnApplicantID.Value;
            
        }
        if (Request.QueryString["ID"] != null)
        {
            //page load
            applicantID = Request.QueryString["ID"].ToString();
        }
        if (applicantID != "")
        {
            //returning from Profile page
            selectedStudent = applicantID;
        }
        //strResultMsg += "applicantID: " + applicantID + "<br />";
        if (applicantID != "0" && applicantID != "" && applicantID != null)
        {
            intApplicantID = Convert.ToInt32(applicantID);
            hdnApplicantID.Value = applicantID;
        }
  /*      
        //Not an ASP.Net control
        if (Request.Form["btnSubmit"] != null) { strButtonFace = Request.Form["btnSubmit"].ToString(); }
        //Redirect without processing
        if (strButtonFace == "Continue") { Response.Redirect("PlacementProfile.aspx?ID=" + hdnApplicantID.Value); }
        //Admin function only
        if (strButtonFace == "Reset Fields") { resetFields(); ddlStudents.SelectedValue = "0"; }
        if (strButtonFace == "Return to Admin Home Page" && blIsAdmin){ Response.Redirect("/Homestay/Admin/Default.aspx"); }
 */       

        //strResultMsg += "Button: " + strButtonFace + "<br />";
        strSelectedQuarter = rdoQuarterStart.SelectedValue;

//        Response.Write("postback? " + IsPostBack.ToString() + "<br />");

        if (!IsPostBack)
        {
                litErr.Text = "";

                //Visibility for initial page load
                divMorePersonalInfo.Visible = false;
                recruitingAgent.Visible = false;
                personalInformation.Visible = true;
                divSearchButton.Visible = true;
                homestay18.Visible = false;
                homestay1617.Visible = false;
                PhotoLetter.Visible = false;
                divSave.Visible = false;
                divNextPage.Visible = false;

                if (blIsAdmin)
                {
                    strResultMsg = "<p><b>strLogonUser:</b> " + strLogonUser + "; Is admin? " + blIsAdmin + "</p>";
                    //Admin acess and tools
                    if (ddlStudents.SelectedValue == "")
                    {
                        selectedStudent = applicantID;
                    }
                    else
                    {
                        //override any query string value
                        selectedStudent = ddlStudents.SelectedValue;
                    }
                    if (selectedStudent != "0" && selectedStudent != "" && selectedStudent != null)
                    {
                        applicantID = selectedStudent;
                        intApplicantID = Convert.ToInt32(applicantID);
                        hdnApplicantID.Value = selectedStudent;
                    }
                    //Set visibility
                    divAdmin.Visible = true;
                    divHome.Visible = true;
                    divNextPage.Visible = true;
                    //Fill student drop-down
                    DataTable dtStudents = hsInfo.GetHomestayStudents("Active", 0);
                    if (dtStudents != null)
                    {
                        if (dtStudents.Rows.Count > 0)
                        {
                            ddlStudents.Items.Clear();
                            ListItem LI;
                            LI = new ListItem();
                            LI.Value = "0";
                            LI.Text = "-- Add New Student --";
                            ddlStudents.Items.Add(LI);
                            foreach (DataRow dr in dtStudents.Rows)
                            {
                                LI = new ListItem();
                                LI.Value = dr["applicantID"].ToString();
                                LI.Text = dr["familyName"].ToString() + ", " + dr["firstName"].ToString() + " - " + Convert.ToDateTime(dr["DOB"]).ToShortDateString();
                                ddlStudents.Items.Add(LI);
                            }
                            if (selectedStudent != "") { ddlStudents.SelectedValue = selectedStudent; }
                        }
                        else if (blIsAdmin) { strResultMsg += "Error: dtStudents has no rows.<br />"; }
                    }
                    else if (blIsAdmin) { strResultMsg += "Error: dtStudents is null.<br />"; }

                }
                else
                {
                    //Student view
                    divAdmin.Visible = false;
                    divHome.Visible = false;
                }
                DataTable dtQuarters = QMD.GetRangeRows_Quarter_MetaData_Base(0, 3);
                ListItem LI2;
                if (dtQuarters != null)
                {
                    if (dtQuarters.Rows.Count > 0)
                    {
                        rdoQuarterStart.Items.Clear();
                    LI2 = new ListItem();
                    /*
                    LI2.Value = "Current student";
                    LI2.Text = "Current student";
                    rdoQuarterStart.Items.Add(LI2);
                    */
                    foreach (DataRow dr in dtQuarters.Rows)
                        {
                            LI2 = new ListItem();
                            LI2.Value = Convert.ToDateTime(dr["FirstDayOfClass"]).ToShortDateString();
                            LI2.Text = dr["Name"].ToString() + " - " 
                                        + String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(dr["FirstDayOfClass"]));
                            rdoQuarterStart.Items.Add(LI2);
                            //strResultMsg += "Item: " + dr["Name"].ToString() + " - " + Convert.ToDateTime(dr["FirstDayOfClass"]).ToShortDateString() + "<br />";
                        }
                    
                        if (strSelectedQuarter != "") { rdoQuarterStart.SelectedValue = strSelectedQuarter; }
                    }
                    else if (blIsAdmin) { strResultMsg += "QMD has no rows.<br />"; }
                }
                else if (blIsAdmin) { strResultMsg += "QMD is null.<br />"; }
            postBackControlName = "";
        } else                  // not a postback
        {
            postBackControlName = Page.Request.Params["__EVENTTARGET"];
//            Response.Write("control name: " + postBackControlName + "<br />");
        }
       // if (IsPostBack || postBackControlName.Trim() == "ddlStudents")
        if((IsPostBack && validateError) || Request.QueryString["UA"] != null || (IsPostBack && blIsAdmin && postBackControlName.Trim() == "ddlStudents"))
       // if (postBackControlName.Trim() == "ddlStudents" || Request.QueryString["UA"] != null)
        {
            if (blIsAdmin && postBackControlName.Trim() == "ddlStudents")
            {
                selectedStudent = ddlStudents.SelectedValue;
 //               Response.Write("control: " + selectedStudent + "<br />");
                //set visibility of admin info for admins
                if (blIsAdmin)
                {
                    divAdmin.Visible = true;
                    divHome.Visible = true;
                }
                intApplicantID = Convert.ToInt32(selectedStudent);
            }
            hdnApplicantID.Value = intApplicantID.ToString();
            //do something
            DataTable dtStudent = hsInfo.GetOneInternationalStudent("", intApplicantID, "", "", "", "");
            if (dtStudent != null)
            {
                if (dtStudent.Rows.Count > 0)
                {
                    //clear any values from previous selection
                    resetFields();
                    if (dtStudent.Rows.Count > 1) { blDuplicateBasicInfoRecords = true; }
                    strFirstName = dtStudent.Rows[0]["firstName"].ToString();
                    txtFirstName.Text = strFirstName;
                    strLastName = dtStudent.Rows[0]["familyName"].ToString();
                    txtFamilyName.Text = strLastName;
                    strGender = dtStudent.Rows[0]["Gender"].ToString();
                    strSID = dtStudent.Rows[0]["SID"].ToString();
                    hdnSID.Value = strSID;
                    txtSID.Text = strSID;
                    dtDOB = Convert.ToDateTime(dtStudent.Rows[0]["DOB"]);
                    txtDOB.Text = dtDOB.ToShortDateString();
                    if (strQuarterStartDate != "")
                    {
                        dtFirstDayOfClass = Convert.ToDateTime(strQuarterStartDate);
                    }
                    else
                    {
                        strQuarterStartDate = QMD.NextQtrFirstClass.ToShortDateString();
                        dtFirstDayOfClass = QMD.NextQtrFirstClass;
                    }
                    dtLatestDOB = dtFirstDayOfClass.AddYears(-18);
                    if (dtLatestDOB < dtDOB)
                    {
                        blUnderAge = true;
                        hdnAgeLevel.Value = "true";
                        divNextPage.Visible = true;
                    }
                    else
                    {
                        blUnderAge = false;
                        hdnAgeLevel.Value = "false";
                    }
                    //strResultMsg += "DOB threshold: " + dtLatestDOB.ToShortDateString() + "; underage? " + blUnderAge + "<br />";
                    strCountry = dtStudent.Rows[0]["Country"].ToString();
                    strStudentEmail = dtStudent.Rows[0]["Email"].ToString();
                    blUseAgency = Convert.ToBoolean(dtStudent.Rows[0]["useAgency"]);
                    //Check with Homestay if they want this behavior
                    if (blUseAgency)
                    {
                        recruitingAgent.Visible = true;
                        DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
                        if (dtContact != null)
                        {
                            if (dtContact.Rows.Count > 0)
                            {
                                strAgency = dtContact.Rows[0]["agencyName"].ToString();
                                txtAgency.Text = strAgency;
                                strAgentName = dtContact.Rows[0]["addresseeName"].ToString();
                                txtAgentName.Text = strAgentName;
                                strAgentEmail = dtContact.Rows[0]["Email"].ToString();
                                txtAgentEmail.Text = strAgentEmail;
                                strAgencyAddress = dtContact.Rows[0]["Addr"].ToString();
                                txtAgencyAddress.Text = strAgencyAddress;
                                strAgencyCity = dtContact.Rows[0]["City"].ToString();
                                txtAgencyCity.Text = strAgencyCity;
                                strAgencyState = dtContact.Rows[0]["StateProv"].ToString();
                                txtAgencyState.Text = strAgencyState;
                                strAgencyZIP = dtContact.Rows[0]["Zip"].ToString();
                                txtAgencyZip.Text = strAgencyZIP;
                                strAgencyCountry = dtContact.Rows[0]["Country"].ToString();
                                txtAgencyCountry.Text = strAgencyCountry;
                                strAgencyPhone = dtContact.Rows[0]["Phone"].ToString();
                                txtAgencyPhone.Text = strAgencyPhone;
                                hdnAgencyID.Value = dtContact.Rows[0]["id"].ToString();
                            }
                        }
                    }
                    else
                    {
                        recruitingAgent.Visible = false;
                    }
                    strCollege = dtStudent.Rows[0]["studyWhere"].ToString();
                    if (strCollege == "Pullman") { strCollege = "SFCC"; }
                    if (blIsAdmin)
                    {
                        //radio buttons only available to staff
                        rdoWhereStudy.SelectedValue = strCollege;
                    }
                    rdoGender.SelectedValue = strGender;
                    txtCountry.Text = strCountry;
                    txtPermEmail.Text = strStudentEmail;
                }
            }// end IsPostback generated from the student drop-down
            
        }

        //Set mode: insert or update
        strAddMode = hdnAddMode.Value;
        if (strAddMode.ToLower() == "false") { blAddMode = false; }
        if (Request.QueryString["UA"] != null)
        {
            if (Request.QueryString["UA"] == "f")
                blUnderAge = false;
            else
                blUnderAge = true;
        }
     
        if (!IsPostBack)
        {
            bool blstring = false;
            bool blnull = false;
            if(hdnApplicantID.Value != "")
                blstring = true;
            else
                blstring = false;
            if(hdnApplicantID.Value != null)
                blnull = true;
            else
                blnull = false;

         //   Response.Write("blstring: " + blstring.ToString() + "; blnull: " + blnull.ToString() + "<br />"); 

           // for use when Back button is clicked on profile page
            if (hdnApplicantID.Value != "" && hdnApplicantID.Value != null)
            {
                // get the data from the tables and populate the text boxes
                //Find the student and populate the info
                DataTable dtInfo = hsInfo.GetOneInternationalStudent("", Convert.ToInt32(hdnApplicantID.Value), "01-01-1900", "", "", "");
                if (dtInfo != null)
                {
                    txtFamilyName.Text = dtInfo.Rows[0]["familyName"].ToString();
                    txtFirstName.Text = dtInfo.Rows[0]["firstName"].ToString();
                    txtDOB.Text = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(dtInfo.Rows[0]["DOB"]));
                    txtCountry.Text = dtInfo.Rows[0]["Country"].ToString();
                    txtPermEmail.Text = dtInfo.Rows[0]["Email"].ToString();
                    rdoGender.SelectedValue = dtInfo.Rows[0]["Gender"].ToString();
                }

            }
            
        }

        if ((IsPostBack && blIsAdmin && postBackControlName.Trim() != "ddlStudents") || Request.QueryString["UA"] != null)
        {
            //Collect data for insert/update 
            strFirstName = txtFirstName.Text;
            strLastName = txtFamilyName.Text;
            strDOB = txtDOB.Text;
            strSID = txtSID.Text;
//            Response.Write("ispostback NOT ddlStudents: " + strSID + "<br />");

            
            strAgency = txtAgency.Text.Trim().Replace("'", "''");
            strAgentName = txtAgentName.Text.Trim().Replace("'", "''");
            strAgentEmail = txtAgentEmail.Text.Trim().Replace("'", "''");
            strAgencyAddress = txtAgencyAddress.Text.Trim().Replace("'", "''");
            strAgencyCity = txtAgencyCity.Text.Trim().Replace("'", "''");
            strAgencyState = txtAgencyState.Text.Trim().Replace("'", "''");
            strAgencyZIP = txtAgencyZip.Text.Trim().Replace("'", "''");
            strAgencyCountry = txtAgencyCountry.Text.Trim().Replace("'", "''");
            strAgencyPhone = txtAgencyPhone.Text.Trim().Replace("'", "''");
            strHomestayOption = rdoHomestayOption.SelectedValue;
            strArrivalDate = txtArrivalDate.Text.Trim().Replace("'", "''");
            //Response.Write("arrival date 443: " + strArrivalDate + "<br />");
//            Response.Write("Homestay: " + strHomestayOption + "<br />");

            strCollege = rdoWhereStudy.SelectedValue;
//            Response.Write("college: " + strCollege + "<br />");

            strGender = rdoGender.SelectedValue;
            strCountry = txtCountry.Text.Trim().Replace("'", "''");
            strStudentEmail = txtPermEmail.Text.Trim().Replace("'", "''");

        }

        strQuarterStartDate = rdoQuarterStart.SelectedValue;

       // Response.Write("arrive: " + strArrivalDate + "<br />");
        if (strArrivalDate != "" && strArrivalDate != "1900-01-01")
            txtArrivalDate.Text = strArrivalDate;
        if (strHomestayOption != "")
        {
            rdoHomestayOption.SelectedValue = strHomestayOption;
        }

        if (hdnLetterSubmitted.Value.ToLower() == "true")
        {
            
        }
        if (hdnPhotoSubmitted.Value.ToLower() == "true")
        {

        }

        if (hdnAgeLevel.Value == "true")
        {
            blUnderAge = true;
            homestay1617.Visible = false;
            homestay1617.Visible = true;
        }
        else if (hdnAgeLevel.Value == "false")
        {
            homestay1617.Visible = false;
            homestay18.Visible = true;
        }
        else
        {
            homestay18.Visible = false;
            homestay1617.Visible = false;
        }

        if (intApplicantID > 0 && !blDuplicateBasicInfoRecords)
        {
            //Set visibility
            personalInformation.Visible = true;
            divMorePersonalInfo.Visible = true;
            divSearchButton.Visible = false;
            if (blUnderAge)
            {
                homestay18.Visible = false;
                homestay1617.Visible = true;
            }
            else
            {
                homestay18.Visible = true;
                homestay1617.Visible = false;
            }
            PhotoLetter.Visible = true;
            divSave.Visible = true;
            if (blIsAdmin)
            {
                divNextPage.Visible = true;
            }

            //Check for existing HomestayStudentInfo record
            DataTable dtHSStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtHSStudent != null)
            {
                if (dtHSStudent.Rows.Count > 0)
                {
                    blAddMode = false;
                    hdnAddMode.Value = blAddMode.ToString();
                    //strResultMsg += "blAddMode 1? " + blAddMode + "<br />";
                    Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
  //                  Response.Write("config: " + config.ToString() + "<br />");
                    AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                    strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                    foreach (DataRow drHSStudent in dtHSStudent.Rows)
                    {
                        blLetterSubmitted = Convert.ToBoolean(drHSStudent["letterSubmitted"]);
                        hdnLetterSubmitted.Value = blLetterSubmitted.ToString();
                        blPhotoSubmitted = Convert.ToBoolean(drHSStudent["photoSubmitted"]);
                        hdnPhotoSubmitted.Value = blPhotoSubmitted.ToString();
                        if (blLetterSubmitted || blPhotoSubmitted)
                        {
                            litLetterFile.Text = "";
                            litPhotoFile.Text = "";
                            DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        strFileName = dr["fileName"].ToString();
                                        string filePath = dr["filePath"].ToString();
                                        string uploadServer = "";
                                        if (filePath.Contains("ccs-internet"))
                                        {
                                            uploadServer += @"http:\\apps.spokane.edu\InternetContent\Homestay\";
                                        }
                                        else
                                        {
                                            uploadServer += @"http:\\portal.ccs.spokane.edu\InternetContent\Homestay\";
                                        }
                                        if (strFileName.Contains("Letter"))
                                        {
                                            //get the file name
                                            litLetterFile.Text += "<br /><b>Letter submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                        if (strFileName.Contains("Photo"))
                                        {
                                            //Get the file name
                                            litPhotoFile.Text += "<br /><b>Photo submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                    }
                                }
                            }
                        }
                        //strResultMsg += "Letter submitted: " + Convert.ToBoolean(drHSStudent["letterSubmitted"]) + "<br />";
                        //strResultMsg += "Start Date: " + Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() + "<br />";
                        if (Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() != "1/1/1900")
                        {
                            try
                            {
                                rdoQuarterStart.SelectedValue = Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString();
                            }
                            catch
                            {
                                //do nothing
                            }
                        }
                        txtArrivalDate.Text = Convert.ToDateTime(drHSStudent["arrivalDate"]).ToShortDateString();
                        if (blIsAdmin)
                        {
                            txtDateApplied.Text = Convert.ToDateTime(drHSStudent["dateApplied"]).ToShortDateString();
                        }
                        if (!blUnderAge)
                        {
                            rdoHomestayOption.SelectedValue = drHSStudent["homestayOption"].ToString().Trim();
                        }
                    }
                    if (blIsAdmin)
                    {
                        strResultMsg += "A Homestay student application exists.  You may review the information here.<br />";
                    }
                    else
                    {
                        //setVisibilityNoAccess();
                        if (strButtonFace != "Save")
                        {
                            strResultMsg += "<span style='color:#DD0000;'>Your Homestay application has already been submitted. <br />You may view the information and upload more files</span>.</p>";
                            litAllRequired.Text = "";
                            PhotoLetter.Visible = true;
                            divUpload.Visible = true;
                            divSave.Visible = false;
                            divNextPage.Visible = true;
                        }
                        else
                        {
                            divNextPage.Visible = false;
                        }
                        
                    }
                }
                else { 
                    strResultMsg = "Please complete the application form below.  The application consists of multiple pages.<br />";
                    litAllRequired.Text = "";
                }
            }
            else { 
                strResultMsg = "Please complete the application below.  The application consists of multiple pages.<br />";
                litAllRequired.Text = "";
            }
        }
        if (blNoRecords)
        {
            setVisibilityNoAccess();
            if(!strResultMsg.Contains("Return")) {
                strResultMsg += "<span style=\"color:Red;font-weight:bold\">No records were returned.  You must complete the <a href=\"https://portal.ccs.spokane.edu/_netapps/internationalsa/InternationalStudents/Standardized/applicationForm.aspx?site=ccs\">International Student application</a> before applying for Homestay.</span><br />";
                strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"https://apps.spokane.edu/Homestay/Default.aspx\" > Return to the Homestay Homepage</a><br />";
            }
        }
        if (blDuplicateBasicInfoRecords) 
        {
            setVisibilityNoAccess();
            if (!strResultMsg.Contains("Return"))
            {
                strResultMsg += "<span style=\"color:Red;font-weight:bold\">Warning: multiple records exist with this last name, first name and date of birth.</span>  Please contact the Homestay staff to resolve this issue before continuing with the application process.<br />";
                strResultMsg += "<p style=\"font-size:larger; font-weight:bold;\"><a href=\"https://apps.spokane.edu/Homestay/Default.aspx\">Return to the Homestay Homepage</a><br />";
            }    
        }

        litResultMsg.Text = "<p>" + hsInfo.strResultMsg + strResultMsg +  "</p>";
    }

    void setVisibilityNoAccess()
    {
        //VISIBILITY when not redirecting
        if (!blIsAdmin)
        {
            personalInformation.Visible = false;
            divMorePersonalInfo.Visible = false;
            divSearchButton.Visible = false;
            recruitingAgent.Visible = false;
            homestay18.Visible = false;
            homestay1617.Visible = false;
            PhotoLetter.Visible = false;
            divSave.Visible = false;
            divNextPage.Visible = false;
        }// end is NOT admin
        else
        {
            personalInformation.Visible = true;
            divMorePersonalInfo.Visible = true;
            divSearchButton.Visible = false;
            if (hdnAgencyID.Value == null || hdnAgencyID.Value == "")
            recruitingAgent.Visible = false;
            else
                recruitingAgent.Visible = true;
            if (blUnderAge)
            {
                homestay18.Visible = false;
                homestay1617.Visible = true;
            }
            else
            {
                homestay1617.Visible = false;
                homestay18.Visible = true;
            }
            PhotoLetter.Visible = true;
            divSave.Visible = true;
            divNextPage.Visible = true;
        }// end is Admin
    }

    void resetFields()
    {
        //Stupid viewstate...
        txtSID.Text = "";
        txtFamilyName.Text = "";
        txtFirstName.Text = "";
        txtDOB.Text = "";
        txtDateApplied.Text = "";
        txtArrivalDate.Text = "";
        rdoHomestayOption.ClearSelection();
        rdoQuarterStart.ClearSelection();
        rdoGender.ClearSelection();
        txtCountry.Text = "";
        txtPermEmail.Text = "";
        if (blIsAdmin) { rdoWhereStudy.ClearSelection(); }
        txtAgencyAddress.Text = "";
        txtAgencyCity.Text = "";
        txtAgencyCountry.Text = "";
        txtAgencyPhone.Text = "";
        txtAgencyState.Text = "";
        txtAgencyZip.Text = "";
        txtAgentEmail.Text = "";
        txtAgentName.Text = "";
        litLetterFile.Text = "";
        litPhotoFile.Text = "";
        litResultMsg.Text = "";
    }
    protected void btnCmdSave_Click(object sender, EventArgs e)
    {
        blUnderAge = Convert.ToBoolean(hdnAgeLevel.Value);
        //validate the required fields
        if (strSID != "" && blIsAdmin)
        {
            if (strSID.Length != 9)
            {
                eSID.InnerHtml = "ctcLink ID must be 9 digits long";
                txtSID.Focus();
                return;
            }
            else
            {
                eSID.InnerHtml = "";
              //  strSID = txtSID.Text.Trim();
                txtSID.Text = strSID;
            }
            int goodSIDnum;
            bool goodSID = false;
            goodSID = Int32.TryParse(strSID, out goodSIDnum);
            if (!goodSID)
            {
                eSID.InnerHtml = "ctcLink ID must contain only digits";
                txtSID.Focus();
                return;
            }
            else
            {
                eSID.InnerHtml = "";
              //  strSID = txtSID.Text.Trim();
                txtSID.Text = strSID;
            }
        }// end txtSID contains a value
        try
        {
            // Response.Write("arrival in Save TEXT value: " + txtArrivalDate.Text.Trim() + "<br />");

            if (txtArrivalDate.Text.Trim() == "")
                strArrivalDate = "01/01/1900";
            else
                strArrivalDate = txtArrivalDate.Text.Trim();

            DateTime arriveDte;
            dateSuccess = DateTime.TryParse(strArrivalDate, out arriveDte);
            if (!dateSuccess)
            {
                eArrivalDate.InnerHtml = "Please enter your arrival date in a valid date format";
                txtArrivalDate.Focus();
                validateError = true;
                return;
            }
            else
            {
                eArrivalDate.InnerHtml = "";
                validateError = false;
               // strArrivalDate = txtArrivalDate.Text.Trim().Replace("'", "''");
                txtArrivalDate.Text = strArrivalDate;
            }
            if (!blIsAdmin)
                strHomestayOption = rdoHomestayOption.SelectedValue;
            if(!blUnderAge && strHomestayOption == "")
            {
                eHomeStayOption.InnerHtml = "Please select a Homestay Option";
                rdoHomestayOption.Focus();
                validateError = true;
                return;
            }
            if(!blUnderAge)
            {
                eHomeStayOption.InnerHtml = "";
             //   strHomestayOption = rdoHomestayOption.SelectedValue;
                rdoHomestayOption.SelectedValue = strHomestayOption;
                validateError = false;
            }
        }
        finally
        {

            if (eHomeStayOption.InnerHtml != "" || eArrivalDate.InnerHtml != "")
            {
                if (blUnderAge)
                {
                    homestay1617.Visible = true;
                    homestay18.Visible = false;
                }
                else
                {
                    homestay18.Visible = true;
                    homestay1617.Visible = false;
                }
            }
        }

        //Start the updates
        if (hdnApplicantID.Value != null && hdnApplicantID.Value != "") { intApplicantID = Convert.ToInt32(hdnApplicantID.Value); }
        //1 = Update contct information for agent
        InternationalStudent myIStudent = new InternationalStudent();
        if (hdnAgencyID.Value != null && hdnAgencyID.Value != "") { intAgencyID = Convert.ToInt32(hdnAgencyID.Value); }
        if (intAgencyID == 0 && strAgentName.Trim() != "")
        {
            //Insert new agent record - address code 3
            strSubmitMsg = myIStudent.InsertContactInfo(intApplicantID, 3, strAgentName, strAgency, strAgencyAddress, strAgencyCity, strAgencyState,
                strAgencyZIP, strAgencyCountry, strAgencyPhone, strBlank, strAgentEmail, strBlank, speakEnglish);
        }
        else if (strAgentName.Trim() != "")
        {
            //Update existing agency address - address code 3
            strSubmitMsg = myIStudent.UpdateContactInfo(intAgencyID, strAgentName, strAgency, strAgencyAddress, strAgencyCity, strAgencyState,
                strAgencyZIP, strAgencyCountry, strAgencyPhone, strBlank, strAgentEmail, strBlank, speakEnglish);
        }
        if (strSubmitMsg == "OK contact info") { strResultMsg += "Your agent contact information was updated successfully!<br />"; }

        if (strHomestayOption == "")
        {
            if (blUnderAge)
            {
                //16/17 year olds forced choice
                strHomestayOption = "With food";
            }
            else
            {
                strHomestayOption = "Either";
            }
        }
        //Upload the letter and photo   
        if(strLastName == "" || strFileName == "") {
                strLastName = txtFamilyName.Text.Trim().Replace("'", "''");
                strFirstName = txtFirstName.Text.Trim().Replace("'", "''");
            }
        if (uplLetterFile.HasFile)
        {
 //           Response.Write("upLetter names string: " + strLastName + ", " + strFileName + "  maybe txt: " + txtFamilyName.Text + ", " + txtFirstName + "<br />");
            strFileName = strLastName + "-" + intApplicantID + "-Letter-" + uplLetterFile.FileName;
            strFileName = Utility.replaceIllegalChars(strFileName);
            uplLetterFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your letter, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blLetterSubmitted = true;
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "letterSubmitted");
        }
        else if (hdnLetterSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blLetterSubmitted = true;
        }
        //strResultMsg += "Letter submitted? " + hdnLetterSubmitted.Value + "<br />";
        if (uplPhotoFile.HasFile)
        {
            strFileName = strLastName + "-" + intApplicantID + "-Photo-" + uplPhotoFile.FileName;
            strFileName = Utility.replaceIllegalChars(strFileName);
            uplPhotoFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your photo, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, strFirstName + " " + strLastName, strFileName, strUploadURL);
            blPhotoSubmitted = true;
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "photoSubmitted");
        }
        else if (hdnPhotoSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blPhotoSubmitted = true;
        }
        intApplicantID = Convert.ToInt32(hdnApplicantID.Value);

        //Response.Write("For Insert/Update: <br />" + strSelectedQuarter + "<br />" + strArrivalDate + "<br />" + strHomestayOption + "<br />" + strPayMethod + "<br />" + blLetterSubmitted.ToString() + "<br />" + blPhotoSubmitted.ToString() + "<br />" + intIsActive.ToString() + "<br />" + blAddMode.ToString() + "<Br />" + strCollege + "<br />" + strStudentEmail + "<br />");
        intNewIdentity = hsInfo.InsertUpdateP1HomestayStudentInfo(intApplicantID, strSelectedQuarter, strArrivalDate
                                , strHomestayOption, strPayMethod, blLetterSubmitted, blPhotoSubmitted, intIsActive, blAddMode);
        // for Admins to change the SID, college attending, and student email
        if (blIsAdmin && intNewIdentity == -1)
        {
            myIStudent.UpdateSID(intApplicantID, strSID);
        }
        strResultMsg += "intNewIdentity: " + intNewIdentity + "<br />";
        // If not automatically taken to next page, display result messages
        if (intNewIdentity > 0)
        {
            strResultMsg += "Your student information was inserted successfully!<br />";
        }
        else if (intNewIdentity == -1)
        {
            strResultMsg += "Your student information was updated successfully!<br />";
        }
        if (blAddMode && intNewIdentity > 0)
        {
            //2 = set IsHomestay bitflag in ApplicantBasicInfo table; use applicant ID
            intResult = hsInfo.UpdateHomestayBitflagApplicantBasicInfo(intApplicantID, 1);
            //3 = Update studentStatus; use homestay ID
            intResult = hsInfo.UpdateHomestayStudentStatus(intNewIdentity, 0, studentStatus);
            //4 = Update SID, if provided
            if (hdnSID.Value != null && hdnSID.Value != "")
            {
                //international student DB; use applicant ID
                myIStudent.UpdateSID(intApplicantID, strSID);
            }
            //if (hsInfo.strResultMsg == "")
            //{
            // Go to part 2
            string strRedirect = "PlacementProfile_L.aspx?ID=" + intApplicantID;
            //Add site 
            strRedirect += "&site=" + Request.QueryString["site"];
            Response.Redirect(strRedirect);
            //}
        }
        else if (!blAddMode && intApplicantID > 0)
        {
            //2 = Update SID, if field was empty to start with
            if (strSID != "" && strSID != null && (hdnSID.Value == null || hdnSID.Value == ""))
            {
                myIStudent.UpdateSID(intApplicantID, strSID);
            }
        }
        //Display any messages
        setVisibilityNoAccess();
        divNextPage.Visible = true;

    }// end btnSave_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        litErr.Text = "";
        //Get records by student search
        //validate the required fields
        if (txtFamilyName.Text.Trim() == "")
        {
            eFamName.InnerText = "Please enter your family name";
            txtFamilyName.Focus();
            return;
        }
        else
        {
            eFamName.InnerText = "";
            strLastName = txtFamilyName.Text.Trim().Replace("'", "''");
        }
        if (txtFirstName.Text.Trim() == "")
        {
            eFirstName.InnerText = "Please enter your first name.";
            txtFirstName.Focus();
            return;
        }
        else
        {
            eFirstName.InnerText = "";
            strFirstName = txtFirstName.Text.Trim().Replace("'", "''");
        }
        // test if valid date
        dateSuccess = DateTime.TryParse(txtDOB.Text.Trim(), out outDate);
        if (!dateSuccess)
        {
            eDOB.InnerText = "Please enter a valid date of birth";
            txtDOB.Focus();
            return;
        }
        else
        {
            eDOB.InnerText = "";
            strDOB = outDate.ToShortDateString();
        }

        if (rdoQuarterStart.SelectedIndex == -1)
        {
            eQtrStart.InnerText = "Please select a quarter start date.";
            rdoQuarterStart.Focus();
            return;
        }
        else
        {
            strQuarterStartDate = rdoQuarterStart.SelectedValue;
            eQtrStart.InnerText = "";
        }
        
        //Do some data calculation to determine age       
        if (strDOB != "" && dateSuccess)
        {
            dtDOB = Convert.ToDateTime(strDOB);
            if (strQuarterStartDate != "")
            {
                dtFirstDayOfClass = Convert.ToDateTime(strQuarterStartDate);
            }
            else
            {
                strQuarterStartDate = QMD.NextQtrFirstClass.ToShortDateString();
                dtFirstDayOfClass = QMD.NextQtrFirstClass;
            }
            dtLatestDOB = dtFirstDayOfClass.AddYears(-18);//calculate dtLatestDOB from selected or by default
            //Initial set of mode: true at this point
            hdnAddMode.Value = blAddMode.ToString();
            
            //Find the student and populate the info
            DataTable dtInfo = hsInfo.GetOneInternationalStudent("DOB", 0, strDOB, strFirstName, strLastName, "");
            if (dtInfo != null)
            {
                blNoRecords = false;
                if (dtInfo.Rows.Count > 0)
                {
                   
                    if (dtInfo.Rows.Count > 1) //check if duplicate record
                    {
                        blDuplicateBasicInfoRecords = true;
                    }
                    selectedStudent = dtInfo.Rows[0]["id"].ToString();
                    intApplicantID = Convert.ToInt32(dtInfo.Rows[0]["id"]);
                    hdnApplicantID.Value = intApplicantID.ToString();
                    strGender = dtInfo.Rows[0]["Gender"].ToString();
                    strCountry = dtInfo.Rows[0]["Country"].ToString();
                    strStudentEmail = dtInfo.Rows[0]["Email"].ToString();
                    blUseAgency = Convert.ToBoolean(dtInfo.Rows[0]["useAgency"]);
                    //Check with Homestay if they want this behavior
                    if (blUseAgency)
                    {
                        recruitingAgent.Visible = true;
                        DataTable dtContact = hsInfo.GetOneContact(3, intApplicantID);
                        if (dtContact != null)
                        {
                            if (dtContact.Rows.Count > 0)
                            {
                                strAgency = dtContact.Rows[0]["agencyName"].ToString();
                                txtAgency.Text = strAgency;
                                strAgentName = dtContact.Rows[0]["addresseeName"].ToString();
                                txtAgentName.Text = strAgentName;
                                strAgentEmail = dtContact.Rows[0]["Email"].ToString();
                                txtAgentEmail.Text = strAgentEmail;
                                strAgencyAddress = dtContact.Rows[0]["Addr"].ToString();
                                txtAgencyAddress.Text = strAgencyAddress;
                                strAgencyCity = dtContact.Rows[0]["City"].ToString();
                                txtAgencyCity.Text = strAgencyCity;
                                strAgencyState = dtContact.Rows[0]["StateProv"].ToString();
                                txtAgencyState.Text = strAgencyState;
                                strAgencyZIP = dtContact.Rows[0]["Zip"].ToString();
                                txtAgencyZip.Text = strAgencyZIP;
                                strAgencyCountry = dtContact.Rows[0]["Country"].ToString();
                                txtAgencyCountry.Text = strAgencyCountry;
                                strAgencyPhone = dtContact.Rows[0]["Phone"].ToString();
                                txtAgencyPhone.Text = strAgencyPhone;
                                hdnAgencyID.Value = dtContact.Rows[0]["id"].ToString();
                            }
                        }
                    }
                    else
                    {
                        recruitingAgent.Visible = false;
                    }
                    /*End checking show homestay recruiting agent*/
                    strCollege = dtInfo.Rows[0]["studyWhere"].ToString();
                    if (strCollege == "Pullman") { strCollege = "SFCC"; }
                    if (blIsAdmin)
                    {
                        rdoWhereStudy.SelectedValue = strCollege;
                    }
                    rdoGender.SelectedValue = strGender;
                    txtCountry.Text = strCountry;
                    txtPermEmail.Text = strStudentEmail;
                }
                else
                {
                    blNoRecords = true;
                }
            }//end check if dtInfo != null
            else
            {
                blNoRecords = true;
            }
        }

        if (blNoRecords)
        {
            //no student application records found
            strResultMsg = "<span style=\"color:Red;font-weight:bold\">No records were returned.  You must complete the <a href=\"https://portal.ccs.spokane.edu/_netapps/internationalsa/InternationalStudents/Standardized/applicationForm.aspx?site=ccs\" target='_blank'>International Student application</a> before applying for Homestay.</span><br />";
            strResultMsg += "<p >If you have previously submitted an International Student application, please correct either the family name, first name or date-of-birth in the form below or ";
            strResultMsg += "<a href=\"https://apps.spokane.edu/Homestay/Default.aspx\">Return to the Homestay Homepage</a><p>";
            litErr.Text = strResultMsg;
            divErr.Visible = true;
            return;
        }
        else
        {
            if(blDuplicateBasicInfoRecords)
            {
                strResultMsg += "<span style=\"color:Red;font-weight:bold\">Warning: multiple records exist with this last name, first name and date of birth.</span> <br />We are using the most recent submission for the following information.<br />If this is not the information you want used, please contact the Homestay staff to resolve this issue.<br />";
                litDuplicateRecords.Text = strResultMsg;
            }
            // student records returned so populate fields and show
            divMorePersonalInfo.Visible = true;
           
            PhotoLetter.Visible = true;
            divSave.Visible = true;
            divSearchButton.Visible = false;
            litAllRequired.Visible = false;

            //Check for existing HomestayStudentInfo record
            DataTable dtHSStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtHSStudent != null)
            {
                if (dtHSStudent.Rows.Count > 0)
                {
                    //if  record is found in HomeStayStudentInfo, used quarter start date from db, instead of using selected.
                    dtFirstDayOfClass = Convert.ToDateTime(dtHSStudent.Rows[0]["quarterStartDate"]);
                    dtLatestDOB = dtFirstDayOfClass.AddYears(-18); //recalculate dtLatestDOB from db

                    blAddMode = false;
                    hdnAddMode.Value = blAddMode.ToString();
                    //strResultMsg += "blAddMode 1? " + blAddMode + "<br />";
                    Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                    AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                    strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                    foreach (DataRow drHSStudent in dtHSStudent.Rows)
                    {
                        blLetterSubmitted = Convert.ToBoolean(drHSStudent["letterSubmitted"]);
                        hdnLetterSubmitted.Value = blLetterSubmitted.ToString();
                        blPhotoSubmitted = Convert.ToBoolean(drHSStudent["photoSubmitted"]);
                        hdnPhotoSubmitted.Value = blPhotoSubmitted.ToString();
                        if (blLetterSubmitted || blPhotoSubmitted)
                        {
                            litLetterFile.Text = "";
                            litPhotoFile.Text = "";
                            DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        strFileName = dr["fileName"].ToString();
                                        string filePath = dr["filePath"].ToString();
                                        string uploadServer = "";
                                        if (filePath.Contains("ccs-internet"))
                                        {
                                            uploadServer += @"http:\\apps.spokane.edu\InternetContent\Homestay\";
                                        }
                                        else
                                        {
                                            uploadServer += @"http:\\portal.ccs.spokane.edu\InternetContent\Homestay\";
                                        }
                                        if (strFileName.Contains("Letter"))
                                        {
                                            //get the file name
                                            litLetterFile.Text += "<br /><b>Letter submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                        if (strFileName.Contains("Photo"))
                                        {
                                            //Get the file name
                                            litPhotoFile.Text += "<br /><b>Photo submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                        }
                                    }
                                }
                            }
                        }
                        //strResultMsg += "Letter submitted: " + Convert.ToBoolean(drHSStudent["letterSubmitted"]) + "<br />";
                        //strResultMsg += "Start Date: " + Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() + "<br />";
                        if (Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() != "1/1/1900")
                        {
                            try
                            {
                                rdoQuarterStart.SelectedValue = Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString();
                            }
                            catch
                            {
                                //do nothing
                            }
                        }
                        txtArrivalDate.Text = Convert.ToDateTime(drHSStudent["arrivalDate"]).ToShortDateString();
                        if (blIsAdmin)
                        {
                            txtDateApplied.Text = Convert.ToDateTime(drHSStudent["dateApplied"]).ToShortDateString();
                        }
                        if (!blUnderAge)
                        {
                            rdoHomestayOption.SelectedValue = drHSStudent["homestayOption"].ToString().Trim();
                        }
                    }
                    if (blIsAdmin)
                    {
                        strResultMsg += "A Homestay student application exists.  You may review the information here.<br />";
                    }
                    else
                    {
                        //setVisibilityNoAccess();
                        if (strButtonFace != "Save")
                        {
                           
                            strResultMsg += "<span style='color:#DD0000;'>Your Homestay application has already been submitted. <br />You may view the information and upload more files</span>.</p>";
                            strResultMsg += "<p><span style='color:#0000ff;'><i>";
                            strResultMsg += @"Note: The Homestay Application must be finished in one submission.  
                                            Students will not be able to change or add to the Homestay Application after submission.  Students may return to VIEW their application or to UPLOAD documents after submission.  
                                            Please allow 15 minutes to complete the application.  
                                            Homestay Applications are requested a minimum of 30 days prior to arrival at CCS.  Homestay Placements will be released to students following confirmation of immigration visa approval.";
                            strResultMsg += "</i></span></p>";
                            litAllRequired.Text = "";
                            PhotoLetter.Visible = true;
                            divUpload.Visible = true;
                            divSave.Visible = false;
                            divNextPage.Visible = true;
                        }
                        else
                        {
                            divNextPage.Visible = false;
                        }
                    }
                }
                else
                {
                    strResultMsg = "Please complete the application form below.  The application consists of multiple pages.<br />";
                    litAllRequired.Text = "";
                }
                
                if (dtLatestDOB < dtDOB)
                {
                    blUnderAge = true;
                    hdnAgeLevel.Value = "true";
                    //divNextPage.Visible = true;
                }
                else
                {
                    blUnderAge = false;
                    hdnAgeLevel.Value = "false";
                }

                if (blUnderAge)
                {
                    homestay1617.Visible = true;
                    homestay18.Visible = false;
                }
                else
                {
                    homestay18.Visible = true;
                    homestay1617.Visible = false;
                }
            } //end if dtHSStudent != null
            else
            {
                strResultMsg = "Please complete the application below.  The application consists of multiple pages.<br />";
                litAllRequired.Text = "";
            }
            litResultMsg.Text = "<p>" + hsInfo.strResultMsg + strResultMsg + "</p>";
        }//end if blNoRecord
    }// end btnSearch_Click
    protected void btnCmdContinue_Click(object sender, EventArgs e)
    {
        string strRedirect = "PlacementProfile_L.aspx?ID=" + hdnApplicantID.Value;
        if (blUnderAge)
        {
            strRedirect +=  "&UA=t";
        }
        //Add site
        strRedirect += "&site=" + Request.QueryString["site"];
        Response.Redirect(strRedirect);
    }
    protected void btnCmdReturnAdminHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Homestay/Admin");
    }

    protected void btnCmdResetFields_Click(object sender, EventArgs e)
    {
        resetFields();
        ddlStudents.SelectedValue = "0";
        return;
    }
    protected void btnCmdUpload_Click(object sender, EventArgs e)
    {
        //Reset error message
        eLetter.InnerText = ""; 
        ePhoto.InnerText = "";
        //Upload the letter and photo        
        if (uplLetterFile.HasFile)
        {
            
            if (Utility.ContainsNonEnglishChars(uplLetterFile.FileName))
            {
                Response.Write(uplLetterFile.FileName);
                eLetter.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
                btnCmdUpload.Focus();
                return;
            }
            strFileName = txtFamilyName.Text.Trim().Replace("'", "''") + "-" + intApplicantID + "-Letter-" + uplLetterFile.FileName;
            strFileName = Utility.replaceIllegalChars(strFileName);
            uplLetterFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your letter, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, txtFirstName.Text.Trim().Replace("'", "''") + " " + txtFamilyName.Text.Trim().Replace("'", "''"), strFileName, strUploadURL);
            blLetterSubmitted = true;
            hdnLetterSubmitted.Value = blLetterSubmitted.ToString();
            //Insert bit value into HomestayStudentInfo table
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "letterSubmitted");
        }
        else if (hdnLetterSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blLetterSubmitted = true;
        }
        //strResultMsg += "Letter submitted? " + hdnLetterSubmitted.Value + "<br />";
        if (uplPhotoFile.HasFile)
        {
            if (Utility.ContainsNonEnglishChars(uplPhotoFile.FileName))
            {
                Response.Write(uplPhotoFile.FileName);
                ePhoto.InnerText = "You have selected a file containing invalid characters. Please rename the file with only English characters!";
                btnCmdUpload.Focus();
                return;
            }
            
            strFileName = txtFamilyName.Text.Trim().Replace("'", "''") + "-" + intApplicantID + "-Photo-" + uplPhotoFile.FileName;
            strFileName = Utility.replaceIllegalChars(strFileName);
            uplPhotoFile.SaveAs(strUploadURL + strFileName);
            strResultMsg += "Your photo, <span style=\"color:red;\">" + strFileName + "</span>, was successfully uploaded!<br />";
            //Insert record in UploadedFilesInformation table
            intResult = hsInfo.InsertUploadedFilesInformation(intApplicantID, txtFirstName.Text.Trim().Replace("'", "''") + " " + txtFamilyName.Text.Trim().Replace("'", "''"), strFileName, strUploadURL);
            blPhotoSubmitted = true;
            hdnPhotoSubmitted.Value = blPhotoSubmitted.ToString();
            //Insert bit value into HomestayStudentInfo table
            intResult = hsInfo.UpdateAgreementSubmittedHomestayStudentInfo(intApplicantID, 1, "photoSubmitted");
        }
        else if (hdnPhotoSubmitted.Value.ToLower() == "true")
        {
            //previous submission exists
            blPhotoSubmitted = true;
        }
        litLetterFile.Text = "";
        litPhotoFile.Text = "";
        DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strFileName = dr["fileName"].ToString();
                    string filePath = dr["filePath"].ToString();
                    string uploadServer = "";
                    if (filePath.Contains("ccs-internet"))
                    {
                        uploadServer += @"http:\\apps.spokane.edu\InternetContent\Homestay\";
                    }
                    else
                    {
                        uploadServer += @"http:\\portal.ccs.spokane.edu\InternetContent\Homestay\";
                    }
                    if (strFileName.Contains("Letter"))
                    {
                        //get the file name
                        litLetterFile.Text += "<br /><b>Letter submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                    }
                    if (strFileName.Contains("Photo"))
                    {
                        //Get the file name
                        litPhotoFile.Text += "<br /><b>Photo submitted:</b> " + "<a href=\"" + uploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                    }
                }
            }
        }
        btnCmdUpload.Focus();
    }// end btnCmdUpload

    protected void BackPreviousPage()
    {
        // get the ID and populate the page
        //Set visibility
        personalInformation.Visible = true;
        divMorePersonalInfo.Visible = true;
        divSearchButton.Visible = false;
        if (blUnderAge)
        {
            homestay18.Visible = false;
            homestay1617.Visible = true;
        }
        else
        {
            homestay18.Visible = true;
            homestay1617.Visible = false;
        }
        PhotoLetter.Visible = true;
        divSave.Visible = true;
        if (blIsAdmin)
        {
            divNextPage.Visible = true;
        }

        //Check for existing HomestayStudentInfo record
        DataTable dtHSStudent = hsInfo.GetOneHomestayStudent(intApplicantID);
        if (dtHSStudent != null)
        {
            if (dtHSStudent.Rows.Count > 0)
            {
                blAddMode = false;
                hdnAddMode.Value = blAddMode.ToString();
                //strResultMsg += "blAddMode 1? " + blAddMode + "<br />";
                Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                strUploadServer = "/InternetContent" + appSettings.Settings["HomestayShare"].Value;
                foreach (DataRow drHSStudent in dtHSStudent.Rows)
                {
                    blLetterSubmitted = Convert.ToBoolean(drHSStudent["letterSubmitted"]);
                    hdnLetterSubmitted.Value = blLetterSubmitted.ToString();
                    blPhotoSubmitted = Convert.ToBoolean(drHSStudent["photoSubmitted"]);
                    hdnPhotoSubmitted.Value = blPhotoSubmitted.ToString();
                    if (blLetterSubmitted || blPhotoSubmitted)
                    {
                        litLetterFile.Text = "";
                        litPhotoFile.Text = "";
                        DataTable dt = hsInfo.GetUploadedFiles(intApplicantID);
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    strFileName = dr["fileName"].ToString();
                                    if (strFileName.Contains("Letter"))
                                    {
                                        //get the file name
                                        litLetterFile.Text += "<br /><b>Letter submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                    }
                                    if (strFileName.Contains("Photo"))
                                    {
                                        //Get the file name
                                        litPhotoFile.Text += "<br /><b>Photo submitted:</b> " + "<a href=\"" + strUploadServer + strFileName + "\" target=\"_blank\">" + strFileName + "</a>";
                                    }
                                }
                            }
                        }
                    }
                    //strResultMsg += "Letter submitted: " + Convert.ToBoolean(drHSStudent["letterSubmitted"]) + "<br />";
                    //strResultMsg += "Start Date: " + Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() + "<br />";
                    if (Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString() != "1/1/1900")
                    {
                        try
                        {
                            rdoQuarterStart.SelectedValue = Convert.ToDateTime(drHSStudent["quarterStartDate"]).ToShortDateString();
                        }
                        catch
                        {
                            //do nothing
                        }
                    }
                    txtArrivalDate.Text = Convert.ToDateTime(drHSStudent["arrivalDate"]).ToShortDateString();
                    if (blIsAdmin)
                    {
                        txtDateApplied.Text = Convert.ToDateTime(drHSStudent["dateApplied"]).ToShortDateString();
                    }
                    if (!blUnderAge)
                    {
                        rdoHomestayOption.SelectedValue = drHSStudent["homestayOption"].ToString().Trim();
                    }
                }
                if (blIsAdmin)
                {
                    strResultMsg += "A Homestay student application exists.  You may review the information here.<br />";
                }
                else
                {
                    //setVisibilityNoAccess();
                    if (strButtonFace != "Save")
                    {
                        strResultMsg += "<span style='color:#DD0000;'>Your Homestay application has already been submitted. <br />You may view the information and upload more files</span>.</p>";
                        litAllRequired.Text = "";
                        PhotoLetter.Visible = true;
                        divUpload.Visible = true;
                        divSave.Visible = false;
                        divNextPage.Visible = true;
                    }
                    else
                    {
                        divNextPage.Visible = false;
                    }
                    
                }
            }
            else
            {
                strResultMsg = "Please complete the application form below.  The application consists of multiple pages.<br />";
                litAllRequired.Text = "";
            }
        }
        else
        {
            strResultMsg = "Please complete the application below.  The application consists of multiple pages.<br />";
            litAllRequired.Text = "";
        }
    }// end BackPreviousPage

    
}