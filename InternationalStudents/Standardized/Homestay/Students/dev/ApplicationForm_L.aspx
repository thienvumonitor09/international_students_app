﻿<%@ Page Title="CCS Homestay Program" Language="C#" AutoEventWireup="true" CodeFile="ApplicationForm_L.aspx.cs" Inherits="Homestay_ApplicationForm_L" Debug="true" MasterPageFile="~/HMccs.master" %>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" />
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />         
    <meta http-equiv="X-UA-Compatible" content="IE=9" />    
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>    
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>    
    <script src="/_Upload/iAcceptableFilesOS.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkFiles() {
            //alert("HELLO WORLD!");
            var blCheckOK = false
            if (document.forms[1].txtLetterFile.value != "") {
                blCheckOK = checkAcceptableFiles(document.forms[1].FileUpload1.value, true)
                if (blCheckOK) {
                    if (document.forms[1].txtPhotoFile.value != "") {
                        blCheckOK = checkAcceptableFiles(document.forms[1].txtPhotoFile.value, true)
                        if (blCheckOK) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
            return true;
        }

        function validateRequired() {
            var oForm = document.forms['form1'];
            var arriveValue = oForm.elements['txtArrivalDate'].value;
            if (arriveValue == "") {
                alert("Please enter your estimated arrival date.");
                return false;
            }
        }
    </script>

</asp:Content>



    
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" Runat="Server">
   <script type="text/javascript">
        window.onload = function () {
            if (document.readyState == 'complete') {
                document.getElementById("hidUsingJS").value = "usingJS";
            };
        };
    </script>
     <% var siteName = Request.QueryString["site"].ToUpper(); %>
    <section class="two-column">
    <div class="container">
        <div class="row">
            <section class="col-xs-12">
                <ol class="breadcrumb">
                    <% if(siteName=="SCC") { %>
                    <a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student" class="CMSBreadCrumbsLink">International Student</a> 
                    <% } else if(siteName=="SFCC") { %>
                     <a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student" class="CMSBreadCrumbsLink">International Student</a>
                    <% } else { %>
                     <a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student" class="CMSBreadCrumbsLink">International Student</a>
                    <% } %>&nbsp;>&nbsp;
                    <% if(siteName=="SCC") { %>
                    <a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start" class="CMSBreadCrumbsLink">How Do I Start?</a>
                    <% } else if(siteName=="SFCC") { %>
                     <a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start" class="CMSBreadCrumbsLink">How Do I Start?</a>
                    <% } else { %>
                    <a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start" class="CMSBreadCrumbsLink">How Do I Start?</a>
                    <% } %>
                    &nbsp;>&nbsp;
                    <span class="CMSBreadCrumbsCurrentItem">Homestay Application Form - Students</span>
                </ol>
                <div class="page-title interior"><h1>Homestay Application Form - Students</h1></div>
            </section>
            <section class="left-nav-main col-xs-12 col-sm-8 col-sm-push-4 col-md-9 col-md-push-3">
                <section class="white-bg">
                    <div class="container">
                        <div class="content-block">
                            <div id="global_subpage_image"></div>

                            <!-- Main Form -->
                            
                            <h2>Application Form - Students</h2>
                            <div id="divErr" runat="server" Visible="false">
                            <fieldset id="generalError">
                                <legend></legend>
                                <span style="color:#DD0000"><b><asp:Literal ID="litErr" runat="server"></asp:Literal></b></span>
                            </fieldset>
                            </div>
                            <div id="divAdmin" runat="server">
                            <fieldset id="fldAdmin">
                                <legend>Administration Options</legend>
                                <ul>
                                    <li>Select a Homestay student application from the drop-down list to edit.</li>
                                    <li>Or select <b>Add New Student</b> and use the <b>Personal Information</b> fields below to search for an International Student.</li>
                                </ul><br />
                                <div>
                                    <div id="divResetFields" runat="server" style="float:left;margin-right:30px;" class="button btn-group">
                                        <p>
                                            <!-- <input type="submit" name="btnSubmit" value="Reset Fields" /> -->
                                            <asp:Button ID="btnCmdResetFields" runat="server" Text="Reset Fields" OnClick="btnCmdResetFields_Click" class="FormButton btn btn-primary"/>
                                        </p>
                                    </div>
                                    <div id="divHome" runat="server" style="float:left;" class="button btn-group">
                                        <p>
                                          <!--  <input type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp; -->
                                            <asp:Button ID="btnCmdReturnAdminHome" runat="server" Text="Return to Admin Home Page" OnClick="btnCmdReturnAdminHome_Click" class="FormButton btn btn-primary"/>&nbsp&nbsp;
                                        </p>
                                    </div>
                                </div>
                                <div style="clear:both;"><p>&nbsp;</p></div>
                                <asp:DropDownList ID="ddlStudents" runat="server" AutoPostBack="true"></asp:DropDownList>
                                <div id="divAdminInfo" runat="server">
                                    <p>
                                        <label for="txtDateApplied">Application Date</label><br />
                                        <asp:TextBox ID="txtDateApplied" Enabled="false" runat="server"></asp:TextBox>
                                    </p>
                                    <p>
                                        <label for="txtSID">ctcLink ID</label><br />
                                        <asp:TextBox ID="txtSID" runat="server"></asp:TextBox>
                                        <asp:HiddenField ID="hdnSID" runat="server" />
                                    </p>
                                    <div id="eSID" class="divError" runat="server"></div>
                                    <p>
                                        <label for="rdoWhereStudy">College student plans to attend:</label><br />
                                        <asp:RadioButtonList ID="rdoWhereStudy" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
                                            <asp:ListItem Value="SCC">Spokane Community College</asp:ListItem>
                                            <asp:ListItem Value="SFCC">Spokane Falls Community College</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </p> 
                                </div>
                            </fieldset>
                        </div> <!-- End divAdmin -->
                            <span style="color:#DD0000"><b><asp:Literal ID="litResultMsg" runat="server"></asp:Literal></b></span>
                            <asp:HiddenField ID="hdnApplicantID" runat="server" />
                            <asp:HiddenField ID="hdnAgencyID" runat="server" />
                            <asp:HiddenField ID="hdnAddMode" runat="server" />
                            <asp:HiddenField ID="hdnAgeLevel" runat="server" />
                            <fieldset id="personalInformation" runat="server">
                                <legend>Personal Information</legend>
                                <% var siteName = Request.QueryString["site"].ToUpper(); %>
                                    <p ><em>
                                        The information below must match with your earlier submission in the 
                                        
                                        <% if(siteName=="SCC") { %>
                        <a href="https://portal.ccs.spokane.edu/_netapps/internationalsa/InternationalStudents/Standardized/applicationForm.aspx?site=SCC">International Student Application Form</a>
                        <% } else if(siteName=="SFCC") { %>
                        <a href="https://portal.ccs.spokane.edu/_netapps/internationalsa/InternationalStudents/Standardized/applicationForm.aspx?site=SFCC">International Student Application Form</a>
                        <% } else { %>
                        <a href="https://portal.ccs.spokane.edu/_netapps/internationalsa/InternationalStudents/Standardized/applicationForm.aspx?site=shared">International Student Application Form</a><% } %>
                                        </em>

                                    </p>
                                    <asp:Literal ID="litAllRequired" runat="server" Visible="true" Text="<p style='color:#DD0000;'><em>All fields required</em></p>"></asp:Literal>
                                    <div id="eFamName" class="divError" runat="server"></div>
                                    <p>
                                        <label for="txtFamilyName" class="fieldTitle">Family name<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                        <asp:TextBox Width="500px" ID="txtFamilyName" runat="server" BackColor="ghostwhite" class="required" MaxLength="30" ToolTip="Enter Family Name - required"></asp:TextBox>
                                    </p>
                                    <div id="eFirstName" class="divError" runat="server"></div>
                                    <p>
                                        <label for="txtFirstName" class="fieldTitle">First Name <span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                        <asp:TextBox ID="txtFirstName" runat="server" Width="500px" BackColor="ghostwhite" class="required" MaxLength="20" ToolTip="Enter First Name - required"></asp:TextBox>&nbsp;&nbsp
                                    </p>  
                                    <div id="eDOB" class="divError" runat="server"></div>
                                    <p>
                                        <label for="txtDOB">Date of Birth<span style="color:#DD0000;font-weight:600;"> * </span></label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
                                        <asp:TextBox ID="txtDOB" runat="server" Width="500px" BackColor="ghostwhite" class="required" MaxLength="30" ToolTip="Enter Date of Birth - required"></asp:TextBox>&nbsp;&nbsp;
                                    </p>
                                    <div id="eQtrStart" class="divError" runat="server"></div>
                                    <p>
                                        <label for="rdoQuarterStart" class="fieldTitle">Quarter Start Date <span style="color:#DD0000;font-weight:600;"> * </span> </label><br />
                                        <asp:RadioButtonList ID="rdoQuarterStart" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" ToolTip="Select Quarter Start Date - required">
                                        </asp:RadioButtonList>&nbsp;&nbsp;
                                    </p>
    
                                    <asp:Literal ID="litDuplicateRecords" runat="server"></asp:Literal>
                                    <div id="divMorePersonalInfo" runat="server" visible="false">
                                        <p>
                                            <label for="rdoGender">Gender</label><br />
                                            <asp:RadioButtonList ID="rdoGender" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;" class="intlRadioList" BackColor="ghostwhite">
                                                <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem Value="O">Other</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </p>
                                        <p>
                                            <label for="txtCountry">Home Country</label><br />
                                            <asp:TextBox ID="txtCountry" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
                                        </p>
                                        <p>
                                            <label for="txtPermEmail">Student Email</label><br />
                                            <asp:TextBox ID="txtPermEmail" Width="300px" runat="server" MaxLength="75"></asp:TextBox>
                                        </p>
                                         <div id="eArrivalDate" class="divError" runat="server"></div>
                                        <p>
                                            <label for="txtArrivalDate">Estimated date of arrival:</label>&nbsp;&nbsp;(Format: mm/dd/yyyy)<br />
                                            <asp:TextBox ID="txtArrivalDate" BackColor="ControlLight" runat="server" Width="300px" MaxLength="30"></asp:TextBox>
                                        </p>
       
                                    </div>
                                    <div id="divSearchButton" runat="server" class="button btn-group">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" class="FormButton btn btn-primary" />
                                    </div>
                            </fieldset>
    <fieldset id="recruitingAgent" runat="server">
    <legend>Recruiting Agent</legend>
    <p>Please verify that the contact information for your agent is up to date:</p>
        <p>
            <label for="txtAgency">Agency:</label><br />
            <asp:TextBox ID="txtAgency" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="txtAgentName">Agent Name:</label><br />
            <asp:TextBox ID="txtAgentName" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        </p>
        <p>
            <label for="txtAgentEmail">Agent Email:</label><br />
            <asp:TextBox ID="txtAgentEmail" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
        </p>
        <p>
        <label for="txtAgencyAddress" class="fieldTitle">Agency Street Address</label><br />
        <asp:TextBox ID="txtAgencyAddress" runat="server" Width="400px" MaxLength="75"></asp:TextBox>
        </p>
        <table style="width:100%;">
            <tr>
                <td><label for="txtAgencyCity" class="fieldTitle">Agency City</label></td>
                <td><label for="txtAgencyState" class="fieldTitle">Agency State/Province</label></td>
                <td><label for="txtAgencyZip" class="fieldTitle">Agency Zip/Postal Code</label></td>
            </tr>
            <tr>
                <td><asp:TextBox ID="txtAgencyCity" runat="server" Width="250px" MaxLength="30"></asp:TextBox></td>
                <td><asp:TextBox ID="txtAgencyState" Width="250px" runat="server" MaxLength="30"></asp:TextBox></td>
                <td><asp:TextBox ID="txtAgencyZip" Width="100px" runat="server" MaxLength="15"></asp:TextBox></td>
            </tr>
        </table>
        
        <p>
        <label for="txtAgencyCountry" class="fieldTitle">Agency Country</label><br />
        <asp:TextBox ID="txtAgencyCountry" runat="server"></asp:TextBox>
        </p>

        <p>
        <label for="txtAgencyPhone" id="agencyPhone" class="fieldTitle">Agency Phone</label><br />
        <asp:TextBox ID="txtAgencyPhone" Width="150px" runat="server"></asp:TextBox>
        </p>
    </fieldset>

    <fieldset id="homestay18" runat="server">
    <legend>Homestay Options</legend>
        <ul>
            <li>Homestay Application fee is $200 for Homestay Applicants 18 years old or older.</li>
            <li>The Homestay Application Fee will be charged to your student account when you are assigned a Homestay Family.</li>
            <li>Homestay placement is not guaranteed.  However, if an appropriate family is not located, 
                you will not be charged the Homestay Application Fee. </li>
        </ul>
         <div id="eHomeStayOption" class="divError" runat="server"></div>
        <p>
            <label for="rdoHomestayOption"><span style="color:#DD0000;font-weight:600;"> * </span>Choose One:</label><br /> 
            <asp:RadioButtonList ID="rdoHomestayOption" runat="server" RepeatDirection="Vertical" BackColor="ControlLight" RepeatLayout="Flow" style="width:100%;">
                <asp:ListItem Value="With food">$700.00 per month for Homestay with food </asp:ListItem>
                <asp:ListItem Value="Without food">$400.00 per month for Homestay without food</asp:ListItem>
                <asp:ListItem Value="Either">Either is okay</asp:ListItem>
            </asp:RadioButtonList>   
        </p>
   
    </fieldset>

    <fieldset id="homestay1617" runat="server">

    <legend>Homestay Terms</legend>
        <ul>
            <li>Students who are 16 or 17 years old are required to live with a Homestay Family.</li>
            <li>Homestay Application fee is $300 for 16 or 17 year old Homestay Applicants.</li>
            <li>The Homestay Application Fee will be charged to your student account when you are assigned a Homestay Family.</li>
            <li>Full Homestay is $850 per month, including meals.</li>
        </ul>
    </fieldset>


    
    <fieldset id="PhotoLetter" runat="server">
    <legend>Photo and Letter</legend>
        
        <p>Please upload a photo and short letter about yourself using the file upload below.  You may return later to your application later to upload files.</p>
        <div id="eLetter"  class="divError" runat="server"></div> 
        
        <p>
            Letter:  <asp:FileUpload ID="uplLetterFile" runat="server" Size="70" /> 
            
            <asp:Literal ID="litLetterFile" runat="server"></asp:Literal>
            <asp:HiddenField ID="hdnLetterSubmitted" runat="server" /><br />
        </p>
        <div id="ePhoto"  class="divError" runat="server"></div> 
        <p>
            Photo:  <asp:FileUpload ID="uplPhotoFile" runat="server" Size="70" /> <asp:Literal ID="litPhotoFile" runat="server"></asp:Literal>
            <asp:HiddenField ID="hdnPhotoSubmitted" runat="server" />
        </p>
    </fieldset>
    <div id="divSave" runat="server" visible="false" style="float:left;margin-right:30px;" class="button btn-group">
      <!--  <input type="submit" name="btnSubmit" id="btnSaveOLD" value="Save" onclick="validateRequired()" /> -->
        <asp:Button ID="btnCmdSave" runat="server" Text="Save and Upload" OnClick="btnCmdSave_Click" class="FormButton btn btn-primary"/>
    </div>
    <div id="divUpload" runat="server" visible="false" style="float:left;margin-right:30px;" class="button btn-group">
      <!--  <input type="submit" name="btnSubmit" id="btnUpload" value="Upload" /> -->
        <asp:Button ID="btnCmdUpload" runat="server" Text="Upload" OnClick="btnCmdUpload_Click" class="FormButton btn btn-primary"/>
    </div>
    <div id="divNextPage" runat="server" style="float:left;margin-right:30px;" class="button btn-group">
       <!-- <input type="submit" name="btnSubmit" id="btnContinue" value="Continue"/>&nbsp;&nbsp; -->
        <asp:Button ID="btnCmdContinue" runat="server" Text="Continue" OnClick="btnCmdContinue_Click" class="FormButton btn btn-primary"/>&nbsp;&nbsp;
    </div>
        

                            
                        </div>
                    </div>
                </section>
            </section>
            <section class="col-xs-12 col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9">
                <div class="left-nav">
                    <h2><% if(siteName=="SCC") { %>
                        <a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student">I am an International Student</a>
                        <% } else if(siteName=="SFCC") { %>
                        <a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student">I am an International Student</a>
                        <% } else { %>
                        <a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student">I am an International Student</a><% } %></h2>
                    <ul>
                        <% if(siteName=="SCC") { %>
                        <li><a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start">How Do I Start?</a></li>
                        <% } else if(siteName=="SFCC") { %> 
                        <li><a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start">How Do I Start?</a></li>
                        <% } else { %>
                        <li><a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start">How Do I Start?</a></li>
                        <% } %>
                        <% if(siteName=="SCC") { %>
                        <li><a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Discover-Spokane">Discover Spokane</a></li>
                        <% } else if(siteName=="SFCC") { %>
                        <li><a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Discover-Spokane">Discover Spokane</a></li>
                        <% } else { %>
                        <li><a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Discover-Spokane">Discover Spokane</a></li>
                        <% } %>
                        <% if(siteName=="SCC") { %>
                        <li><a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Contact-Us">Contact Us</a></li>
                         <% } else if(siteName=="SFCC") { %> 
                        <li><a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Contact-Us">Contact Us</a></li>
                        <% } else { %>
                        <li><a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Contact-Us">Contact Us</a></li>
                        <% } %>
                    </ul>
                </div>
            </section>
                
       </div>
    </div>
</section>
    
</asp:Content>
