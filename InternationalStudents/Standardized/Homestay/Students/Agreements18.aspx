﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Agreements18.aspx.cs" Inherits="Homestay_Students_Agreements18" MasterPageFile="~/HMccs.master" %>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" Runat="Server">
    <title>CCS Homestay Program</title>
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" />
    <link href="/Homestay/_css/Homestay.css" rel="stylesheet" type="text/css" />  
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />    
    <link href="/Homestay/_css/Print.css" rel="stylesheet" media="print" type="text/css" />  
    <meta http-equiv="X-UA-Compatible" content="IE=9" />        
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>
    <script type="text/javascript" src="/_js/jquery-1.9.1.min.js"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" Runat="Server">
    <% var siteName = Request.QueryString["site"].ToUpper(); %>
    <section class="two-column">
    <div class="container">
        <div class="row">
            <section class="col-xs-12">
                <ol class="breadcrumb">
                    <% if(siteName=="SCC") { %>
                    <a href="http://scc.spokane.edu/Become-a-Student/I-am-an-International-Student" class="CMSBreadCrumbsLink">International Student</a> 
                    <% } else if(siteName=="SFCC") { %>
                     <a href="http://sfcc.spokane.edu/Become-a-Student/I-am-an-International-Student" class="CMSBreadCrumbsLink">International Student</a>
                    <% } else { %>
                     <a href="http://shared.spokane.edu/Become-a-Student/I-am-an-International-Student" class="CMSBreadCrumbsLink">International Student</a>
                    <% } %>&nbsp;>&nbsp;
                    <% if(siteName=="SCC") { %>
                    <a href="http://scc.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start" class="CMSBreadCrumbsLink">How Do I Start?</a>
                    <% } else if(siteName=="SFCC") { %>
                     <a href="http://sfcc.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start" class="CMSBreadCrumbsLink">How Do I Start?</a>
                    <% } else { %>
                    <a href="http://shared.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start" class="CMSBreadCrumbsLink">How Do I Start?</a>
                    <% } %>
                    &nbsp;>&nbsp;
                    <span class="CMSBreadCrumbsCurrentItem">Application Form</span>
                </ol>
                <div class="page-title interior"><h1>HOMESTAY APPLICATION FORM - STUDENTS</h1></div>
            </section>
            <section class="left-nav-main col-xs-12 col-sm-8 col-sm-push-4 col-md-9 col-md-push-3">
                <section class="white-bg">
                    <div class="container">
                        <div class="content-block">
                            <div id="global_subpage_image"></div>
                            <!-- Main Form -->
                            <h1>CCS Homestay Program</h1>
<asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnUA" runat="server" />
<asp:HiddenField ID="hdnApplicantID" runat="server" />
<div id="div1" runat="server">
    <h3>Student's Rights and Responsibilities Regarding Homestay Program</h3>
    <div class="divError">You must check the box indicating you have read and understand each agreement before you can proceed to the next page.</div>
    <p><b>Students who participate in the Homestay Program are subject to specific rights and
    responsibilities. </b></p>

    <p><b>Failure to comply may result in the student's removal from the Homestay Program. </b>The applicable rights 
    and responsibilities are detailed in CCS Policies and CCS Rules and Regulations: <br />
    <a href="http://catalog.spokane.edu/StudentRights.aspx" target="_blank">CCS Student Rights and Responsibilities</a></p>

    <p><b>Additionally, students must comply with the following requirements:</b></p>

    <ol>
        <li>Respect and follow the guidelines of your homestay family.</li>

        <li>Pay for any damage you or your guests cause to your homestay family home or property. </li>

        <li>Pay all personal bills and obligations. </li>

        <li>Use a prepaid phone card or a personal cell phone; do not charge calls on a homestay phone. </li>

        <li>Abstain from illegal activities at all times during your stay. </li>

        <li>Try to resolve conflict with your homestay family by talking with the 
            Manager of Immigration and Student Success. </li>

        <li>Notify your family and the International Office prior to moving out.  30 days notice is required.</li>

        <li>Communicate to your homestay family if you will not be home for dinner or will be arriving late. </li>

        <li>Pay the homestay fee on the first day of each month.  <b>NO refunds</b> if you leave without 30 days notice.</li>

        <li>Attend classes regularly.</li>
    </ol>
    <p class="noPrint">
        <p class="noPrint"><span style="color:#DD0000;font-style:italic;"> * required field</span><br />
            <div class="divError">You must check the box indicating you have read and understand the student agreement before you can proceed to the next page.</div>
            <div id="eConduct18" class="divError" runat="server"></div>
            <asp:CheckBox ID="chkCCSConduct18" runat="server" Text="I understand what is required of me during my stay with a Homestay Host Family." />
        </p>
    </p>
     <a href="./_PDF/18-Rights And Responsibilities.pdf" target="_blank"  class="button btn-group">
            <button type="button" class="btn btn-primary">Download</button>
     </a>
</div>


<div id="div2" runat="server">
    <h2>Instructions</h2>
<p>Please read, print and sign the following waiver and submit a scanned copy using the File Upload below.</p>
    <div class="divError">You must check the box indicating you have read and understand each agreement before you can proceed to the next page.</div>
<h3>Agreement, Release, Liability Waiver, and Assumption of Risk by Student</h3>

<p>In the State of Washington, individuals who are who are 18 years old or above are generally considered an adult.</p>
<p>&nbsp;</p>
<p>I, _______________________________________________________________<i>(name of student)</i>, 

agree to the following terms and conditions which I understand will constitute a
legally binding Agreement, Release, Liability Waiver, and Assumption of Risk upon my signature.</p>

<p>I understand that a Homestay placement is a placement in a private family home and includes a private room, bathing facilities, and a place to study. 
The Homestay family provides necessary household items such as linens and towels. The student's room is furnished, including a bed/bedding, desk or table, 
lamp dresser, and/or closet. In the <b>Homestay with Food Plan</b>, the Homestay family is responsible for providing food for meals and snacks seven days a week. 
I may also choose a <b>Homestay without Food Plan</b>, and agree to purchase, prepare all my own food and clean up afterwards.</p>

<p>I understand that I'm obliged to follow the rules of the Homestay family. All members of the Homestay family will be proficient in English and will 
only speak English when I/the student, am present. The primary form of transportation for I/the student is the city bus; however, the Homestay family 
may occasionally offer me a ride in their personal vehicles.

<p>I consent to my placement in a Homestay family. Additionally, I understand and agree that there may be modifications in my Homestay placement. 
I, the student, agree to release the STATE OF WASHINGTON, Community Colleges of Spokane (CCS), the Homestay families, and their officers, agents, 
employees, agencies, and departments from any responsibility and liability for changing who will be assigned as the Homestay family prior to or 
after my arrival in the United States, placement in more than one Homestay family or denial of placement if an appropriate Homestay family 
is not available.</p>

<h4>Medical</h4>

<p>I, the student, agree to purchase and maintain adequate medical insurance and personal liability insurance on my personal belongings 
while I attend CCS. The medical insurance shall comply with any requirements mandated by the U.S. Visa Program. I agree and understand 
that CCS assumes no responsibility for verifying the medical insurance standards of coverage if purchased outside the CCS Lewer plan. I 
agree and understand that CCS assumes no responsibility to verify the purchase/adequacy of my personal liability insurance.</p>

<p>I authorize the Homestay family to take whatever action they feel is reasonably warranted to obtain medical and dental attention for 
myself during the period in which I live with the homestay family. This authority and permission includes, but is not limited to the following: 
medical exams, testing, x-rays, anesthetic, surgical and hospital care and treatment, medical procedures, and treatment to be performed 
for me by a licensed health care provider, or hospital when, in the sole discretion of the attending health care provider, such care, 
treatment and procedures are immediately necessary or advisable in the interest of my health and well-being, and it's not advisable to 
take time to contact family members in advance. Under the circumstances set forth above, I elect to not be informed in advance of the 
nature and character of the proposed treatment, its anticipated results, possible alternatives, and risks, complications and anticipated 
benefits involved in the proposed treatment and the alternative forms of the treatment, including non-treatment.</p>

<p>I further grant permission for the health care treatment providers, the homestay family, and CCS to release information regarding me to 
health care providers and facilities who are engaged in providing health care to me, or my child, under these circumstances.</p>

<p>I, the student, agree to be financially responsible for the costs of all medical and dental care and treatment obtained or provided to 
me during my attendance at CCS and stay with the Homestay family. I and my heirs, assigns, or other successors in interest agree to release 
the STATE OF WASHINGTON, Community Colleges of Spokane (CCS), the Homestay families, and their officers, agents, employees, agencies, and 
departments from any responsibility or liability for any medical or dental related costs.</p>

<h4>Additional Costs</h4>

<p>I agree to reimburse my Homestay for repairs/costs incurred by me in the event of damage to the Homestay family home or property.</p>

<h4>Activities, Travel and Trips:</h4>

<p>I understand and acknowledge that there is a risk of injury to me by my participation in trips and activities with my Homestay family.</p>

<p>I further understand that it is voluntary for me to participate in trips and activities with the Homestay family and that CCS and the Homestay 
program do not require my participation. I hereby release the Homestay Family, CCS and the State of Washington, their employees, officers, agents, 
and trustees, and waive any and all right and claims for damages from any and all injures that I may suffer as a result of my participation in 
trips and/or activities.</p>

<p>I agree to hold harmless and indemnify the Homestay Family, CCS and the State of Washington, their employees, officers, agents, and trustees 
for any action, claim, or proceeding initiated as a result of any injury suffered by me or any third party through my participation in any trips 
and/or activities. Typically these activities may include, but are not limited to: transport in Homestay family member vehicles to shop, commute 
to/from school, dining out, community/religious events, family recreational, vacation activities, etc.</p>
<p>&nbsp;</p> 

Student's signature: ___________________________________________________________
<p>&nbsp;</p>	

Date: _________________
<p>&nbsp;</p>

Parent's signature: __________________________________________________  (If student is 17 years old)
<p>&nbsp;</p>	

Date: _________________
<p>&nbsp;</p>	
    <div id="divUpload" runat="server" class="noPrint" style="border: 1px solid Gray;width:30%;padding:15px;">
        <div id="eWaiveLiability18File"  class="divError" runat="server"></div> 
        <p>
            <label for="uplWaiveLiability18File">Document Upload:</label>&nbsp;
            <asp:FileUpload ID="uplWaiveLiability18File" runat="server" Size="70" /> <asp:Literal ID="litWaiveLiability18File" runat="server"></asp:Literal>
            <asp:HiddenField ID="hdnWaiveLiability18Submitted" runat="server" /><br />
         <!--   <input type="submit" name="btnSubmit" id="Submit2" value="Upload Waiver" /> -->
            <asp:Button ID="btnCmdUploadWaiver" runat="server" Text="Upload Waiver" OnClick="btnCmdUploadWaiver_Click" />
        </p>
        <div id="eUploadWaiver" runat="server" class="divError"></div>
    </div>
    <div class="noPrint">
        <p class="noPrint"><span style="color:#DD0000;font-style:italic;"> * required field</span><br />
        <div class="divError">You must check the box indicating you have read and understand the student agreement before you can proceed to the next page.</div>
        <div id="eWaiveLiability18" class="divError" runat="server"></div>
        <p>
            <asp:CheckBox ID="chkWaiveLiability18" runat="server" Text="I acknowledge that I have
            read this waiver and agreement." /><label for="chkWaiveLiability18">&nbsp;I understand what is required of me during my stay with a Homestay Host Family.</label>
        </p>
         <a href="./_PDF/Agreement-Release-Liability Waiver and Assumption of Risk by Student.pdf" target="_blank"  class="button btn-group">
            <button type="button" class="btn btn-primary">Download</button>
     </a>
    </div>
</div>
<div id="divConfirmation" runat="server">
    <h2>Thank your for your interest in the CCS Homestay Program!</h2>
    <p>Someone from the Homestay Office will be in contact with you soon.</p>
    <p>In the meantime, you can browse the information below to prepare for your stay:</p>
    <ul>
        <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Families/_PDF/2018-19-Lewermark.pdf" target="_blank">Student Insurance Plan 2018-19</a></li>
        <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Families/_PDF/MorneauShepell-studentCounseling.pdf" target="_blank">Student Counseling and Support Service-Morneau Shepell</a></li>
        <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Students/_PDF/Homestay-Student-brochure.pdf" target="_blank">Student brochure</a></li>
        <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Students/_PDF/Homestay-Rooms.pdf" target="_blank">Typical Homestay Rooms</a></li>
         <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Students/_PDF/Guidelines/HouseGuidelines_English.pdf" target="_blank" title="House Guidelines">House Guidelines-English</a></li>
        <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Students/_PDF/Guidelines/HouseGuidelines_Arabic.pdf" target="_blank" title="House Guidelines">House Guidelines-Arabic</a></li>
        <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Students/_PDF/Guidelines/HouseGuidelines_Chinese.pdf" target="_blank" title="House Guidelines">House Guidelines-Chinese</a></li>
        <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Students/_PDF/Guidelines/HouseGuidelines_Japanese.pdf" target="_blank" title="House Guidelines">House Guidelines-Japanese</a></li>
         <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Students/_PDF/Guidelines/HouseGuidelines_Korean.pdf" target="_blank" title="House Guidelines">House Guidelines-Korean</a></li>
        <li><a href="/_netapps/internationalsa/InternationalStudents/Standardized/Homestay/Students/_PDF/Guidelines/HouseGuidelines_Vietnamese.pdf" target="_blank" title="House Guidelines">House Guidelines-Vietnamese</a></li>

    </ul>
    <p>&nbsp;</p>
    <asp:Literal ID="litMissingUploads" runat="server"></asp:Literal>
</div>
<p>&nbsp;</p>
<div id="divButtons" class="noPrint" runat="server">
    <div id="divSaveContinue" runat="server" style="float:left;" class="button btn-group">
        <input type="submit" name="btnSubmit" value="Save and Continue" class="FormButton btn btn-primary"/>&nbsp;&nbsp;
      <!--  <asp:Button ID="btnCmdSaveAndContinue" runat="server" Text="Save and Continue" OnClick="btnCmdSaveAndContinue_Click" />&nbsp;&nbsp; --> 
    </div>
    <div id="divSaveAgreement" runat="server" style="float:left;" visible="false" class="button btn-group">
        <input type="submit" name="btnSubmit" value="Save Agreement and Continue" class="FormButton btn btn-primary"/>&nbsp;&nbsp;
    </div>
    <div id="divPrevious" runat="server" style="float:left;" class="button btn-group">
        <input type="submit" name="btnSubmit" value="Back" class="FormButton btn btn-primary"/>&nbsp;&nbsp;
    </div>
    <div id="divSave" runat="server" style="float:left;" class="button btn-group">
      <!--  <input type="submit" name="btnSubmit" value="Save" />&nbsp;&nbsp; -->
        <asp:Button ID="btnCmdSave" runat="server" Text="Save" OnClick="btnCmdSave_Click" class="FormButton btn btn-primary"/>&nbsp;&nbsp;
    </div>
    <!-- 
    <div id="divPrint" runat="server" style="float:left;margin-left:30px;" class="button btn-group">
        <input type="button" name="btnPrint" onclick="javascript: window.print();" value="Print" class="FormButton btn btn-primary"/>
    </div>
        -->
    <div id="divHomepage" runat="server" class="noPrint" style="float:left;margin-left:30px;" visible="false">
        <input style="float:left;margin-left:30px;" type="submit" name="btnSubmit" id="btnHome" value="Return to Admin Home Page"/>&nbsp;&nbsp;
    </div>
</div>
    <asp:HiddenField ID="hdnPartNo" runat="server" />
                        </div>
                    </div>
                </section>
            </section>
            <section class="col-xs-12 col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9">
                <div class="left-nav">
                    <h2><% if(siteName=="SCC") { %>
                        <a href="http://scc.spokane.edu/Become-a-Student/I-am-an-International-Student">I am an International Student</a>
                        <% } else if(siteName=="SFCC") { %>
                        <a href="http://sfcc.spokane.edu/Become-a-Student/I-am-an-International-Student">I am an International Student</a>
                        <% } else { %>
                        <a href="http://shared.spokane.edu/Become-a-Student/I-am-an-International-Student">I am an International Student</a><% } %></h2>
                    <ul>
                        <% if(siteName=="SCC") { %>
                        <li><a href="http://scc.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start">How Do I Start?</a></li>
                        <% } else if(siteName=="SFCC") { %> 
                        <li><a href="http://sfcc.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start">How Do I Start?</a></li>
                        <% } else { %>
                        <li><a href="http://shared.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start">How Do I Start?</a></li>
                        <% } %>
                        <% if(siteName=="SCC") { %>
                        <li><a href="http://scc.spokane.edu/Become-a-Student/I-am-an-International-Student/Discover-Spokane">Discover Spokane</a></li>
                        <% } else if(siteName=="SFCC") { %>
                        <li><a href="http://sfcc.spokane.edu/Become-a-Student/I-am-an-International-Student/Discover-Spokane">Discover Spokane</a></li>
                        <% } else { %>
                        <li><a href="http://shared.spokane.edu/Become-a-Student/I-am-an-International-Student/Discover-Spokane">Discover Spokane</a></li>
                        <% } %>
                        <% if(siteName=="SCC") { %>
                        <li><a href="http://scc.spokane.edu/Become-a-Student/I-am-an-International-Student/Contact-Us">Contact Us</a></li>
                         <% } else if(siteName=="SFCC") { %> 
                        <li><a href="http://sfcc.spokane.edu/Become-a-Student/I-am-an-International-Student/Contact-Us">Contact Us</a></li>
                        <% } else { %>
                        <li><a href="http://shared.spokane.edu/Become-a-Student/I-am-an-International-Student/Contact-Us">Contact Us</a></li>
                        <% } %>
                    </ul>
                </div>
            </section>
        </div>
    </div>
 </section>
</asp:Content>



