﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PlacementProfile_L.aspx.cs" Inherits="Homestay_PlacementProfile_L" MasterPageFile="~/HMccs.master"%>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link id="Link1" href="/_css/ccs.css" rel="stylesheet"  type="text/css" /> 
    <link href="/_css/Forms.css" rel="stylesheet" type="text/css" />         
    <meta http-equiv="X-UA-Compatible" content="IE=9" />        
    <script type="text/javascript" src="../_js/pushToHttps.js"></script>
    <script type="text/javascript" src="../_js/jquery-1.9.1.min.js"></script>
    <link href="../_css/Homestay.css" rel="stylesheet" type="text/css" />
    <script src="../_js/jquery.mask.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dobSibling1').mask('00/00/0000');
            $('.dobSibling2').mask('00/00/0000');
            $('.dobSibling3').mask('00/00/0000');
            $('.dobSibling4').mask('00/00/0000');
        });
    </script>
    <script type="text/javascript">
        window.onload = function () {
            if (document.readyState == 'complete') {
                document.getElementById("hidUsingJS").value = "usingJS";
            };
        };
    </script>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" Runat="Server">
    
    <% var siteName = Request.QueryString["site"].ToUpper(); %>
    <section class="two-column">
    <div class="container">
        <div class="row">
            <section class="col-xs-12">
                <ol class="breadcrumb">
                    <% if(siteName=="SCC") { %>
                    <a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student" class="CMSBreadCrumbsLink">International Student</a> 
                    <% } else if(siteName=="SFCC") { %>
                     <a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student" class="CMSBreadCrumbsLink">International Student</a>
                    <% } else { %>
                     <a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student" class="CMSBreadCrumbsLink">International Student</a>
                    <% } %>&nbsp;>&nbsp;
                    <% if(siteName=="SCC") { %>
                    <a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start" class="CMSBreadCrumbsLink">How Do I Start?</a>
                    <% } else if(siteName=="SFCC") { %>
                     <a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start" class="CMSBreadCrumbsLink">How Do I Start?</a>
                    <% } else { %>
                    <a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start" class="CMSBreadCrumbsLink">How Do I Start?</a>
                    <% } %>
                    &nbsp;>&nbsp;
                    <span class="CMSBreadCrumbsCurrentItem">Application Form</span>
                </ol>
                <div class="page-title interior"><h1>International Student Application Form</h1></div>
            </section>

            <section class="left-nav-main col-xs-12 col-sm-8 col-sm-push-4 col-md-9 col-md-push-3">
                <section class="white-bg">
                    <div class="container">
                        <div class="content-block">
                            <div id="global_subpage_image"></div>
                            <h1>CCS Homestay Program</h1>

                            <h2>Student Profile</h2>
                            <asp:Literal ID="litResultMsg" runat="server"></asp:Literal>
                                <div class="divError">* indicates required field</div>
                            
                            <fieldset id="fldParents" runat="server">
                            <legend>Legal Guardians</legend>
                                <div id="eFFamName" class="divError" runat="server"></div>
                                <p>
                                    <label for="familyNameFather">Guardian 1's Family  Name <span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:TextBox ID="familyNameFather" runat="server" Width="500px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                <div id="eFFirstName" class="divError" runat="server"></div>
                                <p>
                                    <label for="firstNameFather">Guardian 1's First Name<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:TextBox ID="firstNameFather" runat="server" Width="500px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                <div id="eFathOcc" class="divError" runat="server"></div>
                                 <p>
                                    <label for="occupationFather">Guardian 1's Occupation<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:TextBox ID="occupationFather" runat="server" Width="500px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                    <asp:HiddenField ID="hdnFatherID" runat="server" />
                                </p>
                                <div id="eMFamName" class="divError" runat="server"></div>
                                <p>
                                    <label for="familyNameMother">Guardian 2's Family Name</label><br />
                                    <asp:TextBox ID="familyNameMother" runat="server" Width="500px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                <div id="eMFirstName" class="divError" runat="server"></div>
                                <p>
                                    <label for="firstNameMother">Guardian 2's First Name</label><br />
                                    <asp:TextBox ID="firstNameMother" runat="server" Width="500px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                <div id="eMomOcc" class="divError" runat="server"></div>
                                <p>
                                    <label for="occupationMother">Guardian 2's Occupation</label><br />
                                    <asp:TextBox ID="occupationMother" runat="server" Width="500px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                    <asp:HiddenField ID="hdnMotherID" runat="server" />
                                </p>
    
                            </fieldset>
                            <br />
                            <fieldset id="fldSiblings" runat="server">
                            <legend>Siblings</legend>
                            <div id="accordion">
                                <h3 class="accordion accordion-toggle">Sibling 1</h3>
                                <div class="panel accordion-content">
                                    <!--
                                <p>
                                    <label for="familyNameSibling1">Family Name</label><br />
                                    <asp:TextBox ID="familyNameSibling1" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="firstNameSibling1">First Name</label><br />
                                    <asp:TextBox ID="firstNameSibling1" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                    <div id="eDOBSib1" class="divError" runat="server"></div>
                                <p>
                                    <label for="dobSibling1">Date of Birth</label> (MM/DD/YYYY)<br />
                                    <asp:TextBox ID="dobSibling1" runat="server" Width="300px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
                                </p>
                                    -->
                                <p>
                                    <label for="ageSibling1">Age</label><br />
                                    <asp:TextBox ID="ageSibling1" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="genderSibling1">Gender</label><br />
                                    <asp:RadioButtonList ID="genderSibling1" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="O">Other</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:HiddenField ID="hdnSibling1ID" runat="server" />
                                </p>
                               </div>
                               <h3 class="accordion accordion-toggle">Sibling 2</h3>
                               <div class="panel accordion-content">
                                   <!--
                                <p>
                                    <label for="familyNameSibling2">Family Name</label><br />
                                    <asp:TextBox ID="familyNameSibling2" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="firstNameSibling2">First Name</label><br />
                                    <asp:TextBox ID="firstNameSibling2" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                   <div id="eDOBSib2" class="divError" runat="server"></div>
                                <p>
                                    <label for="dobSibling2">Date of Birth</label> (MM/DD/YYYY)<br />
                                    <asp:TextBox ID="dobSibling2" runat="server" Width="300px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
                                </p>
                                   -->
                                <p>
                                    <label for="ageSibling2">Age</label><br />
                                    <asp:TextBox ID="ageSibling2" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="genderSibling2">Gender</label><br />
                                    <asp:RadioButtonList ID="genderSibling2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="O">Other</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:HiddenField ID="hdnSibling2ID" runat="server" />
                                </p>
                               </div>
                               <h3 class="accordion accordion-toggle">Sibling 3</h3>
                               <div class="panel accordion-content">
                                   <!--
                                <p>
                                    <label for="familyNameSibling3">Family Name</label><br />
                                    <asp:TextBox ID="familyNameSibling3" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="firstNameSibling3">First Name</label><br />
                                    <asp:TextBox ID="firstNameSibling3" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                   <div id="eDOBSib3" class="divError" runat="server"></div>
                                <p>
                                    <label for="dobSibling3">Date of Birth</label> (MM/DD/YYYY)<br />
                                    <asp:TextBox ID="dobSibling3" runat="server" Width="300px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
                                </p>
                                   -->
                                <p>
                                    <label for="ageSibling3">Age</label><br />
                                    <asp:TextBox ID="ageSibling3" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="genderSibling3">Gender</label><br />
                                    <asp:RadioButtonList ID="genderSibling3" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="O">Other</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:HiddenField ID="hdnSibling3ID" runat="server" />
                                </p>
                               </div>
                               <h3 class="accordion accordion-toggle">Sibling 4</h3>
                               <div class="panel accordion-content">
                                   <!--
                                <p>
                                    <label for="familyNameSibling4">Family Name</label><br />
                                    <asp:TextBox ID="familyNameSibling4" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="familyNameSibling4">First Name</label><br />
                                    <asp:TextBox ID="firstNameSibling4" runat="server" Width="300px" BackColor="ControlLight" MaxLength="30"></asp:TextBox>
                                </p>
                                   <div id="eDOBSib4" class="divError" runat="server"></div>
                                <p>
                                    <label for="dobSibling4">Date of Birth</label> (MM/DD/YYYY)<br />
                                    <asp:TextBox ID="dobSibling4" runat="server" Width="300px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
                                </p>
                                   -->
                                <p>
                                    <label for="ageSibling4">Age</label><br />
                                    <asp:TextBox ID="ageSibling4" runat="server" Width="100px" BackColor="ControlLight" MaxLength="15"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="genderSibling4">Gender</label><br />
                                    <asp:RadioButtonList ID="genderSibling4" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="M">Male&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="F">Female&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="O">Other</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:HiddenField ID="hdnSibling4ID" runat="server" />
                                </p>
                                </div>
                            </div>

                            </fieldset>
                            <br />
                            <fieldset id="fldPreferences" runat="server">
                            <legend>Preferences</legend>
                                <p><u>While all student preferences will be considered, CCS cannot guarantee they will all be satisfied.</u></p>
                                <div id="eSmallChildren" class="divError" runat="server"></div>
                                <p>
                                    <label for="rdoSmallChildren">I would like to live in a home with:<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:RadioButtonList ID="rdoSmallChildren" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="Young children">Young children&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Older children">Older children&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="No children">No children &nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Children no preference">No Preference</asp:ListItem>
                                    </asp:RadioButtonList>
                                </p>
                                <div id="eTravel" class="divError" runat="server"></div>
                                <p>
                                    <label for="rdoTraveledOutsideCountry">Have you traveled outside your country before?<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:RadioButtonList ID="rdoTraveledOutsideCountry" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="Traveled outside country">Yes&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Not traveled outside country">No&nbsp;&nbsp;</asp:ListItem>
                                    </asp:RadioButtonList>
                                </p>
    
                                <p>
                                    <label for="traveledWhere">Which countries?</label><br />
                                    <asp:TextBox ID="traveledWhere" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
                                </p>
                                <div id="eSmoker" class="divError" runat="server"></div>
                                <p>
                                    <label for="rdoSmoker">Do you smoke?<span style="color:#DD0000;font-weight:600;"> * </span></label><br /> <span id="divApartment" runat="server">(Most host families do not allow smoking. CCS Homestay team recommends pursuing 
                                    an apartment if you are a smoker.)</span><br />
                                    <asp:RadioButtonList ID="rdoSmoker" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="Smoker">Yes&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Occasional smoker">Sometimes&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Nonsmoker">No&nbsp;&nbsp;</asp:ListItem>
                                    </asp:RadioButtonList>
                                </p>
    
                                <p>
                                    <label for="smokingHabits">Explain your smoking habits:</label><br />
                                    <asp:TextBox ID="smokingHabits" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
                                </p>
                                <div id="eOtherSmokerOK" class="divError" runat="server"></div>
                                <p>
                                    <label for="rdoOtherSmokerOK">Is it okay if there are smokers in your homestay family?<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:RadioButtonList ID="rdoOtherSmokerOK" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="Other smoker OK">Yes&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Other smoker not OK">No&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Other smoker no preference">No Preference</asp:ListItem>
                                    </asp:RadioButtonList>
                                </p>
    
                                <p>
                                    What hobbies, sports or other activities do you enjoy?<br />
                                    <asp:CheckBox ID="MusicalInstrument" runat="server" Text="Play a musical instrument" /><br />
                                    <asp:CheckBox ID="Art" runat="server" Text="Drawing/painting" /><br />
                                    <asp:CheckBox ID="TeamSports" runat="server" Text="Team sports" /><br />
                                    <asp:CheckBox ID="IndividualSports" runat="server" Text="Individual sports" /><br />
                                    <asp:CheckBox ID="ListenMusic" runat="server" Text="Listen to music" /><br />
                                    <asp:CheckBox ID="Drama" runat="server" Text="Drama" /><br />
                                    <asp:CheckBox ID="WatchMovies" runat="server" Text="Watch movies" /><br />
                                    <asp:CheckBox ID="Singing" runat="server" Text="Singing" /><br />
                                    <asp:CheckBox ID="Shopping" runat="server" Text="Shopping" /><br />
                                    <asp:CheckBox ID="ReadBooks" runat="server" Text="Read books" /><br />
                                    <asp:CheckBox ID="Outdoors" runat="server" Text="Hiking/camping/outdoors" /><br />
                                    <asp:CheckBox ID="Cooking" runat="server" Text="Cooking" /><br />
                                    <asp:CheckBox ID="Photography" runat="server" Text="Photography" /><br />
                                    <asp:CheckBox ID="Gaming" runat="server" Text="Gaming" />
                                </p>
                                <p>
                                    <label for="activitiesEnjoyed">Other hobbies/activities you enjoy:</label><br />
                                    <asp:TextBox ID="activitiesEnjoyed" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
                                </p>
                                <div id="eInteraction" class="divError" runat="server"></div>
                                <p>
                                    <label for="rdoInteraction">How much interaction and conversation with your homestay family do you prefer?<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:RadioButtonList ID="rdoInteraction" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="Interaction every day">Every Day&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Interaction frequent">Frequent, but not every day</asp:ListItem>
                                        <asp:ListItem Value="Interaction minimal">I'm independent and minimal conversation is okay</asp:ListItem>
                                    </asp:RadioButtonList>
                                </p>
                                <div id="eHomeEnvironment" class="divError" runat="server"></div>
                                <p>
                                    <label for="rdoHomeEnvironment">I would like to live in a home that is more:<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:RadioButtonList ID="rdoHomeEnvironment" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="Home environment quiet">Quiet&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Home environment active">Busy and Active&nbsp;&nbsp;</asp:ListItem>
                                    </asp:RadioButtonList>
                                </p>
     
                                <p>
                                    <label for="homeEnvironmentPreferences">Explain:</label><br />
                                    <asp:TextBox ID="homeEnvironmentPreferences" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
                                </p>
                                <p>
                                    Please indicate your dietary needs:<br />
                                    <asp:CheckBox ID="Vegetarian" runat="server" Text="Vegetarian" /><br />
                                    <asp:CheckBox ID="GlutenFree" runat="server" Text="Gluten free" /><br />
                                    <asp:CheckBox ID="DairyFree" runat="server" Text="Dairy free" /><br />
                                    <asp:CheckBox ID="OtherFoodAllergy" runat="server" Text="Other food allergy" /><br />
                                    <asp:CheckBox ID="Halal" runat="server" Text="Halal" />  (Food prepared as prescribed by Muslim law)<br />
                                    <asp:CheckBox ID="Kosher" runat="server" Text="Kosher" /><br />
                                    <asp:CheckBox ID="NoSpecialDiet" runat="server" Text="I have no special dietary needs" /><br />
                                </p>
                                </fieldset>
                            <br />
                            <fieldset id="fldConcerns" runat="server">
                            <legend>Other Concerns</legend>
                                <p>
                                    <label for="allergies">Do you have any allergies to food, drugs, animals or anything else?</label><br />
                                    <asp:TextBox ID="allergies" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
                                </p>
                                <p>
                                    <label for="healthConditions">Do you have any health conditions that would prevent you from participation in normal and regular physical activities?</label><br />
                                    <asp:TextBox ID="healthConditions" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
                                </p>
                                <div id="eHealthStatus" class="divError" runat="server"></div>
                                <p>
                                    <label for="rdoHealthStatus">How would you describe your present condition of health?<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:RadioButtonList ID="rdoHealthStatus" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="Health excellent">Excellent&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Health average">Average&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="Health poor">Poor</asp:ListItem>
                                    </asp:RadioButtonList>
                                </p>
    
                                <p>
                                    <label for="rdoDriveCar">Are you planning to drive a car as soon as you arrive?<span style="color:#DD0000;font-weight:600;"> * </span></label><br />
                                    <asp:RadioButtonList ID="rdoDriveCar" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="width:100%;">
                                        <asp:ListItem Value="1">Yes&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="0">No&nbsp;&nbsp;</asp:ListItem>
                                    </asp:RadioButtonList>
                                </p>
                                <p>
                                    <label for="anythingElse">Is there anything else you'd like us to know about you?</label><br />
                                    <asp:TextBox ID="anythingElse" runat="server" Width="500px" BackColor="ControlLight" MaxLength="100"></asp:TextBox>
                                </p>

                            </fieldset>
                            <br />
                            <div id="divWaiver" runat="server">

                            <h2></h2>
                            </div>
                            <div id="divButtons" runat="server" >
                                <div id="btnSave" runat="server" style="float:left;" class="button btn-group">
                                    <!-- <input type="submit" name="btnSubmit" value="Save"/>&nbsp;&nbsp; -->
                                    <asp:Button ID="btnCmdSave" runat="server" Text="Save" OnClick="btnCmdSave_Click" class="FormButton btn btn-primary" />&nbsp;&nbsp;
                                </div>
                                <div id="divUpdate" runat="server" visible="false" style="float:left;" class="button btn-group">
                                  <!--  <input type="submit" name="btnSubmit" id="btnUpdate" value="Save Changes" /> -->
                                    <asp:Button ID="btnCmdSaveChanges" runat="server" Text="Save Changes" OnClick="btnCmdSave_Click" class="FormButton btn btn-primary" />
                                </div>
                                <div id="divNextPage" runat="server" style="float:left;margin-left:30px;" class="button btn-group">
                                  <!--  <input type="submit" name="btnSubmit" value="Continue"/>&nbsp;&nbsp; -->
                                    <asp:Button ID="btnCmdContinue" runat="server" Text="Continue" OnClick="btnCmdContinue_Click" class="FormButton btn btn-primary"/>&nbsp;&nbsp;
                                </div>
                                <div id="btnPrevious" runat="server" style="float:left;margin-left:30px;" class="button btn-group">
                                   <!-- <input type="submit" name="btnSubmit" value="Back" /> -->
                                    <asp:Button ID="btnCmdBack" runat="server" Text="Back" OnClick="btnCmdBack_Click"  class="FormButton btn btn-primary"/>
                                </div>
                            </div>
                            <asp:HiddenField ID="hdnApplicantID" runat="server" />
                            <script type="text/javascript">

                                $(function () {
                                    //To display "+" or "-" for accordion 
                                    $(".accordion").each(function (index, item) {
                                        item.addEventListener("click", function () {
                                            this.classList.toggle("active");
                                        });
                                    });
                                    /*
                                    var acc = document.getElementsByClassName("accordion");
                                    for (var i = 0; i < acc.length; i++) {
                                        acc[i].addEventListener("click", function () {
                                            this.classList.toggle("active");
                                        });
                                    }
                                    */
                                    $('#accordion').find('.accordion-toggle').each(function () {
                                        //$(this).next().slideToggle('fast');
                                        //if the first input field has data, it will expand
                                        if ($(this).next().find("input[type=text]").first().val() != "") {
                                            $(this).next().slideToggle('fast');
                                        }

                                    });
                                    $('#accordion').find('.accordion-toggle').click(function () {
                                        //Expand or collapse this panel
                                        $(this).next().slideToggle('fast');
                                        //$("#sib1").slideToggle('fast');
                                        //Hide the other panels
                                        //$(".accordion-content").not($(this).next()).slideUp('fast');

                                    });
                                });
</script>
                        </div>
                    </div>
                </section>
            </section>
            <section class="col-xs-12 col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9">
            <div class="left-nav">
                <h2><% if(siteName=="SCC") { %>
                    <a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student">I am an International Student</a>
                    <% } else if(siteName=="SFCC") { %>
                    <a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student">I am an International Student</a>
                    <% } else { %>
                    <a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student">I am an International Student</a><% } %></h2>
                <ul>
                    <% if(siteName=="SCC") { %>
                    <li><a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start">How Do I Start?</a></li>
                    <% } else if(siteName=="SFCC") { %> 
                    <li><a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start">How Do I Start?</a></li>
                    <% } else { %>
                    <li><a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/How-Do-I-Start">How Do I Start?</a></li>
                    <% } %>
                    <% if(siteName=="SCC") { %>
                    <li><a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Discover-Spokane">Discover Spokane</a></li>
                    <% } else if(siteName=="SFCC") { %>
                    <li><a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Discover-Spokane">Discover Spokane</a></li>
                    <% } else { %>
                    <li><a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Discover-Spokane">Discover Spokane</a></li>
                    <% } %>
                    <% if(siteName=="SCC") { %>
                    <li><a href="http://sccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Contact-Us">Contact Us</a></li>
                        <% } else if(siteName=="SFCC") { %> 
                    <li><a href="http://sfccbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Contact-Us">Contact Us</a></li>
                    <% } else { %>
                    <li><a href="http://sharedbeta.spokane.edu/Become-a-Student/I-am-an-International-Student/Contact-Us">Contact Us</a></li>
                    <% } %>
                </ul>
            </div>
        </section>
        </div>
    </div>
</asp:Content>
