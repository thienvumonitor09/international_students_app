﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Homestay_PlacementProfile_L : System.Web.UI.Page
{
    String strButtonFace = "";
    String applicantID = "";
    String strResultMsg = "";
    Boolean blUnderAge = false;
    Boolean blAddMode = true;
    //Preferences
    Int32 intResult = 0;
    String strBlank = "";
    Int32 intApplicantID = 0;
    String strRelationship = "";
    
    Boolean blIsAdmin = false;
    Boolean isValidDate = false;
    DateTime outDate;               // used in try/catch to validate dob on form
    Boolean dateSuccess = false;    // used in try/catch to validate dob on form

    string strTravel;

    Homestay hsInfo = new Homestay();
    protected void Page_Load(object sender, EventArgs e)
    {
        String strLogonUser = Request.ServerVariables["LOGON_USER"];
        if (strLogonUser != "" && strLogonUser != null) { blIsAdmin = true; }

        if (Request.Form["btnSubmit"] != null)
        {
            strButtonFace = Request.Form["btnSubmit"].ToString();
        }
        if (Request.QueryString["ID"] != null)
        {
            //page load
            applicantID = Request.QueryString["ID"].ToString();
            hdnApplicantID.Value = applicantID;
        }
        if (applicantID == "")
        {
            //after postback
            applicantID = hdnApplicantID.Value;
        }
        //strResultMsg += "applicantID: " + applicantID + "<br />";
        if (applicantID != "0" && applicantID != "" && applicantID != null)
        {
            intApplicantID = Convert.ToInt32(applicantID);
        }
        if (intApplicantID <= 0)
        {
            strResultMsg += "Unable to Update Homestay Student Info: applicant ID not set.<br />";
            /*
            switch (strButtonFace)
            {
                case "Save":
                case "Save Changes":
                    
                    break;
                case "Continue":
                    if (blUnderAge)
                    {
                        Response.Redirect("AgreementsUA.aspx?ID=" + hdnApplicantID.Value);
                    }
                    else
                    {
                        Response.Redirect("Agreements18.aspx?ID=" + hdnApplicantID.Value);
                    }
                    break;
                case "Back":
                    Response.Redirect("ApplicationForm.aspx?ID=" + hdnApplicantID.Value);
                    break;
            }
             */
            
        }

        if (!IsPostBack)
        {
            //LOAD PAGE WITH EXISTING RECORD, IF ANY
            DataTable dtInfo = hsInfo.GetOneHomestayStudent(intApplicantID);
            if (dtInfo != null)
            {
                if (dtInfo.Rows.Count > 0)
                {
                    traveledWhere.Text = dtInfo.Rows[0]["traveledOutsideCountry"].ToString().Trim();
                    smokingHabits.Text = dtInfo.Rows[0]["smokingHabits"].ToString().Trim();
                    activitiesEnjoyed.Text = dtInfo.Rows[0]["activitiesEnjoyed"].ToString().Trim();
                    allergies.Text = dtInfo.Rows[0]["allergies"].ToString().Trim();
                    homeEnvironmentPreferences.Text = dtInfo.Rows[0]["homeEnvironmentPreferences"].ToString().Trim();
                    healthConditions.Text = dtInfo.Rows[0]["healthConditions"].ToString().Trim();
                    rdoHealthStatus.SelectedValue = dtInfo.Rows[0]["healthStatus"].ToString().Trim();
                    Boolean blDriveCar = Convert.ToBoolean(dtInfo.Rows[0]["driveCar"]);
                    if (blDriveCar)
                    {
                        rdoDriveCar.SelectedValue = "1";
                    }
                    else
                    {
                        rdoDriveCar.SelectedValue = "0";
                    }
                    anythingElse.Text = dtInfo.Rows[0]["anythingElse"].ToString().Trim();
                }
            }
            DataTable dtRelatives = hsInfo.GetHomestayRelatives(intApplicantID, 0);
            if (dtRelatives != null)
            {
                if (dtRelatives.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in dtRelatives.Rows)
                    {
                        blAddMode = false;
                        strRelationship = dtRow["relationship"].ToString().Trim();
                        //strResultMsg += "Relationship: " + strRelationship + "<br />";
                        switch (strRelationship)
                        {
                            case "Father":
                                familyNameFather.Text = dtRow["familyName"].ToString().Trim();
                                firstNameFather.Text = dtRow["firstName"].ToString().Trim();
                                occupationFather.Text = dtRow["occupation"].ToString().Trim();
                                hdnFatherID.Value = dtRow["id"].ToString();
                                break;
                            case "Mother":
                                familyNameMother.Text = dtRow["familyName"].ToString().Trim();
                                firstNameMother.Text = dtRow["firstName"].ToString().Trim();
                                occupationMother.Text = dtRow["occupation"].ToString().Trim();
                                hdnMotherID.Value = dtRow["id"].ToString();
                                break;
                            case "Sibling 1":
                                familyNameSibling1.Text = dtRow["familyName"].ToString().Trim();
                                firstNameSibling1.Text = dtRow["firstName"].ToString().Trim();
                                dobSibling1.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                genderSibling1.Text = dtRow["gender"].ToString().Trim();
                                hdnSibling1ID.Value = dtRow["id"].ToString();
                                ageSibling1.Text = "";
                                if (!Convert.IsDBNull(dtRow["age"]))
                                {
                                    ageSibling1.Text = dtRow["age"].ToString();
                                }
                                break;
                            case "Sibling 2":
                                familyNameSibling2.Text = dtRow["familyName"].ToString().Trim();
                                firstNameSibling2.Text = dtRow["firstName"].ToString().Trim();
                                dobSibling2.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                genderSibling2.Text = dtRow["gender"].ToString().Trim();
                                hdnSibling2ID.Value = dtRow["id"].ToString();
                                ageSibling2.Text = "";
                                if (!Convert.IsDBNull(dtRow["age"]))
                                {
                                    ageSibling2.Text = dtRow["age"].ToString();
                                }
                                break;
                            case "Sibling 3":
                                familyNameSibling3.Text = dtRow["familyName"].ToString().Trim();
                                firstNameSibling3.Text = dtRow["firstName"].ToString().Trim();
                                dobSibling3.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                genderSibling3.Text = dtRow["gender"].ToString().Trim();
                                hdnSibling3ID.Value = dtRow["id"].ToString();
                                ageSibling3.Text = "";
                                if (!Convert.IsDBNull(dtRow["age"]))
                                {
                                    ageSibling3.Text = dtRow["age"].ToString();
                                }
                                break;
                            case "Sibling 4":
                                familyNameSibling4.Text = dtRow["familyName"].ToString().Trim();
                                firstNameSibling4.Text = dtRow["firstName"].ToString().Trim();
                                dobSibling4.Text = Convert.ToDateTime(dtRow["DOB"]).ToShortDateString();
                                genderSibling4.Text = dtRow["gender"].ToString().Trim();
                                hdnSibling4ID.Value = dtRow["id"].ToString();
                                ageSibling4.Text = "";
                                if (!Convert.IsDBNull(dtRow["age"]))
                                {
                                    ageSibling4.Text = dtRow["age"].ToString();
                                }
                                break;
                        }
                    }
                }
                else if (blIsAdmin) { strResultMsg += "No HomestayRelatives records returned.<br />"; }
            }
            else if (blIsAdmin) { strResultMsg += "No HomestayRelatives records returned.<br />"; }

            //Get preferences
            DataTable dtPref = hsInfo.GetHomestayPreferenceSelections(intApplicantID, 0);
            if (dtPref != null)
            {
                if (dtPref.Rows.Count > 0)
                {
                    foreach (DataRow dtPrefRow in dtPref.Rows)
                    {
                        String objID = dtPrefRow["fieldID"].ToString().Trim();
                        String objValue = dtPrefRow["fieldValue"].ToString().Trim();
                        if (objID.Substring(0, 3).Contains("rdo"))
                        {
                            //Radio Button
                            //declare specific type of control

                            //RadioButtonList myRDO = (RadioButtonList)Page.Master.FindControl("MainContent").FindControl("fldPreferences").FindControl(objID);
                            //RadioButtonList myRDO = (RadioButtonList)FindControl("fldPreferences").FindControl(objID);
                            //RadioButtonList myRDO = (RadioButtonList)Master.FindControl("fldPreferences").FindControl(objID);
                            //RadioButtonList myRDO = null;

                            // Gets a reference to a TextBox control inside 
                            // a ContentPlaceHolder
                            //Because the RadioButtonList control is inside a ContentPlaceHolder control, 
                            //you must first get a reference to the ContentPlaceHolder and then use its FindControl method to locate the RadioButtonList control.
                            ContentPlaceHolder mpContentPlaceHolder;
                            RadioButtonList myRDO;
                            mpContentPlaceHolder = (ContentPlaceHolder)Master.FindControl("ContentHolder");
                            //RadioButtonList myRDO = (RadioButtonList)FindControl("fldPreferences").FindControl(objID);
                            if (mpContentPlaceHolder != null)
                            {
                                myRDO =(RadioButtonList)mpContentPlaceHolder.FindControl("fldPreferences").FindControl(objID);
                                if (myRDO != null)
                                {
                                    //set value
                                    myRDO.SelectedValue = objValue;
                                }
                                else if (blIsAdmin) { strResultMsg += "Radio button list " + objID + " with a value of " + objValue + " is null.<br />"; }
                            }
                            
                        }
                        else
                        {
                            //Checkbox
                            //declare specific type of control
                            //CheckBox myControl = (CheckBox)Page.Master.FindControl("MainContent").FindControl("fldPreferences").FindControl(objID);
                            //CheckBox myControl = (CheckBox)FindControl("fldPreferences").FindControl(objID);
                            //CheckBox myControl = null;
                            ContentPlaceHolder mpContentPlaceHolder;
                            CheckBox myControl;
                            mpContentPlaceHolder = (ContentPlaceHolder)Master.FindControl("ContentHolder");
                            if (mpContentPlaceHolder != null)
                            {
                                myControl =(CheckBox)mpContentPlaceHolder.FindControl("fldPreferences").FindControl(objID);
                                if (myControl != null)
                                {
                                    //set value
                                    myControl.Checked = true;
                                }
                                else if (blIsAdmin) { strResultMsg += "Checkbox " + objID + " with a value of " + objValue + " is null.<br />"; }
                            }
                        }
                    }
                }
                else if (blIsAdmin) { strResultMsg += "Preference Selections returned no rows.<br />"; }
            }
            else if (blIsAdmin) { strResultMsg += "Preference Selections table is null<br />"; }
        }// end not a postback

        //Verify age
        DataTable dtStudent = hsInfo.GetHomestayStudents("ID", intApplicantID);
        if (dtStudent != null)
        {
            if (dtStudent.Rows.Count > 0)
            {
                //clear any values from previous selection
                DateTime dtDOB = Convert.ToDateTime(dtStudent.Rows[0]["DOB"]);
                DateTime dtFirstDayOfClass = Convert.ToDateTime(dtStudent.Rows[0]["quarterStartDate"]);
                DateTime dtLatestDOB = dtFirstDayOfClass.AddYears(-18);
                if (dtLatestDOB < dtDOB)
                {
                    blUnderAge = true;
                    divApartment.Visible = false;
                }
            }
        }
        
        //Visibility settings
        if (blAddMode)
        {
            divUpdate.Visible = false;
            btnSave.Visible = true;
            if (!blIsAdmin)
            {
                divNextPage.Visible = false;
                btnPrevious.Visible = false;
            }
        }
        else
        {
            if (blIsAdmin)
            {
                divUpdate.Visible = true;
                btnPrevious.Visible = true;
            }
            btnSave.Visible = false;
            divNextPage.Visible = true;
        }
        litResultMsg.Text = "<p>" + strResultMsg + hsInfo.strResultMsg + "</p>";
    }
    protected void btnCmdSave_Click(object sender, EventArgs e)
    {
        if (familyNameFather.Text.Trim() == "")
        {
            eFFamName.InnerHtml = "Please enter your father's family name.";
            familyNameFather.Focus();
            return;
        }
        else
            eFFamName.InnerHtml = "";
        if (firstNameFather.Text.Trim() == "")
        {
            eFFirstName.InnerHtml = "Please enter your father's first name.";
            firstNameFather.Focus();
            return;
        }
        else
            eFFirstName.InnerHtml = "";
        if (occupationFather.Text.Trim() == "")
        {
            eFathOcc.InnerHtml = "Please enter your father's occupation.";
            occupationFather.Focus();
            return;
        }
        else
            eFathOcc.InnerHtml = "";
        /*
        if (familyNameMother.Text.Trim() == "")
        {
            eMFamName.InnerHtml = "Please enter your mother's family name.";
            familyNameMother.Focus();
            return;
        }
        else
            eMFamName.InnerHtml = "";
        if (firstNameMother.Text.Trim() == "")
        {
            eMFirstName.InnerHtml = "Please enter your mother's first name.";
            firstNameMother.Focus();
            return;
        }
        else
            eMFirstName.InnerHtml = "";
        if (occupationMother.Text.Trim() == "")
        {
            eMomOcc.InnerHtml = "Please enter your mother's occupation.";
            occupationMother.Focus();
            return;
        }
        else{
        eMomOcc.InnerHtml = "";
        }
            
            */
        // check sibling date of birth if entered
        if (dobSibling1.Text.Trim() != "")
        {
            isValidDate = checkDate(dobSibling1);
            if(!isValidDate)
            {
                eDOBSib1.InnerHtml = "Please enter a valid date of birth for Sibling 1.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for Sibling 1 information</span>.";
                return;
            } else
            {
                eDOBSib1.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in Sibling 1 DOB
        if (dobSibling2.Text.Trim() != "")
        {
            isValidDate = checkDate(dobSibling2);
            if (!isValidDate)
            {
                eDOBSib2.InnerHtml = "Please enter a valid date of birth for Sibling 2.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for Sibling 2 information</span>.";
                return;
            }
            else
            {
                eDOBSib2.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in Sibling 2 DOB
        if (dobSibling3.Text.Trim() != "")
        {
            isValidDate = checkDate(dobSibling3);
            if (!isValidDate)
            {
                eDOBSib3.InnerHtml = "Please enter a valid date of birth for Sibling 3.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for Sibling 3 information</span>.";
                return;
            }
            else
            {
                eDOBSib3.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in Sibling 3 DOB
        if (dobSibling4.Text.Trim() != "")
        {
            isValidDate = checkDate(dobSibling4);
            if (!isValidDate)
            {
                eDOBSib4.InnerHtml = "Please enter a valid date of birth for Sibling 4.";
                litResultMsg.Text = "<span style='color:#dd0000'>Please enter a valid date of birth for Sibling 4 information</span>.";
                return;
            }
            else
            {
                eDOBSib4.InnerHtml = "";
                litResultMsg.Text = "";
            }
        }// entry in Sibling 4 DOB

        if (rdoSmallChildren.SelectedIndex == -1)
        {
            eSmallChildren.InnerHtml = "Please indicate your preferences about children in the home.";
            rdoSmallChildren.Focus();
            return;
        }
        else
            eSmallChildren.InnerHtml = "";
        if (rdoTraveledOutsideCountry.SelectedIndex == -1)
        {
            eTravel.InnerHtml = "Please indicate if you have traveled outside your country before.";
            rdoTraveledOutsideCountry.Focus();
            return;
        }
        else
            eTravel.InnerHtml = "";
        if (rdoSmoker.SelectedIndex == -1)
        {
            eSmoker.InnerHtml = "Please indicate if you are a smoker or not.";
            rdoSmoker.Focus();
            return;
        }
        else
            eSmoker.InnerHtml = "";
        if (rdoOtherSmokerOK.SelectedIndex == -1)
        {
            eOtherSmokerOK.InnerHtml = "Please indicate your preference about other smokers in the home.";
            rdoOtherSmokerOK.Focus();
            return;
        }
        else
            eOtherSmokerOK.InnerHtml = "";
        if (rdoInteraction.SelectedIndex == -1)
        {
            eInteraction.InnerHtml = "Please indicate how much interaction with the host family you prefer.";
            rdoInteraction.Focus();
            return;
        }
        else
            eInteraction.InnerHtml = "";
        if (rdoHomeEnvironment.SelectedIndex == -1)
        {
            eHomeEnvironment.InnerHtml = "Please indicate how busy you prefer the home to be.";
            rdoHomeEnvironment.Focus();
            return;
        }
        else
            eHomeEnvironment.InnerHtml = "";
        if (rdoHealthStatus.SelectedIndex == -1)
        {
            eHealthStatus.InnerHtml = "Please indicate  your health status.";
            rdoHealthStatus.Focus();
            return;
        }
        else
            eHealthStatus.InnerHtml = "";


        //process submission
        //Insert HomestayRelatives table
        //Int32 applicantID, Int32 familyID, String familyName, String firstName, String occupation, String DOB, String gender,
        //String relationship, String email, String cellPhone, String workPhone, String driversLicenseNumber, String middleName
        if (firstNameFather.Text != "")
        {
            String FamilyNameFather = familyNameFather.Text.Replace("'", "''");
            String FirstNameFather = firstNameFather.Text.Replace("'", "''");
            String OccupationFather = occupationFather.Text.Replace("'", "''");
            String FatherID = hdnFatherID.Value;

            if (FatherID != "" && FatherID != "0")
            {
                Int32 intFatherID = Convert.ToInt32(FatherID);
                intResult = hsInfo.UpdateHomestayRelatives(intFatherID, FamilyNameFather, FirstNameFather, OccupationFather, "", 0, "M", "Father", "", "", "", "", "");
            }
            else if (FamilyNameFather != "" && FirstNameFather != "")
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameFather, FirstNameFather, OccupationFather, strBlank, 0, "M", "Father", strBlank, strBlank, strBlank, strBlank, strBlank);
        }
        //if (firstNameMother.Text != "")
        //{
        String FamilyNameMother = familyNameMother.Text.Replace("'", "''");
        String FirstNameMother = firstNameMother.Text.Replace("'", "''");
        String OccupationMother = occupationMother.Text.Replace("'", "''");
        String MotherID = hdnMotherID.Value;

        if (MotherID != "" && MotherID != "0")
        {
            Int32 intMotherID = Convert.ToInt32(MotherID);
            intResult = hsInfo.UpdateHomestayRelatives(intMotherID, FamilyNameMother, FirstNameMother, OccupationMother, "", 0, "F", "Mother", strBlank, strBlank, strBlank, strBlank, strBlank);
        }
        else if (FamilyNameMother != "" && FirstNameMother != "")
            intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameMother, FirstNameMother, OccupationMother, strBlank, 0, "F", "Mother", strBlank, strBlank, strBlank, strBlank, strBlank);
        //}
        if (ageSibling1.Text != "")
        {
            String FamilyNameSibling1 = familyNameSibling1.Text.Replace("'", "''");
            String FirstNameSibling1 = firstNameSibling1.Text.Replace("'", "''");
            String DOBSibling1 = dobSibling1.Text.Replace("'", "''");
            //String AgeSibling1 = ageSibling1.Text.Replace("'", "''");
            Int32 AgeSibling1 = Convert.ToInt32(ageSibling1.Text);
            String GenderSibling1 = genderSibling1.Text;
            String Sibling1ID = hdnSibling1ID.Value;

            if (Sibling1ID != "" && Sibling1ID != "0")
            {
                Int32 intSibling1ID = Convert.ToInt32(Sibling1ID);
                intResult = hsInfo.UpdateHomestayRelatives(intSibling1ID, FamilyNameSibling1, FirstNameSibling1, strBlank, DOBSibling1, AgeSibling1, GenderSibling1, "Sibling 1", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
            else
            {
                //insert new relative
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameSibling1, FirstNameSibling1, strBlank, DOBSibling1, AgeSibling1, GenderSibling1, "Sibling 1", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
        }
        if (ageSibling2.Text != "")
        {
            String FamilyNameSibling2 = familyNameSibling2.Text.Replace("'", "''");
            String FirstNameSibling2 = firstNameSibling2.Text.Replace("'", "''");
            String DOBSibling2 = dobSibling2.Text.Replace("'", "''");
            Int32 AgeSibling2 = Convert.ToInt32(ageSibling2.Text);
            String GenderSibling2 = genderSibling2.Text;
            String Sibling2ID = hdnSibling2ID.Value;

            if (Sibling2ID != "" && Sibling2ID != "0")
            {
                Int32 intSibling2ID = Convert.ToInt32(Sibling2ID);
                intResult = hsInfo.UpdateHomestayRelatives(intSibling2ID, FamilyNameSibling2, FirstNameSibling2, strBlank, DOBSibling2, AgeSibling2, GenderSibling2, "Sibling 2", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
            else
            {
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameSibling2, FirstNameSibling2, strBlank, DOBSibling2, AgeSibling2, GenderSibling2, "Sibling 2", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
        }
        if (ageSibling3.Text != "")
        {
            String FamilyNameSibling3 = familyNameSibling3.Text.Replace("'", "''");
            String FirstNameSibling3 = firstNameSibling3.Text.Replace("'", "''");
            String DOBSibling3 = dobSibling3.Text.Replace("'", "''");
            Int32 AgeSibling3 = Convert.ToInt32(ageSibling3.Text);
            String GenderSibling3 = genderSibling3.Text;
            String Sibling3ID = hdnSibling3ID.Value;

            if (Sibling3ID != "" && Sibling3ID != "0")
            {
                Int32 intSibling3ID = Convert.ToInt32(Sibling3ID);
                intResult = hsInfo.UpdateHomestayRelatives(intSibling3ID, FamilyNameSibling3, FirstNameSibling3, strBlank, DOBSibling3, AgeSibling3, GenderSibling3, "Sibling 3", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
            else
            {
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, strBlank, strBlank, strBlank, DOBSibling3, AgeSibling3, GenderSibling3, "Sibling 3", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
        }
        if (ageSibling4.Text != "")
        {
            String FamilyNameSibling4 = familyNameSibling4.Text.Replace("'", "''");
            String FirstNameSibling4 = firstNameSibling4.Text.Replace("'", "''");
            String DOBSibling4 = dobSibling4.Text.Replace("'", "''");
            Int32 AgeSibling4 = Convert.ToInt32(ageSibling4.Text);
            String GenderSibling4 = genderSibling4.Text;
            String Sibling4ID = hdnSibling4ID.Value;

            if (Sibling4ID != "" && Sibling4ID != "0")
            {
                Int32 intSibling4ID = Convert.ToInt32(Sibling4ID);
                intResult = hsInfo.UpdateHomestayRelatives(intSibling4ID, FamilyNameSibling4, FirstNameSibling4, strBlank, DOBSibling4, AgeSibling4, GenderSibling4, "Sibling 4", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
            else
            {
                intResult = hsInfo.InsertHomestayRelatives(intApplicantID, 0, FamilyNameSibling4, FirstNameSibling4, strBlank, DOBSibling4, AgeSibling4, GenderSibling4, "Sibling 4", strBlank, strBlank, strBlank, strBlank, strBlank);
            }
        }

        //Insert HomestayStudentInfo table
        String TraveledWhere = traveledWhere.Text.Replace("'", "''");
        String SmokingHabits = smokingHabits.Text.Replace("'", "''");
        String ActivitiesEnjoyed = activitiesEnjoyed.Text.Replace("'", "''");
        String Allergies = allergies.Text.Replace("'", "''");
        String HomeEnvironmentPreferences = homeEnvironmentPreferences.Text.Replace("'", "''");
        String HealthConditions = healthConditions.Text.Replace("'", "''");
        String selectedHealthStatus = rdoHealthStatus.SelectedValue;
        String selectedDriveCar = rdoDriveCar.SelectedValue;
        if (selectedDriveCar == "") { selectedDriveCar = "0"; }
        String AnythingElse = anythingElse.Text.Replace("'", "''");
        intResult = hsInfo.UpdateP2HomestayStudentInfo(intApplicantID, TraveledWhere, SmokingHabits, ActivitiesEnjoyed, HomeEnvironmentPreferences, Allergies,
            HealthConditions, selectedHealthStatus, Convert.ToInt32(selectedDriveCar), AnythingElse);

        //Delete all previous selections; then re-add new ones
        intResult = hsInfo.DeleteHomestayPreferenceSelections(intApplicantID, 0);

        //Insert into HomestayPreferenceSelections table
        String SmokersOK = rdoOtherSmokerOK.SelectedValue;
        if (SmokersOK != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(SmokersOK, intApplicantID, 0);
        }
        String StudentSmokes = rdoSmoker.SelectedValue;
        if (StudentSmokes != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(StudentSmokes, intApplicantID, 0);
        }
        String selectedSmallChildren = rdoSmallChildren.SelectedValue;
        if (selectedSmallChildren != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedSmallChildren, intApplicantID, 0);
        }
        String selectedTraveledOutsideCountry = rdoTraveledOutsideCountry.SelectedValue;
        if (selectedTraveledOutsideCountry != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedTraveledOutsideCountry, intApplicantID, 0);
        }
        if (MusicalInstrument.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("MusicalInstrument", intApplicantID, 0);
        }
        if (Art.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Art", intApplicantID, 0);
        }
        if (TeamSports.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("TeamSports", intApplicantID, 0);
        }
        if (IndividualSports.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("IndividualSports", intApplicantID, 0);
        }
        if (ListenMusic.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("ListenMusic", intApplicantID, 0);
        }
        if (Drama.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Drama", intApplicantID, 0);
        }
        if (WatchMovies.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("WatchMovies", intApplicantID, 0);
        }
        if (Singing.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Singing", intApplicantID, 0);
        }
        if (Shopping.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Shopping", intApplicantID, 0);
        }
        if (ReadBooks.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("ReadBooks", intApplicantID, 0);
        }
        if (Outdoors.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Outdoors", intApplicantID, 0);
        }
        if (Cooking.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Cooking", intApplicantID, 0);
        }
        if (Photography.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Photography", intApplicantID, 0);
        }
        if (Gaming.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Gaming", intApplicantID, 0);
        }
        String selectedInteraction = rdoInteraction.SelectedValue;
        if (selectedInteraction != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedInteraction, intApplicantID, 0);
        }
        String selectedHomeEnvironment = rdoHomeEnvironment.SelectedValue;
     //   Response.Write("rdoHomeEnviro.SelectedValue: " + selectedHomeEnvironment + "<br />");
        if (selectedHomeEnvironment != "")
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections(selectedHomeEnvironment, intApplicantID, 0);
       //     Response.Write("result: " + intResult.ToString() + "<br /");
        }
        if (Vegetarian.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Vegetarian", intApplicantID, 0);
        }
        if (GlutenFree.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("GlutenFree", intApplicantID, 0);
        }
        if (DairyFree.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("DairyFree", intApplicantID, 0);
        }
        if (OtherFoodAllergy.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("OtherFoodAllergy", intApplicantID, 0);
        }
        if (Halal.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Halal", intApplicantID, 0);
        }
        if (Kosher.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("Kosher", intApplicantID, 0);
        }
        if (NoSpecialDiet.Checked)
        {
            intResult = hsInfo.InsertUpdateHomestayPreferenceSelections("NoSpecialDiet", intApplicantID, 0);
        }
        //Then navigate to next page
        string strRedirect = "";
        if (blUnderAge)
        {
            strRedirect = "AgreementsUAp1.aspx?ID=" + hdnApplicantID.Value;
            // add the site
            strRedirect += "&site=" + Request.QueryString["site"];
            Response.Redirect(strRedirect);
        }
        else
        {
            strRedirect = "Agreements18.aspx?ID=" + hdnApplicantID.Value;
            // add the site
            strRedirect += "&site=" + Request.QueryString["site"];
            Response.Redirect(strRedirect);
        } 
    }
    protected void btnCmdBack_Click(object sender, EventArgs e)
    {
        string strRedirect = "ApplicationForm_L.aspx?ID=" + hdnApplicantID.Value;
        if (Request.QueryString["UA"] == null)
            strRedirect += "&UA=f";
        else if(Request.QueryString["UA"] == "f")
            strRedirect += "&UA=f";
        else
            strRedirect += "&UA=t";
        strRedirect += ("&site=" + Request.QueryString["site"]);
        Response.Redirect(strRedirect);
    }
    protected void btnCmdContinue_Click(object sender, EventArgs e)
    {
        string strRedirect = "";
        if (blUnderAge)
        {
            strRedirect += "AgreementsUAp1.aspx?ID=" + hdnApplicantID.Value;
        }
        else
        {
            strRedirect += "Agreements18.aspx?ID=" + hdnApplicantID.Value;
        }
        //Add site
        strRedirect += "&site=" + Request.QueryString["site"];
        Response.Redirect(strRedirect);
    }

    Boolean checkDate(TextBox txtBox)
    {
        dateSuccess = DateTime.TryParse(txtBox.Text.Trim(), out outDate);
        if (dateSuccess)
            txtBox.Text = outDate.ToShortDateString();
        else
            txtBox.Focus();
        return dateSuccess;
    }
}



