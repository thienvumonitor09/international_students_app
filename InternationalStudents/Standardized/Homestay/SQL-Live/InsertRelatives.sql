USE [CCSInternationalStudent]
GO
/****** Object:  StoredProcedure [dbo].[uspInsertHomestayRelatives]    Script Date: 1/8/2019 10:25:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kamori Cattadoris
-- Create date: March 13, 2018
-- Description:	insert into HomestayRelatives
-- =============================================
ALTER PROCEDURE [dbo].[uspInsertHomestayRelatives] 
       (
           @applicantID int
           , @familyID int
           , @familyName varchar(30)
           , @firstName varchar(20)
           , @occupation varchar(20)
           , @DOB datetime
           , @gender nchar(1)
           , @relationship varchar(20)
           , @email varchar(50)
           , @cellPhone varchar(15)
           , @workPhone varchar(15)
           , @driversLicenseNumber varchar(50)
           , @middleName varchar(20)
       )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
INSERT INTO [CCSInternationalStudent].[dbo].[HomestayRelatives]
       (
           [applicantID]
           ,[familyID]
           ,[familyName]
           ,[firstName]
           ,[occupation]
           ,[DOB]
           ,[gender]
           ,[relationship]
           ,[email]
           ,[cellPhone]
           ,[workPhone]
           ,[driversLicenseNumber]
           ,[middleName]
       )
     VALUES
       (
           @applicantID
           , @familyID
           , @familyName
           , @firstName
           , @occupation
           , @DOB
           , @gender
           , @relationship
           , @email
           , @cellPhone
           , @workPhone
           , @driversLicenseNumber
           , @middleName
       )
END
