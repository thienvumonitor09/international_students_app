USE [CCSInternationalStudent]
GO
/****** Object:  StoredProcedure [dbo].[uspGetHomestayRelatives]    Script Date: 1/8/2019 10:35:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kamori Cattadoris
-- Create date: 3/14/2018
-- Description:	Select using various criteria
-- =============================================
ALTER PROCEDURE [dbo].[uspGetHomestayRelatives] 
	@applicantID int,
	@familyID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @familyID = 0 AND @applicantID > 0
	BEGIN
		SELECT [id]
			  ,[applicantID]
			  ,[familyID]
			  ,[familyName]
			  ,[firstName]
			  ,[occupation]
			  ,[DOB]
			  ,[gender]
			  ,[relationship]
			  ,[email]
			  ,[cellPhone]
			  ,[workPhone]
			  ,[driversLicenseNumber]
			  ,[middleName]
		  FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
		  WHERE [applicantID] = @applicantID
	  END
	  IF @familyID > 0 AND @applicantID = 0
	  BEGIN
		SELECT [id]
			  ,[applicantID]
			  ,[familyID]
			  ,[familyName]
			  ,[firstName]
			  ,[occupation]
			  ,[DOB]
			  ,[gender]
			  ,[relationship]
			  ,[email]
			  ,[cellPhone]
			  ,[workPhone]
			  ,[driversLicenseNumber]
			  ,[middleName]
		  FROM [CCSInternationalStudent].[dbo].[HomestayRelatives]
		  WHERE [familyID] = @familyID
	  END
END
