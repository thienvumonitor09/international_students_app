USE [CCSInternationalStudent]
GO
/****** Object:  StoredProcedure [dbo].[uspUpdateHomestayRelatives]    Script Date: 1/8/2019 10:23:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kamori Cattadoris
-- Create date: March 13, 2018
-- Description:	Update HomestayRelatives table by id
-- =============================================
ALTER PROCEDURE [dbo].[uspUpdateHomestayRelatives] 
			@id int
           , @familyName varchar(30)
           , @firstName varchar(20)
           , @occupation varchar(20)
           , @DOB datetime
           , @gender nchar(1)
           , @relationship varchar(20)
           , @email varchar(50)
           , @cellPhone varchar(15)
           , @workPhone varchar(15)
           , @driversLicenseNumber varchar(10)
           , @middleName varchar(30)
 AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [CCSInternationalStudent].[dbo].[HomestayRelatives]
    SET
		[familyName] = @familyName
		, [firstName] = @firstName
		, [occupation] = @occupation
		, [DOB] = @DOB
		, [gender] = @gender
		, [relationship] = @relationship
		, [email] = @email
		, [cellPhone] = @cellPhone
		, [workPhone] = @workPhone
		, [driversLicenseNumber] = @driversLicenseNumber
		, [middleName] = @middleName
    WHERE id = @id
END
